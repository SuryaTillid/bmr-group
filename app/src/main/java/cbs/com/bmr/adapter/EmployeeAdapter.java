package cbs.com.bmr.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cbs.com.bmr.R;
import cbs.com.bmr.model.EmployeeTrackingList;

public class EmployeeAdapter extends BaseAdapter {
    private final LayoutInflater inflter;
    private Context mcontext;
    private List<EmployeeTrackingList> departmentLists;

    public EmployeeAdapter(Context context, List<EmployeeTrackingList> departmentLists1) {
        mcontext = context;
        this.departmentLists = departmentLists1;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return departmentLists.size();
    }

    @Override
    public String getItem(int i) {
        return departmentLists.get(i).getFirstname() + " " + departmentLists.get(i).getLastname();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_spinner_item, null);
        TextView textView = view.findViewById(R.id.custom_spinner_item);
        textView.setText(String.format("%s %s", departmentLists.get(i).getFirstname(), departmentLists.get(i).getLastname()));
        return view;
    }
}
