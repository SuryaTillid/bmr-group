package cbs.com.bmr.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cbs.com.bmr.Listener.MyTodoListCallback;
import cbs.com.bmr.R;
import cbs.com.bmr.holder.MytodoLitsHolder;
import cbs.com.bmr.model.MyTodoList;

import static cbs.com.bmr.Utilities.Utils.getRelativeTime;


public class MyToDolistAdapter extends RecyclerView.Adapter<MytodoLitsHolder> {
    private List<MyTodoList> mTeamList;
    private MyTodoListCallback mCallback;
    private Context mContext;


    public MyToDolistAdapter(Context context, List<MyTodoList> ticketList, MyTodoListCallback callback) {
        mTeamList = ticketList;
        mCallback = callback;
        mContext = context;

    }

    @NonNull
    @Override
    public MytodoLitsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MytodoLitsHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mytodolist, parent, false), mCallback);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MytodoLitsHolder holder, final int position) {
        final MyTodoList myTodoList = mTeamList.get(position);
        holder.mTextTaskName.setText(myTodoList.getTask_name());
        holder.mTextCreatedDate.setText("Created by: " + myTodoList.getEmp_name());
        holder.mTextDuedate.setText("Due " + getRelativeTime(myTodoList.getTask_due()));

        if (myTodoList.getPriority().equals("0")) {
            holder.mViewPriority.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        } else if (myTodoList.getPriority().equals("1")) {
            holder.mViewPriority.setBackgroundColor(ContextCompat.getColor(mContext, R.color.s1));
        } else if (myTodoList.getPriority().equals("2")) {
            holder.mViewPriority.setBackgroundColor(ContextCompat.getColor(mContext, R.color.s0));
        }


        Log.e("getstatys", "" + myTodoList.getTask_status());
        if (myTodoList.getTask_status().equals("0")) {
            holder.mTextStatus.setText("Open");
            holder.mTextStatus.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        } else if (myTodoList.getTask_status().equals("1")) {
            holder.mTextStatus.setText("In Progress");
            holder.mTextStatus.setTextColor(ContextCompat.getColor(mContext, R.color.home_orange));
        } else if (myTodoList.getTask_status().equals("2")) {
            holder.mTextStatus.setText("Completed");
            holder.mTextStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green));
        } else if (myTodoList.getTask_status().equals("3")) {
            holder.mTextStatus.setText("Cancelled");
            holder.mTextStatus.setTextColor(ContextCompat.getColor(mContext, R.color.s0));
        }
    }

    @Override
    public int getItemCount() {
        return mTeamList.size();
    }


}

