package cbs.com.bmr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cbs.com.bmr.Listener.EmployeeTrackingCallback;
import cbs.com.bmr.R;
import cbs.com.bmr.holder.RegionListHolder;
import cbs.com.bmr.model.EmployeeRegionList;

public class RegionListAdapter extends RecyclerView.Adapter<RegionListHolder> {

    private ArrayList<EmployeeRegionList> mIncidentList;
    private EmployeeTrackingCallback mCallback;
    private Context mContext;

    public RegionListAdapter(Context context, ArrayList<EmployeeRegionList> incidentlist, EmployeeTrackingCallback callback) {
        mIncidentList = incidentlist;
        mCallback = callback;
        mContext = context;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public RegionListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RegionListHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_region_list, parent, false), mCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull RegionListHolder holder, final int position) {
        EmployeeRegionList briefingDocuments = mIncidentList.get(position);
        holder.mTextName.setText(briefingDocuments.getZone_name());
    }


    @Override
    public int getItemCount() {
        return mIncidentList.size();
    }
}
