package cbs.com.bmr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cbs.com.bmr.R;
import cbs.com.bmr.model.TaskScheduler;

/********************************************************************
 * Created by Barani on 26-09-2019 in TableMateNew
 ********************************************************************/
public class ViewScheduleAdapter extends RecyclerView.Adapter<ViewScheduleAdapter.ScheduleHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<TaskScheduler> s_list;

    public ViewScheduleAdapter(Context context, ArrayList<TaskScheduler> list) {
        this.context = context;
        this.s_list = list;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<TaskScheduler> list) {
        s_list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewScheduleAdapter.ScheduleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_schedule_list_row, parent, false);
        return new ViewScheduleAdapter.ScheduleHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewScheduleAdapter.ScheduleHolder holder, int position) {
        TaskScheduler taskScheduler = s_list.get(position);

        holder.t_customer.setText(taskScheduler.getC_name());
    }

    @Override
    public int getItemCount() {
        return s_list.size();
    }

    public class ScheduleHolder extends RecyclerView.ViewHolder {

        TextView t_customer;

        public ScheduleHolder(@NonNull View v) {
            super(v);

            t_customer = v.findViewById(R.id.txt_customer);
        }
    }
}
