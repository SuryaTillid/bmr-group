package cbs.com.bmr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import cbs.com.bmr.R;
import cbs.com.bmr.model.ZoneMaster;

/*********************************************************************
 * Created by Barani on 03-10-2019 in TableMateNew
 ***********************************************************************/
public class RegionSelectionAdapter extends ArrayAdapter<ZoneMaster> implements Filterable {

    Context context;
    private int resource, textViewResourceId;
    private ArrayList<ZoneMaster> items, tempItems, suggestions;
    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    private Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((ZoneMaster) resultValue).getZone_name();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (ZoneMaster list : tempItems) {
                    if (list.getZone_name().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(list);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<ZoneMaster> r_list = (ArrayList<ZoneMaster>) results.values;
            if (results.count > 0) {
                clear();
                addAll(r_list);
                notifyDataSetChanged();
            }
        }
    };

    public RegionSelectionAdapter(Context context, int resource, int textViewResourceId, ArrayList<ZoneMaster> items) {
        super(context, resource, textViewResourceId, items);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<ZoneMaster>(items); // this makes the difference.
        suggestions = new ArrayList<ZoneMaster>();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (inflater != null) {
                view = inflater.inflate(R.layout.item_customername, parent, false);
            }
        }
        ZoneMaster region = items.get(position);
        if (region != null) {
            TextView lblName = view.findViewById(R.id.txt_stations);
            if (lblName != null)
                lblName.setText(region.getZone_name());
        }
        return view;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }
}

