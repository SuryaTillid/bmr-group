package cbs.com.bmr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cbs.com.bmr.Listener.EmployeeSessionListCallback;
import cbs.com.bmr.R;
import cbs.com.bmr.holder.EmployeeSessionListHolder;
import cbs.com.bmr.model.EmployeeSessionList;

public class EmployeeSessionListAdapter extends RecyclerView.Adapter<EmployeeSessionListHolder> {

    private ArrayList<EmployeeSessionList> mIncidentList;
    private EmployeeSessionListCallback mCallback;
    private Context mContext;

    public EmployeeSessionListAdapter(Context context, ArrayList<EmployeeSessionList> incidentlist, EmployeeSessionListCallback callback) {
        mIncidentList = incidentlist;
        mCallback = callback;
        mContext = context;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public EmployeeSessionListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EmployeeSessionListHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_employe_list_item, parent, false), mCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeSessionListHolder holder, final int position) {
        EmployeeSessionList briefingDocuments = mIncidentList.get(position);
        holder.mTextName.setText(String.format("%s", briefingDocuments.getEmp_name()));

        if (briefingDocuments.getStatus() != null && briefingDocuments.getStatus().equals("1")) {
            holder.mImageStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.bg_round_green));
        } else {
            holder.mImageStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.bg_round_red));
        }
    }


    public ArrayList<EmployeeSessionList> dataList() {
        return mIncidentList;
    }

    @Override
    public int getItemCount() {
        return mIncidentList.size();
    }
}
