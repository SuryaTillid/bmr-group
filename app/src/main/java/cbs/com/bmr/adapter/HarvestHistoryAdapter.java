package cbs.com.bmr.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import cbs.com.bmr.R;
import cbs.com.bmr.model.SampleHarvestHistory;

/*********************************************************************
 * Created by Barani on 25-09-2019 in TableMateNew
 ***********************************************************************/
public class HarvestHistoryAdapter extends RecyclerView.Adapter<HarvestHistoryAdapter.HistoryHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<SampleHarvestHistory> historyDetailsList;

    public HarvestHistoryAdapter(Context context, ArrayList<SampleHarvestHistory> historyList) {
        this.context = context;
        this.historyDetailsList = historyList;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<SampleHarvestHistory> list) {
        historyDetailsList = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public HarvestHistoryAdapter.HistoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_harvest_history, parent, false);
        return new HarvestHistoryAdapter.HistoryHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HarvestHistoryAdapter.HistoryHolder h, int position) {

        SampleHarvestHistory list = historyDetailsList.get(position);

        h.t_harvest_date.setText(validateDate(list.getSampling_date()));
        h.t_abw.setText(list.getAbw());

        h.t_h_qty.setText(list.getHarvest_quantity());
        h.t_productivity.setText(list.getProductivity());
        h.t_a_biomass.setText(list.getA_biomass());
        h.t_e_biomass.setText(list.getE_biomass());
        h.t_salinity.setText(list.getSalinity());
        h.t_density.setText(list.getDensity());
        h.t_daily_feed.setText(list.getDaily_feed());
        h.t_adg.setText(list.getAdg());
        h.t_fcr.setText(list.getFcr());
        h.t_ph.setText(list.getPh());
        h.t_survival.setText(list.getSurvival());
        h.t_wsa.setText(list.getAcres());
        h.t_seedsinmillion.setText(list.getTotal_feed());
        h.t_total_feed.setText(list.getTotal_feed());
        h.t_doc.setText(list.getDoc());
        h.t_seedsource.setText(list.getSeed_source());
        h.t_seedsinmillion.setText(list.getSeed_stocking());
    }


    private String validateDate(String stocking_date) {
        try {
            if (!TextUtils.isEmpty(stocking_date)) {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
                Date date = format.parse(stocking_date);
                DateFormat date_formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                stocking_date = date_formatter.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return stocking_date;
    }

    @Override
    public int getItemCount() {
        return historyDetailsList.size();
    }

    public class HistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView t_wsa, t_abw, t_daily_feed, t_harvest_date, t_ph, t_salinity, t_doc_label,
                t_a_biomass, t_e_biomass, t_density, t_adg, t_fcr, t_productivity, t_h_qty, t_seedsource, t_total_feed, t_doc, t_survival, t_seedsinmillion;

        LinearLayout layout_header_harvest, layout_child_harvest;
        ImageView mImageDirection;
        int i = 0;

        public HistoryHolder(@NonNull View v) {
            super(v);

            t_wsa = v.findViewById(R.id.t_wsa);
            t_abw = v.findViewById(R.id.t_abw);
            t_daily_feed = v.findViewById(R.id.t_daily_feed);
            t_harvest_date = v.findViewById(R.id.t_harvest_date);
            t_adg = v.findViewById(R.id.t_adg);
            t_density = v.findViewById(R.id.t_density);
            t_seedsinmillion = v.findViewById(R.id.t_seedsinmillion);
            t_total_feed = v.findViewById(R.id.t_total_feed);
            t_seedsource = v.findViewById(R.id.t_seedsource);
            t_doc = v.findViewById(R.id.t_doc);
            layout_header_harvest = v.findViewById(R.id.layout_header_harvest);
            layout_child_harvest = v.findViewById(R.id.layout_child_harvest);
            t_doc_label = v.findViewById(R.id.t_doc_label);
            mImageDirection = v.findViewById(R.id.img_direction);

            t_ph = v.findViewById(R.id.t_ph);
            t_salinity = v.findViewById(R.id.t_salinity);
            t_a_biomass = v.findViewById(R.id.t_a_biomass);
            t_e_biomass = v.findViewById(R.id.t_e_biomass);
            t_survival = v.findViewById(R.id.t_survival);
            t_fcr = v.findViewById(R.id.t_fcr);
            t_productivity = v.findViewById(R.id.t_productivity);
            t_h_qty = v.findViewById(R.id.t_harvest_qty);
            v.setOnClickListener(this);
            layout_header_harvest.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (v == layout_header_harvest) {
                if (i == 0) {
                    layout_header_harvest.setBackgroundResource(R.drawable.bg_arrow);
                    mImageDirection.setImageResource(R.drawable.ic_down_symbol);
                    t_doc.setTextColor(ContextCompat.getColor(context, R.color.white));
                    t_harvest_date.setTextColor(ContextCompat.getColor(context, R.color.white));
                    t_doc_label.setTextColor(ContextCompat.getColor(context, R.color.white));
                    t_harvest_date.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_calendar_new_white, 0, 0, 0);
                    layout_child_harvest.setVisibility(View.VISIBLE);
                    i++;
                } else {
                    layout_header_harvest.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                    mImageDirection.setImageResource(R.drawable.ic_forward_symbol);
                    layout_child_harvest.setVisibility(View.GONE);
                    t_doc.setTextColor(ContextCompat.getColor(context, R.color.txt_doc_value));
                    t_harvest_date.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_calendar_new, 0, 0, 0);
                    t_harvest_date.setTextColor(ContextCompat.getColor(context, R.color.txt_doc_value));
                    t_doc_label.setTextColor(ContextCompat.getColor(context, R.color.txt_doc));
                    i--;

                }

            }

        }
    }
}

