package cbs.com.bmr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cbs.com.bmr.R;
import cbs.com.bmr.model.FeedbackQuestions;

/********************************************************************
 * Created by Barani on 30-03-2019 in TableMateNew
 ********************************************************************/
public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.MyHolder> {
    private ArrayList<FeedbackQuestions> feedbackQuestions;
    private Context context;
    private LayoutInflater inflater;

    public FeedbackAdapter(Context context, ArrayList<FeedbackQuestions> feedbackQuestions) {
        this.context = context;
        this.feedbackQuestions = feedbackQuestions;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<FeedbackQuestions> list) {
        feedbackQuestions = list;
        notifyDataSetChanged();
    }

    @Override
    public FeedbackAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.fragment_feedbackfromcustomer, parent, false);
        return new FeedbackAdapter.MyHolder(v);
    }

    @Override
    public void onBindViewHolder(FeedbackAdapter.MyHolder holder, int position) {
        FeedbackQuestions taskList = feedbackQuestions.get(position);

        holder.t_question.setText(taskList.getQues_name());

        //EditText editText = new EditText (context);
        //editText.setTextSize(12);
        //editText.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        //editText.setPadding(20, 20, 20, 20);
        //holder.linearLayout.addView(editText );
    }

    @Override
    public int getItemCount() {
        return feedbackQuestions.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView t_question;
        LinearLayout linearLayout;

        public MyHolder(View v) {
            super(v);

            t_question = v.findViewById(R.id.QuestionName);
            linearLayout = v.findViewById(R.id.l2);
        }
    }
}
