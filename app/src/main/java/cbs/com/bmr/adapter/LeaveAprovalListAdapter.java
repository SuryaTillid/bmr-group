package cbs.com.bmr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Listener.LeaveApproveListCallback;
import cbs.com.bmr.R;
import cbs.com.bmr.holder.LeaveApproveListHolder;
import cbs.com.bmr.model.LeaveApprovalList;

import static cbs.com.bmr.Utilities.Utils.getPreviousSamplindate;

public class LeaveAprovalListAdapter extends RecyclerView.Adapter<LeaveApproveListHolder> {

    private ArrayList<LeaveApprovalList> leaveApprovalLists;
    private LeaveApproveListCallback mCallback;
    private Context mContext;
    private ConfigurationSettings configurationSettings;

    public LeaveAprovalListAdapter(Context context, ArrayList<LeaveApprovalList> incidentlist, LeaveApproveListCallback callback) {
        leaveApprovalLists = incidentlist;
        mCallback = callback;
        mContext = context;
        configurationSettings = new ConfigurationSettings(context);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public LeaveApproveListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LeaveApproveListHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_leave_list, parent, false), mCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull LeaveApproveListHolder holder, final int position) {
        LeaveApprovalList leaveApprovalList = leaveApprovalLists.get(position);

        holder.mTextName.setText(leaveApprovalList.getCreated_emp_name());
        holder.mTextFdate.setText(getPreviousSamplindate(leaveApprovalList.getFrom_date()));
        if (leaveApprovalList.getLeave_days() != null) {
            holder.mTextNoofdays.setText(String.format("No of days : %s", leaveApprovalList.getLeave_days()));
        }

        if (leaveApprovalList.getLeave_status() != null) {
            if (leaveApprovalList.getLeave_status().equals("Pending")) {
                holder.mTextStatus.setText(leaveApprovalList.getLeave_status());
                holder.mTextStatus.setTextColor(ContextCompat.getColor(mContext, R.color.home_orange));
                holder.mTextStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circle_leave_pending, 0, 0, 0);
            } else if (leaveApprovalList.getLeave_status().equals("Approved")) {
                holder.mTextStatus.setText(leaveApprovalList.getLeave_status());
                holder.mTextStatus.setTextColor(ContextCompat.getColor(mContext, R.color.home_green));
                holder.mTextStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circle_leave_approved, 0, 0, 0);
            } else if (leaveApprovalList.getLeave_status().equals("Cancelled")) {
                holder.mTextStatus.setText(leaveApprovalList.getLeave_status());
                holder.mTextStatus.setTextColor(ContextCompat.getColor(mContext, R.color.home_cancelled));
                holder.mTextStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circle_leave_cancelled, 0, 0, 0);
            }
        }


        holder.mTextExpFdate.setText(String.format("From Date: %s", getPreviousSamplindate(leaveApprovalList.getFrom_date())));
        holder.mTextExpTodate.setText(String.format("To Date: %s", getPreviousSamplindate(leaveApprovalList.getTo_date())));

        holder.mTextNoofLeaveAvail.setText(String.format("Leave Available in Days :\nCasual  :  %s   Sick  :  %s    Privilege  :  %s",
                leaveApprovalList.getCasual_leave(), leaveApprovalList.getSick_leave(), leaveApprovalList.getPrivilege_leave()));
        holder.mTextLeaveType.setText(String.format("Leave Type : %s", leaveApprovalList.getLeavetype()));
        holder.mTextPurpose.setText(String.format("Purpose : %s", leaveApprovalList.getPurpose()));

        if (leaveApprovalList.getEmp_id().equals(configurationSettings.getEmployee_ID())) {
            holder.mLayoutApprove.setVisibility(View.GONE);
            holder.viewfinalline.setVisibility(View.GONE);
        } else {
            holder.mLayoutApprove.setVisibility(View.VISIBLE);
            holder.viewfinalline.setVisibility(View.VISIBLE);

        }

    }


    @Override
    public int getItemCount() {
        return leaveApprovalLists.size();
    }
}
