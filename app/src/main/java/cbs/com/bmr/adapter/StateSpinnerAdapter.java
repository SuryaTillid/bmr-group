package cbs.com.bmr.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import cbs.com.bmr.R;

public class StateSpinnerAdapter extends BaseAdapter {
    private final Context mcontext;
    private final List<HashMap<String, String>> departments;
    private final LayoutInflater inflter;

    public StateSpinnerAdapter(Context context, List<HashMap<String, String>> departments) {
        mcontext = context;
        this.departments = departments;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return departments.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_spinner_item, null);
        TextView textview = view.findViewById(R.id.custom_spinner_item);

        if (departments.get(i).get("Name") != null) {
            textview.setText(departments.get(i).get("Name"));
        }

        return view;
    }
}
