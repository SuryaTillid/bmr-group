package cbs.com.bmr.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import cbs.com.bmr.R;
import cbs.com.bmr.model.EmployeeList;

public class EmployeeNameAdapter extends ArrayAdapter<EmployeeList> implements Filterable {

    Context context;
    private int resource, textViewResourceId;
    private ArrayList<EmployeeList> items, tempItems, suggestions;


    private Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((EmployeeList) resultValue).getFirstname();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (EmployeeList busStations : tempItems) {
                    if (busStations.getFirstname().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(busStations);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<EmployeeList> mbusStations = (ArrayList<EmployeeList>) results.values;
            if (results.count > 0) {
                clear();
                addAll(mbusStations);
                notifyDataSetChanged();
            }

        }
    };

    public EmployeeNameAdapter(Context context, int resource, int textViewResourceId, ArrayList<EmployeeList> items) {
        super(context, resource, textViewResourceId, items);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<EmployeeList>(items); // this makes the difference.
        suggestions = new ArrayList<EmployeeList>();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (inflater != null) {
                view = inflater.inflate(R.layout.item_customername, parent, false);
            }
        }
        EmployeeList people = items.get(position);
        if (people != null) {
            TextView lblName = view.findViewById(R.id.txt_stations);
            if (lblName != null)
                Log.e("Lastname", "" + people.getLastname());
            lblName.setText(String.format("%s %s", people.getFirstname(), people.getLastname()));
        }
        return view;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }
}