package cbs.com.bmr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cbs.com.bmr.Listener.EmployeeTrackingCallback;
import cbs.com.bmr.R;
import cbs.com.bmr.holder.EmployeeTrackingHolder;
import cbs.com.bmr.model.EmployeeTrackingList;

public class EmployeeTrackingAdapter extends RecyclerView.Adapter<EmployeeTrackingHolder> {

    private ArrayList<EmployeeTrackingList> mIncidentList;
    private EmployeeTrackingCallback mCallback;
    private Context mContext;

    public EmployeeTrackingAdapter(Context context, ArrayList<EmployeeTrackingList> incidentlist, EmployeeTrackingCallback callback) {
        mIncidentList = incidentlist;
        mCallback = callback;
        mContext = context;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public EmployeeTrackingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EmployeeTrackingHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_employee_list, parent, false), mCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeTrackingHolder holder, final int position) {
        EmployeeTrackingList briefingDocuments = mIncidentList.get(position);
        holder.mTextName.setText(String.format("%s", briefingDocuments.getEmp_name()));
    }

    public void updateList(ArrayList<EmployeeTrackingList> list) {
        mIncidentList = list;
        notifyDataSetChanged();
    }

    public ArrayList<EmployeeTrackingList> dataList() {
        return mIncidentList;
    }

    @Override
    public int getItemCount() {
        return mIncidentList.size();
    }
}
