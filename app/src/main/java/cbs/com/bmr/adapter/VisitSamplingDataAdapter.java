package cbs.com.bmr.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cbs.com.bmr.Listener.Pond_details_click_listener;
import cbs.com.bmr.R;
import cbs.com.bmr.model.PondList;

/********************************************************************
 * Created by Barani on 19-08-2019 in TableMateNew
 ********************************************************************/
public class VisitSamplingDataAdapter extends RecyclerView.Adapter<VisitSamplingDataAdapter.SamplingHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<PondList> samplingDetailsList;
    private Pond_details_click_listener listener;

    public VisitSamplingDataAdapter(Context context, ArrayList<PondList> samplingList) {
        this.context = context;
        this.samplingDetailsList = samplingList;
        inflater = LayoutInflater.from(context);
    }

    public void setSamplingListListener(Pond_details_click_listener listener) {
        this.listener = listener;
    }

    public void MyDataChanged(ArrayList<PondList> list) {
        samplingDetailsList = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VisitSamplingDataAdapter.SamplingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_visit_sampling, parent, false);
        return new VisitSamplingDataAdapter.SamplingHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull VisitSamplingDataAdapter.SamplingHolder holder, int position) {

        PondList list = samplingDetailsList.get(position);

        holder.t_pond_name.setText(list.getPond_id());
        holder.t_farmerName.setText(list.getWsa());
    }

    private String validateDate(String stocking_date) {
        try {
            if (!TextUtils.isEmpty(stocking_date)) {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = format.parse(stocking_date);
                DateFormat date_formatter = new SimpleDateFormat("dd-MM-yyyy");
                stocking_date = date_formatter.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return stocking_date;
    }

    @Override
    public int getItemCount() {
        return samplingDetailsList.size();
    }

    public class SamplingHolder extends RecyclerView.ViewHolder {

        TextView t_farmerName, t_pond_name, t_ponddetails, t_history, t_sampling;

        public SamplingHolder(@NonNull View v) {
            super(v);

            t_farmerName = v.findViewById(R.id.txt_farmer_name);
            t_pond_name = v.findViewById(R.id.txt_pond_name);
            t_ponddetails = v.findViewById(R.id.txt_ponddetails);
            t_history = v.findViewById(R.id.txt_history);
            t_sampling = v.findViewById(R.id.txt_sampling);

            t_sampling.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onSamplingClick(getAdapterPosition(), samplingDetailsList.get(getAdapterPosition()).getId(),
                            samplingDetailsList.get(getAdapterPosition()).getCycle_id());
                }
            });

            t_history.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onHistoryClick(getAdapterPosition(), samplingDetailsList.get(getAdapterPosition()).getId(),
                            samplingDetailsList.get(getAdapterPosition()).getCycle_id());
                }
            });

            t_ponddetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.ondetailsClick(getAdapterPosition(), samplingDetailsList.get(getAdapterPosition()).getId(),
                            samplingDetailsList.get(getAdapterPosition()).getCycle_id());
                }
            });
        }
    }
}
