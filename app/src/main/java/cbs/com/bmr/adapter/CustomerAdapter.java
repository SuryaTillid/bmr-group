package cbs.com.bmr.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import cbs.com.bmr.R;
import cbs.com.bmr.model.CustomerList;

public class CustomerAdapter extends ArrayAdapter<CustomerList> implements Filterable {

    Context context;
    private int resource, textViewResourceId;
    private ArrayList<CustomerList> items, tempItems, suggestions;
    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    private Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((CustomerList) resultValue).getFirst_name();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (CustomerList busStations : tempItems) {
                    if (busStations.getFirst_name().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(busStations);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<CustomerList> mbusStations = (ArrayList<CustomerList>) results.values;
            if (results.count > 0) {
                clear();
                addAll(mbusStations);
                notifyDataSetChanged();
            }
        }
    };

    public CustomerAdapter(Context context, int resource, int textViewResourceId, ArrayList<CustomerList> items) {
        super(context, resource, textViewResourceId, items);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<CustomerList>(items); // this makes the difference.
        suggestions = new ArrayList<CustomerList>();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (inflater != null) {
                view = inflater.inflate(R.layout.item_customername, parent, false);
            }
        }
        CustomerList people = items.get(position);
        if (people != null) {
            TextView lblName = view.findViewById(R.id.txt_stations);
            TextView lblAddress = view.findViewById(R.id.txt_address);
            if (lblName != null)
                Log.e("Lastname", "" + people.getLast_name());
            lblName.setText(String.format("%s %s", people.getFirst_name(), people.getLast_name()));
            lblAddress.setText(String.format("%s %s", people.getCity_village(), people.getState()));
        }
        return view;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }
}