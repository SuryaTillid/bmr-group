package cbs.com.bmr.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import cbs.com.bmr.R;
import cbs.com.bmr.model.SampleHarvestHistory;

/*********************************************************************
 * Created by Barani on 25-09-2019 in TableMateNew
 ***********************************************************************/
public class Sampling_History_Adapter extends RecyclerView.Adapter<Sampling_History_Adapter.HistoryHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<SampleHarvestHistory> historyDetailsList;

    public Sampling_History_Adapter(Context context, ArrayList<SampleHarvestHistory> historyList) {
        this.context = context;
        this.historyDetailsList = historyList;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<SampleHarvestHistory> list) {
        historyDetailsList = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Sampling_History_Adapter.HistoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_sampling_history, parent, false);
        return new Sampling_History_Adapter.HistoryHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Sampling_History_Adapter.HistoryHolder h, int position) {

        SampleHarvestHistory list = historyDetailsList.get(position);
        h.t_sampling_date.setText(validateDate(list.getSampling_date()));
        h.t_abw.setText(String.format("%s", list.getAbw()));
        h.t_salinity.setText(list.getSalinity());
        h.t_daily_feed.setText(String.format("%s", list.getDaily_feed()));
        h.t_ph.setText(list.getPh());
        h.t_doc.setText(list.getDoc());
        h.t_adg.setText(list.getAdg());

        Log.e("doc0", "" + list.getDoc());
        h.t_seedsinmillion.setText(list.getSeed_stocking());
    }

    private String validateDate(String stocking_date) {
        try {
            if (!TextUtils.isEmpty(stocking_date)) {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                Date date = format.parse(stocking_date);
                DateFormat date_formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                stocking_date = date_formatter.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return stocking_date;
    }

    @Override
    public int getItemCount() {
        return historyDetailsList.size();
    }

    public class HistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView t_sampling_date, t_abw, t_daily_feed, t_ph, t_salinity, t_seedsinmillion, t_adg, t_doc, t_doc_label;
        LinearLayout layout_header_harvest, layout_child_harvest;
        ImageView mImageDirection;
        int i = 0;

        public HistoryHolder(@NonNull View v) {
            super(v);

            t_seedsinmillion = v.findViewById(R.id.t_seedsinmillion);
            t_sampling_date = v.findViewById(R.id.t_sampling_date);
            t_abw = v.findViewById(R.id.t_abw);
            t_daily_feed = v.findViewById(R.id.t_daily_feed);
            t_adg = v.findViewById(R.id.t_adg);
            t_ph = v.findViewById(R.id.t_ph);
            t_salinity = v.findViewById(R.id.t_salinity);
            t_doc = v.findViewById(R.id.t_doc);
            mImageDirection = v.findViewById(R.id.img_direction);
            layout_header_harvest = v.findViewById(R.id.layout_header_harvest);
            layout_child_harvest = v.findViewById(R.id.layout_child_harvest);
            t_doc_label = v.findViewById(R.id.t_doc_label);
            v.setOnClickListener(this);
            layout_header_harvest.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (v == layout_header_harvest) {
                if (i == 0) {
                    layout_header_harvest.setBackgroundResource(R.drawable.bg_arrow);
                    mImageDirection.setImageResource(R.drawable.ic_down_symbol);
                    t_doc.setTextColor(ContextCompat.getColor(context, R.color.white));
                    t_sampling_date.setTextColor(ContextCompat.getColor(context, R.color.white));
                    t_doc_label.setTextColor(ContextCompat.getColor(context, R.color.white));
                    t_sampling_date.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_calendar_new_white, 0, 0, 0);
                    layout_child_harvest.setVisibility(View.VISIBLE);
                    i++;
                } else {
                    layout_header_harvest.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                    mImageDirection.setImageResource(R.drawable.ic_forward_symbol);
                    layout_child_harvest.setVisibility(View.GONE);
                    t_doc.setTextColor(ContextCompat.getColor(context, R.color.txt_doc_value));
                    t_sampling_date.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_calendar_new, 0, 0, 0);
                    t_sampling_date.setTextColor(ContextCompat.getColor(context, R.color.txt_doc_value));
                    t_doc_label.setTextColor(ContextCompat.getColor(context, R.color.txt_doc));
                    i--;

                }

            }

        }
    }
}

