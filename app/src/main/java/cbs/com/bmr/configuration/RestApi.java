package cbs.com.bmr.configuration;

import java.util.ArrayList;

import cbs.com.bmr.model.CustomerDetails;
import cbs.com.bmr.model.CustomerList;
import cbs.com.bmr.model.DepartmentList;
import cbs.com.bmr.model.EmployeeInfoForManagementRequest;
import cbs.com.bmr.model.EmployeeList;
import cbs.com.bmr.model.EmployeeRegionList;
import cbs.com.bmr.model.EmployeeRouteList;
import cbs.com.bmr.model.EmployeeRouteListHistory;
import cbs.com.bmr.model.EmployeeSessionList;
import cbs.com.bmr.model.EmployeeTrackingList;
import cbs.com.bmr.model.FeedbackQuestions;
import cbs.com.bmr.model.LeaveApprovalList;
import cbs.com.bmr.model.LeaveApprovalSuccess;
import cbs.com.bmr.model.LeaveBalanceList;
import cbs.com.bmr.model.ManagementRequestList;
import cbs.com.bmr.model.MyTodoList;
import cbs.com.bmr.model.PhysicalTestFeedbackResponse;
import cbs.com.bmr.model.PondDetailedList;
import cbs.com.bmr.model.PondList;
import cbs.com.bmr.model.SampleHarvestHistory;
import cbs.com.bmr.model.SamplingCycle;
import cbs.com.bmr.model.StateList;
import cbs.com.bmr.model.SuccessMessage;
import cbs.com.bmr.model.TODO_Taskslist;
import cbs.com.bmr.model.TaskList;
import cbs.com.bmr.model.TaskScheduler;
import cbs.com.bmr.model.ZoneMaster;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/*********************************************************************
 * Created by Barani on 21-03-2019 in TableMateNew
 ***********************************************************************/
public interface RestApi {

    @FormUrlEncoded
    @POST("index.php/tasksummary/updatetasksummaryapi")
    Call<SuccessMessage> updateTaskSummary(@Field("task_id") String task_id, @Field("td_id") String td_id,
                                           @Field("task_date") String task_date,
                                           @Field("created_by_id") String created_by_id,
                                           @Field("approved_by") String approved_by_id,
                                           @Field("created_date") String created_date, @Field("taskSchedule") String task);

    @FormUrlEncoded
    @POST("index.php/tasksummary/getlisttaskapi")
    Call<ArrayList<TaskList>> getTaskList(@Field("emp_id") String emp_id, @Field("region_id") String region_id,
                                          @Field("from_date") String from_date, @Field("to_date") String to_date);

    @FormUrlEncoded
    @POST("index.php/tasksummary/getlistallpendingtaskapi")
    Call<ArrayList<TaskList>> getTaskApproval(@Field("emp_id") String emp_id, @Field("region_id") String region_id,
                                              @Field("from_date") String from_date, @Field("to_date") String to_date);
    @FormUrlEncoded
    @POST("index.php/tasksummary/getcurrentlisttaskapi")
    Call<ArrayList<TaskList>> getCurrentdayTaskList(@Field("emp_id") String emp_id, @Field("region_id") String region_id,
                                                    @Field("from_date") String from_date, @Field("to_date") String to_date);
    @FormUrlEncoded
    @POST("index.php/customers/getlistapi")
    Call<ArrayList<CustomerList>> getCustomerList(@Field("login_id") String emp_id);

    @FormUrlEncoded
    @POST("index.php/customers/getlistapi")
    Observable<Response<ArrayList<CustomerList>>> getCustomerListRX(@Field("login_id") String emp_id);

    @GET("index.php/regionalzonemaster/getlistapi")
    Call<ArrayList<ZoneMaster>> getZoneList();

    @FormUrlEncoded
    @POST("index.php/employee/getlistapi")
    Call<ArrayList<EmployeeList>> getEmployee_List(@Field("login_id") String id);

    @GET("index.php/employee/getdepartmentlistapi")
    Call<ArrayList<EmployeeInfoForManagementRequest>> getDepartment_List();

    @GET("index.php/feedbackquestion/getlist")
    Call<ArrayList<FeedbackQuestions>> getFeedbackQuestions(@Query("type") String type);

    @FormUrlEncoded
    @POST("/index.php/tasksummary/createtasksummaryapi")
    Call<SuccessMessage> createTaskSummary(@Field("task_id") String task_id, @Field("task_date") String task_date,
                                           @Field("created_by_id") String created_by_id,
                                           @Field("approved_by") String approved_by_id,
                                           @Field("assigned_by_id") String assigned_by_id,
                                           @Field("created_date") String created_date, @Field("taskSchedule") String taskSchedule);

    @FormUrlEncoded
    @POST("/index.php/feedbackQuestion/feedbackquestionsubmitapi")
    Call<SuccessMessage> feedbackSubmit(@Field("td_id") String td_id, @Field("fb_submit_time") String fb_submit,
                                        @Field("fb_geo_json") String fb_geo,
                                        @Field("feedBack") String feedback,
                                        @Field("login_id") String emp_id, @Field("fb_comments") String comments);

    @FormUrlEncoded
    @POST("/index.php/employee/loginapi")
    Call<SuccessMessage> login_bmr(@Field("username") String username, @Field("password") String password,
                                   @Field("version_no") String version_no);

    @FormUrlEncoded
    @POST("/index.php/employee/createattendance")
    Call<SuccessMessage> employee_in_create_attendance(@Field("empid") String emp_id, @Field("check_in") String check_in,
                                                       @Field("geo_checkin") String geo_in, @Field("starting_meter") String starting_meter);

    @FormUrlEncoded
    @POST("/index.php/employee/updateattendance")
    Call<SuccessMessage> employee_out_update_attendance(@Field("empid") String emp_id, @Field("update_id") String update_id,
                                                        @Field("check_out") String check_out,
                                                        @Field("geo_checkout") String geo_checkout, @Field("closing_meter") String closing_meter);


    @FormUrlEncoded
    @POST("index.php/tasksummary/tasksummarycheckinapi")
    Observable<Response<SuccessMessage>> check_in_taskRX(@Field("td_id") String id, @Field("check_in") String check_in,
                                                         @Field("geo_checkin") String geo_in, @Field("login_id") String emp_id);


    @FormUrlEncoded
    @POST("index.php/tasksummary/tasksummarycheckoutapi")
    Call<SuccessMessage> check_out_task(@Field("td_id") String id, @Field("check_out") String check_in,
                                        @Field("geo_checkout") String geo_out, @Field("login_id") String emp_id);

    @GET("index.php/taskSummary/gettodolistapi")
    Call<ArrayList<TODO_Taskslist>> getTodo_Tasks_list();

    @FormUrlEncoded
    @POST("index.php/taskSummary/gettaskdataapi")
    Call<ArrayList<TODO_Taskslist>> get_Task_By_ID(@Field("task_id") String task_id);

    /*Create To-do List*/
    @FormUrlEncoded
    @POST("index.php/taskSummary/createtodotaskapi")
    Call<SuccessMessage> create_Task_TodoList(@Field("task_text") String task_text, @Field("task_due") String task_due,
                                              @Field("task_notes") String task_notes, @Field("created_by") String created_by);

    /*Update To-do List*/
    @FormUrlEncoded
    @POST("index.php/taskSummary/updatetodotaskapi")
    Call<SuccessMessage> update_Task_TodoList(@Field("task_id") String task_id, @Field("task_text") String task_text, @Field("task_due") String task_due,
                                              @Field("task_notes") String task_notes);

    /*Delete Task*/
    @FormUrlEncoded
    @POST("index.php/taskSummary/deletetaskapi")
    Call<SuccessMessage> delete_task(@Field("task_id") String task_id);

    /*Task Approval - status update*/
    @FormUrlEncoded
    @POST("index.php/tasksummary/updatetaskstatusapi")
    Call<SuccessMessage> check_approved_status(@Field("user_id") String user_id, @Field("taskid") String task_id,
                                               @Field("approvedstatus") String approved_status);

    /*Get List of Management Requests*/
    @GET("index.php/managementrequest/getlist/{id}")
    Call<ArrayList<ManagementRequestList>> getRequestsList(@Path("id") String id);

    /*Create Management Request*/
    @FormUrlEncoded
    @POST("index.php/managementRequest/createRequest")
    Call<SuccessMessage> createManagementRequest(@Field("req_by") String emp_id, @Field("req_dep") String dept_id,
                                                 @Field("req_desc") String req_desc, @Field("comments") String comments,
                                                 @Field("req_qty") String req_qty, @Field("expected_budget") String expect_budget);

    /*Update - Management Request*/
    @FormUrlEncoded
    @POST("index.php/managementRequest/updateRequest")
    Call<SuccessMessage> updateManagementRequest(@Field("id") String c_id, @Field("req_by") String emp_id, @Field("req_dep") String dept_id,
                                                 @Field("req_desc") String req_desc, @Field("comments") String comments,
                                                 @Field("req_qty") String req_qty, @Field("expected_budget") String expect_budget);

    /*Delete Management Request*/
    @FormUrlEncoded
    @POST("index.php/managementRequest/deleteRequest")
    Call<SuccessMessage> deleteManagementRequest(@Field("req_id") String request_id);

    /*Retrieve Employee Information&Department Details*/
    @GET("index.php/managementRequest/getemployeedepartment/{id}")
    Call<EmployeeInfoForManagementRequest> getEmployeeInfoList(@Path("id") String id);

    /*Day in status verification*/
    @GET("index.php/employee/dayinverification")
    Call<SuccessMessage> getDayInStatus(@Query("emp_id") String emp_id);

    /*change Password*/
    @FormUrlEncoded
    @POST("index.php/employee/changepasswordapi")
    Call<SuccessMessage> changePassword(@Field("emp_id") String emp_id, @Field("password") String password);

    /*
     * Send Mail
     * */
    @FormUrlEncoded
    @POST("index.php/employee/sendcustomermail")
    Call<SuccessMessage> sendMail(@Field("guest_email") String guest_email, @Field("emp_name") String emp_name,
                                  @Field("task_id") String customer_name, @Field("comments") String comments);

    /*
     * Create Customer
     * */
    @FormUrlEncoded
    @POST("index.php/customers/customercreateapi")
    Call<SuccessMessage> createCustomerInfo(@Field("contact_no") String c_num, @Field("customercategory") String category,
                                            @Field("firstname") String fName,
                                            @Field("lastname") String lName,
                                            @Field("address1") String address1, @Field("address2") String address2,
                                            @Field("city") String city, @Field("state") String state,
                                            @Field("zone") String zone, @Field("comments") String comments,
                                            @Field("cust_type") String cust_type, @Field("customerof") String customerof,
                                            @Field("status") String status);

    /*
     * Update Customer
     * */
    @FormUrlEncoded
    @POST("index.php/customers/customerupdateapi")
    Call<SuccessMessage> updateCustomerInfo(@Field("custid") String id, @Field("contact_no") String c_num,
                                            @Field("customercategory") String category,
                                            @Field("firstname") String fName, @Field("lastname") String lName,
                                            @Field("address1") String address1, @Field("address2") String address2,
                                            @Field("city") String city, @Field("state") String state,
                                            @Field("zone") String zone, @Field("comments") String comments,
                                            @Field("cust_type") String cust_type, @Field("customerof") String customerof,
                                            @Field("status") String status);

    /*
     * POND Creation
     * */
    @FormUrlEncoded
    @POST("index.php/tasksummary/createpondapi")
    Call<SuccessMessage> createPond(@Field("emp_id") String emp_id, @Field("cust_id") String cust_id, @Field("pond_id") String pond_id,
                                    @Field("comments") String comments, @Field("wsa") String wsa, @Field("density") String density,
                                    @Field("status") String status);

    /*Employee GPS*/
    @FormUrlEncoded
    @POST("index.php/employee/employeegpslog")
    Call<SuccessMessage> employee_GPS(@Field("emp_id") String emp_id,
                                      @Field("timestamp") String timestamp,
                                      @Field("device_id") String device_id,

                                      @Field("gps_coordinates") String gps_coordinates,
                                      @Field("battery_level") String battery_level);

    /*
     * POND Creation
     * */
    @FormUrlEncoded
    @POST("index.php/tasksummary/createpondapi")
    Call<SuccessMessage> createPond(@Field("emp_id") String emp_id, @Field("cust_id") String cust_id, @Field("pond_id") String pond_id,
                                    @Field("comments") String comments, @Field("size") String size, @Field("density") String density,
                                    @Field("pond_refer") String pond_ref, @Field("status") String status, @Field("wsa") String wsa,
                                    @Field("salinity") String salinity, @Field("seed_stocking") String seed_stocking, @Field("ph") String ph, @Field("stocking_date") String stocking_date,
                                    @Field("recorded_date") String recorded_date);

    /*
     * POND Update
     * */
    @FormUrlEncoded
    @POST("index.php/tasksummary/updatepondapi")
    Call<SuccessMessage> updatePond(@Field("emp_id") String emp_id, @Field("cust_id") String cust_id, @Field("pondid") String pond_id,
                                    @Field("cycle_id") String cycle_id, @Field("pond_id") String cust_pond_id,
                                    @Field("comments") String comments, @Field("size") String size, @Field("density") String density,
                                    @Field("pond_refer") String pond_ref, @Field("status") String status, @Field("wsa") String wsa,
                                    @Field("seed_stocking") String seed_stocking, @Field("ph") String ph, @Field("salinity") String salinity,
                                    @Field("stocking_date") String stocking_date,
                                    @Field("recorded_date") String recorded_date);

    /*Get POND List*/
    @GET("index.php/tasksummary/getpondlist")
    Call<ArrayList<PondList>> getPondList(@Query("cust_id") String customer_id);

    /*Get POND Details List*/
    @FormUrlEncoded
    @POST("index.php/tasksummary/getponddata")
    Call<PondDetailedList> getPondDetailedList(@Field("cycle_id") String cycle_id, @Field("pondid") String pond_id);

    /*Create Sampling*/
    @FormUrlEncoded
    @POST("index.php/tasksummary/createsampling")
    Call<SuccessMessage> createSampling(@Field("cycle_id") String cycle_id, @Field("emp_id") String emp_id,
                                        @Field("daily_feed") String daily_feed, @Field("recorded_date") String recorded_date,
                                        @Field("sample_harvest_flag") String sample_harvest_flag,
                                        @Field("abw") String abw, @Field("sampling_date") String sampling_date);

    /*Update Sampling*/
    @FormUrlEncoded
    @POST("index.php/tasksummary/updatesampling")
    Call<SuccessMessage> updateSampling(@Field("sample_id ") String sample_id, @Field("emp_id") String emp_id,
                                        @Field("daily_feed") String daily_feed, @Field("recorded_date") String recorded_date,
                                        @Field("sample_harvest_flag") String sample_harvest_flag,
                                        @Field("abw") String abw);

    /*Get Cycling detailed list*/
    @FormUrlEncoded
    @POST("index.php/tasksummary/getPondlistSampling")
    Call<ArrayList<PondList>> getPondSamplingList(@Field("cust_id") String customer_id);

    /*Region List*/
    @FormUrlEncoded
    @POST("index.php/employee/getregionlist")
    Call<ArrayList<EmployeeRegionList>> getRegionList(@Field("login_id") String loginid);

    /*Region based Employee List*/
    @FormUrlEncoded
    @POST("index.php/employee/getregionemployeelist")
    Call<ArrayList<EmployeeTrackingList>> getEmployeeList(@Field("region_id") String regionid,
                                                          @Field("login_id") String loginid);

    /*Get Live Route*/
    @FormUrlEncoded
    @POST("index.php/employee/getliveroute")
    Call<ArrayList<EmployeeRouteList>> getEmployeeRoute(@Field("emp_id") String empid, @Field("date") String date);

    /*Physical Test API*/
    @FormUrlEncoded
    @POST("/index.php/feedbackquestion/qcphysicaltestapi")
    Call<PhysicalTestFeedbackResponse> PhysicalTest(@Field("task_id") String taskid,
                                                    @Field("login_id") String loginid,
                                                    @Field("datetime") String datetime,
                                                    @Field("powder_hg_1_2") String powder_hg_1_2,
                                                    @Field("hg3s_3p_4s_4_5") String hg3s_3p_4s_4_5,
                                                    @Field("pride3p_4s_4") String pride3p_4s_4,
                                                    @Field("fcr") String fcr,
                                                    @Field("molds_infection") String molds_infection,
                                                    @Field("fibrous") String fibrous,
                                                    @Field("stichingproblem") String stichingproblem,
                                                    @Field("feedsinkingtime") String feedsinkingtime,
                                                    @Field("farmer_id") String farmer_id,
                                                    @Field("pond_id") String pond_id,
                                                    @Field("comments") String comments
    );

    @FormUrlEncoded
    @POST("/index.php/feedbackquestion/qcphysicaltestapi")
    Call<PhysicalTestFeedbackResponse> PhysicalTestgeo_json(@Field("geo_json") String geo_json,
                                                            @Field("login_id") String loginid,
                                                            @Field("datetime") String datetime,
                                                            @Field("powder_hg_1_2") String powder_hg_1_2,
                                                            @Field("hg3s_3p_4s_4_5") String hg3s_3p_4s_4_5,
                                                            @Field("pride3p_4s_4") String pride3p_4s_4,
                                                            @Field("fcr") String fcr,
                                                            @Field("molds_infection") String molds_infection,
                                                            @Field("fibrous") String fibrous,
                                                            @Field("stichingproblem") String stichingproblem,
                                                            @Field("feedsinkingtime") String feedsinkingtime,
                                                            @Field("farmer_id") String farmer_id,
                                                            @Field("pond_id") String pond_id,
                                                            @Field("comments") String comments
    );
    @FormUrlEncoded
    @POST("/index.php/feedbackquestion/qcphysicaltestapi")
    Call<PhysicalTestFeedbackResponse> TechnicalTest(@Field("task_id") String taskid,
                                                     @Field("login_id") String loginid,
                                                     @Field("datetime") String datetime,
                                                     @Field("farmer_id") String farmer_id,
                                                     @Field("pond_id") String pond_id,
                                                     @Field("water_sources") String water_sources,
                                                     @Field("pumping") String pumping,
                                                     @Field("treatment") String treatment,
                                                     @Field("check_tray_feed_qty") String check_tray_feed_qty,
                                                     @Field("sedimentation_tank") String sedimentation_tank,
                                                     @Field("pre_treatment_available") String pre_treatment_available,
                                                     @Field("fencing_and_disinfectants") String fencing_and_disinfectants,
                                                     @Field("applied_any_fermentation") String applied_any_fermentation,
                                                     @Field("probiotic_supplementation") String probiotic_supplementation,
                                                     @Field("aerators_chlorination_after_crap") String aerators_chlorination_after_crap,
                                                     @Field("comments") String comments);

    @FormUrlEncoded
    @POST("/index.php/feedbackquestion/qcphysicaltestapi")
    Call<PhysicalTestFeedbackResponse> TechnicalTestGeoJson(@Field("geo_json") String geo_json,
                                                            @Field("login_id") String loginid,
                                                            @Field("datetime") String datetime,
                                                            @Field("farmer_id") String farmer_id,
                                                            @Field("pond_id") String pond_id,
                                                            @Field("water_sources") String water_sources,
                                                            @Field("pumping") String pumping,
                                                            @Field("treatment") String treatment,
                                                            @Field("check_tray_feed_qty") String check_tray_feed_qty,
                                                            @Field("sedimentation_tank") String sedimentation_tank,
                                                            @Field("pre_treatment_available") String pre_treatment_available,
                                                            @Field("fencing_and_disinfectants") String fencing_and_disinfectants,
                                                            @Field("applied_any_fermentation") String applied_any_fermentation,
                                                            @Field("probiotic_supplementation") String probiotic_supplementation,
                                                            @Field("aerators_chlorination_after_crap") String aerators_chlorination_after_crap,
                                                            @Field("comments") String comments
    );
    /*Get Route List API*/

    //@POST("index.php/employee/getroutelistapi")
    @FormUrlEncoded
    @POST("index.php/employee/getemployeelivelog")
    Call<ArrayList<EmployeeRouteListHistory>> getEmployeeRouteHistory(@Field("emp_id") String empid, @Field("date") String date);

    /*Pending Task Lists*/
    @FormUrlEncoded
    @POST("index.php/tasksummary/getlistpendingtaskapi")
    Call<ArrayList<TaskList>> getPendingTaskList(@Field("emp_id") String employee_id);

    /*Check the sampling cycle is active/inactive*/
    @FormUrlEncoded
    @POST("/index.php/tasksummary/checkactivecycleapi")
    Call<ArrayList<SamplingCycle>> getsamplingCycle(@Field("pond_id") String pondId, @Field("cust_id") String CustId);


    /*List of Sampling last 5 entries*/
    @FormUrlEncoded
    @POST("index.php/tasksummary/getSampleHistory")
    Call<ArrayList<SampleHarvestHistory>> getHistory_for_sampling(@Field("cust_id") String cust_id,
                                                                  @Field("cycle_id") String cycle_id,
                                                                  @Field("pond_id") String pond_id,
                                                                  @Field("request") String req);


    @FormUrlEncoded
    @POST("index.php/tasksummary/getmytodolist")
    Call<ArrayList<MyTodoList>> getMytodolist(@Field("emp_id") String emp_id,
                                              @Field("assignee_type") String assignee_type,
                                              @Field("priority") String priority,
                                              @Field("status") String status);


    @FormUrlEncoded
    @POST("/index.php/tasksummary/gettodolistcreatedbyme")
    Call<ArrayList<MyTodoList>> getAssignedbyme(@Field("emp_id") String emp_id,
                                                @Field("assignee_type") String assignee_type,
                                                @Field("priority") String priority,
                                                @Field("status") String status);


    @FormUrlEncoded
    @POST("/index.php/tasksummary/updatemytodotaskapi")
    Call<MyTodoList> UpdateMyTodoList(@Field("task_id") String pondId, @Field("status") String status,
                                      @Field("priority") String priority,
                                      @Field("task_notes") String task_notes, @Field("login_id") String login_id);

    @GET("index.php/employee/getdepartmentlistapi")
    Call<ArrayList<DepartmentList>> getDepartment();


    @GET("index.php/employee/getdepartmentlistapi")
    Observable<Response<ArrayList<DepartmentList>>> getDepartment1();

    @GET("index.php/employee/getlistapi")
    Call<ArrayList<EmployeeTrackingList>> getEmployeeList(@Query("login_id") String loginId);

    @FormUrlEncoded
    @POST("/index.php/tasksummary/updatetodotaskapi")
    Call<MyTodoList> UpdateAssignedMeList(@Field("task_id") String taskid, @Field("task_name") String task_name,
                                          @Field("status") String status, @Field("priority") String priority,
                                          @Field("task_due") String task_due, @Field("task_notes") String task_notes,
                                          @Field("assigned_to") String assigned_to, @Field("assigned_type") String assigned_type,
                                          @Field("login_id") String login_id);



    @Multipart
    @POST("/index.php/tasksummary/createsampling")
    Call<SamplingCycle> CreateNewsamplingCycle(@Part("task_id") RequestBody task_id,
                                               @Part("pond_id") RequestBody pondId,
                                               @Part("cust_id") RequestBody custid,
                                               @Part("cycle_id") RequestBody cycleId,
                                               @Part("emp_id") RequestBody empId,
                                               @Part("new_cycle") RequestBody newcycle,
                                               @Part("culture_seed_date") RequestBody culture_seed_date,
                                               @Part("doc") RequestBody doc,
                                               @Part("stocking_date") RequestBody stocking_date,
                                               @Part("seed_stocking") RequestBody seed_stocking,
                                               @Part("ph") RequestBody ph,
                                               @Part("abw") RequestBody abw,
                                               @Part("salinity") RequestBody salinity,
                                               @Part("sampling_date") RequestBody sampling_date,
                                               @Part("daily_feed") RequestBody daily_feed,
                                               @Part("adg") RequestBody adg,
                                               @Part MultipartBody.Part sampling_file
    );

    @Multipart
    @POST("/index.php/tasksummary/createsampling")
    Call<SamplingCycle> HomeCreateNewsamplingCycle(@Part("geo_json") RequestBody geo_json, @Part("pond_id") RequestBody pondId, @Part("cust_id") RequestBody custid,
                                                   @Part("cycle_id") RequestBody cycleId, @Part("emp_id") RequestBody empId,
                                                   @Part("new_cycle") RequestBody newcycle, @Part("culture_seed_date") RequestBody culture_seed_date,
                                                   @Part("doc") RequestBody doc, @Part("stocking_date") RequestBody stocking_date,
                                                   @Part("seed_stocking") RequestBody seed_stocking,
                                                   @Part("ph") RequestBody ph, @Part("abw") RequestBody abw,
                                                   @Part("salinity") RequestBody salinity, @Part("sampling_date") RequestBody sampling_date,
                                                   @Part("daily_feed") RequestBody daily_feed, @Part("adg") RequestBody adg,
                                                   @Part MultipartBody.Part sampling_file
    );


    @Multipart
    @POST("/index.php/tasksummary/createsampling")
    Call<SamplingCycle> CreatesamplingCycle(@Part("task_id") RequestBody task_id, @Part("pond_id") RequestBody pondId, @Part("cust_id") RequestBody custid,
                                            @Part("cycle_id") RequestBody cycleId, @Part("emp_id") RequestBody empId,
                                            @Part("ph") RequestBody newcycle, @Part("abw") RequestBody culture_seed_date,
                                            @Part("salinity") RequestBody salinity, @Part("sampling_date") RequestBody stocking_date,
                                            @Part("daily_feed") RequestBody daily_feed, @Part("doc") RequestBody doc,
                                            @Part("adg") RequestBody adg,
                                            @Part MultipartBody.Part sampling_file);

    @Multipart
    @POST("/index.php/tasksummary/createsampling")
    Call<SamplingCycle> HomeCreatesamplingCycle(@Part("geo_json") RequestBody geo_json, @Part("pond_id") RequestBody pondId, @Part("cust_id") RequestBody custid,
                                                @Part("cycle_id") RequestBody cycleId, @Part("emp_id") RequestBody empId,
                                                @Part("ph") RequestBody newcycle, @Part("abw") RequestBody culture_seed_date,
                                                @Part("salinity") RequestBody salinity, @Part("sampling_date") RequestBody stocking_date,
                                                @Part("daily_feed") RequestBody daily_feed, @Part("doc") RequestBody doc,
                                                @Part("adg") RequestBody adg,
                                                @Part MultipartBody.Part sampling_file);
    //1 -> partially harvest , 2 -> fully harvest
    /*Create Harvest*/
    @Multipart
    @POST("index.php/tasksummary/createharvest")
    Call<SuccessMessage> createHarvestCycle(@Part("task_id") RequestBody taskid, @Part("cycle_id") RequestBody cycle_id,
                                            @Part("emp_id") RequestBody emp_id, @Part("pond_id") RequestBody pond_id,
                                            @Part("cust_id") RequestBody cust_id, @Part("harvest_date") RequestBody harvest_date,
                                            @Part("sample_harvest_flag") RequestBody sample_harvest_flag,
                                            @Part("daily_feed") RequestBody daily_feed,
                                            @Part("abw") RequestBody abw, @Part("survival") RequestBody survival,
                                            @Part("ph") RequestBody ph, @Part("salinity") RequestBody salinity,
                                            @Part("e_biomass") RequestBody e_biomass,
                                            @Part("a_biomass") RequestBody a_biomass,
                                            @Part("density") RequestBody density, @Part("adg") RequestBody adg,
                                            @Part("fcr") RequestBody fcr, @Part("productivity") RequestBody productivity,
                                            @Part("qty") RequestBody qty,
                                            @Part("seed_source") RequestBody seed_source1,
                                            @Part("doc") RequestBody doc,
                                            @Part("acres") RequestBody acres,
                                            @Part("total_feed") RequestBody total_feed,
                                            @Part MultipartBody.Part sampling_file);


    @Multipart
    @POST("index.php/tasksummary/createharvest")
    Call<SuccessMessage> createNewHarvest(@Part("task_id") RequestBody taskid, @Part("cycle_id") RequestBody cycle_id,
                                          @Part("emp_id") RequestBody emp_id, @Part("pond_id") RequestBody pond_id,
                                          @Part("cust_id") RequestBody cust_id,
                                          @Part("new_cycle") RequestBody newcycle, @Part("culture_seed_date") RequestBody culture_seed_date,
                                          @Part("stocking_date") RequestBody stocking_date, @Part("seed_stocking") RequestBody seed_stocking,
                                          @Part("harvest_date") RequestBody harvest_date,
                                          @Part("sample_harvest_flag") RequestBody sample_harvest_flag,
                                          @Part("daily_feed") RequestBody daily_feed,
                                          @Part("abw") RequestBody abw, @Part("survival") RequestBody survival,
                                          @Part("ph") RequestBody ph, @Part("salinity") RequestBody salinity,
                                          @Part("e_biomass") RequestBody e_biomass,
                                          @Part("a_biomass") RequestBody a_biomass,
                                          @Part("density") RequestBody density, @Part("adg") RequestBody adg,
                                          @Part("fcr") RequestBody fcr, @Part("productivity") RequestBody productivity,
                                          @Part("qty") RequestBody qty,
                                          @Part("seed_source") RequestBody seed_source1,
                                          @Part("doc") RequestBody doc,
                                          @Part("acres") RequestBody acres,
                                          @Part("total_feed") RequestBody total_feed,
                                          @Part MultipartBody.Part sampling_file);


    @Multipart
    @POST("index.php/tasksummary/createharvest")
    Call<SuccessMessage> HomecreateNewHarvest(@Part("geo_json") RequestBody geo_json, @Part("cycle_id") RequestBody cycle_id,
                                              @Part("emp_id") RequestBody emp_id, @Part("pond_id") RequestBody pond_id,
                                              @Part("cust_id") RequestBody cust_id,
                                              @Part("new_cycle") RequestBody newcycle, @Part("culture_seed_date") RequestBody culture_seed_date,
                                              @Part("stocking_date") RequestBody stocking_date, @Part("seed_stocking") RequestBody seed_stocking,
                                              @Part("harvest_date") RequestBody harvest_date,
                                              @Part("sample_harvest_flag") RequestBody sample_harvest_flag,
                                              @Part("daily_feed") RequestBody daily_feed,
                                              @Part("abw") RequestBody abw, @Part("survival") RequestBody survival,
                                              @Part("ph") RequestBody ph, @Part("salinity") RequestBody salinity,
                                              @Part("e_biomass") RequestBody e_biomass,
                                              @Part("a_biomass") RequestBody a_biomass,
                                              @Part("density") RequestBody density, @Part("adg") RequestBody adg,
                                              @Part("fcr") RequestBody fcr, @Part("productivity") RequestBody productivity,
                                              @Part("qty") RequestBody qty,
                                              @Part("seed_source") RequestBody seed_source1,
                                              @Part("doc") RequestBody doc,
                                              @Part("acres") RequestBody acres,
                                              @Part("total_feed") RequestBody total_feed,
                                              @Part MultipartBody.Part sampling_file);

    @Multipart
    @POST("index.php/tasksummary/createharvest")
    Call<SuccessMessage> HomecreateHarvestRecord(@Part("geo_json") RequestBody geo_json, @Part("cycle_id") RequestBody cycle_id,
                                                 @Part("emp_id") RequestBody emp_id, @Part("pond_id") RequestBody pond_id,
                                                 @Part("cust_id") RequestBody cust_id, @Part("harvest_date") RequestBody harvest_date,
                                                 @Part("sample_harvest_flag") RequestBody sample_harvest_flag,
                                                 @Part("daily_feed") RequestBody daily_feed,
                                                 @Part("abw") RequestBody abw, @Part("survival") RequestBody survival,
                                                 @Part("ph") RequestBody ph, @Part("salinity") RequestBody salinity,
                                                 @Part("e_biomass") RequestBody e_biomass,
                                                 @Part("a_biomass") RequestBody a_biomass,
                                                 @Part("density") RequestBody density, @Part("adg") RequestBody adg,
                                                 @Part("fcr") RequestBody fcr,
                                                 @Part("productivity") RequestBody productivity,
                                                 @Part("qty") RequestBody qty,
                                                 @Part("seed_source") RequestBody seed_source1,
                                                 @Part("doc") RequestBody doc,
                                                 @Part("acres") RequestBody acres,
                                                 @Part("total_feed") RequestBody total_feed,
                                                 @Part MultipartBody.Part sampling_file);

    @FormUrlEncoded
    @POST("/index.php/tasksummary/createtodotaskapi")
    Call<MyTodoList> CreateTodoList(@Field("task_text") String task_text,
                                    @Field("status") String status, @Field("priority") String priority,
                                    @Field("created_by") String created_by, @Field("task_due") String task_due,
                                    @Field("task_notes") String task_notes,
                                    @Field("assigned_to") String assigned_to, @Field("assigned_type") String assigned_type);

    @FormUrlEncoded
    @POST("/index.php/tasksummary/getinchargebyclusterdataapi")
    Call<ArrayList<TaskScheduler>> getScheduleList(@Field("login_id") String login_id);


    @FormUrlEncoded
    @POST("index.php/employee/logout")
    Call<Void> LogoutUser(@Field("emp_id") String regionid);

    @POST("index.php/employee/employeeloginlistapi")
    Call<ArrayList<EmployeeSessionList>> getEmployeeSession();

    @FormUrlEncoded
    @POST("/index.php/employee/resetemployeeloginapi")
    Call<EmployeeSessionList> ResetSession(
            @Field("emp_id") String empid,
            @Field("username") String username,
            @Field("password") String password);

    @GET("index.php/customers/getstatecity")
    Observable<Response<ArrayList<StateList>>> getStateListRX();


    @FormUrlEncoded
    @POST("index.php/leaverequest/getbalanceleaveapi")
    Observable<Response<ArrayList<LeaveBalanceList>>> getLeavebalance(@Field("emp_id") String empid);

    @FormUrlEncoded
    @POST("index.php/leaverequest/createleaverequestapi")
    Observable<Response<LeaveBalanceList>> createLeaveRequest(@Field("emp_id") String empid,
                                                              @Field("leave_type") String leave_type,
                                                              @Field("purpose") String purpose,
                                                              @Field("leave_days") String leave_days,
                                                              @Field("from_date") String from_date,
                                                              @Field("to_date") String to_date);


    @FormUrlEncoded
    @POST("index.php/leaverequest/leavelistapi")
    Observable<Response<ArrayList<LeaveApprovalList>>> getLeaveApprovalList(@Field("emp_id") String empid);

    @FormUrlEncoded
    @POST("index.php/leaverequest/approvalleaverequestapi")
    Observable<Response<LeaveApprovalSuccess>> createLeaveApproval(@Field("emp_id") String empid,
                                                                   @Field("leave_id") String leave_id,
                                                                   @Field("status") String status);

    @FormUrlEncoded
    @POST("index.php/customers/customercheckapi")
    Call<CustomerDetails> getDefaultCustomerDetails(@Field("emp_id") String empid,

                                                    @Field("mobile") String mobile);

}