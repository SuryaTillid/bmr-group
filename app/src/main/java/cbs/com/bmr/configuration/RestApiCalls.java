package cbs.com.bmr.configuration;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;

import cbs.com.bmr.model.CustomerList;
import cbs.com.bmr.model.EmployeeInfoForManagementRequest;
import cbs.com.bmr.model.EmployeeList;
import cbs.com.bmr.model.FeedbackQuestions;
import cbs.com.bmr.model.ManagementRequestList;
import cbs.com.bmr.model.PondDetailedList;
import cbs.com.bmr.model.PondList;
import cbs.com.bmr.model.SampleHarvestHistory;
import cbs.com.bmr.model.SuccessMessage;
import cbs.com.bmr.model.TODO_Taskslist;
import cbs.com.bmr.model.TaskList;
import cbs.com.bmr.model.TaskScheduler;
import cbs.com.bmr.model.ZoneMaster;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/*********************************************************************
 * Created by Barani on 21-03-2019 in TableMateNew
 ***********************************************************************/
public class RestApiCalls {

    public ArrayList<TaskList> getTaskDetailsList(App app, String emp_id, String region_id, String from, String to) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<TaskList> taskLists = new ArrayList<>();
        try {
            taskLists = apiService.getTaskList(emp_id, region_id, from, to).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskLists;
    }

    public ArrayList<TaskList> getTaskApproval(App app, String emp_id, String region_id, String from, String to) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<TaskList> taskLists = new ArrayList<>();
        try {
            taskLists = apiService.getTaskApproval(emp_id, region_id, from, to).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskLists;
    }
    public ArrayList<TaskList> getCurrentTaskDetailsList(App app, String emp_id, String region_id, String from, String to) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<TaskList> taskLists = new ArrayList<>();
        try {
            taskLists = apiService.getCurrentdayTaskList(emp_id, region_id, from, to).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskLists;
    }
    public ArrayList<CustomerList> getCustomerMasterList(App app, String login_id) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<CustomerList> customerLists = new ArrayList<>();
        try {
            customerLists = apiService.getCustomerList(login_id).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customerLists;
    }

    public ArrayList<EmployeeList> getEmployeeList(App app, String login_id) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<EmployeeList> employeeLists = new ArrayList<>();
        try {
            employeeLists = apiService.getEmployee_List(login_id).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return employeeLists;
    }

    public ArrayList<FeedbackQuestions> getFeedbackQuestionsList(App app, String type) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<FeedbackQuestions> feedbackQuestions = new ArrayList<>();
        try {
            feedbackQuestions = apiService.getFeedbackQuestions(type).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return feedbackQuestions;
    }

    public SuccessMessage createTask(App app, String task_id, String task_date, String created_by_id,
                                     String approved_by, String assigned_by_id,
                                     String create_date, String task_schedule) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.createTaskSummary(task_id, task_date, created_by_id, approved_by, assigned_by_id, create_date, task_schedule).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public SuccessMessage updateTask(App app, String task_id, String td_id, String task_date, String created_by_id,
                                     String approved_by, String create_date, String task_schedule) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {

            Log.e("Updaye", "" + apiService.updateTaskSummary(task_id, td_id, task_date, created_by_id, approved_by, create_date, task_schedule).execute().toString());
            response = apiService.updateTaskSummary(task_id, td_id, task_date, created_by_id, approved_by, create_date, task_schedule).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public SuccessMessage FeedbackSubmit(App app, String t_id, String date, String geo_values, String feedback_content, String e_id, String fb_comments) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.feedbackSubmit(t_id, date, geo_values, feedback_content, e_id, fb_comments).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public SuccessMessage LoginSubmit(App app, String user_name, String password, String version) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.login_bmr(user_name, password, version).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public SuccessMessage EmployeeIn_Create_attendance(App app, String emp_id, String check_in, String geo_in, String starting_meter) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.employee_in_create_attendance(emp_id, check_in, geo_in, starting_meter).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public SuccessMessage EmployeeOut_Update_attendance(App app, String emp_id, String update_id, String check_out, String geo_out, String closing_meter) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.employee_out_update_attendance(emp_id, update_id, check_out, geo_out, closing_meter).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public SuccessMessage CheckOut_Task_update(App app, String t_id, String check_out, String geo_out, String e_id) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.check_out_task(t_id, check_out, geo_out, e_id).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Create Todo_list task
     * */
    public SuccessMessage Create_task_todo_list(App app, String task_text, String task_due, String task_notes, String created_by) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.create_Task_TodoList(task_text, task_due, task_notes, created_by).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Update Todo_list task
     * */
    public SuccessMessage Update_task_todo_list(App app, String task_id, String task_text, String task_due, String task_notes) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.update_Task_TodoList(task_id, task_text, task_due, task_notes).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Get Todo_Task List
     * */
    public ArrayList<TODO_Taskslist> getTodo_tasksList(App app) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<TODO_Taskslist> todo_tasksLists = new ArrayList<>();
        try {
            todo_tasksLists = apiService.getTodo_Tasks_list().execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return todo_tasksLists;
    }

    /*
     * Approved Status
     * */
    public SuccessMessage CheckApprovedStatus(App app, String user_id, String task_id, String approve_status) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.check_approved_status(user_id, task_id, approve_status).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Get Management Request List - By Id
     * */
    public ArrayList<ManagementRequestList> getManagementRequestList(App app, String id) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<ManagementRequestList> list = new ArrayList<>();
        try {
            list = apiService.getRequestsList(id).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /*
     *  Get Employee Info List for Managemnet
     * */

    public EmployeeInfoForManagementRequest getEmployeeInfoForRequestList(App app, String id) {
        RestApi apiService = app.createRestAdaptor();
        EmployeeInfoForManagementRequest list = new EmployeeInfoForManagementRequest();
        try {
            list = apiService.getEmployeeInfoList(id).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /*
     * Request Create
     * */
    public SuccessMessage managementRequestCreate(App app, String req_by, String req_dept, String req_desc, String comments, String req_qty, String expected_budget) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.createManagementRequest(req_by, req_dept, req_desc, comments, req_qty, expected_budget).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Request Update
     * */
    public SuccessMessage managementRequestUpdate(App app, String id, String req_by, String req_dept, String req_desc, String comments, String req_qty, String expected_budget) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.updateManagementRequest(id, req_by, req_dept, req_desc, comments, req_qty, expected_budget).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Request Create
     * */
    public SuccessMessage ManagementRequestDelete(App app, String req_id) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.deleteManagementRequest(req_id).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * GET DEPARTMENT LIST
     * */

    public ArrayList<EmployeeInfoForManagementRequest> getDepartmentList(App app) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<EmployeeInfoForManagementRequest> departmentList = new ArrayList<>();
        try {
            departmentList = apiService.getDepartment_List().execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return departmentList;
    }

    /*
     * Change Password
     * */
    public SuccessMessage call_for_ChangePassword(App app, String emp_id, String password) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.changePassword(emp_id, password).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Get Day In Status
     * */

    public SuccessMessage call_for_getDayIn_status(App app, String emp_id) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.getDayInStatus(emp_id).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Send Comments to mail
     * */
    public SuccessMessage call_for_SendMail(App app, String guest_email, String emp_name, String task_id, String comments) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.sendMail(guest_email, emp_name, task_id, comments).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public ArrayList<ZoneMaster> getZone_List(App app) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<ZoneMaster> zone_list = new ArrayList<>();
        try {
            zone_list = apiService.getZoneList().execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return zone_list;
    }

    /*
     * Create Customer(Farmer/Dealer)
     * */
    public SuccessMessage createCustomerMaster(App app, String contact_num, String category, String firstName, String lastName,
                                               String addr1, String addr2, String city, String state, String zone,
                                               String comments, String c_type, String customer_of, String status) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.createCustomerInfo(contact_num, category, firstName, lastName, addr1, addr2, city, state, zone, comments, c_type, customer_of, status).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Create POND
     * */
    public SuccessMessage createPOND(App app, String emp_id, String cust_id, String pond_id, String comments,
                                     String size, String density, String pond_refer, String status,
                                     String wsa, String salinity, String seed_stocking, String ph, String stocking_date,
                                     String recorded_date) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.createPond(emp_id, cust_id, pond_id, comments, size, density, pond_refer, status, wsa, salinity, seed_stocking, ph,
                    stocking_date, recorded_date).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Create POND
     * */
    public SuccessMessage createPONDModified(App app, String emp_id, String cust_id, String pond_id, String comments,
                                             String wsa, String density, String status) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.createPond(emp_id, cust_id, pond_id, comments, wsa, density, status).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Update POND
     * */
    public SuccessMessage updatePOND(App app, String emp_id, String cust_id, String pond_id, String cycle_id, String cust_pond_id,
                                     String comments, String size, String density, String pond_refer, String status,
                                     String wsa, String seed_stocking, String ph, String salinity, String stocking_date,
                                     String recorded_date) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.updatePond(emp_id, cust_id, pond_id, cycle_id, cust_pond_id, comments, size, density, pond_refer, status, wsa, seed_stocking, ph, salinity,
                    stocking_date, recorded_date).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     *  Get POND List
     * */
    public ArrayList<PondList> getPONDName(App app, String cust_id) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<PondList> response = new ArrayList<>();
        try {
            response = apiService.getPondList(cust_id).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     *  Get POND detailed List
     * */
    public PondDetailedList getPOND_Details(App app, String cycle_id, String pond_id) {
        RestApi apiService = app.createRestAdaptor();
        PondDetailedList response = new PondDetailedList();
        try {
            response = apiService.getPondDetailedList(cycle_id, pond_id).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Post Employee GPS Co-ordinates
     * */
    public SuccessMessage postEmployeeGPSLog(App app, String emp_id, String time_stamp, String deviceid, String gps, String batlevel) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.employee_GPS(emp_id, time_stamp, deviceid, gps, batlevel).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Create Sampling Details
     * */
    public SuccessMessage createSamplingRecord(App app, String cycle_id, String emp_id, String daily_feed, String recorded_date,
                                               String sample_harvest_flag, String abw, String sampling_date) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.createSampling(cycle_id, emp_id, daily_feed, recorded_date,
                    sample_harvest_flag, abw, sampling_date).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Update Sampling Details
     * */
    public SuccessMessage updateSamplingRecord(App app, String sample_id, String emp_id, String daily_feed, String recorded_date,
                                               String sample_harvest_flag, String abw) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.updateSampling(sample_id, emp_id, daily_feed, recorded_date, sample_harvest_flag, abw).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Pond Sampling Detailed List
     * */

    public ArrayList<PondList> getPONDSamplingDetails(App app, String cust_id) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<PondList> response = new ArrayList<>();
        try {
            response = apiService.getPondSamplingList(cust_id).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Pending Tasks List
     * */
    public ArrayList<TaskList> getPendingTaskDetailsList(App application, String employee_id) {
        RestApi apiService = application.createRestAdaptor();
        ArrayList<TaskList> taskLists = new ArrayList<>();
        try {
            taskLists = apiService.getPendingTaskList(employee_id).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskLists;
    }

    /*
     * Sampling Harvest History - Last 5 records
     * */
    public ArrayList<SampleHarvestHistory> getSamplingHarvestHistory(App application, String c_id, String cycle_id, String pondid, String request) {
        RestApi apiService = application.createRestAdaptor();
        ArrayList<SampleHarvestHistory> s_list = new ArrayList<>();
        try {
            s_list = apiService.getHistory_for_sampling(c_id, cycle_id, pondid, request).execute().body();
            Log.e("harvesthistory", "" + new Gson().toJson(apiService.getHistory_for_sampling(c_id, cycle_id, pondid, request).execute().body()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s_list;
    }

    /*
     * Create Harvest
     * */


    public SuccessMessage createNewHarvest(App app, RequestBody taskId, RequestBody cycle_id, RequestBody emp_id, RequestBody pond_id, RequestBody cust_id,
                                           RequestBody NewCycle, RequestBody CultureseedDate, RequestBody Stockingdate,
                                           RequestBody seed_stocking, RequestBody harvestDate, RequestBody h_flag, RequestBody daily_feed,
                                           RequestBody abw, RequestBody survival,
                                           RequestBody ph, RequestBody salinity, RequestBody e_biomass, RequestBody a_biomass, RequestBody density,
                                           RequestBody adg, RequestBody fcr, RequestBody productivity, RequestBody qty, RequestBody seedsource,
                                           RequestBody Doc, RequestBody Acres, RequestBody totalfeed, MultipartBody.Part imageFileBody) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.createNewHarvest(taskId, cycle_id, emp_id, pond_id, cust_id, NewCycle, CultureseedDate,
                    Stockingdate, seed_stocking, harvestDate, h_flag, daily_feed, abw,
                    survival, ph, salinity, e_biomass, a_biomass, density, adg, fcr, productivity, qty, seedsource, Doc, Acres, totalfeed, imageFileBody).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public SuccessMessage createHarvestCycle(App app, RequestBody taskId, RequestBody cycle_id, RequestBody emp_id, RequestBody pond_id, RequestBody cust_id,
                                             RequestBody harvestDate, RequestBody h_flag, RequestBody daily_feed, RequestBody abw, RequestBody survival,
                                             RequestBody ph, RequestBody salinity, RequestBody e_biomass, RequestBody a_biomass, RequestBody density,
                                             RequestBody adg, RequestBody fcr, RequestBody productivity, RequestBody qty, RequestBody seedsource,
                                             RequestBody Doc, RequestBody Acres, RequestBody totalfeed, MultipartBody.Part imageFileBody) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.createHarvestCycle(taskId, cycle_id, emp_id, pond_id, cust_id, harvestDate, h_flag, daily_feed, abw,
                    survival, ph, salinity, e_biomass, a_biomass, density, adg, fcr, productivity, qty, seedsource, Doc, Acres, totalfeed, imageFileBody).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public SuccessMessage createNewHarvestHome(App app, RequestBody GeoJson, RequestBody cycle_id, RequestBody emp_id, RequestBody pond_id, RequestBody cust_id,
                                               RequestBody NewCycle, RequestBody CultureseedDate, RequestBody Stockingdate,
                                               RequestBody seed_stocking, RequestBody harvestDate, RequestBody h_flag, RequestBody daily_feed, RequestBody abw, RequestBody survival,
                                               RequestBody ph, RequestBody salinity, RequestBody e_biomass, RequestBody a_biomass,
                                               RequestBody density,
                                               RequestBody adg, RequestBody fcr,
                                               RequestBody productivity, RequestBody qty, RequestBody seedsource,
                                               RequestBody Doc, RequestBody Acres, RequestBody totalfeed,
                                               MultipartBody.Part imageFileBody) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.HomecreateNewHarvest(GeoJson, cycle_id, emp_id, pond_id, cust_id, NewCycle, CultureseedDate,
                    Stockingdate, seed_stocking, harvestDate, h_flag, daily_feed, abw,
                    survival, ph, salinity, e_biomass, a_biomass, density, adg, fcr, productivity, qty, seedsource, Doc, Acres, totalfeed, imageFileBody).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public SuccessMessage createHarvestCycleHome(App app, RequestBody GeoJson, RequestBody cycle_id, RequestBody emp_id, RequestBody pond_id, RequestBody cust_id,
                                                 RequestBody harvestDate, RequestBody h_flag, RequestBody daily_feed, RequestBody abw, RequestBody survival,
                                                 RequestBody ph, RequestBody salinity, RequestBody e_biomass, RequestBody a_biomass, RequestBody density,
                                                 RequestBody adg, RequestBody fcr, RequestBody productivity, RequestBody qty, RequestBody seedsource,
                                                 RequestBody Doc, RequestBody Acres, RequestBody totalfeed, MultipartBody.Part imageFileBody) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = apiService.HomecreateHarvestRecord(GeoJson, cycle_id, emp_id, pond_id, cust_id, harvestDate, h_flag, daily_feed, abw,
                    survival, ph, salinity, e_biomass, a_biomass, density, adg, fcr, productivity, qty, seedsource, Doc, Acres, totalfeed, imageFileBody).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public ArrayList<TaskScheduler> getScheduleListValues(App app, String employee_id) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<TaskScheduler> s_list = new ArrayList<>();
        try {
            s_list = apiService.getScheduleList(employee_id).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s_list;
    }
}