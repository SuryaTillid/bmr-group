package cbs.com.bmr.Helper;

import android.content.Context;
import android.content.SharedPreferences;

/*********************************************************************
 * Created by Barani on 26-03-2019 in TableMateNew
 ***********************************************************************/
public class ConfigurationSettings {

    //Configuration
    final String IS_CLICKED_POND = "isClicked_pond_creation";
    final String IS_CONFIGURED = "isConfigured";
    final String IS_CHECKED_IN = "isCheckIn";
    final String CHECKED_IN_TIME = "checkInTime";
    final String STRATING_TIME = "starting_meter";
    final String CHECKED_OUT_TIME = "checkOutTime";
    final String IP_ADDRESS = "ipAddress";
    final String USER_NAME = "user_name";
    final String Employee_ID = "employee_id";
    final String UPDATE_ID = "update_id";
    final String REGION_ID = "region_id";
    final String REGION_NAME = "region_name";
    final String PASSWORD = "password";
    private final String PREFERENCE_NAME = "BMR_Configure_Pref";
    private final int MODE = 0;
    private Context context;
    private SharedPreferences sPref;
    private SharedPreferences.Editor editor;


    public ConfigurationSettings(Context context) {
        this.context = context;
        sPref = context.getSharedPreferences(PREFERENCE_NAME, MODE);
        editor = sPref.edit();
    }

    public boolean isPOND_CLICKED() {
        return sPref.getBoolean(IS_CLICKED_POND, false);
    }

    public void setPOND_CLICKED(Boolean val) {
        editor.putBoolean(IS_CLICKED_POND, val);
        editor.commit();
    }

    public boolean isCheckIn() {
        return sPref.getBoolean(IS_CHECKED_IN, false);
    }

    public void setIS_CHECKED_IN(Boolean value) {
        editor.putBoolean(IS_CHECKED_IN, value);
        editor.commit();
    }

    public void setIS_CONFIGURED() {
        editor.putBoolean(IS_CONFIGURED, true);
        editor.commit();
    }

    public String getIP_ADDRESS() {
        return sPref.getString(IP_ADDRESS, null);
    }

    public void setIP_ADDRESS(String ipAddress) {
        editor.putString(IP_ADDRESS, ipAddress);
        editor.commit();
    }

    public String getSTRATING_TIME() {
        return sPref.getString(STRATING_TIME, null);
    }

    public void setSTARTING_TIME(String checkedInTime) {
        editor.putString(STRATING_TIME, checkedInTime);
        editor.commit();
    }
    public String getCHECKED_IN_TIME() {
        return sPref.getString(CHECKED_IN_TIME, null);
    }

    public void setCHECKED_IN_TIME(String checkedInTime) {
        editor.putString(CHECKED_IN_TIME, checkedInTime);
        editor.commit();
    }

    public String getCHECKED_OUT_TIME() {
        return sPref.getString(CHECKED_OUT_TIME, null);
    }

    public void setCHECKED_OUT_TIME(String checked_out_time) {
        editor.putString(CHECKED_OUT_TIME, checked_out_time);
        editor.commit();
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public String getUSER_NAME() {
        return sPref.getString(USER_NAME, null);
    }

    public void setUSER_NAME(String userName) {
        editor.putString(USER_NAME, userName);
        editor.commit();
    }

    public String getEmployee_ID() {
        return sPref.getString(Employee_ID, null);
    }

    public void setEmployee_ID(String employee_ID) {
        editor.putString(Employee_ID, employee_ID);
        editor.commit();
    }

    public String getUPDATE_ID() {
        return sPref.getString(UPDATE_ID, null);
    }

    public void setUPDATE_ID(String update_id) {
        editor.putString(UPDATE_ID, update_id);
        editor.commit();
    }

    public String getREGION_ID() {
        return sPref.getString(REGION_ID, null);
    }

    public void setREGION_ID(String region_id) {
        editor.putString(REGION_ID, region_id);
        editor.commit();
    }

    public String getREGION_NAME() {
        return sPref.getString(REGION_NAME, null);
    }

    public void setREGION_NAME(String region_name) {
        editor.putString(REGION_NAME, region_name);
        editor.commit();
    }

    public String getPASSWORD() {
        return sPref.getString(PASSWORD, null);
    }

    public void setPASSWORD(String password) {
        editor.putString(PASSWORD, password);
        editor.commit();
    }
}