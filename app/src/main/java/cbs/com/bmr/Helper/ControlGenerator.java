package cbs.com.bmr.Helper;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.util.Calendar;

import cbs.com.bmr.R;

/*********************************************************************
 * Created by Barani on 03-04-2019 in TableMateNew
 ***********************************************************************/
public class ControlGenerator {
    private Context context;
    private int mYear, mMonth, mDay;

    public ControlGenerator(Context context) {
        this.context = context;
    }

    public TextView createTextView(int id, String text) {
        TextView textView = new TextView(context);
        textView.setId(id);
        textView.setTextSize(16);

        textView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        textView.setText(text);
        return textView;
    }

    public EditText createEditText(int id, int drawable, String questionType) {
        EditText edit = new EditText(context);
        edit.setId(id);
        edit.setBackgroundResource(drawable);
        edit.setTextSize(15);
        edit.setPadding(30, 30, 30, 30);
        if (questionType.equalsIgnoreCase("0")) {
            edit.setInputType(InputType.TYPE_CLASS_NUMBER);
            edit.setInputType(InputType.TYPE_CLASS_DATETIME);
        } else {
            edit.setInputType(InputType.TYPE_CLASS_TEXT);
        }
        return edit;
    }

    public RadioGroup createRadioGroup(int id, String[] q_List, int orientation) {
        RadioGroup rg = new RadioGroup(context);
        rg.setId(id);
        for (String s : q_List) {
            RadioButton rb = new RadioButton(context);
            rb.setText(s);
            rb.setTextColor(Color.BLACK);
            rg.addView(rb);
            rg.setOrientation(orientation);
            //rg.setLayoutParams(p);
        }
        return rg;
    }


    public EditText createDatePicker(int id, int drawable, String questionType, int drwabledatepicker) {
        Log.e("sdsfd", "callingg....");
        final EditText edit = new EditText(context);
        edit.setId(id);
        edit.setBackgroundResource(drawable);
        edit.setTextSize(15);
        edit.setPadding(30, 30, 30, 30);
        edit.setCompoundDrawablesWithIntrinsicBounds(0, 0, drwabledatepicker, 0);
        edit.setFocusable(false);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatePickerHarvest(edit);
            }
        });
        return edit;
    }

    private void mDatePickerHarvest(final EditText editText) {
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if (dayOfMonth > 9 || monthOfYear > 9) {
                            String Docdate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            editText.setText(Docdate);
                        } else {
                            String Docdate = "0" + dayOfMonth + "/" + "0" + (monthOfYear + 1) + "/" + year;
                            editText.setText(Docdate);
                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

}
