package cbs.com.bmr.Helper;

import android.text.TextUtils;

/*********************************************************************
 * Created by Barani on 05-04-2019 in TableMateNew
 ***********************************************************************/
public class Utility {
    public static int customCounter = 0;


    public static String convertToString(int i) {
        if (i != 0) {
            return null;
        } else {
            try {
                return String.valueOf(i);
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }
    }

    public static int convertToInteger(String itemQty) {
        if (TextUtils.isEmpty(itemQty)) {
            return 0;
        } else {
            try {
                return Integer.parseInt(itemQty);
            } catch (Exception ex) {
                ex.printStackTrace();
                return 0;
            }
        }
    }

    public static double convertToDouble(String p_total) {
        if (TextUtils.isEmpty(p_total)) {
            return 0.0;
        } else {
            try {
                return Double.parseDouble(p_total);
            } catch (Exception ex) {
                ex.printStackTrace();
                return 0.0;
            }
        }
    }

    public static double convertToFloat(String p_total) {
        if (TextUtils.isEmpty(p_total)) {
            return 0.0;
        } else {
            try {
                return Float.parseFloat(p_total);
            } catch (Exception ex) {
                ex.printStackTrace();
                return 0.0;
            }
        }
    }
}


