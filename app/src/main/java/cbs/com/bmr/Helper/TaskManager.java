package cbs.com.bmr.Helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Map;

import cbs.com.bmr.model.TaskScheduler;

/*********************************************************************
 * Created by Barani on 27-03-2019 in TableMateNew
 ********************************************************************/
public class TaskManager {
    private final String PREFERENCE_NAME = "TableMate_Cart_Pref";
    private final int MODE = 0;
    private Context context;
    private SharedPreferences sPref;
    private SharedPreferences.Editor editor;
    private Gson gson;

    public TaskManager(Context context) {
        this.context = context;
        sPref = context.getSharedPreferences(PREFERENCE_NAME, MODE);
        editor = sPref.edit();
        gson = new Gson();
    }

    public void clearTasks() {
        editor.clear();
        editor.commit();
    }

    public void addTasks(TaskScheduler t) {
        String taskID = t.getT_id();
        String alreadyExists = sPref.getString("" + taskID, "");
        if (alreadyExists.equals("")) {
            String json = gson.toJson(t);
            editor.putString("" + taskID, json);
        } else {
            TaskScheduler taskScheduler = gson.fromJson(alreadyExists, TaskScheduler.class);
            if (taskScheduler != null) {
                String json1 = gson.toJson(taskScheduler);
                editor.putString("" + taskID, json1);
            }
        }
        editor.commit();
    }

    public int getQuantity(TaskScheduler t) {
        int qty = 0;
        for (TaskScheduler taskScheduler : getTaskList()) {
            if (taskScheduler.getTask_id().equalsIgnoreCase(t.getTask_id()))
                qty++;
        }
        return qty;
    }

    public ArrayList<TaskScheduler> getTaskList() {
        ArrayList<TaskScheduler> tList = new ArrayList<>();
        Map<String, ?> tasks = sPref.getAll();
        for (String key : tasks.keySet()) {
            String json = (String) tasks.get(key);
            Gson gson = new Gson();
            TaskScheduler task = gson.fromJson(json, TaskScheduler.class);
            tList.add(task);
        }
        return tList;
    }
}