package cbs.com.bmr.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.PondDetailedList;

import static cbs.com.bmr.Utilities.Activity.launchWithBundle;

/*********************************************************************
 * Created by Barani on 20-08-2019 in TableMateNew
 ***********************************************************************/
public class PondDetailedListActivity extends AppCompatActivity {

    private Context context;
    private TextView t_pond_name, t_wsa, t_seed_stock, t_density, t_comments, t_status, t_cycle_status, t_stock_date, t_record_date, txt_physicaltest;
    private TextView t_update;
    private LinearLayout l_edit_layout;
    private String cycle_id, pond_id, Farmer_id, customerID, customerNAME, TaskId, GeoJson;
    private String NoEdit;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pond_details);

        context = this;
        initialize();
        receiveValuesFromBundle();


        /*API for get POND detailed list*/
        new GetPOND_Detailed_List().execute(cycle_id, pond_id);

        t_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("POND_ID", pond_id);
                bundle.putString("CYCLE_ID", cycle_id);
                bundle.putString("CustomerName", customerNAME);
                bundle.putString("CustomerId", customerID);

                launchWithBundle(context, PondUpdateActivity.class, bundle);
            }
        });

        txt_physicaltest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("PondId", pond_id);
                bundle.putString("Farmer_id", customerID);
                bundle.putString("CustomerName", customerNAME);
                bundle.putString("CustomerName", customerNAME);
                bundle.putString("TaskId", TaskId);
                bundle.putString("GeoJson", GeoJson);
                launchWithBundle(context, TechnicalServicesActivity.class, bundle);
            }
        });

    }



    private void receiveValuesFromBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            cycle_id = bundle.getString("CYCLE_ID");
            pond_id = bundle.getString("POND_ID");
            customerID = bundle.getString("C_ID");
            customerNAME = bundle.getString("C_NAME");
            TaskId = bundle.getString("TaskId");
            GeoJson = bundle.getString("GeoJson");
            NoEdit = bundle.getString("Noedit");

            if (Objects.requireNonNull(NoEdit).equals("1")) {
                l_edit_layout.setVisibility(View.GONE);
            } else {
                l_edit_layout.setVisibility(View.VISIBLE);
            }
            AppLog.write("POND--", "--" + pond_id + "--" + cycle_id + "---" + NoEdit);
        }
    }

    private void initialize() {
        txt_physicaltest = findViewById(R.id.txt_physicaltest);
        t_pond_name = findViewById(R.id.toolbar_title);
        t_wsa = findViewById(R.id.txtWSA);
        t_seed_stock = findViewById(R.id.txtSeedStocking);
        t_density = findViewById(R.id.txtDensity);
        t_comments = findViewById(R.id.txtComments);
        t_status = findViewById(R.id.txtStatus);
        t_cycle_status = findViewById(R.id.txtCycle_Status);
        t_stock_date = findViewById(R.id.txtStockingDate);
        t_record_date = findViewById(R.id.txtRecordedDate);
        t_update = findViewById(R.id.txt_update);
        l_edit_layout = findViewById(R.id.edit_layout);
        mImageBack = findViewById(R.id.img_back);

        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onResume() {
        super.onResume();
       /* //((Home_Page_Activity) Objects.requireNonNull(getActivity())).setActionBarTitle("Pond Details");
        window.setStatusBarColor(ContextCompat.getColor(context, R.color.s3));
        ActionBar bar = ((Home_Page_Activity) getActivity()).getSupportActionBar();
        if (bar != null) {
            bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#6699ff")));
        }*/
    }


    private void setPondValues(PondDetailedList p) {
        Farmer_id = p.getFarmer_id();
        t_pond_name.setText(p.getPond_id());

        Log.e("Density", "" + p.getDensity());
        t_wsa.setText(p.getWSA());
        t_seed_stock.setText(p.getSeed_stocking());
        t_comments.setText(p.getComments());
        t_density.setText(p.getDensity());
        t_status.setText(validateStatus(p.getStatus()));
        t_cycle_status.setText(validateCycleStatus(p.getCycle_status()));
        t_stock_date.setText(validateDate(p.getStocking_date()));
        t_record_date.setText(validateDate(p.getRecorded_date()));
    }

    @SuppressLint("SimpleDateFormat")
    private String validateDate(String stocking_date) {
        try {
            if (!TextUtils.isEmpty(stocking_date) && !stocking_date.equals("0")) {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = format.parse(stocking_date);
                DateFormat date_formatter = new SimpleDateFormat("dd-MM-yyyy");
                stocking_date = date_formatter.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return stocking_date;
    }

    private String validateCycleStatus(String cycle_status) {
        if (!TextUtils.isEmpty(cycle_status)) {
            if (cycle_status.equalsIgnoreCase("0")) {
                cycle_status = "Active";
            } else if (cycle_status.equalsIgnoreCase("1")) {
                cycle_status = "Harvested";
            } else {
                cycle_status = "Not Updated";
            }
        }
        return cycle_status;
    }

    private String validateStatus(String status) {
        if (!TextUtils.isEmpty(status)) {
            if (status.equalsIgnoreCase("0")) {
                status = "Active";
            } else {
                status = "In Active";
            }
        }
        return status;
    }

    private class GetPOND_Detailed_List extends AsyncTask<String, String, PondDetailedList> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected PondDetailedList doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            return call.getPOND_Details((App) getApplication(), s[0], s[1]);
        }

        @Override
        protected void onPostExecute(PondDetailedList list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != list) {
                setPondValues(list);
            } else {
                Toast.makeText(context, "No Records Found..!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
