package cbs.com.bmr.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.CustomerNameAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.CustomerList;
import cbs.com.bmr.model.SuccessMessage;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/*********************************************************************
 * Created by Barani on 19-08-2019 in TableMateNew
 ********************************************************************/
public class PondCreationActivity extends AppCompatActivity implements View.OnClickListener {

    private ConfigurationSettings settings;
    private Context context;
    private EditText e_size, e_density, e_comments, e_reference, e_wsa, e_salinity, e_seed_stocking, e_ph, e_pond_name, e_customer;
    private String stocking_date, recorded_date, emp_id, CustomerName;
    private Button btn_create_pond, btn_active, btn_inactive;
    private String active_status = "0";
    private ArrayList<CustomerList> customerList = new ArrayList<>();
    private ArrayList<String> customer_list_spinner = new ArrayList<>();
    private String customerID, customerNAME;
    private CustomerNameAdapter adapter;
    private LinearLayout layout_customer;
    private ImageView mImageBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pond_creation);


        context = this;
        settings = new ConfigurationSettings(context);
        emp_id = settings.getEmployee_ID();

        initialize();
        //new GetCustomerList().execute();
        processBundle();
        btn_create_pond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                String density = e_density.getText().toString();
                String comments = e_comments.getText().toString();
                String wsa = e_wsa.getText().toString();
                String size = e_size.getText().toString();
                String reference = e_reference.getText().toString();
                String salinity = e_salinity.getText().toString();
                String seed_stocking = e_seed_stocking.getText().toString();
                String ph = e_ph.getText().toString();
                String pond_id = "pond_" + e_pond_name.getText().toString().trim();
                SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                stocking_date = date_format.format(c.getTime());
                recorded_date = date_format.format(c.getTime());


                if (validate()) {

                    Log.e("CustomerId", "" + customerID);
                    //new CreatePOND_Details().execute(comments, size, density, reference, active_status, wsa, salinity, seed_stocking, ph, stocking_date, recorded_date, pond_id);
                    new CreatePOND_Details().execute(pond_id, comments, wsa, active_status);
                }
            }
        });

        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

       /* edit_customer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                customerID = customerList.get(position).getId();
                customerNAME = customerList.get(position).getFirst_name();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            boolean isSampling = bundle.getBoolean("IsSampling");

            if (isSampling) {
                CustomerName = bundle.getString("CustomerName");
                customerID = bundle.getString("CustomerId");
                e_customer.setVisibility(View.VISIBLE);
                e_customer.setText(CustomerName);
                layout_customer.setVisibility(View.GONE);
            } else {
                e_customer.setVisibility(View.GONE);
                layout_customer.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private String getPondID() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat key_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String timeStamp = key_format.format(calendar.getTime());
        String pondID = "pond_" + timeStamp;
        return pondID;
    }

    private void initialize() {
        layout_customer = findViewById(R.id.layout_customer);
        e_customer = findViewById(R.id.e_customer);
        mImageBack = findViewById(R.id.img_back);
        //edit_customer = findViewById(R.id.edit_customer);
        e_pond_name = findViewById(R.id.edit_pond_name);
        e_comments = findViewById(R.id.edit_comment);
        e_density = findViewById(R.id.edit_density);
        e_ph = findViewById(R.id.edit_ph);
        e_reference = findViewById(R.id.edit_pond_reference);
        e_salinity = findViewById(R.id.edit_salinity);
        e_seed_stocking = findViewById(R.id.edit_seed_stocking);
        e_size = findViewById(R.id.edit_pond_size);
        e_wsa = findViewById(R.id.edit_wsa);
        btn_create_pond = findViewById(R.id.btn_create_request);
        btn_active = findViewById(R.id.btn_active);
        btn_inactive = findViewById(R.id.btn_in_active);
        btn_inactive.setOnClickListener(this);
        btn_active.setOnClickListener(this);
    }

    private boolean validate() {
        boolean isValid = true;
       /* if (edit_customer.getText().toString().equals("")) {
            edit_customer.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            edit_customer.setError(null);
        }*/

        if (e_comments.getText().toString().equals("")) {
            e_comments.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            e_comments.setError(null);
        }

        if (e_pond_name.getText().toString().equals("")) {
            e_pond_name.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            e_pond_name.setError(null);
        }

        if (e_wsa.getText().toString().isEmpty()) {
            e_wsa.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            e_wsa.setError(null);
        }

       /* if (e_size.getText().toString().equals("")) {
            e_size.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            e_size.setError(null);
        }

        if (e_seed_stocking.getText().toString().equals("")) {
            e_seed_stocking.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            e_seed_stocking.setError(null);
        }

        if (e_salinity.getText().toString().equals("")) {
            e_salinity.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            e_salinity.setError(null);
        }

        if (e_ph.getText().toString().equals("")) {
            e_ph.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            e_ph.setError(null);
        }

          if (e_reference.getText().toString().equals("")) {
            e_reference.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            e_reference.setError(null);
        }
*/
//        if (e_density.getText().toString().equals("")) {
//            e_density.setError("Field cannot be left blank..!");
//            isValid = false;
//        } else {
//            e_density.setError(null);
//        }
        return isValid;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_active:
                active_status = "0";
                changeColor(btn_active);
                break;
            case R.id.btn_in_active:
                active_status = "1";
                changeColor(btn_inactive);
                break;
        }
    }

    private void changeColor(Button selected_btn) {
        btn_active.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_inactive.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_active.setTextColor(Color.parseColor("#4e66f5"));
        btn_inactive.setTextColor(Color.parseColor("#4e66f5"));
        selected_btn.setBackgroundResource(R.drawable.blue_button_rounded_edges);
        selected_btn.setTextColor(Color.parseColor("#ffffff"));
    }


    /* *
     * Get customer record - auto complete
     * */
    private void getCustomerName() {
        /*AppLog.write("CUSTOMER----","=>"+new Gson().toJson(customerList));
        adapter = new CustomerNameAdapter(context, R.layout.activity_customer_auto_text_row, R.id.textView_customerName, customerList);
        edit_customer.setThreshold(1);
        edit_customer.showDropDown();
        edit_customer.setAdapter(adapter);
        edit_customer.setTextColor(Color.BLACK);*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class CreatePOND_Details extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Creating Pond...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            return call.createPONDModified((App) getApplication(), emp_id, customerID, s[0], s[1], s[2], "0", s[3]);
        }

        @Override
        protected void onPostExecute(SuccessMessage response) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();

            Log.e("CreatePond", "" + response.toString());
            if (response != null) {
                if (response.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "POND Created..!", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                } else {
                    Toast.makeText(context, "Failed to create..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Failed to create..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetCustomerList extends AsyncTask<Void, Void, ArrayList<CustomerList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<CustomerList> doInBackground(Void... v) {
            ArrayList<CustomerList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getCustomerMasterList((App) getApplication(), settings.getEmployee_ID());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<CustomerList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            customerList.clear();
            customer_list_spinner.clear();
            for (CustomerList c_list : list) {
                customerList.add(c_list);
                customer_list_spinner.add(c_list.getFirst_name());
            }
            AppLog.write("CUSTOMER_RECORD--", "--" + new Gson().toJson(customerList));
            getCustomerName();
        }
    }
}
