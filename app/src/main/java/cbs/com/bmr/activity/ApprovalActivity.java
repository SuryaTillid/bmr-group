package cbs.com.bmr.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import cbs.com.bmr.R;
import cbs.com.bmr.adapter.MyPagerAdapter;
import cbs.com.bmr.fragment.LeaveApprovalFragment;
import cbs.com.bmr.fragment.TaskApprovalFragment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ApprovalActivity extends AppCompatActivity implements View.OnClickListener, TabLayout.OnTabSelectedListener {


    private Context mContext;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private MyPagerAdapter mPagerAdapter;
    private List<Fragment> mFragmentList;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval);
        initObjects();
        iniCallbacks();
        initTabs();
    }

    private void iniCallbacks() {
        mImageBack.setOnClickListener(this);
        mTabLayout.addOnTabSelectedListener(this);
    }

    private void initObjects() {
        mContext = this;
        mImageBack = findViewById(R.id.img_back);
        mTabLayout = findViewById(R.id.tabs);
        mViewPager = findViewById(R.id.viewpager);
        mFragmentList = new ArrayList<>();
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFragmentList);
    }

    private void initTabs() {
        mTabLayout.addTab(mTabLayout.newTab().setText("Task Approval"));
        mFragmentList.add(new TaskApprovalFragment());
        mTabLayout.addTab(mTabLayout.newTab().setText("Leave Approval"));
        mFragmentList.add(new LeaveApprovalFragment());
        mPagerAdapter.notifyDataSetChanged();
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        setTabsFont();
    }

    @Override
    public void onClick(View v) {
        if (v == mImageBack) {
            onBackPressed();
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setTabsFont() {
        ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(mContext.getAssets(), "SourceSansPro-Semibold.ttf"), Typeface.BOLD);
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(mContext, Home_Page_Activity.class);
        startActivity(i);
    }
}
