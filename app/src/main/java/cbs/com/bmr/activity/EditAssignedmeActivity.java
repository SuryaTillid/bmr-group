package cbs.com.bmr.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Calendar;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.Utilities.Activity;
import cbs.com.bmr.adapter.DepartmentAdapter;
import cbs.com.bmr.adapter.EmployeeAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.model.DepartmentList;
import cbs.com.bmr.model.EmployeeTrackingList;
import cbs.com.bmr.model.MyTodoList;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EditAssignedmeActivity extends AppCompatActivity implements View.OnClickListener {


    String statusname = "", priorityname = "", AssignedTo, AssignedType;
    String duedate;
    private Context mContext;
    private EditText mEditTaskName, mEditCreatedby, mEditDuedate, mEditcomments;
    private ImageView mImageBack, mImageUpload;
    private Spinner mSpinnerPriority, mSpinnerStatus, mSpinnerAssignedto;
    private LinearLayout mSpinnerLayout;
    private String Taskname, Createdby, Duedate, Priority, Status, mId, mAssignedto, Assignedtype, Empname;
    private String[] priority = {"Low", "Medium", "High"};
    private String[] status = {"Select Status", "Open", "In-Progress", "Completed", "Cancelled"};
    private Button mBtnSubmit;
    private RadioGroup radioAssignee;
    private RadioButton mRadioEmp, mRadioDep;
    private ConfigurationSettings configurationSettings;
    private int mYear, mMonth, mDay;

    private MyCustomDialog dialog;
    private ArrayList<EmployeeTrackingList> EployeeList = new ArrayList<>();
    private ArrayList<DepartmentList> departmentlist = new ArrayList<>();
    private ArrayList<String> StringEmployeeList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_assinedme);
        processBundle();
        initObjects();
        initCallbacks();
        initsetData();

    }

    private void initsetData() {
        mEditTaskName.setText(Taskname);
        mEditCreatedby.setText(configurationSettings.getUSER_NAME());
        mEditDuedate.setFocusable(false);
        mEditDuedate.setText(Duedate);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Taskname = bundle.getString("TaskName");
            Createdby = bundle.getString("CreatedBy");
            Duedate = bundle.getString("DueDate");
            Priority = bundle.getString("priority");
            Status = bundle.getString("status");
            mId = bundle.getString("mId");
            mAssignedto = bundle.getString("Assignedto");
            Assignedtype = bundle.getString("Assignedtype");
            Empname = bundle.getString("Empname");
        }
    }

    private void initCallbacks() {
        mBtnSubmit.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
        mEditDuedate.setOnClickListener(this);
    }

    private void initObjects() {
        mContext = this;
        configurationSettings = new ConfigurationSettings(mContext);
        radioAssignee = findViewById(R.id.radio_group);
        mRadioEmp = findViewById(R.id.radio_employee);
        mRadioDep = findViewById(R.id.radio_department);
        mSpinnerAssignedto = findViewById(R.id.spinner_assignedto);
        mSpinnerLayout = findViewById(R.id.spinner_layout1);
        mImageBack = findViewById(R.id.img_back);
        mEditTaskName = findViewById(R.id.edit_task);
        mEditCreatedby = findViewById(R.id.edit_createdby);
        mEditDuedate = findViewById(R.id.edit_duedate);
        mEditcomments = findViewById(R.id.edit_comments);
        mSpinnerPriority = findViewById(R.id.spinner_priority);
        mSpinnerStatus = findViewById(R.id.spinner_status);
        mBtnSubmit = findViewById(R.id.btn_submit);

        ArrayAdapter adapter = new ArrayAdapter<String>(mContext, R.layout.custom_spinner_item, R.id.custom_spinner_item, priority);
        mSpinnerPriority.setAdapter(adapter);
        ArrayAdapter adapter1 = new ArrayAdapter<String>(mContext, R.layout.custom_spinner_item, R.id.custom_spinner_item, status);
        mSpinnerStatus.setAdapter(adapter1);

        Log.e("Empname", Empname);
        if (Assignedtype.equals("1")) {
            AssignedType = "1";
            mRadioEmp.setChecked(true);
            getEployeee((App) getApplication(), configurationSettings.getEmployee_ID());

            mSpinnerAssignedto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    AssignedTo = EployeeList.get(position).getId();
                }
                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        } else if (Assignedtype.equals("2")) {
            AssignedType = "2";
            mRadioDep.setChecked(true);
            getDepartment((App) getApplication());
            mSpinnerAssignedto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    AssignedTo = departmentlist.get(position).getId();

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
        if (Priority.equals("0")) {
            mSpinnerPriority.setSelection(0);
        } else if (Priority.equals("1")) {
            mSpinnerPriority.setSelection(1);
        } else if (Priority.equals("2")) {
            mSpinnerPriority.setSelection(2);
        }

        if (Status.equals("0")) {
            mSpinnerStatus.setSelection(1);
        } else if (Status.equals("1")) {
            mSpinnerStatus.setSelection(2);
        } else if (Status.equals("2")) {
            mSpinnerStatus.setSelection(3);
        } else if (Status.equals("3")) {
            mSpinnerStatus.setSelection(4);
        }
        mSpinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mSpinnerStatus.getSelectedItem().toString().equals("Open")) {
                    statusname = "0";
                } else if (mSpinnerStatus.getSelectedItem().toString().equals("In-Progress")) {
                    statusname = "1";
                } else if (mSpinnerStatus.getSelectedItem().toString().equals("Completed")) {
                    statusname = "2";
                } else if (mSpinnerStatus.getSelectedItem().toString().equals("Cancelled")) {
                    statusname = "3";
                } else {
                    statusname = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mSpinnerPriority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mSpinnerPriority.getSelectedItem().toString().equals("Low")) {
                    priorityname = "0";
                } else if (mSpinnerPriority.getSelectedItem().toString().equals("Medium")) {
                    priorityname = "1";
                } else if (mSpinnerPriority.getSelectedItem().toString().equals("High")) {
                    priorityname = "2";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        radioAssignee.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_employee:
                        Assignedtype = "1";
                        getEployeee((App) getApplication(), configurationSettings.getEmployee_ID());
                        mSpinnerAssignedto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                                AssignedTo = EployeeList.get(position).getId();
                                Log.e("AssignedTo", "" + AssignedTo);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                        break;
                    case R.id.radio_department:
                        Assignedtype = "2";
                        getDepartment((App) getApplication());
                        mSpinnerAssignedto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                                AssignedTo = departmentlist.get(position).getId();

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                        break;
                }
            }
        });
    }


    private void selectValue(Spinner spinner, String value) {
        for (int i = 0; i < spinner.getAdapter().getCount(); i++) {
            if (spinner.getAdapter().getItem(i).toString().equals(value)) {
                spinner.setSelection(i);
                break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mImageBack) {
            onBackPressed();
        } else if (v == mBtnSubmit) {
            processSubmit();
        } else if (v == mEditDuedate) {
            mDatePicker();
        }
    }

    private void processSubmit() {
        String Taskname = mEditTaskName.getText().toString().trim();
        String duedate = mEditDuedate.getText().toString().trim();
        String comments = mEditcomments.getText().toString().trim();

        if (Validate(Taskname)) {
            UpdateList((App) getApplication(), mId, Taskname, statusname, priorityname, duedate, comments,
                    AssignedTo, AssignedType, configurationSettings.getEmployee_ID());

        }
    }


    private boolean Validate(String taskname) {
        if (TextUtils.isEmpty(taskname)) {
            mEditTaskName.setError("Field Cannot be Blank..");
            return false;
        }
        return true;
    }

    private void UpdateList(App app, String mId, String taskname, String statusname, String priorityname, String duedate,
                            String comments, String assignedTo, String assignedType, String employee_id) {
        dialog = new MyCustomDialog(this, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<MyTodoList> call = apiService.UpdateAssignedMeList(mId, taskname, statusname, priorityname, duedate, comments, assignedTo, assignedType, employee_id);
        call.enqueue(new Callback<MyTodoList>() {
            @Override
            public void onResponse(@NonNull Call<MyTodoList> call, @NonNull Response<MyTodoList> response) {
                MyTodoList samplingCycle = response.body();
                Log.e("sdfdfsf", "" + response.toString());
                if (response.isSuccessful() && samplingCycle != null) {
                    dialog.dismiss();
                    Log.e("dsf", "" + samplingCycle.getSuccess());
                    if (samplingCycle.getSuccess().equals("1")) {
                        Bundle bundle23 = new Bundle();
                        bundle23.putString("priorityname", "");
                        bundle23.putString("statusname", "");
                        bundle23.putString("assintype", "");
                        bundle23.putString("listtype", "1");
                        Activity.launchClearTopWithBundle(mContext, TodoListActivity.class, bundle23);
                        Toast.makeText(EditAssignedmeActivity.this, "Update Successfully", Toast.LENGTH_SHORT).show();

                    } else {
                        Bundle bundle23 = new Bundle();
                        bundle23.putString("priorityname", "");
                        bundle23.putString("statusname", "");
                        bundle23.putString("assintype", "");
                        bundle23.putString("listtype", "1");
                        Activity.launchClearTopWithBundle(mContext, TodoListActivity.class, bundle23);
                        Toast.makeText(EditAssignedmeActivity.this, "Update Successfully", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.dismiss();

                }
            }

            @Override
            public void onFailure(@NonNull Call<MyTodoList> call, @NonNull Throwable t) {
                Log.e("dsfdfsf", "" + t.toString());
                dialog.dismiss();
            }
        });

    }


    public void getDepartment(App app) {
        RestApi apiService = app.createRestAdaptor();
        Observable<Response<ArrayList<DepartmentList>>> testObservable1 = apiService.getDepartment1();
        testObservable1
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ArrayList<DepartmentList>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ArrayList<DepartmentList>> arrayListResponse) {
                        Log.e("arrayListResponse", "" + arrayListResponse.toString());
                        ArrayList<DepartmentList> arrayList = arrayListResponse.body();
                        if (arrayList != null) {
                            if (arrayList.size() > 0) {
                                departmentlist = arrayList;
                            }
                        }
                    }
                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        DepartmentAdapter adapter = new DepartmentAdapter(mContext, departmentlist);
                        mSpinnerAssignedto.setAdapter(adapter);
                        selectValue(mSpinnerAssignedto, Empname);
                    }
                });
    }
    public void getEployeee(App app, String LoginId) {
        RestApi apiService = app.createRestAdaptor();
        Call<ArrayList<EmployeeTrackingList>> call = apiService.getEmployeeList(LoginId);
        call.enqueue(new Callback<ArrayList<EmployeeTrackingList>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<EmployeeTrackingList>> call, @NonNull Response<ArrayList<EmployeeTrackingList>> response) {
                ArrayList<EmployeeTrackingList> mEmployeeList = response.body();
                Log.e("employeelist", "--" + mEmployeeList.size());
                if (response.isSuccessful() && mEmployeeList != null) {
                    if (mEmployeeList.size() > 0) {
                        EployeeList = mEmployeeList;
                        StringEmployeeList.clear();
                        for (EmployeeTrackingList employeeTrackingList : EployeeList) {
                            StringEmployeeList.add(employeeTrackingList.getFirstname() + employeeTrackingList.getLastname());
                        }
                        EmployeeAdapter adapter = new EmployeeAdapter(mContext, EployeeList);
                        mSpinnerAssignedto.setAdapter(adapter);
                        selectValue(mSpinnerAssignedto, Empname);
                    }
                }
            }
            @Override
            public void onFailure(@NonNull Call<ArrayList<EmployeeTrackingList>> call, @NonNull Throwable t) {
                Log.e("failure", "--" + t.toString());
            }
        });
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    private void mDatePicker() {
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        if (dayOfMonth > 9 || monthOfYear > 9) {
                            duedate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            mEditDuedate.setText(duedate);
                        } else {
                            duedate = "0" + dayOfMonth + "/" + "0" + (monthOfYear + 1) + "/" + year;
                            mEditDuedate.setText(duedate);
                        }

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

}
