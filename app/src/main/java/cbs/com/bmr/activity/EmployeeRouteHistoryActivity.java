package cbs.com.bmr.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.model.EmployeeRouteListHistory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EmployeeRouteHistoryActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    private String id, date, empName;
    private MyCustomDialog dialog;
    private ArrayList<LatLng> addPoints;
    private LatLng start, waypoint;
    private LatLng end;
    private EditText ed_datePicker;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private TextView mTextName;
    private int mYear, mMonth, mDay;

    private List<Polyline> polylines = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_route_history);
        processBundle();
        initObjects();
        initCallback();
        initMap();
    }

    private void initCallback() {
        ed_datePicker.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initObjects() {
        mTextName = findViewById(R.id.txt_empname);
        ed_datePicker = findViewById(R.id.txt_datePicker);

    }

    private void processBundle() {
        Intent intent = getIntent();
        id = intent.getStringExtra("EmpId");
        empName = intent.getStringExtra("EmpNameList");
        Log.e("Route", "" + empName);
        //
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        date = dateFormat.format(Calendar.getInstance().getTime());
        // ed_datePicker.setText(date);
    }

    private void initMap() {
        if (mMap == null) {
            FragmentManager fm = getSupportFragmentManager();
            mapFragment = ((SupportMapFragment) fm.findFragmentById(R.id.map));
            mapFragment.getMapAsync(EmployeeRouteHistoryActivity.this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getEmployeeLocation((App) getApplication(), id, date);



    }

    public void getEmployeeLocation(App app, String empId, String date) {
        dialog = new MyCustomDialog(this, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<ArrayList<EmployeeRouteListHistory>> call = apiService.getEmployeeRouteHistory(empId, date);
        call.enqueue(new Callback<ArrayList<EmployeeRouteListHistory>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<EmployeeRouteListHistory>> call, @NonNull Response<ArrayList<EmployeeRouteListHistory>> response) {
                Log.e("Route", "" + response.toString());

                ArrayList<EmployeeRouteListHistory> mEmployeeList = response.body();
                if (response.isSuccessful() && mEmployeeList != null) {

                    if (mEmployeeList.size() > 0) {
                        for (int i = 0; i < mEmployeeList.size(); i++) {
                            initgetAddress(mEmployeeList.get(i).getGps(), mEmployeeList.get(i).getLoginTime(), mEmployeeList.get(i).getBattery());
                            Log.e("LogInTime", "" + mEmployeeList.get(i).getLoginTime());
                            dialog.dismiss();
                            mMap.setPadding(0, 0, 0, 0);
                            mMap.getUiSettings().setZoomControlsEnabled(true);
                            mMap.getUiSettings().setMapToolbarEnabled(true);
                            mMap.getUiSettings().setCompassEnabled(true);

                            mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                                public void onMapLoaded() {
                                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(start).zoom(10).build()));
                                }
                            });
                            CameraUpdate zoom = CameraUpdateFactory.zoomTo(18);

                            mMap.moveCamera(zoom);
                        }
                    } else {
                        onBackPressed();
                        dialog.dismiss();
                        Toast.makeText(EmployeeRouteHistoryActivity.this, "No Route History Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<EmployeeRouteListHistory>> call, @NonNull Throwable t) {
                Log.e("failure", "" + t.toString());
                dialog.dismiss();
            }
        });
    }

   /* public void getEmployeeLocation(App app, String empId, String date) {
        dialog = new MyCustomDialog(this, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<ArrayList<EmployeeRouteListHistory>> call = apiService.getEmployeeRouteHistory(empId, date);
        call.enqueue(new Callback<ArrayList<EmployeeRouteListHistory>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<EmployeeRouteListHistory>> call, @NonNull Response<ArrayList<EmployeeRouteListHistory>> response) {
                Log.e("Route", "" + response.toString());

                ArrayList<EmployeeRouteListHistory> mEmployeeList = response.body();
                if (response.isSuccessful() && mEmployeeList != null) {

                    if (mEmployeeList.size() > 0) {
                        for (int i = 0; i < mEmployeeList.size(); i++) {
                            initgetAddress(mEmployeeList.get(i).getOrigin(), mEmployeeList.get(i).getDestination(), mEmployeeList.get(i).getCheckins());
                            dialog.dismiss();
                        }
                    } else {
                        onBackPressed();
                        dialog.dismiss();
                        Toast.makeText(EmployeeRouteHistoryActivity.this, "No Route History Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<EmployeeRouteListHistory>> call, @NonNull Throwable t) {
                Log.e("failure", "" + t.toString());
                dialog.dismiss();
            }
        });
    }*/

    MarkerOptions options = new MarkerOptions();


    /* @Override
     public void onRoutingFailure(RouteException e) {

         Log.e("RouteException", e.toString());
     }

     @Override
     public void onRoutingStart() {

     }*/
    private void initgetAddress(String gps, String loginTime, String batteryLevel) {


        String test = gps;
        test = test.replaceAll("[\\\\[\\\\]\\\":(){}]", "");
        String[] values = test.split(",");
        String latitude = values[0].replaceAll("lat", "");
        String longitude = values[1].replaceAll("lon", "");
        double lat = Double.parseDouble(latitude);
        double lan = Double.parseDouble(longitude);
        createMarker(lat, lan, loginTime, batteryLevel);

        start = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));

        mTextName.setText(empName);



        /*Routing routing = new Routing.Builder().travelMode(Routing.TravelMode.DRIVING)
                .withListener(this).waypoints(start, waypoint, waypoint).key("AIzaSyAnVFmsKEPZrDmHMPpE-2efO13f6UFOmns").build();
        routing.execute();*/
    }

    protected Marker createMarker(Double latitude, Double longitude, String loginTime, String batteryLevel) {
        int height = 100;
        int width = 100;
        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.route_start);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        return mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title("Time:" + loginTime)
                .snippet("BatteryLevel :" + batteryLevel)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.route_start)));
        // mMap.setPadding(0, 0, 0, 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_datePicker: {
                mDatePicker();
                break;
            }


        }
    }

    private void mDatePicker() {
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                if (dayOfMonth > 9 || month > 9) {
                    date = year + "-" + (month + 1) + "-" + dayOfMonth;
                    ed_datePicker.setText(date);
                    getEmployeeLocation((App) getApplication(), id, date);
                } else {
                    date = year + "-" + "0" + (month + 1) + "0" + dayOfMonth;
                    ed_datePicker.setText(date);
                    getEmployeeLocation((App) getApplication(), id, date);
                }
            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

  /*  private void initgetAddress(EmployeeRouteListHistory.OriginS originS, EmployeeRouteListHistory.Destination destination,
                                ArrayList<EmployeeRouteListHistory.CheckinList> Checkins) {


        if (!originS.getGeo_in().isEmpty()) {
            String test = originS.getGeo_in();
            test = test.replaceAll("[\\\\[\\\\]\\\":(){}]", "");
            String[] values = test.split(",");
            String latitude = values[0].replaceAll("lat", "");
            String longitude = values[1].replaceAll("lon", "");
            start = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        }

        if (!destination.getGeo_in().isEmpty()) {
            String test = destination.getGeo_in();
            test = test.replaceAll("[\\\\[\\\\]\\\":(){}]", "");
            String[] values = test.split(",");
            String latitude = values[0].replaceAll("lat", "");
            String longitude = values[1].replaceAll("lon", "");
            end = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
            if (destination.getEmp_name() != null) {
                mTextName.setText(destination.getEmp_name());
            }
        }

        if (Checkins.size() > 0) {
            Log.e("Checkins", "" + Checkins.size());
            for (EmployeeRouteListHistory.CheckinList checkinList : Checkins) {
                String test = checkinList.getGeo_in();
                test = test.replaceAll("[\\\\[\\\\]\\\":(){}]", "");
                String[] values = test.split(",");
                String latitude = values[0].replaceAll("lat", "");
                String longitude = values[1].replaceAll("lon", "");
                waypoint = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));


            }

        } else {
            waypoint = new LatLng(end.latitude, end.longitude);

        }


        Routing routing = new Routing.Builder().travelMode(Routing.TravelMode.DRIVING)
                .withListener(this).waypoints(start, waypoint, waypoint).key("AIzaSyAnVFmsKEPZrDmHMPpE-2efO13f6UFOmns").build();
        routing.execute();
    }*/

/*    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int i) {
        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();
        for (int k = 0; k < route.size(); k++) {

            Log.e("RouteException", "" + route.get(k).getPoints());

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(R.color.green));
            polyOptions.width(10 + k * 3);
            polyOptions.addAll(route.get(k).getPoints());
            Polyline polyline = mMap.addPolyline(polyOptions);
            polylines.add(polyline);
            options = new MarkerOptions();
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.route_start));
            options.position(waypoint);
            mMap.addMarker(options);
            // Toast.makeText(getApplicationContext(), "Route " + (i + 1) + ": distance - " + route.get(i).getDistanceValue() + ": duration - " + route.get(i).getDurationValue(), Toast.LENGTH_SHORT).show();
        }

        // Start marker
        options = new MarkerOptions();
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.route_start));
        options.position(start);

        mMap.addMarker(options);

        // End marker
        options = new MarkerOptions();
        options.position(end);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.route_start));

        mMap.addMarker(options);
        mMap.setPadding(0, 0, 0, 0);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            public void onMapLoaded() {
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(start).zoom(20).build()));
            }
        });
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(18);

        mMap.moveCamera(zoom);
    }

    @Override
    public void onRoutingCancelled() {
        Log.e("fsd", "fsdfdsfs");
    }*/
}
