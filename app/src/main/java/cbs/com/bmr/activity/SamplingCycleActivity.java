package cbs.com.bmr.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.model.SamplingCycle;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static cbs.com.bmr.Utilities.Utils.getCurrentDateNew;
import static cbs.com.bmr.Utilities.Utils.getCurrentDateTime;
import static cbs.com.bmr.Utilities.Utils.getDocumentsDate;
import static cbs.com.bmr.Utilities.Utils.getPreviousSamplindate;
import static cbs.com.bmr.Utilities.Utils.getSamplindate;

public class SamplingCycleActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_STORAGE_PERMISSION = 4;
    private static final int REQUEST_FEED_IMAGE = 4;
    Date Stockingdate1 = new Date();
    Date Samplingdate2 = new Date();
    String PreviousSamplingdate;
    MultipartBody.Part imageFileBody;
    private Context mContext;
    private String PondId, customerID;
    private MyCustomDialog dialog;
    private int mYear, mMonth, mDay;
    private String Docdate, CycleId, TaskId;
    private Button mBtnSubmit;
    private boolean IsNewCycle;
    private ImageView mImageBack;
    private ConfigurationSettings configurationSettings;
    private TextInputEditText mEditStockingDate, mEditSeeds, mEditPreviousSamplingDate, EditSamplingDate, mEditDailyFeed, mEditABW, mEditPh, mEditSalinity,
            mEditDoc, mEditADG;
    private TextView mTextCameraUpload, mTextImageRemove;
    private ImageView mImagePhotoView;
    private String mImagePath;
    private File file;
    private TextInputLayout lay_previousSampling;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sampling_cycle);
        intObjects();
        processBundle();
        initCallbacks();

    }

    private void initCallbacks() {
        mEditStockingDate.setOnClickListener(this);
        EditSamplingDate.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
        mTextCameraUpload.setOnClickListener(this);
        mTextImageRemove.setOnClickListener(this);
    }

    private void intObjects() {
        mContext = this;
        configurationSettings = new ConfigurationSettings(mContext);
        mImageBack = findViewById(R.id.img_back);
        mEditPreviousSamplingDate = findViewById(R.id.edit_previoussamplingdate);
        mEditStockingDate = findViewById(R.id.edit_stockingdate);
        mEditSeeds = findViewById(R.id.edit_SeedIn);
        EditSamplingDate = findViewById(R.id.editsamplingdate);
        mEditDailyFeed = findViewById(R.id.edit_dailyfeed);
        mEditABW = findViewById(R.id.edit_abw);
        mEditADG = findViewById(R.id.edit_adg);
        mEditPh = findViewById(R.id.edit_ph);
        mEditSalinity = findViewById(R.id.edit_salinity);
        lay_previousSampling = findViewById(R.id.lay_previousSampling);
        mEditDoc = findViewById(R.id.editdoc);
        mBtnSubmit = findViewById(R.id.btn_submit);
        mTextCameraUpload = findViewById(R.id.btn_camera);
        mTextImageRemove = findViewById(R.id.btn_remove);
        mImagePhotoView = findViewById(R.id.img_photo_view);

        initForumulaCalculations();

    }

    private void initForumulaCalculations() {
        mEditABW.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mEditABW.setError(null);
                if (s.length() > 0) {
                    initADGFormula();
                }
            }
        });
        mEditDoc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mEditDoc.setError(null);
                if (s.length() > 0 && !Objects.requireNonNull(mEditStockingDate.getText()).toString().trim().isEmpty()
                        && !mEditABW.getText().toString().trim().isEmpty()) {
                    initADGFormula();
                }
            }


        });

        EditSamplingDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                EditSamplingDate.setError(null);
                if (s.length() > 0) {
                    StockingDateValidation();
                    initDOCFormula();

                }
                if (s.length() > 0 && !Objects.requireNonNull(mEditStockingDate.getText()).toString().trim().isEmpty()) {
                    StockingDateValidation();
                    initDOCFormula();

                } else if (s.length() > 0 && !Objects.requireNonNull(mEditStockingDate.getText()).toString().trim().isEmpty()
                        && !mEditDoc.getText().toString().trim().isEmpty()
                        && !mEditABW.getText().toString().trim().isEmpty()) {
                    initADGFormula();
                }

            }
        });
        mEditStockingDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mEditStockingDate.setError(null);
                if (s.length() > 0 && !Objects.requireNonNull(EditSamplingDate.getText()).toString().trim().isEmpty()) {
                    StockingDateValidation();
                    initDOCFormula();

                } else if (s.length() > 0 && !EditSamplingDate.getText().toString().trim().isEmpty()
                        && !mEditDoc.getText().toString().trim().isEmpty()
                        && !mEditABW.getText().toString().trim().isEmpty()) {
                    initADGFormula();
                }
            }
        });

    }

    private void initDOCFormula() {
        if (Objects.requireNonNull(mEditStockingDate.getText()).toString().trim().isEmpty()) {
            mEditStockingDate.requestFocus();
            mEditStockingDate.setError("Field Cannot be Empty");

        } else if (Objects.requireNonNull(EditSamplingDate.getText()).toString().trim().isEmpty()) {
            EditSamplingDate.requestFocus();
            EditSamplingDate.setError("Field Cannot be Empty");

        } else {
            String StockingDate = Objects.requireNonNull(mEditStockingDate.getText()).toString().trim();
            String Samplindate = Objects.requireNonNull(EditSamplingDate.getText()).toString().trim();
            SimpleDateFormat dates = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            try {
                Stockingdate1 = dates.parse(StockingDate);
                Samplingdate2 = dates.parse(Samplindate);
                long diff = Samplingdate2.getTime() - Stockingdate1.getTime();
                int numOfDays = (int) (diff / (1000 * 60 * 60 * 24));
                mEditDoc.setText(String.valueOf(numOfDays + 1));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private void StockingDateValidation() {

        if (Objects.requireNonNull(mEditStockingDate.getText()).toString().trim().isEmpty()) {
            mEditStockingDate.requestFocus();
            mEditStockingDate.setError("Please enter Valid Date");

        } else if (Objects.requireNonNull(EditSamplingDate.getText()).toString().trim().isEmpty()) {
            EditSamplingDate.requestFocus();
            EditSamplingDate.setError("Please enter Valid Date");

        } else {
            String StockingDate = Objects.requireNonNull(mEditStockingDate.getText()).toString().trim();
            String Samplindate = Objects.requireNonNull(EditSamplingDate.getText()).toString().trim();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

            try {
                Date stardate = sdf.parse(StockingDate);
                Date enddate = sdf.parse(Samplindate);
                if (enddate.before(stardate)) {
                    EditSamplingDate.requestFocus();
                    EditSamplingDate.setError("Sampling Date not Lesser than Stocking Date");
                } else if (stardate.after(enddate)) {
                    mEditStockingDate.requestFocus();
                    mEditStockingDate.setError("Stocking Date not greater than Sampling Date");

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            PondId = bundle.getString("PondId");
            customerID = bundle.getString("customerID");
            TaskId = bundle.getString("TaskId");
            Log.e("pondid", PondId);

            getCycle((App) getApplication(), PondId, customerID);
        }
    }

    public void getCycle(App app, String pondid, String customerid) {
        dialog = new MyCustomDialog(this, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<ArrayList<SamplingCycle>> call = apiService.getsamplingCycle(pondid, customerid);
        call.enqueue(new Callback<ArrayList<SamplingCycle>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<SamplingCycle>> call, @NonNull Response<ArrayList<SamplingCycle>> response) {
                ArrayList<SamplingCycle> samplingCycleArrayList = response.body();
                if (response.isSuccessful() && samplingCycleArrayList != null) {
                    if (samplingCycleArrayList.size() > 0) {
                        dialog.dismiss();
                        for (SamplingCycle samplingCycle : samplingCycleArrayList) {
                            if (samplingCycle.getSuccess().equals("1")) {
                                IsNewCycle = true;
                                CycleId = "";
                                mEditStockingDate.setFocusable(true);
                                mEditStockingDate.setClickable(true);
                                mEditStockingDate.setBackgroundResource(R.drawable.edit_background_noneditable);
                                mEditSeeds.setBackgroundResource(R.drawable.edit_background_noneditable);
                                mEditSeeds.setFocusable(true);
                            } else if (samplingCycle.getSuccess().equals("2")) {
                                IsNewCycle = false;
                                CycleId = samplingCycle.getCycle_id();
                                mEditStockingDate.setFocusable(false);
                                mEditStockingDate.setClickable(false);
                                mEditSeeds.setFocusable(false);
                                if (samplingCycle.getSeed_stocking() != null) {
                                    mEditSeeds.setText(samplingCycle.getSeed_stocking());
                                }

                                Log.e("getPrevious_sampling", samplingCycle.getPrevious_sampling());
                                if (samplingCycle.getStocking_date() != null) {
                                    mEditStockingDate.setText(getSamplindate(samplingCycle.getStocking_date()));
                                }
                                if (samplingCycle.getPrevious_sampling() != null) {
                                    lay_previousSampling.setVisibility(View.VISIBLE);
                                    mEditPreviousSamplingDate.setText(getPreviousSamplindate(samplingCycle.getPrevious_sampling()));
                                } else {
                                    lay_previousSampling.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                } else {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<SamplingCycle>> call, @NonNull Throwable t) {
                dialog.dismiss();
            }
        });
    }

    private void initADGFormula() {
        if (Objects.requireNonNull(mEditDoc.getText()).toString().trim().isEmpty()) {
            mEditDoc.requestFocus();
            Toast.makeText(mContext, "Field Cannot be Empty", Toast.LENGTH_SHORT).show();
        } else if (Objects.requireNonNull(mEditABW.getText()).toString().trim().isEmpty()) {
            mEditABW.requestFocus();
            Toast.makeText(mContext, "Field Cannot be Empty", Toast.LENGTH_SHORT).show();

        } else {
            String ABWValue = Objects.requireNonNull(mEditABW.getText()).toString().trim();
            String DOC = Objects.requireNonNull(mEditDoc.getText()).toString().trim();


            double abw = Double.parseDouble(ABWValue);
            double doc = Double.parseDouble(DOC);

            if (doc > 0 && abw > 0) {
                Double ADGValue = abw / doc;
                mEditADG.setText(String.format("%.2f", ADGValue));
            } else {
                mEditADG.setText("0");
            }

        }
    }

    private boolean Validate(EditText EditSamplingDate, EditText mEditDoc, EditText mEditDailyfeed, EditText mEditABW, EditText mEditPh, EditText mEditSalinity) {

        if (Objects.requireNonNull(EditSamplingDate.getText()).toString().trim().isEmpty()) {
            EditSamplingDate.requestFocus();
            EditSamplingDate.setError("Field cannot be blank");
            return false;
        } else if (Objects.requireNonNull(mEditDoc.getText()).toString().trim().isEmpty()) {
            mEditDoc.requestFocus();
            mEditDoc.setError("Field cannot be blank");
            return false;
        } else if (Objects.requireNonNull(mEditDailyfeed.getText()).toString().trim().isEmpty()) {
            mEditDailyfeed.requestFocus();
            mEditDailyfeed.setError("Field cannot be blank");
            return false;

        } else if (Objects.requireNonNull(mEditABW.getText()).toString().trim().isEmpty()) {
            mEditABW.requestFocus();
            mEditABW.setError("Field cannot be blank");
            return false;

        } else if (Objects.requireNonNull(mEditPh.getText()).toString().trim().isEmpty()) {
            mEditPh.requestFocus();
            mEditPh.setError("Field cannot be blank");
            return false;
        } else if (Objects.requireNonNull(mEditSalinity.getText()).toString().trim().isEmpty()) {
            mEditSalinity.requestFocus();
            mEditSalinity.setError("Field cannot be blank");
            return false;
        }

        return true;
    }

    private void processPickImage() {
        if (hasStoragePermission()) {
            pickImage();
        } else {
            requestStoragePermission(REQUEST_STORAGE_PERMISSION);
        }
    }

    private void pickImage() {
        EasyImage.configuration(mContext).setImagesFolderName(getString(R.string.app_name));
        EasyImage.openChooserWithGallery(this, "Select Image", REQUEST_FEED_IMAGE);
    }

    private boolean hasStoragePermission() {
        return ContextCompat.checkSelfPermission(mContext,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestStoragePermission(int permission) {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, permission);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles,
                                       EasyImage.ImageSource source, int type) {
                mImagePath = imageFiles.get(0).getPath();

                Bitmap bitmap = BitmapFactory.decodeFile(mImagePath);
                getResizedBitmap(bitmap, 480, 640);
                Uri tempUri = getImageUri(getApplicationContext(), bitmap);
                Log.e("imagepathdataprofile", "" + tempUri.toString());
                file = new File(getRealPathFromURI(tempUri));

                Log.e("file", "" + file.getName());
                displayImage();

            }
        });
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] imageInByte = bytes.toByteArray();
        long lengthbmp = imageInByte.length;
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
    }

    private void displayImage() {
        mImagePhotoView.setVisibility(View.VISIBLE);
        mTextImageRemove.setVisibility(View.VISIBLE);
        Glide.with(mContext).load(mImagePath).into(mImagePhotoView);
    }

    private void processRemove() {
        mImagePath = null;
        mImagePhotoView.setVisibility(View.GONE);
        mTextImageRemove.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        if (v == mEditStockingDate) {
            if (IsNewCycle) {
                mDatePicker();
            }
        } else if (v == EditSamplingDate) {
            mDatePicker1();
        } else if (v == mBtnSubmit) {
            if (Validate(EditSamplingDate, mEditDoc, mEditDailyFeed, mEditABW, mEditPh, mEditSalinity)) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                String StockingDate = Objects.requireNonNull(mEditStockingDate.getText()).toString().trim();
                String Samplindate = Objects.requireNonNull(EditSamplingDate.getText()).toString().trim();

                Date stardate = null;
                try {
                    stardate = sdf.parse(StockingDate);
                    Date enddate = sdf.parse(Samplindate);
                    Date currentdate = sdf.parse(getCurrentDateNew());
                    if (enddate.before(stardate)) {
                        EditSamplingDate.requestFocus();
                        EditSamplingDate.setError("Sampling Date not Lesser than Stocking Date");
                    } else if (stardate.after(enddate)) {
                        mEditStockingDate.requestFocus();
                        mEditStockingDate.setError("Stocking Date not greater than Sampling Date");
                    } else if (stardate.after(currentdate)) {
                        mEditStockingDate.requestFocus();
                        mEditStockingDate.setError("Stocking Date not greater than Current Date");
                    } else if (enddate.after(currentdate)) {
                        EditSamplingDate.requestFocus();
                        EditSamplingDate.setError("Sampling Date  not greater than Current Date");
                    } else {
                        processCreate();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }


            }

        } else if (v == mImageBack) {
            onBackPressed();
        } else if (v == mTextCameraUpload) {
            processPickImage();
        } else if (v == mTextImageRemove) {
            processRemove();
        }
    }

    private void processCreate() {
        String stockingdate = Objects.requireNonNull(mEditStockingDate.getText()).toString().trim();
        String mSeedsIn = Objects.requireNonNull(mEditSeeds.getText()).toString().trim();
        String samplingDate = Objects.requireNonNull(EditSamplingDate.getText()).toString().trim();
        String dailyfeed = Objects.requireNonNull(mEditDailyFeed.getText()).toString().trim();
        String ABW = Objects.requireNonNull(mEditABW.getText()).toString().trim();
        String Ph = Objects.requireNonNull(mEditPh.getText()).toString().trim();
        String Salinity = Objects.requireNonNull(mEditSalinity.getText()).toString().trim();
        String DOC = Objects.requireNonNull(mEditDoc.getText()).toString().trim();
        String ADG = Objects.requireNonNull(mEditADG.getText()).toString().trim();

        if (IsNewCycle) {
            CreateNewCycle((App) getApplication(), TaskId, PondId, customerID, CycleId, configurationSettings.getEmployee_ID(),
                    "1",
                    getCurrentDateTime(),
                    DOC,
                    getDocumentsDate(stockingdate), mSeedsIn, Ph, ABW, Salinity,
                    getDocumentsDate(samplingDate), dailyfeed,
                    ADG);
        } else {

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            String StockingDate = Objects.requireNonNull(mEditStockingDate.getText()).toString().trim();
            String Samplingdate = Objects.requireNonNull(EditSamplingDate.getText()).toString().trim();
            try {
                if (!mEditPreviousSamplingDate.getText().toString().trim().isEmpty()) {
                    PreviousSamplingdate = Objects.requireNonNull(mEditPreviousSamplingDate.getText()).toString().trim();
                }
                Date stardate = sdf.parse(StockingDate);
                Date enddate = sdf.parse(Samplingdate);
                Date previousdate = sdf.parse(PreviousSamplingdate);
                if (enddate.before(stardate)) {
                    EditSamplingDate.requestFocus();
                    Toast.makeText(mContext, "Sampling date not lesser than stocking date", Toast.LENGTH_SHORT).show();
                } else if (stardate.after(enddate)) {
                    mEditStockingDate.requestFocus();
                    Toast.makeText(mContext, "Stocking Date not greater than sampling date", Toast.LENGTH_SHORT).show();
                } else if (enddate.before(previousdate)) {
                    mEditPreviousSamplingDate.requestFocus();
                    Toast.makeText(mContext, "Sampling date not lesser than previous sampling date", Toast.LENGTH_SHORT).show();
                } else {
                    CreateSampling((App) getApplication(), TaskId, PondId, customerID, CycleId,
                            configurationSettings.getEmployee_ID(), Ph, ABW, Salinity, getDocumentsDate(samplingDate), dailyfeed, DOC,
                            ADG);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private void CreateNewCycle(App app, String taskId, String pondId, String customerID, String cycleid, String employee_id, String newcycle,
                                String cultureseeddate,
                                String doc1, String stockingdate, String mSeedsIn, String ph, String abw, String salinity, String samplingDate,
                                String dailyfeed, String ADG) {
        Log.e("stockingdate", "" + stockingdate);
        dialog = new MyCustomDialog(this, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        RequestBody taskId1 = RequestBody.create(MediaType.parse("text/plain"), taskId);
        RequestBody pondId1 = RequestBody.create(MediaType.parse("text/plain"), pondId);
        RequestBody customerID1 = RequestBody.create(MediaType.parse("text/plain"), customerID);
        RequestBody cycleid1 = RequestBody.create(MediaType.parse("text/plain"), cycleid);
        RequestBody employee_id1 = RequestBody.create(MediaType.parse("text/plain"), employee_id);
        RequestBody ph1 = RequestBody.create(MediaType.parse("text/plain"), ph);
        RequestBody abw1 = RequestBody.create(MediaType.parse("text/plain"), abw);
        RequestBody salinity1 = RequestBody.create(MediaType.parse("text/plain"), salinity);
        RequestBody samplingDate1 = RequestBody.create(MediaType.parse("text/plain"), samplingDate);
        RequestBody dailyfeed1 = RequestBody.create(MediaType.parse("text/plain"), dailyfeed);
        RequestBody stockingdate1 = RequestBody.create(MediaType.parse("text/plain"), stockingdate);
        RequestBody mSeedsIn1 = RequestBody.create(MediaType.parse("text/plain"), mSeedsIn);
        RequestBody cultureseeddate1 = RequestBody.create(MediaType.parse("text/plain"), cultureseeddate);
        RequestBody newcycle1 = RequestBody.create(MediaType.parse("text/plain"), newcycle);
        RequestBody doc11 = RequestBody.create(MediaType.parse("text/plain"), doc1);
        RequestBody ADG1 = RequestBody.create(MediaType.parse("text/plain"), ADG);
        if (mImagePath != null) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            imageFileBody = MultipartBody.Part.createFormData("sampling_file", file.getName(), requestBody);
        } else {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            imageFileBody = MultipartBody.Part.createFormData("sampling_file", "", requestBody);
        }
        Call<SamplingCycle> call = apiService.CreateNewsamplingCycle(taskId1, pondId1, customerID1, cycleid1, employee_id1, newcycle1,
                cultureseeddate1, doc11,
                stockingdate1, mSeedsIn1, ph1, abw1, salinity1, samplingDate1, dailyfeed1, ADG1, imageFileBody);
        call.enqueue(new Callback<SamplingCycle>() {
            @Override
            public void onResponse(@NonNull Call<SamplingCycle> call, @NonNull Response<SamplingCycle> response) {
                SamplingCycle samplingCycle = response.body();

                if (response.isSuccessful() && samplingCycle != null) {
                    dialog.dismiss();
                    if (samplingCycle.getSuccess().equals("1")) {
                        onBackPressed();
                        Toast.makeText(mContext, "New Cycle Created Successfully", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<SamplingCycle> call, @NonNull Throwable t) {
                dialog.dismiss();
            }
        });

    }

    private void CreateSampling(App app, String taskid, String pondId, String customerID, String cycleid, String employee_id, String ph, String Abw,
                                String Salinity, String samplingDate, String dailyfeed, String doc1, String ADG) {
        dialog = new MyCustomDialog(this, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        RequestBody taskid1 = RequestBody.create(MediaType.parse("text/plain"), taskid);
        RequestBody pondId1 = RequestBody.create(MediaType.parse("text/plain"), pondId);
        RequestBody customerID1 = RequestBody.create(MediaType.parse("text/plain"), customerID);
        RequestBody cycleid1 = RequestBody.create(MediaType.parse("text/plain"), cycleid);
        RequestBody employee_id1 = RequestBody.create(MediaType.parse("text/plain"), employee_id);
        RequestBody ph1 = RequestBody.create(MediaType.parse("text/plain"), ph);
        RequestBody Abw1 = RequestBody.create(MediaType.parse("text/plain"), Abw);
        RequestBody Salinity1 = RequestBody.create(MediaType.parse("text/plain"), Salinity);
        RequestBody samplingDate1 = RequestBody.create(MediaType.parse("text/plain"), samplingDate);
        RequestBody dailyfeed1 = RequestBody.create(MediaType.parse("text/plain"), dailyfeed);
        RequestBody doc11 = RequestBody.create(MediaType.parse("text/plain"), doc1);
        RequestBody ADG1 = RequestBody.create(MediaType.parse("text/plain"), ADG);
        if (mImagePath != null) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            imageFileBody = MultipartBody.Part.createFormData("sampling_file", file.getName(), requestBody);
        } else {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            imageFileBody = MultipartBody.Part.createFormData("sampling_file", "", requestBody);
        }
        Call<SamplingCycle> call = apiService.CreatesamplingCycle(taskid1, pondId1, customerID1, cycleid1, employee_id1, ph1, Abw1, Salinity1,
                samplingDate1, dailyfeed1, doc11, ADG1, imageFileBody);
        call.enqueue(new Callback<SamplingCycle>() {
            @Override
            public void onResponse(@NonNull Call<SamplingCycle> call, @NonNull Response<SamplingCycle> response) {
                SamplingCycle samplingCycle = response.body();
                Log.e("samplingCycle", "" + response.toString());

                if (response.isSuccessful() && samplingCycle != null) {
                    dialog.dismiss();
                    if (samplingCycle.getSuccess().equals("1")) {
                        onBackPressed();
                        Toast.makeText(mContext, "Sampling Created Successfully", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<SamplingCycle> call, @NonNull Throwable t) {
                dialog.dismiss();
            }
        });
    }

    private void mDatePicker() {
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if (dayOfMonth > 9 || monthOfYear > 9) {
                            Docdate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            mEditStockingDate.setText(Docdate);
                        } else {
                            Docdate = "0" + dayOfMonth + "/" + "0" + (monthOfYear + 1) + "/" + year;
                            mEditStockingDate.setText(Docdate);
                        }
                    }
                }, mYear, mMonth, mDay);
        //    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void mDatePicker1() {
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if (dayOfMonth > 9 || monthOfYear > 9) {
                            Docdate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            EditSamplingDate.setText(Docdate);
                        } else {
                            Docdate = "0" + dayOfMonth + "/" + "0" + (monthOfYear + 1) + "/" + year;
                            EditSamplingDate.setText(Docdate);
                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}