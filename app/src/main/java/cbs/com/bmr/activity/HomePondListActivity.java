package cbs.com.bmr.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.FragmentCallback;
import cbs.com.bmr.Listener.Pond_details_click_listener;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.PondNameListAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.CustomerList;
import cbs.com.bmr.model.PondList;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static cbs.com.bmr.Utilities.Activity.launchWithBundle;


public class HomePondListActivity extends AppCompatActivity implements Pond_details_click_listener, View.OnClickListener, FragmentCallback {

    private PondNameListAdapter adapter;
    private SearchableSpinner edit_customer;
    private LinearLayout l_customer;
    private TextInputLayout t_layout;
    private EditText t_customer;
    private String customerID, customerNAME, GeoJson;
    private Context context;
    private ConfigurationSettings settings;
    private RecyclerView rc_pond_list;
    private ArrayList<String> customer_list_spinner = new ArrayList<>();
    private ArrayList<CustomerList> customerList = new ArrayList<>();
    private ArrayList<PondList> pondList = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    private FloatingActionButton fab_pond_create;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_pondlisting);

        context = this;
        settings = new ConfigurationSettings(context);

        initialize();
        getCustomerValues();
        new GetCustomerList().execute();

        adapter = new PondNameListAdapter(context, pondList);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        adapter.setPondListListener(this);
        rc_pond_list.setLayoutManager(mLayoutManager);
        rc_pond_list.setAdapter(adapter);

        fab_pond_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putBoolean("IsSampling", true);
                bundle.putString("CustomerName", customerNAME);
                bundle.putString("CustomerId", customerID);


            }
        });
        edit_customer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                customerID = customerList.get(position).getId();
                customerNAME = customerList.get(position).getFirst_name();
                // fab_pond_create.setVisibility(View.VISIBLE);
                //  new GetPondList().execute(customerID);
                Bundle bundle = new Bundle();
                bundle.putString("CustomerName", customerNAME);
                bundle.putString("CustomerId", customerID);
                bundle.putString("GeoJson", GeoJson);
                launchWithBundle(context, HomeTechnicalServicesActivity.class, bundle);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parenFFt) {

            }
        });

        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void launchFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body1, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    private void getCustomerValues() {
        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null) {
            GeoJson = bundle.getString("GeoJson");

        }


    }

    private void getCustomerName() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, customer_list_spinner);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edit_customer.setTitle("Select Customer");
        edit_customer.setAdapter(ad);
    }

    @Override
    public void onResume() {
        super.onResume();
        getCustomerValues();
//        if (customerID != null) {
//            new GetPondList().execute(customerID);
//        }

    }

    private void initialize() {
        mImageBack = findViewById(R.id.img_back);
        edit_customer = findViewById(R.id.edit_customer);
        rc_pond_list = findViewById(R.id.rc_pond_list);
        fab_pond_create = findViewById(R.id.fab_pond_create);
        t_layout = findViewById(R.id.t_layout);
        l_customer = findViewById(R.id.l_customer);
        t_customer = findViewById(R.id.t_customer);
        //  t_customer.setOnClickListener(this);
    }


    @Override
    public void onPondClick(int position, String id, String cycle_id) {
        Bundle bundle = new Bundle();
        bundle.putString("POND_ID", id);
        bundle.putString("CYCLE_ID", cycle_id);
        bundle.putString("C_ID", customerID);
        bundle.putString("C_NAME", customerNAME);
        bundle.putString("GeoJson", GeoJson);
        bundle.putString("TaskId", "");
        bundle.putString("Noedit", "2");
        Log.e("customerID", "" + customerID);

        launchWithBundle(context, PondDetailedListActivity.class, bundle);

    }

    @Override
    public void onSamplingClick(int position, String id, String cycle_id) {
    }

    @Override
    public void onHistoryClick(int position, String id, String cycle_id) {
    }

    @Override
    public void ondetailsClick(int position, String id, String cycle_id) {
    }

    @Override
    public void onClick(View v) {
        l_customer.setVisibility(View.VISIBLE);
        t_layout.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class GetPondList extends AsyncTask<String, String, ArrayList<PondList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<PondList> doInBackground(String... v) {
            ArrayList<PondList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getPONDName((App) getApplication(), customerID);
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<PondList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != list) {
                pondList.clear();
                for (PondList pond : list) {
                    AppLog.write("Checking_1.", "---------" + new Gson().toJson(pond));
                    pondList.add(pond);
                }
                adapter.MyDataChanged(pondList);
            } else {
                Toast.makeText(context, "No Records Found..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetCustomerList extends AsyncTask<Void, Void, ArrayList<CustomerList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<CustomerList> doInBackground(Void... v) {
            ArrayList<CustomerList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getCustomerMasterList((App) getApplication(), settings.getEmployee_ID());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<CustomerList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            customerList.clear();
            customer_list_spinner.clear();
            for (CustomerList c_list : list) {
                customerList.add(c_list);
                customer_list_spinner.add(c_list.getFirst_name() + "\n" +

                        c_list.getCity_village() + " ," + c_list.getState());
            }
            AppLog.write("CUSTOMER_RECORD--", "--" + new Gson().toJson(customerList));
            getCustomerName();
        }
    }

}
