package cbs.com.bmr.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import cbs.com.bmr.R;

import static cbs.com.bmr.Utilities.Activity.launch;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {


    private TextView mTextChangePassword, mTextLeaveRequest;
    private ImageView mImageback;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initObject();
        initCallbacks();
    }
    private void initCallbacks() {

        mTextChangePassword.setOnClickListener(this);
        mImageback.setOnClickListener(this);
        mTextLeaveRequest.setOnClickListener(this);
    }
    private void initObject() {
        mContext = this;
        mTextChangePassword = findViewById(R.id.txt_changepassword);
        mTextLeaveRequest = findViewById(R.id.txt_leaverequest);
        mImageback = findViewById(R.id.img_back);
    }
    @Override
    public void onClick(View v) {
        if (v == mTextChangePassword) {
            launch(mContext, ChangePasswordActivity.class);
        } else if (v == mTextLeaveRequest) {
            launch(mContext, LeaveRequestActivity.class);
        } else if (v == mImageback) {
            onBackPressed();
        }

    }

}
