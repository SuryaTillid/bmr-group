package cbs.com.bmr.activity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.Utilities.Activity;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.model.MyTodoList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EditMyTodoListActivity extends AppCompatActivity implements View.OnClickListener {


    String statusname, priorityname;
    private Context mContext;
    private EditText mEditTaskName, mEditCreatedby, mEditDuedate, mEditcomments;
    private ImageView mImageBack, mImageUpload;
    private Spinner mSpinnerPriority, mSpinnerStatus;
    private LinearLayout mSpinnerLayout;
    private String Taskname, Createdby, Duedate, Priority, Status, mId;
    private String[] priority = {"Low", "Medium", "High"};
    private String[] status = {"Select Status", "Open", "In-Progress", "Completed", "Cancelled"};
    private Button mBtnSubmit;
    private MyCustomDialog dialog;
    private ConfigurationSettings configurationSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_my_todo_list);
        processBundle();
        initObjects();
        initCallbacks();
        initsetData();
    }

    private void initsetData() {

        mEditTaskName.setText(Taskname);
        mEditTaskName.setBackgroundResource(R.drawable.edit_background_noneditable);
        mEditCreatedby.setText(configurationSettings.getUSER_NAME());
        mEditCreatedby.setBackgroundResource(R.drawable.edit_background_noneditable);
        mEditDuedate.setText(Duedate);
        mEditDuedate.setBackgroundResource(R.drawable.edit_background_noneditable);
        mSpinnerLayout.setBackgroundResource(R.drawable.edit_background_noneditable);

    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Taskname = bundle.getString("TaskName");
            Createdby = bundle.getString("CreatedBy");
            Duedate = bundle.getString("DueDate");
            Priority = bundle.getString("priority");
            Status = bundle.getString("status");
            mId = bundle.getString("mId");
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    private void initCallbacks() {
        mBtnSubmit.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
    }

    @SuppressWarnings("unchecked")
    private void initObjects() {
        mContext = this;
        configurationSettings = new ConfigurationSettings(mContext);
        mSpinnerLayout = findViewById(R.id.spinner_layout1);
        mImageBack = findViewById(R.id.img_back);
        mEditTaskName = findViewById(R.id.edit_task);
        mEditCreatedby = findViewById(R.id.edit_createdby);
        mEditDuedate = findViewById(R.id.edit_duedate);
        mEditcomments = findViewById(R.id.edit_comments);
        mSpinnerPriority = findViewById(R.id.spinner_priority);
        mSpinnerStatus = findViewById(R.id.spinner_status);
        mBtnSubmit = findViewById(R.id.btn_submit);

        ArrayAdapter adapter = new ArrayAdapter<String>(mContext, R.layout.custom_spinner_item, R.id.custom_spinner_item, priority);
        mSpinnerPriority.setAdapter(adapter);
        ArrayAdapter adapter1 = new ArrayAdapter<String>(mContext, R.layout.custom_spinner_item, R.id.custom_spinner_item, status);
        mSpinnerStatus.setAdapter(adapter1);
        mSpinnerPriority.setSelected(false);
        if (Priority.equals("0")) {
            priorityname = "0";
            mSpinnerPriority.setSelection(0);
        } else if (Priority.equals("1")) {
            priorityname = "1";
            mSpinnerPriority.setSelection(1);
        } else if (Priority.equals("2")) {
            priorityname = "2";
            mSpinnerPriority.setSelection(2);
        }

        Log.e("Status", Status);
        if (Status.equals("0")) {
            statusname = "0";
            mSpinnerStatus.setSelection(1);
        } else if (Status.equals("1")) {
            statusname = "1";
            mSpinnerStatus.setSelection(2);
        } else if (Status.equals("2")) {
            statusname = "2";
            mSpinnerStatus.setSelection(3);
        } else if (Status.equals("3")) {
            statusname = "3";
            mSpinnerStatus.setSelection(4);
        }
        mSpinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mSpinnerStatus.getSelectedItem().toString().equals("Open")) {
                    statusname = "0";
                } else if (mSpinnerStatus.getSelectedItem().toString().equals("In-Progress")) {
                    statusname = "1";
                } else if (mSpinnerStatus.getSelectedItem().toString().equals("Completed")) {
                    statusname = "2";
                } else if (mSpinnerStatus.getSelectedItem().toString().equals("Cancelled")) {
                    statusname = "3";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mSpinnerPriority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mSpinnerPriority.getSelectedItem().toString().equals("Low")) {
                    priorityname = "0";
                } else if (mSpinnerPriority.getSelectedItem().toString().equals("Medium")) {
                    priorityname = "1";
                } else if (mSpinnerPriority.getSelectedItem().toString().equals("High")) {
                    priorityname = "2";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == mImageBack) {
            onBackPressed();
        } else if (v == mBtnSubmit) {
            processSubmit();
        }
    }

    private void processSubmit() {

        String comments = mEditcomments.getText().toString().trim();

        UpdateList((App) getApplication(), mId, statusname, priorityname, comments, configurationSettings.getEmployee_ID());
    }


    private void UpdateList(App app, String TaskId, String Status, String priority, String comments, String LoginId) {
        Log.e("statusname", "" + statusname);
        Log.e("Status", "" + Status);
        Log.e("comments", "" + comments);
        Log.e("LoginId", "" + LoginId);
        dialog = new MyCustomDialog(this, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<MyTodoList> call = apiService.UpdateMyTodoList(TaskId, Status, priority, comments, LoginId);
        call.enqueue(new Callback<MyTodoList>() {
            @Override
            public void onResponse(@NonNull Call<MyTodoList> call, @NonNull Response<MyTodoList> response) {
                MyTodoList samplingCycle = response.body();
                Log.e("sdfdfsf", "" + response.toString());
                if (response.isSuccessful() && samplingCycle != null) {
                    dialog.dismiss();
                    Log.e("dsf", "" + samplingCycle.getSuccess());
                    if (samplingCycle.getSuccess().equals("1")) {
                        Bundle bundle23 = new Bundle();
                        bundle23.putString("priorityname", "");
                        bundle23.putString("statusname", "");
                        bundle23.putString("assintype", "");
                        bundle23.putString("listtype", "1");
                        Activity.launchClearTopWithBundle(mContext, TodoListActivity.class, bundle23);
                        Toast.makeText(EditMyTodoListActivity.this, "Update Successfully", Toast.LENGTH_SHORT).show();
                    } else {
                        Bundle bundle23 = new Bundle();
                        bundle23.putString("priorityname", "");
                        bundle23.putString("statusname", "");
                        bundle23.putString("assintype", "");
                        bundle23.putString("listtype", "1");
                        Activity.launchClearTopWithBundle(mContext, TodoListActivity.class, bundle23);
                        Toast.makeText(EditMyTodoListActivity.this, "Update Successfully", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.dismiss();

                }
            }

            @Override
            public void onFailure(@NonNull Call<MyTodoList> call, @NonNull Throwable t) {
                dialog.dismiss();
            }
        });
    }

}
