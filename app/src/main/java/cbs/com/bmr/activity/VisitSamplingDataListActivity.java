package cbs.com.bmr.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.FragmentCallback;
import cbs.com.bmr.Listener.Pond_details_click_listener;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.CustomerNameAdapter;
import cbs.com.bmr.adapter.VisitSamplingDataAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.CustomerList;
import cbs.com.bmr.model.PondList;
import cbs.com.bmr.model.SuccessMessage;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static cbs.com.bmr.Utilities.Activity.launchWithBundle;

/*********************************************************************
 * Created by Barani on 19-08-2019 in TableMateNew
 ***********************************************************************/
public class VisitSamplingDataListActivity extends AppCompatActivity implements Pond_details_click_listener, View.OnClickListener, FragmentCallback {

    private RecyclerView rc_pond_list;
    private SearchableSpinner edit_customer;
    private ArrayList<CustomerList> customerList = new ArrayList<>();
    private ArrayList<String> customer_list_spinner = new ArrayList<>();
    private String customerID, customerNAME, employeeID, harvest_flag = "0", TaskId;
    private CustomerNameAdapter customerNameAdapter;
    private VisitSamplingDataAdapter adapter;
    private ArrayList<PondList> pondList = new ArrayList<>();
    private ConfigurationSettings settings;
    private LinearLayoutManager mLayoutManager;
    private Context context;
    private Dialog dialog;
    private DatePickerDialog datePickerDialog;
    private String sampling_date, sampling_date_display;
    private Calendar myCalendar = Calendar.getInstance();
    private EditText edit_sampling_date;
    private DatePickerDialog.OnDateSetListener date_of_task;
    private LinearLayout l_customer;
    private TextInputLayout t_layout;
    private EditText t_customer;
    private ImageView mImageBack;
    private FloatingActionButton mFloatingPondCreate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_sampling);
        context = this;
        settings = new ConfigurationSettings(context);
        employeeID = settings.getEmployee_ID();

        initialize();
        getCustomerValues();

        date_of_task = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                getDate();
            }
        };

        datePickerDialog = new DatePickerDialog(context, date_of_task, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        adapter = new VisitSamplingDataAdapter(context, pondList);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        adapter.setSamplingListListener(this);
        rc_pond_list.setLayoutManager(mLayoutManager);
        rc_pond_list.setAdapter(adapter);

        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mFloatingPondCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("IsSampling", true);
                bundle.putString("CustomerName", customerNAME);
                bundle.putString("CustomerId", customerID);

                launchWithBundle(context, PondCreationActivity.class, bundle);
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onResume() {
        super.onResume();
        getCustomerValues();
    }

    private void getCustomerValues() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            customerID = bundle.getString("C_ID");
            customerNAME = bundle.getString("C_NAME");
            TaskId = bundle.getString("TaskId");
            if (!TextUtils.isEmpty(customerID) && !TextUtils.isEmpty(customerNAME)) {
                t_customer.setText(customerNAME);
            }
            new GetPondSamplingDetails().execute(customerID);
        }

    }


    private void initialize() {
        mFloatingPondCreate = findViewById(R.id.fab_pond_create);
        edit_customer = findViewById(R.id.edit_customer);
        rc_pond_list = findViewById(R.id.rc_pond_list);
        t_layout = findViewById(R.id.t_layout);
        l_customer = findViewById(R.id.l_customer);
        t_customer = findViewById(R.id.t_customer);
        mImageBack = findViewById(R.id.img_back);
        edit_customer.setVisibility(View.GONE);
    }


    private void getDate() {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy", Locale.getDefault());
        sampling_date_display = sdf.format(myCalendar.getTime());
        sampling_date = sdf1.format(myCalendar.getTime());
        edit_sampling_date.setText(sampling_date_display);
        AppLog.write("Date", "--" + sdf.format(myCalendar.getTime()) + sampling_date_display);
    }

    private boolean validate(EditText e_abw, EditText e_daily_feed) {
        boolean isValid = true;
        if (e_abw.getText().toString().equals("")) {
            e_abw.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            e_abw.setError(null);
        }

        if (e_daily_feed.getText().toString().equals("")) {
            e_daily_feed.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            e_daily_feed.setError(null);
        }
        return isValid;
    }

    private void changeColor(Button selected_btn, Button btn_active, Button btn_harvest) {
        btn_active.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_harvest.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_active.setTextColor(Color.parseColor("#4e66f5"));
        btn_harvest.setTextColor(Color.parseColor("#4e66f5"));

        selected_btn.setBackgroundResource(R.drawable.blue_button_rounded_edges);
        selected_btn.setTextColor(Color.parseColor("#ffffff"));
    }

    /* *
     * Get customer record - auto complete
     * */
    private void getCustomerName() {
        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, customer_list_spinner);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edit_customer.setTitle("Select Customer");
        edit_customer.setAdapter(ad);
    }

    @Override
    public void onClick(View v) {
        //l_customer.setVisibility(View.VISIBLE);
        //t_layout.setVisibility(View.GONE);
    }

    @Override
    public void onPondClick(int position, String id, String cycle_id) {

    }

    @Override
    public void onSamplingClick(int position, String id, String cycle_id) {
        Bundle bundle = new Bundle();
        bundle.putString("PondId", id);
        bundle.putString("customerID", customerID);
        bundle.putString("TaskId", TaskId);
        launchWithBundle(context, SamplingCycleActivity.class, bundle);
    }

    @Override
    public void onHistoryClick(int position, String id, String cycle_id) {
        Bundle bundle = new Bundle();
        bundle.putString("customerID", customerID);
        bundle.putString("cycleID", cycle_id);
        bundle.putString("Request", "0");
        bundle.putString("PondId", id);
        bundle.putString("TaskId", TaskId);
        bundle.putString("CustomerName", customerNAME);
        launchWithBundle(context, SamplingHistoryActivity.class, bundle);
    }

    @Override
    public void ondetailsClick(int position, String id, String cycle_id) {
        Bundle bundle = new Bundle();
        bundle.putString("C_ID", customerID);
        bundle.putString("CYCLE_ID", cycle_id);
        bundle.putString("C_NAME", customerNAME);
        bundle.putString("TaskId", TaskId);
        bundle.putString("POND_ID", id);
        bundle.putString("Noedit", "1");
        launchWithBundle(context, PondDetailedListActivity.class, bundle);
    }

    @Override
    public void launchFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private class GetPondSamplingDetails extends AsyncTask<String, String, ArrayList<PondList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<PondList> doInBackground(String... v) {
            ArrayList<PondList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getPONDSamplingDetails((App) getApplication(), v[0]);
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<PondList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != list) {
                pondList.clear();
                for (PondList pond : list) {
                    AppLog.write("Checking_1.", "---------" + new Gson().toJson(pond));
                    pondList.add(pond);
                }
                adapter.MyDataChanged(pondList);
            } else {
                Toast.makeText(context, "No Records Found..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetCustomerList extends AsyncTask<Void, Void, ArrayList<CustomerList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<CustomerList> doInBackground(Void... v) {
            ArrayList<CustomerList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getCustomerMasterList((App) getApplication(), settings.getEmployee_ID());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<CustomerList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            customerList.clear();
            customer_list_spinner.clear();
            for (CustomerList c_list : list) {
                customerList.add(c_list);
                customer_list_spinner.add(c_list.getFirst_name());
            }
            AppLog.write("CUSTOMER_RECORD--", "--" + new Gson().toJson(customerList));
            getCustomerName();
        }
    }

    private class CreateSampling_Details extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog m_dialog;

        @Override
        protected void onPreExecute() {
            m_dialog = new MyCustomDialog(context, "Creating Task...");
            m_dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            return call.createSamplingRecord((App) getApplication(), s[0], s[1], s[2], s[3], s[4], s[5], sampling_date);
        }

        @Override
        protected void onPostExecute(SuccessMessage response) {
            if (null != m_dialog && m_dialog.isShowing())
                m_dialog.dismiss();
            if (response != null) {
                if (response.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Successfully Created..!", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } else if (response.getSuccess().equalsIgnoreCase("3")) {
                    Toast.makeText(context, "Pond name already exist for this Farmer..!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Failed to create..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Failed to create..!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}