package cbs.com.bmr.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cbs.com.bmr.BuildConfig;
import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.DetectConnection;
import cbs.com.bmr.Helper.ExceptionHandler;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Helper.PrimarySettings;
import cbs.com.bmr.Listener.EmployeeSessionListCallback;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.EmployeeSessionListAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.EmployeeList;
import cbs.com.bmr.model.EmployeeSessionList;
import cbs.com.bmr.model.SuccessMessage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/*********************************************************************
 * Created by Barani on 02-04-2019 in TableMateNew
 *********************************************************************/
public class LoginActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, EmployeeSessionListCallback {
    private EditText ed_UserName;
    private EditText ed_Password;
    Dialog dialogEmployeeList;
    private boolean confirmable_login;
    private AppCompatCheckBox check_remember_me;
    private String userName, passWord;
    private Context context;
    private String DayIN = "0", version_num;
    private ConfigurationSettings configurationSettings;
    private PrimarySettings primarySettings;
    private RelativeLayout mSnackbarlayout;
    private TextView mTextVersion;

    private ArrayList<EmployeeList> employeeList = new ArrayList<>();

    public static boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_login);

        context = LoginActivity.this;
        configurationSettings = new ConfigurationSettings(context);
        primarySettings = new PrimarySettings(context);

       /* String IpAddress = "https://www.bmrconnect.com";
        App.IP_ADDRESS = IpAddress;
        configurationSettings.setIP_ADDRESS(IpAddress);*/

        initialize();
        initApiCalls();
        getVersionNumber();
        if (primarySettings.isChecked()) {
            ed_UserName.setText(primarySettings.getUSER_NAME());
            ed_Password.setText(primarySettings.getPASSWORD());
        }
// login commit done

    }

    private void getVersionNumber() {
        try {
            PackageManager manager = this.getPackageManager();
            PackageInfo info = manager.getPackageInfo(this.getPackageName(), PackageManager.GET_ACTIVITIES);
            AppLog.write("VERSION_N", info.versionName);
            version_num = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void initApiCalls() {
        if (DetectConnection.isOnline(context)) {
            new LoadEmployeeRecords().execute();
        }
    }

    Dialog dialogUser;
    private Button btn_login, btn_reset;
    private LinearLayoutManager mLayoutManager;
    private EmployeeSessionListAdapter mAdapter;
    private ArrayList<EmployeeSessionList> employeeSessionLists;
    private RecyclerView mRecyclerView;
    private MyCustomDialog progressDialog;

    private void initialize() {
        mTextVersion = findViewById(R.id.txt_verionnae);
        mSnackbarlayout = findViewById(R.id.main_layout1);
        ed_UserName = findViewById(R.id.ed_userName);
        ed_Password = findViewById(R.id.ed_password);
        check_remember_me = findViewById(R.id.c_remember_me);
        btn_login = findViewById(R.id.btnLogin);
        btn_reset = findViewById(R.id.btn_reset);
        btn_login.setOnClickListener(this);
        btn_reset.setOnClickListener(this);
        check_remember_me.setOnCheckedChangeListener(this);

        mTextVersion.setText(String.format("Bmr version:%s", BuildConfig.VERSION_NAME));
    }

    @Override
    public void onClick(View v) {
        if (v == btn_login) {
            userName = ed_UserName.getText().toString().trim();
            passWord = ed_Password.getText().toString().trim();
            if (validate()) {
                if (userName != null && passWord != null) {
                    if (DetectConnection.isOnline(context)) {
                        new Validate_login().execute(userName, passWord);
                    }
                }
            }
        } else if (v == btn_reset) {
            UserNameDialog(configurationSettings.getEmployee_ID(), primarySettings.getUSER_NAME());
            //  EmployeeSessionList();
        }

    }

    private void UserNameDialog(final String empid, final String userName) {
        dialogUser = new Dialog(context);
        dialogUser.setContentView(R.layout.dialog_user_session);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialogUser.getWindow();
        assert window != null;
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        final EditText mEditUsername = dialogUser.findViewById(R.id.ed_userName);
        final EditText mEditPassword = dialogUser.findViewById(R.id.ed_password);
        Button btn_submit = dialogUser.findViewById(R.id.btn_submit);
        Button btncancel = dialogUser.findViewById(R.id.btncancel);

        mEditUsername.setText(userName);
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogUser.dismiss();
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Username = mEditUsername.getText().toString().trim();
                String password = mEditPassword.getText().toString().trim();
                if (Username.isEmpty()) {
                    mEditUsername.requestFocus();
                    mEditUsername.setError("Please enter Username");
                } else if (password.isEmpty()) {
                    mEditPassword.requestFocus();
                    mEditPassword.setError("Please enter Password");
                } else {
                    getrestSesstion((App) getApplication(), empid, Username, password);
                }
            }
        });

        dialogUser.show();
    }

    private void EmployeeSessionList() {
        dialogEmployeeList = new Dialog(context);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialogEmployeeList.getWindow();
        assert window != null;
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialogEmployeeList.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogEmployeeList.setContentView(R.layout.dialog_employe_list);
        employeeSessionLists = new ArrayList<>();


        getEmployeeList((App) getApplication());
        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView = dialogEmployeeList.findViewById(R.id.recyclerview_employees);
        ImageView img_close = dialogEmployeeList.findViewById(R.id.img_close);
        mAdapter = new EmployeeSessionListAdapter(context, employeeSessionLists, this);
        initRecyclerView();
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogEmployeeList.dismiss();
            }
        });
        dialogEmployeeList.show();
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private boolean validate() {
        userName = ed_UserName.getText().toString().trim();
        passWord = ed_Password.getText().toString().trim();
        if (userName.equals("")) {
            ed_UserName.setError("Enter Username");
            ed_UserName.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(passWord)) {
            ed_Password.setError("Enter Password");
            ed_Password.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        AppLog.write("checkListener", "----" + buttonView);
        if (isChecked) {
            if (validate()) {
                confirmable_login = true;
                primarySettings.setIS_CHECKED_REMEMBER_ME();
            }
        } else {
            confirmable_login = false;
        }
        check_remember_me.setTextColor(confirmable_login ? context.getResources().getColor(R.color.charcoalGrey) :
                context.getResources().getColor(R.color.charcoalGrey));
    }

    private void getRegionId() {
        for (EmployeeList e : employeeList) {
            if (configurationSettings.getEmployee_ID().equalsIgnoreCase(e.getId())) {
                AppLog.write("REGION--", "--" + e.getRegion_id());
                configurationSettings.setREGION_ID(e.getRegion_id());
            }
        }
    }

    @Override
    public void onBackPressed() {
        confirmExit();
    }

    public void getEmployeeList(App app) {
        progressDialog = new MyCustomDialog(context, "Loading...");
        progressDialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<ArrayList<EmployeeSessionList>> call = apiService.getEmployeeSession();
        call.enqueue(new Callback<ArrayList<EmployeeSessionList>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<EmployeeSessionList>> call, @NonNull Response<ArrayList<EmployeeSessionList>> response) {
                ArrayList<EmployeeSessionList> mEmployeeList = response.body();
                if (response.isSuccessful() && mEmployeeList != null) {

                    if (mEmployeeList.size() > 0) {
                        progressDialog.dismiss();
                        employeeSessionLists.clear();
                        employeeSessionLists.addAll(mEmployeeList);
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<EmployeeSessionList>> call, @NonNull Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void confirmExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Are you sure want to exit?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                exit_app();
                //  getLogout((App) getApplication());

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    private void exit_app() {
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }

    public void getLogout(App app) {
        RestApi apiService = app.createRestAdaptor();
        Call<Void> call = apiService.LogoutUser(configurationSettings.getEmployee_ID());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                Log.e("Userlogout", "" + response.code());

                Intent intent = new Intent(LoginActivity.this, ConfigurationActivity.class);
                startActivity(intent);
            }


            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
            }
        });
    }

    @Override
    public void onItemClick(int position) {
        EmployeeSessionList employeeSessionList = employeeSessionLists.get(position);
        String EmpId = employeeSessionList.getEmp_id();
        String username = employeeSessionList.getUsername();
        UserNameDialog(EmpId, username);
    }


    public void getrestSesstion(App app, String employeeId, String userName, String password) {

        progressDialog = new MyCustomDialog(context, "Loading...");
        progressDialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<EmployeeSessionList> call = apiService.ResetSession(employeeId, userName, password);
        call.enqueue(new Callback<EmployeeSessionList>() {
            @Override
            public void onResponse(@NonNull Call<EmployeeSessionList> call, @NonNull Response<EmployeeSessionList> response) {
                EmployeeSessionList employeeSessionList = response.body();
                Log.e("Eddjsdf", "" + response.toString());
                if (response.isSuccessful() && employeeSessionList != null) {

                    Log.e("Eddjsdfsd", "" + employeeSessionList.getSuccess());
                    Log.e("Eddjsdsdf", "" + employeeSessionList.getVal());
                    progressDialog.dismiss();
                    switch (employeeSessionList.getSuccess()) {
                        case "1":
                            Toast.makeText(LoginActivity.this, "Reset " + employeeSessionList.getVal(), Toast.LENGTH_SHORT).show();
                            dialogUser.dismiss();

                            break;
                        case "2":
                            Toast.makeText(LoginActivity.this, employeeSessionList.getVal(), Toast.LENGTH_SHORT).show();
                            dialogUser.dismiss();
                            break;
                        case "3":
                            Toast.makeText(LoginActivity.this, employeeSessionList.getVal(), Toast.LENGTH_SHORT).show();
                            dialogUser.dismiss();
                            break;
                        case "4":
                            Toast.makeText(LoginActivity.this, employeeSessionList.getVal(), Toast.LENGTH_SHORT).show();
                            dialogUser.dismiss();
                            break;
                    }
                } else {
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onFailure(@NonNull Call<EmployeeSessionList> call, @NonNull Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void initSnackbar() {
        Snackbar snackbar = Snackbar
                .make(mSnackbarlayout, "New Version for this app is available in playstore, please click to update your app..!", Snackbar.LENGTH_INDEFINITE)
                .setAction("Click here", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(android.content.Intent.ACTION_VIEW);
                        i.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()));
                        startActivity(i);

                    }
                });
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        TextView textView = snackBarView.findViewById(R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(context, R.color.white));
        snackbar.setActionTextColor(ContextCompat.getColor(context, R.color.white));

        snackbar.show();
    }

    @SuppressLint("StaticFieldLeak")
    private class LoadEmployeeRecords extends AsyncTask<Void, Void, ArrayList<EmployeeList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<EmployeeList> doInBackground(Void... v) {
            RestApiCalls call = new RestApiCalls();
            return call.getEmployeeList((App) getApplication(), configurationSettings.getEmployee_ID());
        }

        @Override
        protected void onPostExecute(ArrayList<EmployeeList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != list) {
                employeeList.addAll(list);
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class GetDayInStatus extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.call_for_getDayIn_status((App) getApplication(), configurationSettings.getEmployee_ID());
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Day_IN_status", "--" + new Gson().toJson(success));
            if (success != null) {
                if (success.getSuccess().equalsIgnoreCase("1")) {
                    DayIN = "1";
                    configurationSettings.setIS_CHECKED_IN(true);
                    configurationSettings.setUPDATE_ID(success.getUpdate_id());
                    Intent intent = new Intent(LoginActivity.this, Home_Page_Activity.class);
                    startActivity(intent);
                } else if (success.getSuccess().equalsIgnoreCase("2")) {
                    DayIN = "2";
                    Intent intent = new Intent(LoginActivity.this, Home_Page_Activity.class);
                    startActivity(intent);
                } else if (success.getSuccess().equalsIgnoreCase("0")) {
                    DayIN = "0";
                    Intent intent = new Intent(LoginActivity.this, Home_Page_Activity.class);
                    startActivity(intent);
                }
            } else {
                AppLog.write("Day_IN_status", "Not Working");
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class Validate_login extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.LoginSubmit((App) getApplication(), p[0], p[1], version_num);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != success) {
                AppLog.write("Login output-------", "-" + new Gson().toJson(success) + "--" + userName);
                String s = success.getSuccess();
                if (!TextUtils.isEmpty(s)) {
                    switch (s) {
                        case "2":
                            Toast.makeText(context, "Incorrect Username..!", Toast.LENGTH_SHORT).show();
                            break;
                        case "3":
                            Toast.makeText(context, "Incorrect Password..!", Toast.LENGTH_SHORT).show();
                            break;
                        case "4":
                            Toast.makeText(context, "Incorrect Username and Password..!", Toast.LENGTH_SHORT).show();
                            break;
                        case "5":
                            Toast.makeText(context, "Version Number Required..!", Toast.LENGTH_SHORT).show();
                            break;
                        case "6":
                            //  Toast.makeText(context, "New Version for this app is available in playstore, please update your app..!", Toast.LENGTH_SHORT).show();
                            initSnackbar();
                            break;
                        case "7":
                            Toast.makeText(context, "Already Logged by other device, Please Reset Session and login again....!", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            if (!result.equals("-1")) {
                                configurationSettings.setUSER_NAME(success.getEmp_name());
                                configurationSettings.setEmployee_ID(success.getE_id());
                                configurationSettings.setPASSWORD(passWord);
                                primarySettings.setUSER_NAME(userName);
                                primarySettings.setPASSWORD(passWord);
                                if (!TextUtils.isEmpty(configurationSettings.getEmployee_ID())) {
                                    getRegionId();
                                    new GetDayInStatus().execute();
                                }

                                configurationSettings.setREGION_NAME(success.getRegion_name());
                                configurationSettings.setREGION_ID(success.getRegion_id());

                                Toast.makeText(context, "Login Succeed...", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Login Failed...", Toast.LENGTH_SHORT).show();
                            }
                            break;
                    }
                } else {
                    Toast.makeText(context, "Please check your network connection and try again..!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}