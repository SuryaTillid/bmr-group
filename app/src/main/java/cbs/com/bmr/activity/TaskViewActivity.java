package cbs.com.bmr.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.ArrayList;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.ViewScheduleAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.TaskScheduler;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/*********************************************************************
 * Created by Barani on 26-09-2019 in TableMateNew
 ***********************************************************************/
public class TaskViewActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView rv_schedule_list;
    private TextView t_monday, t_tuesday, t_wednesday, t_thursday, t_friday, t_saturday;
    private LinearLayoutManager mLayoutManager;
    private Context context;
    private ViewScheduleAdapter adapter;
    private ArrayList<TaskScheduler> scheduleList = new ArrayList<>();
    private ArrayList<TaskScheduler> s_list = new ArrayList<>();
    private ConfigurationSettings settings;
    private ImageView mImageBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_task);

        context = TaskViewActivity.this;
        settings = new ConfigurationSettings(context);


        initObjects();
        initApiCalls();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    private void initObjects() {
        rv_schedule_list = findViewById(R.id.rv_schedule_list);
        t_monday = findViewById(R.id.t_monday);
        t_tuesday = findViewById(R.id.t_tuesday);
        t_wednesday = findViewById(R.id.t_wednesday);
        t_thursday = findViewById(R.id.t_thursday);
        t_friday = findViewById(R.id.t_friday);
        t_saturday = findViewById(R.id.t_saturday);
        mImageBack = findViewById(R.id.img_back);
        t_monday.setOnClickListener(this);
        t_tuesday.setOnClickListener(this);
        t_wednesday.setOnClickListener(this);
        t_thursday.setOnClickListener(this);
        t_friday.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
        t_saturday.setOnClickListener(this);

        adapter = new ViewScheduleAdapter(context, s_list);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_schedule_list.setLayoutManager(mLayoutManager);
        rv_schedule_list.setAdapter(adapter);
    }

    private void initApiCalls() {
        new GetScheduleList().execute();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.t_monday:
                SetSelectedValue(t_monday);
                filterValue("1");
                break;
            case R.id.t_tuesday:
                SetSelectedValue(t_tuesday);
                filterValue("2");
                break;
            case R.id.t_wednesday:
                SetSelectedValue(t_wednesday);
                filterValue("3");
                break;
            case R.id.t_thursday:
                SetSelectedValue(t_thursday);
                filterValue("4");
                break;
            case R.id.t_friday:
                SetSelectedValue(t_friday);
                filterValue("5");
                break;
            case R.id.t_saturday:
                SetSelectedValue(t_saturday);
                filterValue("6");
                break;
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }

    private void SetSelectedValue(TextView t_day) {
        t_monday.setBackgroundResource(R.drawable.day_unselected);
        t_tuesday.setBackgroundResource(R.drawable.day_unselected);
        t_wednesday.setBackgroundResource(R.drawable.day_unselected);
        t_thursday.setBackgroundResource(R.drawable.day_unselected);
        t_friday.setBackgroundResource(R.drawable.day_unselected);
        t_saturday.setBackgroundResource(R.drawable.day_unselected);

        t_day.setBackgroundResource(R.drawable.day_selected);
    }

    private void filterValue(String s) {
        s_list.clear();
        for (TaskScheduler task : scheduleList) {
            if (task.getType().equalsIgnoreCase(s)) {
                s_list.add(task);
            }
        }
        AppLog.write("Schedule_list", new Gson().toJson(s_list));
        adapter.MyDataChanged(s_list);
    }

    private class GetScheduleList extends AsyncTask<String, String, ArrayList<TaskScheduler>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<TaskScheduler> doInBackground(String... v) {
            RestApiCalls call = new RestApiCalls();
            return call.getScheduleListValues((App) getApplication(), settings.getEmployee_ID());
        }

        @Override
        protected void onPostExecute(ArrayList<TaskScheduler> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            scheduleList.clear();
            if (list != null) {
                scheduleList.addAll(list);
            }
            filterValue("1");
        }
    }
}