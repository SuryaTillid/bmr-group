package cbs.com.bmr.activity;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.DetectConnection;
import cbs.com.bmr.Helper.ExceptionHandler;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.Utilities.Activity;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.fragment.AttendanceFragment;
import cbs.com.bmr.fragment.TaskListFragment;
import cbs.com.bmr.fragment.TaskUpdateFragment;
import cbs.com.bmr.fragment.TaskUpdateFragmentForCurrentDayFragment;
import cbs.com.bmr.model.Location_Details;
import cbs.com.bmr.model.SuccessMessage;
import cbs.com.bmr.services.GpsTrackerAlarmReceiver;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static cbs.com.bmr.Utilities.Activity.launch;
import static cbs.com.bmr.Utilities.Activity.launchWithBundle;

/*********************************************************************
 * Created by Barani on 16-08-2019 in TableMateNew
 ***********************************************************************/
public class Home_Page_Activity extends AppCompatActivity implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final int REQUEST_ENABLE_LOCATION = 4;
    private static final long INTERVAL = 1000 * 600;
    private static final long FASTEST_INTERVAL = 1000 * 300;
    public Double lLat, lLng;
    Location mLastLocation, location;
    public LinearLayout l_change_password, l_current_day_task, l_technical, l_customer,
            l_task_list, l_task_schedule, l_sampling, l_harvest, l_track, l_approve, l_todo, l_task_create;
    private TextView t_in_time, t_hrs, t_out, t_curr_date, t_userName;
    private LinearLayout layout_fragment, layout_menu;
    private Context context;
    private boolean isInFront;
    private ConfigurationSettings settings;
    private TextView t_day_in, t_day_out;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private String Cur_latitude, Cur_longitude, Cur_userName;
    private ImageView mImageLogout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.home_landing_new);

        context = Home_Page_Activity.this;
        settings = new ConfigurationSettings(context);

        initialize();
        initAPICalls();
        setCurrentDate();
        calculateHours();

        Cur_userName = settings.getUSER_NAME();


        t_userName.setText("Hello," + Cur_userName.substring(0, 1).toUpperCase() + Cur_userName.substring(1).toLowerCase());
        AppLog.write("UserNAme", Cur_userName);
    }


    private void calculateHours() {

        Calendar calendar = Calendar.getInstance();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        DateFormat time_formatter = new SimpleDateFormat("HH:mm", Locale.getDefault());
        Date d1 = null, d2 = null;

        try {
            AppLog.write("dsds", "--" + settings.getCHECKED_IN_TIME());
            String day_in_time = settings.getCHECKED_IN_TIME();
            if (!TextUtils.isEmpty(day_in_time)) {
                Date date = format.parse(day_in_time);

                AppLog.write("FormatDate", "--" + date);

                String s_time = time_formatter.format(date);
                String e_time = time_formatter.format(calendar.getTime());

                AppLog.write("Format_date", "--" + s_time + "--" + e_time);

                d1 = time_formatter.parse(s_time);
                d2 = time_formatter.parse(e_time);

                long diff = d2.getTime() - d1.getTime();
                long diffMinutes = diff / (60 * 1000) % 60;
                long diffHours = diff / (60 * 60 * 1000) % 24;
                long diffSeconds = diff / 1000 % 60;

                AppLog.write("Hour:", diffHours + " hour, " + "Mins:" + diffMinutes + " minutes ");
                validateHours(diffHours, diffMinutes, diffSeconds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void validateHours(long diffHours, long diffMinutes, long diffSeconds) {
        if (diffHours == 0 && diffMinutes >= 1) {
            t_hrs.setText("Hours Logged :" + diffMinutes + " Min");
        } else if (diffHours == 0 && diffMinutes < 1) {
            t_hrs.setText("Hours Logged :" + diffSeconds + " Sec");
        } else if (diffHours > 0) {
            t_hrs.setText("Hours Logged :" + diffHours + " Hrs" + " " + diffMinutes + " Min");
        }
    }

    private void setCurrentDate() {
        DateFormat format = new SimpleDateFormat("dd MMM, EEE", Locale.getDefault());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String c_date = format.format(timestamp);
        t_curr_date.setText(c_date);
    }

    private void initAPICalls() {
        initLocationRequest();
        buildGoogleApiClient();
        displayLocationSettingsRequest();
        if (DetectConnection.isOnline(context)) {
            new GetDayInStatus().execute();
        }
    }

    private void initialize() {
        mImageLogout = findViewById(R.id.img_logout);
        t_day_in = findViewById(R.id.t_day_in);
        t_day_out = findViewById(R.id.t_day_out);
        t_curr_date = findViewById(R.id.txt_current_date);
        l_change_password = findViewById(R.id.l_change_password);
        l_current_day_task = findViewById(R.id.l_current_task);
        l_task_list = findViewById(R.id.l_task_list);
        l_task_schedule = findViewById(R.id.l_task_schedule);
        t_userName = findViewById(R.id.txt_Login_user_name);
        l_sampling = findViewById(R.id.l_sampling);
        l_harvest = findViewById(R.id.l_harvest);
        l_approve = findViewById(R.id.l_approve);
        l_todo = findViewById(R.id.l_todo);
        l_track = findViewById(R.id.l_track);
        l_task_create = findViewById(R.id.l_task_create);
        l_customer = findViewById(R.id.l_customer);
        l_technical = findViewById(R.id.l_technical);
        layout_fragment = findViewById(R.id.fragment_layout);
        layout_menu = findViewById(R.id.layout_menu);
        t_in_time = findViewById(R.id.t_in_time);
        t_out = findViewById(R.id.t_out_time);
        t_hrs = findViewById(R.id.t_hrs);

        l_change_password.setOnClickListener(this);
        l_current_day_task.setOnClickListener(this);
        l_task_list.setOnClickListener(this);
        l_task_schedule.setOnClickListener(this);
        l_sampling.setOnClickListener(this);
        l_harvest.setOnClickListener(this);
        l_track.setOnClickListener(this);
        l_approve.setOnClickListener(this);
        l_todo.setOnClickListener(this);
        l_task_create.setOnClickListener(this);
        l_customer.setOnClickListener(this);
        l_technical.setOnClickListener(this);

        t_day_in.setOnClickListener(this);
        t_day_out.setOnClickListener(this);
        mImageLogout.setOnClickListener(this);

    }


    private void displayLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest).setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(Home_Page_Activity.this, REQUEST_ENABLE_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }

        calculateHours();
    }

    @SuppressWarnings("deprecation")
    private void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            requestLocationPermission();
        }
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 4);
    }

    private AlarmManager alarmManager;

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    private Intent gpsTrackerIntent;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private PendingIntent pendingIntent;

    public Handler handler = new Handler();

    private String lat_lon, time_value;

    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            Cur_latitude = String.valueOf(latitude);
            Cur_longitude = String.valueOf(longitude);
            lat_lon = new Gson().toJson(getLocationDetails(Cur_latitude, Cur_longitude));
            AppLog.write("LL------", lat_lon);
            time_value = getTime();


            if (DetectConnection.isOnline(context)) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        new SendUpdatedLocation().execute(time_value, lat_lon);
                    }
                });


            }
        }
    }

    private String getTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String s = sdf.format(timestamp);
        return s;
    }

    private Location_Details getLocationDetails(String lat, String lon) {
        Location_Details location = new Location_Details();
        location.setLatitude(lat);
        location.setLongitude(lon);
        return location;
    }

    private void initLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        isInFront = false;
    }

    @Override
    public void onBackPressed() {
        callFragments();
    }


    private void callFragments() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        boolean handled = false;
        for (Fragment f : fragments) {
            if (f instanceof TaskUpdateFragment)
                handled = ((TaskUpdateFragment) f).onBackPressed();
            if (f instanceof AttendanceFragment)
                handled = ((AttendanceFragment) f).onBackPressed();
            if (f instanceof TaskListFragment)
                handled = ((TaskListFragment) f).onBackPressed();
            if (f instanceof TaskUpdateFragmentForCurrentDayFragment)
                handled = ((TaskUpdateFragmentForCurrentDayFragment) f).onBackPressed();
            if (handled) {
                break;
            }
        }
        if (!handled) {
            confirmExit();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
        // getCurrentLocation();
        startAlarmManager();
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        lLat = location.getLatitude();
        lLng = location.getLongitude();
        Cur_latitude = String.valueOf(lLat);
        Cur_longitude = String.valueOf(lLng);
        lat_lon = new Gson().toJson(getLocationDetails(Cur_latitude, Cur_longitude));
        // getCurrentLocation();
    }

    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);

        switch (v.getId()) {
            case R.id.t_day_in:
            case R.id.t_day_out:
                layout_fragment.setVisibility(View.VISIBLE);
                layout_menu.setVisibility(View.GONE);
                fragment = new AttendanceFragment();
                break;
            case R.id.l_change_password:
                launch(context, ProfileActivity.class);
                break;
            case R.id.l_current_task:
                launch(context, CurrentDayTaskActivity.class);
                break;
            case R.id.l_task_list:
                layout_fragment.setVisibility(View.VISIBLE);
                layout_menu.setVisibility(View.GONE);
                fragment = new TaskListFragment();
                break;
            case R.id.l_task_schedule:
                launch(context, TaskViewActivity.class);
                break;
            case R.id.l_task_create:
                launch(context, AssignTaskActivity.class);
                break;
            case R.id.l_customer:
                launch(context, Create_Farmer_Activity.class);
                break;
            case R.id.l_harvest:
                Bundle bundle1 = new Bundle();
                bundle1.putString("GeoJson", lat_lon);
                launchWithBundle(context, HomeHarvestingDataListActivity.class, bundle1);
                break;
            case R.id.l_sampling:
                Bundle bundle = new Bundle();
                bundle.putString("GeoJson", lat_lon);
                launchWithBundle(context, HomeSamplingDataListActivity.class, bundle);
                break;
            case R.id.l_track:
                Intent intent = new Intent(Home_Page_Activity.this, EmployeRegionActivity.class);
                startActivity(intent);
                break;
            case R.id.l_approve:
                launch(context, ApprovalActivity.class);
                break;
            case R.id.l_todo:
                Bundle bundle23 = new Bundle();
                bundle23.putString("priorityname", "");
                bundle23.putString("statusname", "");
                bundle23.putString("assintype", "");
                bundle23.putString("listtype", "1");
                Activity.launchWithBundle(context, TodoListActivity.class, bundle23);

                break;
            case R.id.l_technical:
                Bundle bundle2 = new Bundle();
                bundle2.putString("GeoJson", lat_lon);
                launchWithBundle(context, HomePondListActivity.class, bundle2);

                break;
            case R.id.img_logout:
                logout();

                break;
            default:
                break;
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            // set the toolbar title
        }
    }

    private String date_validate(String task_date) {
        String d = null;
        try {
            if (!TextUtils.isEmpty(task_date)) {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                Date date = format.parse(task_date);
                DateFormat date_formatter = new SimpleDateFormat("hh:mm a", Locale.getDefault());
                d = date_formatter.format(date);


            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    /* *
     * Set Color for tool bar
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 4) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    //    getCurrentLocation();
                    startAlarmManager();
                }
            }
        }
    }
    private class GetDayInStatus extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.call_for_getDayIn_status((App) getApplication(), settings.getEmployee_ID());
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Executed_Day_IN_status", "-" + new Gson().toJson(success));
            if (success != null && success.getSuccess() != null) {
                if (success.getSuccess().equalsIgnoreCase("1")) {
                    AppLog.write("Check_In", "----" + settings.getCHECKED_IN_TIME());

                    settings.setCHECKED_IN_TIME(success.getCheckin_time());
                    settings.setSTARTING_TIME(success.getStarting_meter());
                    t_in_time.setText(date_validate(success.getCheckin_time()));
                    calculateHours();
                } else if (success.getSuccess().equalsIgnoreCase("2")) {
                    AppLog.write("Not_Checked_In", "----");
                }
            }
        }
    }

    private void logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Are you sure want to Logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                getLogout((App) getApplication());
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void confirmExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Are you sure want to exit, your session will be cleared?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                getLogout((App) getApplication());
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void getLogout(App app) {
        RestApi apiService = app.createRestAdaptor();
        Call<Void> call = apiService.LogoutUser(settings.getEmployee_ID());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                Log.e("Userlogout", "" + response.code());
                cancelAlarmManager();
                Intent i = new Intent(context, LoginActivity.class);
                startActivity(i);
                settings.clearSession();
            }


            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
            }
        });
    }

    private class SendUpdatedLocation extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.postEmployeeGPSLog((App) getApplication(), settings.getEmployee_ID(), p[0], p[1], "", "");
        }

        @Override
        protected void onPostExecute(SuccessMessage successMessage) {
            super.onPostExecute(successMessage);
            dialog.dismiss();
            AppLog.write("Response", "---" + new Gson().toJson(successMessage));
        }
    }

    private void startAlarmManager() {
        Log.e("home", "startAlarmManager");
        Context context = getBaseContext();
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        gpsTrackerIntent = new Intent(context, GpsTrackerAlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(context, 0, gpsTrackerIntent, 0);
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime(),
                1000 * 300, // 60000 = 1 minute
                pendingIntent);
    }

    private void cancelAlarmManager() {
        Log.e("home", "cancelAlarmManager");
        Context context = getBaseContext();
        gpsTrackerIntent = new Intent(context, GpsTrackerAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, gpsTrackerIntent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }
}