package cbs.com.bmr.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.EmployeeTrackingCallback;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.EmployeeTrackingAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.model.EmployeeTrackingList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EmployeeTrackingActivity extends AppCompatActivity implements EmployeeTrackingCallback {
    String id;
    private Context mContext;
    private ArrayList<EmployeeTrackingList> mDocumentsList;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ConfigurationSettings configurationSettings;
    private EmployeeTrackingAdapter mAdapter;
    private EditText editTextSearch;
    private Dialog dialog;
    private MyCustomDialog progressDialog;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_tracking);
        processBundle();
        initObjects();
        initRecyclerView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void processBundle() {
        Intent intent = getIntent();
        id = intent.getStringExtra("EmpId");
    }

    private void initObjects() {

        mContext = this;
        configurationSettings = new ConfigurationSettings(mContext);
        mDocumentsList = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView = findViewById(R.id.recyclerview_DocumentsList);
        editTextSearch = findViewById(R.id.edit_search);
        mImageBack = findViewById(R.id.img_back);
        mAdapter = new EmployeeTrackingAdapter(mContext, mDocumentsList, this);
        getEmployeeList((App) getApplication(), configurationSettings.getEmployee_ID());
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //    mAdapter.getFilter().filter(s.toString());
                filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void filter(String text) {
        ArrayList<EmployeeTrackingList> temp = new ArrayList<>();
        for (EmployeeTrackingList d : mDocumentsList) {
            if (d.getEmp_name().contains(text)) {
                temp.add(d);
            }
        }
        mAdapter.updateList(temp);
    }

    public void getEmployeeList(App app, String loginId) {
        progressDialog = new MyCustomDialog(mContext, "Loading...");
        progressDialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<ArrayList<EmployeeTrackingList>> call = apiService.getEmployeeList(id, loginId);
        call.enqueue(new Callback<ArrayList<EmployeeTrackingList>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<EmployeeTrackingList>> call, @NonNull Response<ArrayList<EmployeeTrackingList>> response) {
                ArrayList<EmployeeTrackingList> mEmployeeList = response.body();
                if (response.isSuccessful() && mEmployeeList != null) {

                    if (mEmployeeList.size() > 0) {
                        progressDialog.dismiss();
                        mDocumentsList.clear();
                        mDocumentsList.addAll(mEmployeeList);
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<EmployeeTrackingList>> call, @NonNull Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onItemClick(int position) {

        DialogChoose(position);
    }

    private void DialogChoose(final int position) {
        dialog = new Dialog(mContext);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_track_choose);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        assert window != null;
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        ImageView Cancel = dialog.findViewById(R.id.img_close);
        TextView RouteHistory = dialog.findViewById(R.id.txt_routehistory);
        TextView currentLocation = dialog.findViewById(R.id.txt_currentlocation);

        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        currentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmployeeTrackingList employeeTrackingList = mAdapter.dataList().get(position);
                Intent intent = new Intent(mContext, EmployeeTrackingMapActivity.class);
                intent.putExtra("EmpId", employeeTrackingList.getId());
                startActivity(intent);
                dialog.dismiss();
            }
        });
        RouteHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmployeeTrackingList employeeTrackingList = mAdapter.dataList().get(position);
                Intent intent = new Intent(mContext, EmployeeRouteHistoryActivity.class);
                intent.putExtra("EmpId", employeeTrackingList.getId());
                intent.putExtra("EmpNameList", employeeTrackingList.getEmp_name());
                startActivity(intent);
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}