package cbs.com.bmr.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.Objects;

import cbs.com.bmr.Listener.FragmentCallback;
import cbs.com.bmr.R;
import cbs.com.bmr.fragment.AssignedByMeFragment;
import cbs.com.bmr.fragment.MytodolistFragment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TodoListActivity extends AppCompatActivity implements View.OnClickListener, FragmentCallback {


    private Context mContext;
    private ImageView mImageBack;
    private TextView mTextMytodolist, mTextAssignedbyme;
    String assintype = "", statusname = "", priorityname = "", listtype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_list);
        processBundle();

        iniCallbacks();

    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            if (!Objects.requireNonNull(bundle.getString("priorityname")).isEmpty()) {
                priorityname = bundle.getString("priorityname");
            }
            if (!Objects.requireNonNull(bundle.getString("statusname")).isEmpty()) {
                statusname = bundle.getString("statusname");
            }
            if (!Objects.requireNonNull(bundle.getString("assintype")).isEmpty()) {
                assintype = bundle.getString("assintype");
            }

            if (bundle.getString("listtype") != null) {
                listtype = bundle.getString("listtype");
            } else {
                listtype = "1";
            }
        }
        initObjects();
    }


    private void iniCallbacks() {
        mImageBack.setOnClickListener(this);
        mTextMytodolist.setOnClickListener(this);
        mTextAssignedbyme.setOnClickListener(this);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initObjects() {
        mContext = this;
        mImageBack = findViewById(R.id.img_back);
        mTextMytodolist = findViewById(R.id.txt_mytodolist);
        mTextAssignedbyme = findViewById(R.id.txt_assignedbyme);


        if (listtype.equals("1")) {
            if (assintype.isEmpty()) {
                launchFragment(MytodolistFragment.newInstance(priorityname, statusname, "0"), false);
            } else {
                launchFragment(MytodolistFragment.newInstance(priorityname, statusname, assintype), false);
            }
            mTextMytodolist.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
            mTextAssignedbyme.setTextColor(ContextCompat.getColor(mContext, R.color.txt_todolist));
        } else {
            mTextMytodolist.setTextColor(ContextCompat.getColor(mContext, R.color.txt_todolist));
            mTextAssignedbyme.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
            if (assintype.isEmpty()) {
                launchFragment(AssignedByMeFragment.newInstance(priorityname, statusname, "1"), false);
            } else {
                launchFragment(AssignedByMeFragment.newInstance(priorityname, statusname, assintype), false);
            }
        }

    }


    @Override
    public void onClick(View v) {
        if (v == mImageBack) {
            onBackPressed();
        } else if (v == mTextMytodolist) {
            mTextMytodolist.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
            mTextAssignedbyme.setTextColor(ContextCompat.getColor(mContext, R.color.txt_todolist));
            if (assintype.isEmpty()) {
                launchFragment(MytodolistFragment.newInstance(priorityname, statusname, "0"), false);
            } else {
                launchFragment(MytodolistFragment.newInstance(priorityname, statusname, assintype), false);
            }
        } else if (v == mTextAssignedbyme) {
            mTextMytodolist.setTextColor(ContextCompat.getColor(mContext, R.color.txt_todolist));
            mTextAssignedbyme.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));

            if (assintype.isEmpty()) {
                launchFragment(AssignedByMeFragment.newInstance(priorityname, statusname, "1"), false);
            } else {
                launchFragment(AssignedByMeFragment.newInstance(priorityname, statusname, assintype), false);
            }
        }
    }

    @Override
    public void launchFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();
    }

}
