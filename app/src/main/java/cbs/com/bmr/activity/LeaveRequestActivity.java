package cbs.com.bmr.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.model.LeaveBalanceList;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static cbs.com.bmr.Utilities.Utils.getDocumentsDate;

public class LeaveRequestActivity extends AppCompatActivity implements View.OnClickListener {


    String duedate;
    private Context mcontext;
    private EditText mEditFromDate, mEditTodate, mEditNoofdays, mEditpurpose;
    private int mYear, mMonth, mDay;
    private Button mBtnSubmit;
    LeaveBalanceList leaveBalanceList;
    int numOfDays;
    ArrayList<LeaveBalanceList> LeaveBalanceList = new ArrayList<>();
    private ConfigurationSettings configurationSettings;
    private RadioGroup mRadioGroupFdate, mRadioLeavetype;
    private double Fromdatehalfday = 0, Todatehalfday = 0;
    private CheckBox FdateCheckbox, TodateCheckbox;
    private String mFdate, mTdate;
    private ImageView mImageback;

    private void initCallbacks() {
        mBtnSubmit.setOnClickListener(this);
        mEditTodate.setOnClickListener(this);
        mEditFromDate.setOnClickListener(this);
        mImageback.setOnClickListener(this);
    }

    private TextView mTextCasual, mTextSick, mTextPrivilage;
    private String Leavetype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_request);

        initObjects();
        initCallbacks();
        getLeaveBalance((App) getApplication());
    }

    private void initObjects() {
        mcontext = this;
        configurationSettings = new ConfigurationSettings(mcontext);
        mImageback = findViewById(R.id.img_back);
        mEditFromDate = findViewById(R.id.edit_fromdate);
        mEditTodate = findViewById(R.id.edit_todate);
        mEditNoofdays = findViewById(R.id.edit_totaldays);
        mRadioLeavetype = findViewById(R.id.radio_leave_type);
        mEditpurpose = findViewById(R.id.edit_purpose);
        mBtnSubmit = findViewById(R.id.btn_submit);
        FdateCheckbox = findViewById(R.id.fdatecheckbox);
        TodateCheckbox = findViewById(R.id.todatecheckbox);
        mRadioGroupFdate = findViewById(R.id.radio_fromdate_group);
        mTextCasual = findViewById(R.id.txt_casual);
        mTextSick = findViewById(R.id.txt_sick);
        mTextPrivilage = findViewById(R.id.txt_privilage);
        initRadioGroup();
        initEditText();
        initCheckbox();

    }

    private void initEditText() {
        mEditFromDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mEditFromDate.setError(null);
                if (s.length() > 0) {
                    mFdate = s.toString();
                    FdateCheckbox.setEnabled(true);
                }
            }
        });

        mEditTodate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mTdate = s.toString();
                    mNumberofDays(mFdate, mTdate);
                }
            }
        });
    }

    private void initRadioGroup() {

        mRadioGroupFdate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.radio_fdate_forenoon) {
                    Fromdatehalfday = 0.5;
                } else if (checkedId == R.id.radio_fdate_Aftnoon) {
                    Fromdatehalfday = 0.5;
                }
            }

        });
        mRadioLeavetype.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.radio_casual) {
                    mTextCasual.setBackgroundResource(R.drawable.bg_select_leavetype);
                    mTextSick.setBackgroundResource(R.drawable.bg_select_leavetype_nonselect);
                    mTextPrivilage.setBackgroundResource(R.drawable.bg_select_leavetype_nonselect);
                    Leavetype = "1";
                } else if (checkedId == R.id.radio_sick) {
                    mTextCasual.setBackgroundResource(R.drawable.bg_select_leavetype_nonselect);
                    mTextSick.setBackgroundResource(R.drawable.bg_select_leavetype);
                    mTextPrivilage.setBackgroundResource(R.drawable.bg_select_leavetype_nonselect);
                    Leavetype = "2";
                } else if (checkedId == R.id.radio_privialge) {
                    mTextCasual.setBackgroundResource(R.drawable.bg_select_leavetype_nonselect);
                    mTextSick.setBackgroundResource(R.drawable.bg_select_leavetype_nonselect);
                    mTextPrivilage.setBackgroundResource(R.drawable.bg_select_leavetype);
                    Leavetype = "3";
                }
            }

        });

    }

    private void initCheckbox() {

        FdateCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    Fromdatehalfday = 0.5;
                    if (!mFdate.isEmpty() && !mTdate.isEmpty()) {
                        mNumberofDays(mFdate, mTdate);
                    }
                } else {
                    Fromdatehalfday = 0;
                    if (!mFdate.isEmpty() && !mTdate.isEmpty()) {
                        mNumberofDays(mFdate, mTdate);
                    }
                }


            }
        });

        TodateCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Todatehalfday = 0.5;
                    if (!mFdate.isEmpty() && !mTdate.isEmpty()) {
                        mNumberofDays(mFdate, mTdate);
                    }
                } else {
                    Todatehalfday = 0;
                    if (!mFdate.isEmpty() && !mTdate.isEmpty()) {
                        mNumberofDays(mFdate, mTdate);
                    }
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == mEditFromDate) {
            mDatePicker();
        } else if (v == mEditTodate) {

            if (!TextUtils.isEmpty(mFdate)) {
                mDatePicker2();
            } else {
                mEditFromDate.requestFocus();
                mEditFromDate.setError("Please Select From Date");
            }

        } else if (v == mBtnSubmit) {

            String Fromdate = mEditFromDate.getText().toString().trim();
            String Todate = mEditTodate.getText().toString().trim();
            String Noofdays = mEditNoofdays.getText().toString().trim();
            String purpose = mEditpurpose.getText().toString().trim();

            if (Validate(Fromdate, Todate, Noofdays, purpose)) {
                CreateLeaveRequest((App) getApplication(), Fromdate, Todate, Noofdays, purpose);
            }
        } else if (v == mImageback) {
            onBackPressed();
        }
    }

    private void CreateLeaveRequest(App app, String fromdate, String todate, String noofdays, String purpose) {
        final MyCustomDialog dialog;
        dialog = new MyCustomDialog(mcontext, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();

        Observable<Response<LeaveBalanceList>> testObservable1 = apiService.createLeaveRequest(configurationSettings.getEmployee_ID(),

                Leavetype, purpose, noofdays, getDocumentsDate(fromdate), getDocumentsDate(todate));
        testObservable1
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<LeaveBalanceList>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }
                    @Override
                    public void onNext(Response<LeaveBalanceList> arrayListResponse) {
                        Log.e("arrayListResponse", "" + arrayListResponse.toString());
                        leaveBalanceList = arrayListResponse.body();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                    @Override
                    public void onComplete() {
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                        if (leaveBalanceList.getSuccess() != null && leaveBalanceList.getSuccess().equals("1")) {
                            Toast.makeText(mcontext, "Leave Request Created", Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        } else if (leaveBalanceList.getSuccess() != null && leaveBalanceList.getSuccess().equals("10")) {
                            Toast.makeText(mcontext, "Invalid Employee", Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        }
                    }
                });
    }

    private boolean Validate(String fromdate, String todate, String noofdays, String purpose) {
        if (TextUtils.isEmpty(fromdate)) {
            mEditFromDate.requestFocus();
            mEditFromDate.setError("Please Select the Fromdate");
            return false;
        } else if (TextUtils.isEmpty(todate)) {
            mEditTodate.requestFocus();
            mEditTodate.setError("Please Select the Todate");
            return false;
        } else if (TextUtils.isEmpty(noofdays)) {
            mEditNoofdays.requestFocus();
            mEditNoofdays.setError("Enter the No of Days");
            return false;
        } else if (TextUtils.isEmpty(purpose)) {
            mEditpurpose.requestFocus();
            mEditpurpose.setError("Enter the purpose of Leave");
            return false;
        }
        return true;
    }

    private void mNumberofDays(String Fromdate, String Todate) {

        if (Fromdate.equals(Todate)) {
            TodateCheckbox.setEnabled(false);
            TodateCheckbox.setChecked(false);
            try {
                Date fromdate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(Fromdate);
                Date todate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(Todate);
                long diff = todate.getTime() - fromdate.getTime();
                numOfDays = (int) (diff / (1000 * 60 * 60 * 24) + 1);
                double total = numOfDays - Fromdatehalfday;
                mEditNoofdays.setText(String.valueOf(total));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            TodateCheckbox.setEnabled(true);
            if (Fromdatehalfday > 0 && Todatehalfday == 0) {
                try {
                    Date fromdate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(Fromdate);
                    Date todate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(Todate);
                    long diff = todate.getTime() - fromdate.getTime();
                    numOfDays = (int) (diff / (1000 * 60 * 60 * 24) + 1);
                    double total = numOfDays - Fromdatehalfday;
                    mEditNoofdays.setText(String.valueOf(total));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else if (Fromdatehalfday == 0 && Todatehalfday == 0) {
                try {
                    Date fromdate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(Fromdate);
                    Date todate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(Todate);
                    long diff = todate.getTime() - fromdate.getTime();
                    numOfDays = (int) (diff / (1000 * 60 * 60 * 24) + 1);
                    mEditNoofdays.setText(String.valueOf(numOfDays));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else if (Todatehalfday > 0 && Fromdatehalfday == 0) {
                try {
                    Date fromdate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(Fromdate);
                    Date todate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(Todate);
                    long diff = todate.getTime() - fromdate.getTime();
                    numOfDays = (int) (diff / (1000 * 60 * 60 * 24) + 1);
                    double total = numOfDays - Todatehalfday;
                    mEditNoofdays.setText(String.valueOf(total));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else if (Fromdatehalfday > 0 && Todatehalfday > 0) {
                try {
                    Date fromdate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(Fromdate);
                    Date todate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(Todate);
                    long diff = todate.getTime() - fromdate.getTime();
                    numOfDays = (int) (diff / (1000 * 60 * 60 * 24) + 1);
                    double dat = Fromdatehalfday + Todatehalfday;
                    double total = numOfDays - dat;
                    mEditNoofdays.setText(String.valueOf(total));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void mDatePicker() {
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if (monthOfYear > 9) {
                            duedate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            mEditFromDate.setText(duedate);
                            mEditTodate.setText(duedate);
                        } else if (dayOfMonth > 9 || monthOfYear < 9) {
                            duedate = dayOfMonth + "/" + "0" + (monthOfYear + 1) + "/" + year;
                            mEditFromDate.setText(duedate);
                            mEditTodate.setText(duedate);
                        } else {
                            duedate = "0" + dayOfMonth + "/" + "0" + (monthOfYear + 1) + "/" + year;
                            mEditFromDate.setText(duedate);
                            mEditTodate.setText(duedate);
                        }

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();

    }

    private void mDatePicker2() {
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        if (monthOfYear > 9) {
                            duedate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            mEditTodate.setText(duedate);
                        } else if (dayOfMonth > 9 || monthOfYear < 9) {
                            duedate = dayOfMonth + "/" + "0" + (monthOfYear + 1) + "/" + year;
                            mEditTodate.setText(duedate);
                        } else {
                            duedate = "0" + dayOfMonth + "/" + "0" + (monthOfYear + 1) + "/" + year;
                            mEditTodate.setText(duedate);
                        }

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    public void getLeaveBalance(App app) {
        final MyCustomDialog dialog;
        dialog = new MyCustomDialog(mcontext, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Observable<Response<ArrayList<LeaveBalanceList>>> testObservable1 = apiService.getLeavebalance(configurationSettings.getEmployee_ID());
        testObservable1
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ArrayList<LeaveBalanceList>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }
                    @Override
                    public void onNext(Response<ArrayList<LeaveBalanceList>> arrayListResponse) {
                        Log.e("arrayListResponse", "" + arrayListResponse.toString());
                        ArrayList<LeaveBalanceList> arrayList = arrayListResponse.body();
                        if (arrayList != null) {
                            if (arrayList.size() > 0) {
                                LeaveBalanceList = arrayList;
                            }
                        }
                    }
                    @Override
                    public void onError(Throwable e) {

                    }
                    @Override
                    public void onComplete() {
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();

                        for (LeaveBalanceList leaveBalanceList : LeaveBalanceList) {
                            if (leaveBalanceList.getCasual_balance() != null) {
                                mTextCasual.setText("Casual : " + leaveBalanceList.getCasual_balance());
                            }
                            if (leaveBalanceList.getSick_balance() != null) {
                                mTextSick.setText("Sick : " + leaveBalanceList.getSick_balance());
                            }
                            if (leaveBalanceList.getPrivilege_blance() != null) {
                                mTextPrivilage.setText("Privilege : " + leaveBalanceList.getPrivilege_blance());
                            }
                        }


                    }
                });
    }


}
