package cbs.com.bmr.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.SamplingCycle;
import cbs.com.bmr.model.SuccessMessage;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static cbs.com.bmr.Utilities.Utils.getCurrentDateNew;
import static cbs.com.bmr.Utilities.Utils.getCurrentDateTime;
import static cbs.com.bmr.Utilities.Utils.getDocumentsDate;
import static cbs.com.bmr.Utilities.Utils.getPreviousSamplindate;
import static cbs.com.bmr.Utilities.Utils.getSamplindate;

public class HomeHarvestingcycleActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_STORAGE_PERMISSION = 4;
    private static final int REQUEST_FEED_IMAGE = 4;
    MultipartBody.Part imageFileBody;
    private Context mContext;
    private ConfigurationSettings settings;
    private String PondId, customerID, empID, CycleId, GeoJson;
    private boolean IsNewCycle;
    private ImageView mImageBack;
    private Button btn_create_harvesting, btn_full_harvest, btn_partial_harvest;
    private EditText ed_density, ed_survival, ed_dailyFeed, ed_abw, ed_seedsource, ed_ph, mEditPreviousSamplingDate,
            ed_salinity, ed_actual_biomass, ed_expect_biomass, mEditHarvestDate, mEditWSA, edit_feed_total,
            ed_fcr, ed_productivity, ed_h_qty, mEditStockingDate, mEditSeeds, mEditDoc, mEditADG;
    private String density, survival, daily_feed, harvest_flag = "2", abw, ph, salinity, adg, a_biomass, e_biomass, fcr, productivity;
    private String harvest_date, harvest_qty, seed_source;
    private TextView mTextCameraUpload, mTextImageRemove;
    private ImageView mImagePhotoView;
    private MyCustomDialog dialog;
    private String mImagePath, Docdate, Doc, Stockingdate, SeedsinMillion, Acres, TotalFeed, WSA;
    private File file;
    private int mYear, mMonth, mDay;
    private Date Stockingdate1 = new Date();
    private Date harvestdate = new Date();
    private TextInputLayout lay_previousSampling;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_harvesting_cycle);
        mContext = HomeHarvestingcycleActivity.this;
        settings = new ConfigurationSettings(mContext);
        empID = settings.getEmployee_ID();

        processBundle();
        intObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initCallbacks() {
        mImageBack.setOnClickListener(this);
        mEditStockingDate.setOnClickListener(this);
        mEditHarvestDate.setOnClickListener(this);
    }

    String PreviousSamplingdate;

    private void intObjects() {
        mContext = this;
        mImageBack = findViewById(R.id.img_back);
        btn_create_harvesting = findViewById(R.id.btn_create_harvest);
        btn_full_harvest = findViewById(R.id.btn_harvest_full);
        btn_partial_harvest = findViewById(R.id.btn_partial);
        mEditADG = findViewById(R.id.edit_adg);
        mEditDoc = findViewById(R.id.editdoc);
        mEditWSA = findViewById(R.id.editWsa);
        mEditStockingDate = findViewById(R.id.edit_stockingdate);
        mEditSeeds = findViewById(R.id.edit_SeedIn);
        ed_seedsource = findViewById(R.id.edit_seedsource);
        ed_density = findViewById(R.id.edit_density);
        ed_survival = findViewById(R.id.edit_survival);
        edit_feed_total = findViewById(R.id.edit_feed_total);
        ed_dailyFeed = findViewById(R.id.edit_feed_daily);
        ed_h_qty = findViewById(R.id.ed_harvest_qty);
        ed_abw = findViewById(R.id.edit_abw);
        ed_ph = findViewById(R.id.edit_ph);
        ed_salinity = findViewById(R.id.edit_salinity);
        mEditHarvestDate = findViewById(R.id.edit_harvestdate);
        ed_actual_biomass = findViewById(R.id.ed_actual_biomass);
        ed_expect_biomass = findViewById(R.id.ed_expected_biomass);
        ed_fcr = findViewById(R.id.ed_fcr);
        ed_productivity = findViewById(R.id.ed_productivity);
        mTextCameraUpload = findViewById(R.id.btn_camera);
        mTextImageRemove = findViewById(R.id.btn_remove);
        mImagePhotoView = findViewById(R.id.img_photo_view);
        mEditPreviousSamplingDate = findViewById(R.id.edit_previoussamplingdate);
        lay_previousSampling = findViewById(R.id.lay_previousSampling);
        btn_create_harvesting.setOnClickListener(this);
        btn_partial_harvest.setOnClickListener(this);
        btn_full_harvest.setOnClickListener(this);
        mTextCameraUpload.setOnClickListener(this);
        mTextImageRemove.setOnClickListener(this);
        mEditWSA.setText(WSA);
        getCycle((App) getApplicationContext(), PondId, customerID);

        initFormulaCalculations();
    }

    private void initFormulaCalculations() {
        mEditHarvestDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mEditHarvestDate.setError(null);
                if (s.length() > 0) {
                    StockingDateValidation();
                    initDOCFormula();

                }
                if (s.length() > 0 && !Objects.requireNonNull(mEditStockingDate.getText()).toString().trim().isEmpty()) {
                    StockingDateValidation();
                    initDOCFormula();

                } else if (s.length() > 0 && !Objects.requireNonNull(mEditStockingDate.getText()).toString().trim().isEmpty()
                        && !mEditDoc.getText().toString().trim().isEmpty()
                        && !ed_abw.getText().toString().trim().isEmpty()) {
                    initADGFormula();
                }

            }
        });

        mEditStockingDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mEditStockingDate.setError(null);
                if (s.length() > 0 && !Objects.requireNonNull(mEditHarvestDate.getText()).toString().trim().isEmpty()) {
                    StockingDateValidation();
                    initDOCFormula();

                } else if (s.length() > 0 && !mEditHarvestDate.getText().toString().trim().isEmpty()
                        && !mEditDoc.getText().toString().trim().isEmpty()
                        && !ed_abw.getText().toString().trim().isEmpty()) {
                    initADGFormula();
                }
            }
        });
        mEditDoc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mEditDoc.setError(null);
                if (s.length() > 0 && !Objects.requireNonNull(mEditStockingDate.getText()).toString().trim().isEmpty()
                        && !ed_abw.getText().toString().trim().isEmpty()) {
                    initADGFormula();
                }
            }
        });
        ed_abw.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ed_abw.setError(null);
                if (s.length() > 0 && !mEditDoc.getText().toString().trim().isEmpty()) {
                    initADGFormula();
                }

                if (s.length() > 0 && !mEditSeeds.getText().toString().trim().isEmpty()) {
                    initEBioMassFormula();
                }
            }
        });
        mEditSeeds.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                mEditSeeds.setError(null);
                if (s.length() > 0 && !mEditWSA.getText().toString().trim().isEmpty()) {
                    initDensityFormula();
                }
                if (s.length() > 0 && !ed_abw.getText().toString().trim().isEmpty()) {
                    initEBioMassFormula();
                }
            }
        });
        ed_expect_biomass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                ed_expect_biomass.setError(null);
                if (s.length() > 0 && !ed_actual_biomass.getText().toString().trim().isEmpty()) {
                    initSurvivalFormula();
                }
            }
        });
        ed_actual_biomass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                ed_actual_biomass.setError(null);
                if (s.length() > 0) {
                    initProductivityFormula();
                }

                if (s.length() > 0 && !ed_expect_biomass.getText().toString().trim().isEmpty()) {
                    initSurvivalFormula();
                }
                if (s.length() > 0 && !edit_feed_total.getText().toString().trim().isEmpty()) {
                    initFCRFormula();
                }
            }
        });
        edit_feed_total.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                edit_feed_total.setError(null);
                if (s.length() > 0 && !ed_actual_biomass.getText().toString().trim().isEmpty()) {
                    initFCRFormula();
                }

            }
        });

    }

    private void initProductivityFormula() {
        if (Objects.requireNonNull(ed_actual_biomass.getText()).toString().trim().isEmpty()) {
            ed_actual_biomass.requestFocus();
            Toast.makeText(mContext, "ActualBiomass Cannot be Empty", Toast.LENGTH_SHORT).show();

        } else if (Objects.requireNonNull(mEditWSA.getText()).toString().trim().isEmpty()) {
            mEditWSA.requestFocus();
            Toast.makeText(mContext, "WSA Cannot be Empty", Toast.LENGTH_SHORT).show();

        } else {
            double acutalbiomass = Double.parseDouble(ed_actual_biomass.getText().toString().trim());
            double WSA = Double.parseDouble(mEditWSA.getText().toString().trim());
            double Pvalue = 1 / WSA * acutalbiomass;
            ed_productivity.setText(String.format("%.2f", Pvalue));
        }
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            PondId = bundle.getString("PondId");
            customerID = bundle.getString("customerID");
            GeoJson = bundle.getString("GeoJson");
            WSA = bundle.getString("WSA");
            Log.e("GeoJsodsn", GeoJson);
        }
    }

    private void initSurvivalFormula() {
        if (Objects.requireNonNull(ed_expect_biomass.getText()).toString().trim().isEmpty()) {
            ed_expect_biomass.requestFocus();
            Toast.makeText(mContext, "Expected Biomass Cannot be Empty", Toast.LENGTH_SHORT).show();

        } else if (Objects.requireNonNull(ed_actual_biomass.getText()).toString().trim().isEmpty()) {
            ed_actual_biomass.requestFocus();
            Toast.makeText(mContext, "Actual Biomass Cannot be Empty", Toast.LENGTH_SHORT).show();

        } else {
            double acutalbiomass = Double.parseDouble(ed_actual_biomass.getText().toString().trim());
            double EstimatedBioMass = Double.parseDouble(ed_expect_biomass.getText().toString().trim());
            double survival = acutalbiomass / EstimatedBioMass * 100;
            ed_survival.setText(String.format("%.2f", survival));
        }

    }

    private void initDensityFormula() {

        if (Objects.requireNonNull(mEditSeeds.getText()).toString().trim().isEmpty()) {
            mEditSeeds.requestFocus();
            mEditSeeds.setError("Field Cannot be Empty");

        } else if (Objects.requireNonNull(mEditWSA.getText()).toString().trim().isEmpty()) {
            mEditWSA.requestFocus();
            mEditWSA.setError("Field Cannot be Empty");

        } else {
            double SeedsInMillion = Double.parseDouble(mEditSeeds.getText().toString().trim());
            double mEditWSAvalue = Double.parseDouble(mEditWSA.getText().toString().trim());
            double density = SeedsInMillion / (mEditWSAvalue * 4046.86) * 1000000;
            ed_density.setText(String.format("%.2f", density));
        }
    }

    @SuppressLint("DefaultLocale")
    private void initEBioMassFormula() {
        if (Objects.requireNonNull(mEditSeeds.getText()).toString().trim().isEmpty()) {
            mEditSeeds.requestFocus();
            mEditSeeds.setError("Field Cannot be Empty");

        } else if (Objects.requireNonNull(ed_abw.getText()).toString().trim().isEmpty()) {
            ed_abw.requestFocus();
            ed_abw.setError("Field Cannot be Empty");

        } else {
            double SeedsInMillion = Double.parseDouble(mEditSeeds.getText().toString().trim());
            double ABWValue = Double.parseDouble(ed_abw.getText().toString().trim());
            double EstimatedBioMass = SeedsInMillion * ABWValue * 1000;
            ed_expect_biomass.setText(String.format("%.2f", EstimatedBioMass));
        }

    }
    private void initDOCFormula() {
        if (Objects.requireNonNull(mEditStockingDate.getText()).toString().trim().isEmpty()) {
            mEditStockingDate.requestFocus();
            mEditStockingDate.setError("Field Cannot be Empty");

        } else if (Objects.requireNonNull(mEditHarvestDate.getText()).toString().trim().isEmpty()) {
            mEditHarvestDate.requestFocus();
            mEditHarvestDate.setError("Field Cannot be Empty");

        } else {
            String StockingDate = Objects.requireNonNull(mEditStockingDate.getText()).toString().trim();
            String Samplindate = Objects.requireNonNull(mEditHarvestDate.getText()).toString().trim();
            SimpleDateFormat dates = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            try {
                Stockingdate1 = dates.parse(StockingDate);
                harvestdate = dates.parse(Samplindate);
                long diff = harvestdate.getTime() - Stockingdate1.getTime();
                int numOfDays = (int) (diff / (1000 * 60 * 60 * 24));

                mEditDoc.setText(String.valueOf(numOfDays + 1));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private void initFCRFormula() {
        if (Objects.requireNonNull(ed_actual_biomass.getText()).toString().trim().isEmpty()) {
            ed_actual_biomass.requestFocus();
            ed_actual_biomass.setError("Field Cannot be Empty");

        } else if (Objects.requireNonNull(edit_feed_total.getText()).toString().trim().isEmpty()) {
            edit_feed_total.requestFocus();
            edit_feed_total.setError("Field Cannot be Empty");

        } else {
            double acutalbiomass = Double.parseDouble(ed_actual_biomass.getText().toString().trim());
            double edit_feed_total1 = Double.parseDouble(edit_feed_total.getText().toString().trim());
            double Fcrvalue = edit_feed_total1 / acutalbiomass;
            ed_fcr.setText(String.format("%.2f", Fcrvalue));
        }
    }

    private void StockingDateValidation() {

        if (Objects.requireNonNull(mEditStockingDate.getText()).toString().trim().isEmpty()) {
            mEditStockingDate.requestFocus();
            mEditStockingDate.setError("Please enter Valid Date");

        } else if (Objects.requireNonNull(mEditHarvestDate.getText()).toString().trim().isEmpty()) {
            mEditHarvestDate.requestFocus();
            mEditHarvestDate.setError("Please enter Valid Date");

        } else {
            String StockingDate = Objects.requireNonNull(mEditStockingDate.getText()).toString().trim();
            String Samplindate = Objects.requireNonNull(mEditHarvestDate.getText()).toString().trim();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            try {
                Date stardate = sdf.parse(StockingDate);
                Date enddate = sdf.parse(Samplindate);
                if (enddate.before(stardate)) {
                    mEditHarvestDate.requestFocus();
                    mEditHarvestDate.setError("Harvest Date not Lesser than Stocking Date");
                } else if (stardate.after(enddate)) {
                    mEditStockingDate.requestFocus();
                    mEditStockingDate.setError("Stocking Date not greater than Sampling Date");
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    public void getCycle(App app, String pondid, String customerid) {
        dialog = new MyCustomDialog(this, "Loading...");
        dialog.setCancelable(false);
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<ArrayList<SamplingCycle>> call = apiService.getsamplingCycle(pondid, customerid);
        call.enqueue(new Callback<ArrayList<SamplingCycle>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<SamplingCycle>> call, @NonNull Response<ArrayList<SamplingCycle>> response) {
                ArrayList<SamplingCycle> samplingCycleArrayList = response.body();
                if (response.isSuccessful() && samplingCycleArrayList != null) {
                    if (samplingCycleArrayList.size() > 0) {
                        dialog.dismiss();
                        for (SamplingCycle samplingCycle : samplingCycleArrayList) {
                            if (samplingCycle.getSuccess().equals("1")) {
                                IsNewCycle = true;
                                CycleId = "";
                                mEditStockingDate.setFocusable(true);
                                mEditStockingDate.setClickable(true);
                                mEditStockingDate.setBackgroundResource(R.drawable.edit_background_noneditable);
                                mEditSeeds.setBackgroundResource(R.drawable.edit_background_noneditable);
                                mEditSeeds.setFocusable(true);
                            } else if (samplingCycle.getSuccess().equals("2")) {
                                IsNewCycle = false;
                                CycleId = samplingCycle.getCycle_id();
                                mEditStockingDate.setFocusable(false);
                                mEditStockingDate.setClickable(false);
                                mEditSeeds.setFocusable(false);
                                if (samplingCycle.getSeed_stocking() != null) {
                                    mEditSeeds.setText(samplingCycle.getSeed_stocking());
                                }
                                if (samplingCycle.getStocking_date() != null) {
                                    mEditStockingDate.setText(getSamplindate(samplingCycle.getStocking_date()));

                                }
                                if (samplingCycle.getPrevious_sampling() != null) {
                                    lay_previousSampling.setVisibility(View.VISIBLE);
                                    mEditPreviousSamplingDate.setText(getPreviousSamplindate(samplingCycle.getPrevious_sampling()));
                                } else {
                                    lay_previousSampling.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                } else {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<SamplingCycle>> call, @NonNull Throwable t) {
                dialog.dismiss();
            }
        });
    }

    private void initADGFormula() {
        if (Objects.requireNonNull(mEditDoc.getText()).toString().trim().isEmpty()) {
            mEditDoc.requestFocus();
            mEditDoc.setError("Field Cannot be Empty");

        } else if (Objects.requireNonNull(ed_abw.getText()).toString().trim().isEmpty()) {
            ed_abw.requestFocus();
            ed_abw.setError("Field Cannot be Empty");

        } else {
            String ABWValue = Objects.requireNonNull(ed_abw.getText()).toString().trim();
            String DOC = Objects.requireNonNull(mEditDoc.getText()).toString().trim();
            Double ADGValue = Double.parseDouble(ABWValue) / Double.parseDouble(DOC);
            mEditADG.setText(String.format("%.2f", ADGValue));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_harvest_full:
                harvest_flag = "2";
                changeColor(btn_full_harvest);
                break;
            case R.id.btn_partial:
                harvest_flag = "1";
                changeColor(btn_partial_harvest);
                break;
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_create_harvest:
                getValues();
                Date previousdate = null;
                if (Validated()) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                    String StockingDate = Objects.requireNonNull(mEditStockingDate.getText()).toString().trim();
                    String harvestdate = Objects.requireNonNull(mEditHarvestDate.getText()).toString().trim();
                    if (!mEditPreviousSamplingDate.getText().toString().trim().isEmpty()) {
                        PreviousSamplingdate = Objects.requireNonNull(mEditPreviousSamplingDate.getText()).toString().trim();
                        try {
                            previousdate = sdf.parse(PreviousSamplingdate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                    try {
                        Date stardate = sdf.parse(StockingDate);
                        Date enddate = sdf.parse(harvestdate);
                        Date currentdate = sdf.parse(getCurrentDateNew());
                        if (enddate.before(stardate)) {
                            mEditHarvestDate.requestFocus();
                            mEditHarvestDate.setError("Harvest date not lesser than stocking date");
                        } else if (stardate.after(enddate)) {
                            mEditStockingDate.requestFocus();
                            mEditStockingDate.setError("Stocking Date not greater than sampling date");
                        } else if (previousdate != null && enddate.before(previousdate)) {
                            mEditPreviousSamplingDate.requestFocus();
                            mEditPreviousSamplingDate.setError("Harvest date not lesser than previous sampling date");
                        } else if (enddate.after(currentdate)) {
                            mEditHarvestDate.requestFocus();
                            mEditHarvestDate.setError("Harvest Date  not greater than Current Date");
                        } else {
                            if (IsNewCycle) {
                                new CreateNewHarvest_cycle().execute();
                            } else {
                                new CreateHarvest_cycle().execute();
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.btn_camera:

                processPickImage();
                break;
            case R.id.btn_remove:

                processRemove();
                break;
            case R.id.edit_stockingdate:

                if (IsNewCycle) {
                    mDatePicker();
                }
                break;

            case R.id.edit_harvestdate:
                mDatePickerHarvest();
                break;
        }
    }

    private void mDatePickerHarvest() {
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if (dayOfMonth > 9 || monthOfYear > 9) {
                            String Docdate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            mEditHarvestDate.setText(Docdate);
                        } else {
                            String Docdate = "0" + dayOfMonth + "/" + "0" + (monthOfYear + 1) + "/" + year;
                            mEditHarvestDate.setText(Docdate);
                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }


    private boolean Validated() {

        if (TextUtils.isEmpty(SeedsinMillion) && TextUtils.isEmpty(Stockingdate) &&
                TextUtils.isEmpty(Doc) && TextUtils.isEmpty(survival) &&
                TextUtils.isEmpty(daily_feed) && TextUtils.isEmpty(harvest_qty) &&
                TextUtils.isEmpty(abw) && TextUtils.isEmpty(adg) &&
                TextUtils.isEmpty(seed_source) && TextUtils.isEmpty(ph) &&
                TextUtils.isEmpty(salinity) && TextUtils.isEmpty(a_biomass) &&
                TextUtils.isEmpty(e_biomass) && TextUtils.isEmpty(productivity) &&
                TextUtils.isEmpty(density)) {
            ed_dailyFeed.requestFocus();
            ed_dailyFeed.setError("Field Cannot be Empty");
            ed_abw.requestFocus();
            ed_abw.setError("Field Cannot be Empty");
            ed_ph.requestFocus();
            ed_ph.setError("Field Cannot be Empty");
            ed_survival.requestFocus();
            ed_survival.setError("Field Cannot be Empty");
            ed_salinity.requestFocus();
            ed_salinity.setError("Field Cannot be Empty");
            ed_actual_biomass.requestFocus();
            ed_actual_biomass.setError("Field Cannot be Empty");
            ed_expect_biomass.requestFocus();
            ed_expect_biomass.setError("Field Cannot be Empty");
            ed_density.requestFocus();
            ed_density.setError("Field Cannot be Empty");
            mEditADG.requestFocus();
            mEditADG.setError("Field Cannot be Empty");
            ed_fcr.requestFocus();
            ed_fcr.setError("Field Cannot be Empty");
            ed_productivity.requestFocus();
            ed_productivity.setError("Field Cannot be Empty");
            ed_h_qty.requestFocus();
            ed_h_qty.setError("Field Cannot be Empty");
            ed_seedsource.requestFocus();
            ed_seedsource.setError("Field Cannot be Empty");

            mEditDoc.requestFocus();
            mEditDoc.setError("Field Cannot be Empty");
            mEditStockingDate.requestFocus();
            mEditStockingDate.setError("Field Cannot be Empty");
            mEditSeeds.requestFocus();
            mEditSeeds.setError("Field Cannot be Empty");

            return false;
        } else if (TextUtils.isEmpty(survival)) {
            ed_survival.requestFocus();
            ed_survival.setError("Field Cannot be Empty");
            return false;
        } else if (TextUtils.isEmpty(daily_feed)) {
            ed_dailyFeed.requestFocus();
            ed_dailyFeed.setError("Field Cannot be Empty");
            return false;
        } else if (TextUtils.isEmpty(harvest_qty)) {
            ed_h_qty.requestFocus();
            ed_h_qty.setError("Field Cannot be Empty");
            return false;
        } else if (TextUtils.isEmpty(abw)) {
            ed_abw.requestFocus();
            ed_abw.setError("Field Cannot be Empty");
            return false;
        } else if (TextUtils.isEmpty(seed_source)) {
            ed_seedsource.requestFocus();
            ed_seedsource.setError("Field Cannot be Empty");
            return false;
        } else if (TextUtils.isEmpty(ph)) {
            ed_ph.requestFocus();
            ed_ph.setError("Field Cannot be Empty");
            return false;
        } else if (TextUtils.isEmpty(salinity)) {
            ed_salinity.requestFocus();
            ed_salinity.setError("Field Cannot be Empty");
            return false;
        } else if (TextUtils.isEmpty(a_biomass)) {
            ed_actual_biomass.requestFocus();
            ed_actual_biomass.setError("Field Cannot be Empty");
            return false;
        } else if (TextUtils.isEmpty(e_biomass)) {
            ed_expect_biomass.requestFocus();
            ed_expect_biomass.setError("Field Cannot be Empty");
            return false;
        } else if (TextUtils.isEmpty(productivity)) {
            ed_productivity.requestFocus();
            ed_productivity.setError("Field Cannot be Empty");
            return false;
        } else if (TextUtils.isEmpty(density)) {
            ed_density.requestFocus();
            ed_density.setError("Field Cannot be Empty");
            return false;
        }


        return true;
    }

    private void mDatePicker() {
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if (dayOfMonth > 9 || monthOfYear > 9) {
                            Docdate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            mEditStockingDate.setText(Docdate);
                        } else {
                            Docdate = "0" + dayOfMonth + "/" + "0" + (monthOfYear + 1) + "/" + year;
                            mEditStockingDate.setText(Docdate);
                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void processPickImage() {
        if (hasStoragePermission()) {
            pickImage();
        } else {
            requestStoragePermission(REQUEST_STORAGE_PERMISSION);
        }
    }

    private void pickImage() {
        EasyImage.configuration(mContext).setImagesFolderName(getString(R.string.app_name));
        EasyImage.openChooserWithGallery(this, "Select Image", REQUEST_FEED_IMAGE);
    }

    private boolean hasStoragePermission() {
        return ContextCompat.checkSelfPermission(mContext,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestStoragePermission(int permission) {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, permission);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles,
                                       EasyImage.ImageSource source, int type) {
                mImagePath = imageFiles.get(0).getPath();

                Bitmap bitmap = BitmapFactory.decodeFile(mImagePath);
                getResizedBitmap(bitmap, 480, 640);
                Uri tempUri = getImageUri(getApplicationContext(), bitmap);
                Log.e("imagepathdataprofile", "" + tempUri.toString());
                file = new File(getRealPathFromURI(tempUri));

                Log.e("file", "" + file.getName());
                displayImage();

            }
        });
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] imageInByte = bytes.toByteArray();
        long lengthbmp = imageInByte.length;
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
    }

    private void displayImage() {
        mImagePhotoView.setVisibility(View.VISIBLE);
        mTextImageRemove.setVisibility(View.VISIBLE);
        Glide.with(mContext).load(mImagePath).into(mImagePhotoView);
    }

    private void processRemove() {
        mImagePath = null;
        mImagePhotoView.setVisibility(View.GONE);
        mTextImageRemove.setVisibility(View.GONE);
    }

    private void changeColor(Button selected_btn) {
        btn_partial_harvest.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_full_harvest.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_partial_harvest.setTextColor(Color.parseColor("#4e66f5"));
        btn_full_harvest.setTextColor(Color.parseColor("#4e66f5"));
        selected_btn.setBackgroundResource(R.drawable.blue_button_rounded_edges);
        selected_btn.setTextColor(Color.parseColor("#ffffff"));
    }

    private void getValues() {

        harvest_date = mEditHarvestDate.getText().toString().trim();
        daily_feed = ed_dailyFeed.getText().toString().trim();
        abw = ed_abw.getText().toString().trim();
        ph = ed_ph.getText().toString().trim();
        survival = ed_survival.getText().toString().trim();
        salinity = ed_salinity.getText().toString().trim();
        e_biomass = ed_expect_biomass.getText().toString().trim();
        a_biomass = ed_actual_biomass.getText().toString().trim();
        density = ed_density.getText().toString().trim();
        adg = mEditADG.getText().toString().trim();
        fcr = ed_fcr.getText().toString().trim();
        productivity = ed_productivity.getText().toString().trim();
        harvest_qty = ed_h_qty.getText().toString().trim();
        seed_source = ed_seedsource.getText().toString().trim();
        Doc = mEditDoc.getText().toString().trim();
        Stockingdate = mEditStockingDate.getText().toString().trim();
        SeedsinMillion = mEditSeeds.getText().toString().trim();
        Acres = mEditWSA.getText().toString().trim();
        TotalFeed = edit_feed_total.getText().toString().trim();
    }

    private boolean validate() {
        return false;
    }

    private void callHomeFragment() {
        startActivity(new Intent(mContext, Home_Page_Activity.class));
    }

    private class CreateNewHarvest_cycle extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog m_dialog;

        @Override
        protected void onPreExecute() {
            m_dialog = new MyCustomDialog(mContext, "Creating...");
            m_dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();

            RequestBody GeoJson1 = RequestBody.create(MediaType.parse("text/plain"), GeoJson);
            RequestBody cycleID1 = RequestBody.create(MediaType.parse("text/plain"), CycleId);
            RequestBody empID1 = RequestBody.create(MediaType.parse("text/plain"), empID);
            RequestBody PondId1 = RequestBody.create(MediaType.parse("text/plain"), PondId);
            RequestBody customerID1 = RequestBody.create(MediaType.parse("text/plain"), customerID);
            RequestBody harvest_date1 = RequestBody.create(MediaType.parse("text/plain"),  getDocumentsDate(harvest_date));
            RequestBody harvest_flag1 = RequestBody.create(MediaType.parse("text/plain"), harvest_flag);
            RequestBody daily_feed1 = RequestBody.create(MediaType.parse("text/plain"), daily_feed);
            RequestBody abw1 = RequestBody.create(MediaType.parse("text/plain"), abw);
            RequestBody ph1 = RequestBody.create(MediaType.parse("text/plain"), ph);
            RequestBody survival1 = RequestBody.create(MediaType.parse("text/plain"), survival);
            RequestBody salinity1 = RequestBody.create(MediaType.parse("text/plain"), salinity);
            RequestBody e_biomass1 = RequestBody.create(MediaType.parse("text/plain"), e_biomass);
            RequestBody a_biomass1 = RequestBody.create(MediaType.parse("text/plain"), a_biomass);
            RequestBody density1 = RequestBody.create(MediaType.parse("text/plain"), density);
            RequestBody adg1 = RequestBody.create(MediaType.parse("text/plain"), adg);
            RequestBody fcr1 = RequestBody.create(MediaType.parse("text/plain"), fcr);
            RequestBody productivity1 = RequestBody.create(MediaType.parse("text/plain"), productivity);
            RequestBody harvest_qty1 = RequestBody.create(MediaType.parse("text/plain"), harvest_qty);
            RequestBody seed_source1 = RequestBody.create(MediaType.parse("text/plain"), seed_source);
            RequestBody Doc1 = RequestBody.create(MediaType.parse("text/plain"), Doc);
            RequestBody NewCycle = RequestBody.create(MediaType.parse("text/plain"), "1");
            RequestBody Stockingdate1 = RequestBody.create(MediaType.parse("text/plain"), getDocumentsDate(Stockingdate));
            RequestBody SeedsinMillion1 = RequestBody.create(MediaType.parse("text/plain"), SeedsinMillion);
            RequestBody CultureSeedDate = RequestBody.create(MediaType.parse("text/plain"), getCurrentDateTime());
            RequestBody Acres1 = RequestBody.create(MediaType.parse("text/plain"), Acres);
            RequestBody TotalFeed1 = RequestBody.create(MediaType.parse("text/plain"), TotalFeed);
            if (mImagePath != null) {
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                imageFileBody = MultipartBody.Part.createFormData("sampling_file", file.getName(), requestBody);
            } else {
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                imageFileBody = MultipartBody.Part.createFormData("sampling_file", "", requestBody);
            }
            return call.createNewHarvestHome((App) getApplication(), GeoJson1, cycleID1, empID1, PondId1, customerID1, NewCycle,
                    CultureSeedDate, Stockingdate1, SeedsinMillion1, harvest_date1, harvest_flag1,
                    daily_feed1, abw1, survival1, ph1, salinity1, e_biomass1, a_biomass1, density1, adg1, fcr1,
                    productivity1,
                    harvest_qty1, seed_source1, Doc1, Acres1, TotalFeed1, imageFileBody);
        }

        @Override
        protected void onPostExecute(SuccessMessage response) {
            if (null != m_dialog && m_dialog.isShowing())
                m_dialog.dismiss();
            AppLog.write("Respo", "--" + new Gson().toJson(response));
            if (response != null) {
                if (!TextUtils.isEmpty(response.getSuccess()))
                    if (response.getSuccess().equalsIgnoreCase("1")) {
                        Toast.makeText(mContext, "Created..!", Toast.LENGTH_SHORT).show();
                        //callHomeFragment();
                        onBackPressed();
                    } else if (response.getSuccess().equalsIgnoreCase("2")) {
                        Toast.makeText(mContext, "Cycle not created..!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, "Failed to create..!", Toast.LENGTH_SHORT).show();
                    }
            } else {
                Toast.makeText(mContext, "Failed to create..!", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class CreateHarvest_cycle extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog m_dialog;

        @Override
        protected void onPreExecute() {
            m_dialog = new MyCustomDialog(mContext, "Creating...");
            m_dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();

            RequestBody GeoJson1 = RequestBody.create(MediaType.parse("text/plain"), GeoJson);
            RequestBody cycleID1 = RequestBody.create(MediaType.parse("text/plain"), CycleId);
            RequestBody empID1 = RequestBody.create(MediaType.parse("text/plain"), empID);
            RequestBody PondId1 = RequestBody.create(MediaType.parse("text/plain"), PondId);
            RequestBody customerID1 = RequestBody.create(MediaType.parse("text/plain"), customerID);
            RequestBody harvest_date1 = RequestBody.create(MediaType.parse("text/plain"), getDocumentsDate(harvest_date));
            RequestBody harvest_flag1 = RequestBody.create(MediaType.parse("text/plain"), harvest_flag);
            RequestBody daily_feed1 = RequestBody.create(MediaType.parse("text/plain"), daily_feed);
            RequestBody abw1 = RequestBody.create(MediaType.parse("text/plain"), abw);
            RequestBody ph1 = RequestBody.create(MediaType.parse("text/plain"), ph);
            RequestBody survival1 = RequestBody.create(MediaType.parse("text/plain"), survival);
            RequestBody salinity1 = RequestBody.create(MediaType.parse("text/plain"), salinity);
            RequestBody e_biomass1 = RequestBody.create(MediaType.parse("text/plain"), e_biomass);
            RequestBody a_biomass1 = RequestBody.create(MediaType.parse("text/plain"), a_biomass);
            RequestBody density1 = RequestBody.create(MediaType.parse("text/plain"), density);
            RequestBody adg1 = RequestBody.create(MediaType.parse("text/plain"), adg);
            RequestBody fcr1 = RequestBody.create(MediaType.parse("text/plain"), fcr);
            RequestBody productivity1 = RequestBody.create(MediaType.parse("text/plain"), productivity);
            RequestBody harvest_qty1 = RequestBody.create(MediaType.parse("text/plain"), harvest_qty);
            RequestBody seed_source1 = RequestBody.create(MediaType.parse("text/plain"), seed_source);
            RequestBody Doc1 = RequestBody.create(MediaType.parse("text/plain"), Doc);
            RequestBody Acres1 = RequestBody.create(MediaType.parse("text/plain"), Acres);
            RequestBody TotalFeed1 = RequestBody.create(MediaType.parse("text/plain"), TotalFeed);
            if (mImagePath != null) {
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                imageFileBody = MultipartBody.Part.createFormData("sampling_file", file.getName(), requestBody);
            } else {
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                imageFileBody = MultipartBody.Part.createFormData("sampling_file", "", requestBody);
            }
            return call.createHarvestCycleHome((App) getApplication(), GeoJson1, cycleID1, empID1, PondId1, customerID1, harvest_date1, harvest_flag1,
                    daily_feed1, abw1, survival1, ph1, salinity1, e_biomass1, a_biomass1, density1, adg1, fcr1, productivity1,
                    harvest_qty1, seed_source1, Doc1, Acres1, TotalFeed1, imageFileBody);
        }

        @Override
        protected void onPostExecute(SuccessMessage response) {
            if (null != m_dialog && m_dialog.isShowing())
                m_dialog.dismiss();
            AppLog.write("Respo", "--" + new Gson().toJson(response));
            if (response != null) {
                if (!TextUtils.isEmpty(response.getSuccess()))
                    if (response.getSuccess().equalsIgnoreCase("1")) {
                        Toast.makeText(mContext, "Created..!", Toast.LENGTH_SHORT).show();
                        //callHomeFragment();
                        onBackPressed();
                    } else if (response.getSuccess().equalsIgnoreCase("2")) {
                        Toast.makeText(mContext, "Cycle not created..!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, "Failed to create..!", Toast.LENGTH_SHORT).show();
                    }
            } else {
                Toast.makeText(mContext, "Failed to create..!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}