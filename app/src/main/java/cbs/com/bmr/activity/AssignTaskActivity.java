package cbs.com.bmr.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Helper.TaskManager;
import cbs.com.bmr.Listener.TaskDetails_ClickListener;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.CustomerAdapter;
import cbs.com.bmr.adapter.EmployeeNameAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.CustomerList;
import cbs.com.bmr.model.EmployeeList;
import cbs.com.bmr.model.SuccessMessage;
import cbs.com.bmr.model.TaskList;
import cbs.com.bmr.model.TaskScheduler;

import static cbs.com.bmr.Utilities.Activity.launchClearTop;

/*********************************************************************
 * Created by Barani on 22-03-2019 in TableMateNew
 ********************************************************************/
public class AssignTaskActivity extends AppCompatActivity implements View.OnClickListener, TaskDetails_ClickListener {

    private EditText editTaskDate, editTaskTime, editDescription, edit_location, edit_approved_by, t_customer;
    private CustomerAdapter mAdapter;
    private EmployeeNameAdapter mEmployeeAdapter;
    private AutoCompleteTextView edit_customer, edit_assign_to;
    private TimePickerDialog timePickerDialog;
    private DatePickerDialog datePickerDialog;
    private long current_date = System.currentTimeMillis();
    private Calendar myCalendar = Calendar.getInstance();
    private String task_date, time_calc, task_type = "0", task_description = "", task_time, task_date_selected, geo_json_addr;
    private TextView text_date_to_display;
    private Context context;
    private Date s_date;
    private String customerID, customerNAME;
    private String employeeID, employeeNAME, transport_mode = "1";
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
    private DatePickerDialog.OnDateSetListener date_of_task;
    private int comparison;
    private LinearLayout layout_task;
    private Button btn_submit, btn_general, btn_collection, btn_service, btn_sales;
    private Button btn_bus, btn_bike, btn_car, btn_train, btn_flight;
    private int days = 0;
    private LinearLayout l_layout;
    private ImageView mImageBack;

    private ArrayList<TaskScheduler> task_list = new ArrayList<>();
    private ArrayList<TaskScheduler> t_list = new ArrayList<>();
    private ArrayList<TaskList> t_summary_list = new ArrayList<>();
    private ArrayList<CustomerList> customerList = new ArrayList<>();
    private ArrayList<EmployeeList> employeeList = new ArrayList<>();
    private ArrayList<String> customer_list_spinner = new ArrayList<>();
    private ArrayList<String> employee_spinner_list = new ArrayList<>();
    private TaskManager taskManager;
    private ConfigurationSettings settings;
    private LinearLayout l1;
    public Handler handler = new Handler();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        initObjects();
    }

    private void initObjects() {
        context = this;
        taskManager = new TaskManager(context);
        settings = new ConfigurationSettings(context);
        initControls();

        //checkForTaskDetails();

        date_of_task = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                getDate();
            }
        };

        datePickerDialog = new DatePickerDialog(context, date_of_task, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        editTaskDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

        edit_customer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    customerID = customerList.get(position).getId();
                    customerNAME = customerList.get(position).getFirst_name();
                    for (CustomerList c : customerList) {
                        if (c.getId().equalsIgnoreCase(customerID)) {
                            edit_location.setText(c.getAddress1() + ", " + c.getAddress2() + ", " + c.getCity_village() + ", " + c.getState());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    AppLog.write("IOException--", "" + e);
                }
            }
        });

        edit_assign_to.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                employeeID = employeeList.get(position).getId();
                employeeNAME = employeeList.get(position).getFirstname();
            }
        });

        editTaskTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (!TextUtils.isEmpty(task_date)) {
                Calendar mCurrentTime = Calendar.getInstance();
                final int hours = mCurrentTime.get(Calendar.HOUR_OF_DAY);
                final int minute = mCurrentTime.get(Calendar.MINUTE);

                timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        time_calc = selectedHour + ":" + selectedMinute + ":" + "00";
                        getTime(selectedHour, selectedMinute);
                        int hour = selectedHour % 12;
                        editTaskTime.setText(String.format("%02d:%02d %s", hour == 0 ? 12 : hour,
                                selectedMinute, selectedHour < 12 ? "am" : "pm"));
                           /* if (selectedHour > 12) {
                                ed_reservation_time.setText("" + selectedHour + ":" + selectedMinute);
                            } else {
                                ed_reservation_time.setText("" + selectedHour + ":" + selectedMinute);
                            }*/
                    }
                }, hours, minute, true);
                timePickerDialog.setTitle("Select Time");
                timePickerDialog.show();

                String s = editTaskDate.getText().toString().trim();
                if (TextUtils.isEmpty(s)) {
                    editTaskTime.setFocusable(true);
                    editTaskTime.setError("Please select date");
                } else {
                    editTaskTime.setError(null);
                }
            }
        });


        handler.post(new Runnable() {
            @Override
            public void run() {
                initApiCalls();
            }
        });


        /*On submit*/
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate();
                if (validate()) {
                    getTaskDetails();
                }
            }
        });
    }


    private void initApiCalls() {
        new GetTask_List().execute();
        new GetCustomerList().execute();
        new GetEmployeeList().execute();
    }
    @Override
    public void onResume() {
        super.onResume();
    }

    /*
     * Employee Spinner List
     * */
    private void callSpinnerForEmployeeSelection() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_employee_list_spinner);

        final Spinner spinner_employee_list = dialog.findViewById(R.id.employee_name_list);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);

        ArrayAdapter<String> ad = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, employee_spinner_list);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_employee_list.setAdapter(ad);
        spinner_employee_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                employeeID = employeeList.get(position).getId();
                employeeNAME = employeeList.get(position).getFirstname();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(employeeID)) {
                    populateEmployeeDetail(employeeID);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void populateEmployeeDetail(String employeeID) {
        for (EmployeeList e : employeeList) {
            if (employeeID.equalsIgnoreCase(e.getId())) {
                edit_approved_by.setText(e.getFirstname());
            }
        }
    }

    private void checkForTaskDetails() {
        t_list = taskManager.getTaskList();
        AppLog.write("DAY", "--" + new Gson().toJson(t_list));
        if (t_list.size() > 0) {
            //editTaskDate.setVisibility(View.GONE);
            editTaskDate.setEnabled(false);
            getDate_if_pre_selected(t_list.get(0).getTask_date());
        } else {
            editTaskDate.setVisibility(View.VISIBLE);
            editTaskDate.setEnabled(true);
        }
    }

    private void getDate_if_pre_selected(String task_date) {
        editTaskDate.setText(task_date);
        task_date_selected = task_date;
    }

    private void callTaskListBeforeSubmit() {
        if (validate()) {
            l_layout.setVisibility(View.VISIBLE);
            getTaskDetails();
        }
    }

    private void getTaskDetails() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat key_format = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        SimpleDateFormat time_format = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());

        String timeStamp = key_format.format(calendar.getTime());
        try {
            TaskScheduler t_list = new TaskScheduler();
            t_list.setT_id("t" + timeStamp);
            t_list.setTask_id(settings.getEmployee_ID() + "emp" + timeStamp);
            t_list.setTask_date(date_format.format(calendar.getTime()));
            t_list.setTask_type(task_type);
            t_list.setCustomer_id(customerID);
            t_list.setCustomer_name(customerNAME);
            t_list.setDescription(task_description);
            t_list.setCreated_date(date_format.format(calendar.getTime()));
            t_list.setEmp_id(employeeID);
            t_list.setCreated_by_id(settings.getEmployee_ID());
            t_list.setTime(task_date_selected + " " + time_format.format(calendar.getTime()));
            t_list.setLocation(edit_location.getText().toString().trim());
            //t_list.setGeo_json(geo_json_addr);
            task_list.add(t_list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        AppLog.write("T_LIST__", "" + new Gson().toJson(task_list));
        call_task(task_list);
    }

    private void call_task(ArrayList<TaskScheduler> task_list) {
        for (TaskScheduler t1 : task_list) {
            AppLog.write("task_list--", "--" + new Gson().toJson(t1));
            taskManager.addTasks(t1);
            createTask(t1);
        }
    }

    private void createTask(TaskScheduler t1) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String created_date = date_format.format(calendar.getTime());
        String task_id = t1.getTask_id();
        task_date = t1.getTask_date();
        String created_by_id = settings.getEmployee_ID();
        String task_submit = new Gson().toJson(getTaskDetails_list());
        AppLog.write("Tag_1", task_submit);
        new CreateTaskSummaryDetails().execute(task_id, task_date, created_by_id, created_by_id, created_by_id, created_date, task_submit);
    }

    private ArrayList<TaskScheduler> getTaskDetails_list() {
        ArrayList<TaskScheduler> task_List = new ArrayList<>();
        try {
            for (TaskScheduler t : task_list) {
                TaskScheduler t_list = new TaskScheduler();
                t_list.setTask_type(t.getTask_type());
                t_list.setCustomer_id(t.getCustomer_id());
                t_list.setDescription(t.getDescription());
                t_list.setEmp_id(t.getEmp_id());
                t_list.setTime(t.getTime());
                t_list.setLocation(t.getLocation());
                //t_list.setGeo_json(t.getGeo_json());
                t_list.setImage("");
                t_list.setTransport(transport_mode);
                task_List.add(t_list);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return task_List;
    }

    private void initControls() {
        editTaskDate = findViewById(R.id.edit_task_date);
        editTaskTime = findViewById(R.id.edit_task_time);
        mImageBack = findViewById(R.id.img_back);
        editDescription = findViewById(R.id.edit_description);
        edit_customer = findViewById(R.id.edit_customer);
        edit_location = findViewById(R.id.edit_location);
        edit_approved_by = findViewById(R.id.edit_approval);
        edit_assign_to = findViewById(R.id.edit_assign_to);
        t_customer = findViewById(R.id.t_customer);
        layout_task = findViewById(R.id.layout_task);
        btn_submit = findViewById(R.id.submit_btn);

        text_date_to_display = findViewById(R.id.text_date);
        l_layout = findViewById(R.id.l_layout);

        btn_general = findViewById(R.id.btn_general);
        btn_collection = findViewById(R.id.btn_collection);
        btn_service = findViewById(R.id.btn_service);
        btn_sales = findViewById(R.id.btn_sales);
        btn_bike = findViewById(R.id.btn_bike);
        btn_car = findViewById(R.id.btn_car);
        btn_bus = findViewById(R.id.btn_bus);
        btn_train = findViewById(R.id.btn_train);
        btn_flight = findViewById(R.id.btn_flight);


        btn_general.setOnClickListener(this);
        btn_service.setOnClickListener(this);
        btn_collection.setOnClickListener(this);
        btn_sales.setOnClickListener(this);
        btn_bike.setOnClickListener(this);
        btn_car.setOnClickListener(this);
        btn_bus.setOnClickListener(this);
        btn_train.setOnClickListener(this);
        btn_flight.setOnClickListener(this);

        t_customer.setVisibility(View.GONE);

        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getDate() {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy", Locale.getDefault());
        editTaskDate.setText(sdf.format(myCalendar.getTime()));
        Date date = new Date(sdf.format(myCalendar.getTime()));
        Date c_date = new Date(sdf.format(current_date));
        AppLog.write("Date", "--" + sdf.format(myCalendar.getTime()) + "----" + sdf1.format(current_date));
        comparison = date.compareTo(c_date);
        AppLog.write("Comparison", "--" + comparison);
        if (comparison == -1) {
            editTaskDate.setError("Please select current or future date");
            editTaskDate.requestFocus();
            Snackbar.make(layout_task, "Please select current or future date..!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        } else {
            editTaskDate.setError(null);
            task_date = sdf1.format(myCalendar.getTime());
            task_date_selected = sdf1.format(myCalendar.getTime());
        }
    }

    private void getTime(int selectedHour, int selectedMinute) {
        task_date = selectedHour + ":" + "0" + selectedMinute + ":" + "00";
        AppLog.write("Time---", task_date);
        time_calc(task_date);
    }

    private void time_calc(String task_date) {
        try {
            s_date = timeFormat.parse(task_date);
            AppLog.write("Values------******-----", "---" + task_date + "---" + time_calc);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_general:
                task_type = "0";
                changeColor(btn_general);
                break;
            case R.id.btn_collection:
                task_type = "1";
                changeColor(btn_collection);
                break;
            case R.id.btn_service:
                task_type = "2";
                changeColor(btn_service);
                break;
            case R.id.btn_sales:
                task_type = "3";
                changeColor(btn_sales);
                break;
            case R.id.btn_bike:
                transport_mode = "1";
                changeColorT(btn_bike);
                break;
            case R.id.btn_car:
                transport_mode = "2";
                changeColorT(btn_car);
                break;
            case R.id.btn_bus:
                transport_mode = "3";
                changeColorT(btn_bus);
                break;
            case R.id.btn_train:
                transport_mode = "4";
                changeColorT(btn_train);
                break;
            case R.id.btn_flight:
                transport_mode = "5";
                changeColorT(btn_flight);
                break;
        }
    }

    private void changeColorT(Button selectedButton) {
        btn_bike.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_car.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_bus.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_train.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_flight.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_bike.setTextColor(Color.parseColor("#4e66f5"));
        btn_car.setTextColor(Color.parseColor("#4e66f5"));
        btn_bus.setTextColor(Color.parseColor("#4e66f5"));
        btn_train.setTextColor(Color.parseColor("#4e66f5"));
        btn_flight.setTextColor(Color.parseColor("#4e66f5"));

        selectedButton.setBackgroundResource(R.drawable.blue_button_rounded_edges);
        selectedButton.setTextColor(Color.parseColor("#ffffff"));
    }

    private boolean validate() {
        boolean isValid = true;
        if (editTaskDate.getText().toString().equals("")) {
            editTaskDate.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            editTaskDate.setError(null);
            task_date = editTaskDate.getText().toString().trim();
        }
        if (edit_assign_to.getText().toString().equals("")) {
            edit_assign_to.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            edit_assign_to.setError(null);
        }

        if (edit_customer.getText().toString().equals("")) {
            edit_customer.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            edit_customer.setError(null);
        }
        /*if (editTaskTime.getText().toString().equals("")) {
            editTaskTime.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            editTaskTime.setError(null);
            task_time = editTaskTime.getText().toString().trim();
        }*/

        task_description = editDescription.getText().toString().trim();

        return isValid;
    }

    private void changeColor(Button selectedButton) {
        btn_general.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_collection.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_service.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_sales.setBackgroundResource(R.drawable.grey_hollow_round_button);

        btn_general.setTextColor(Color.parseColor("#4e66f5"));
        btn_service.setTextColor(Color.parseColor("#4e66f5"));
        btn_collection.setTextColor(Color.parseColor("#4e66f5"));
        btn_sales.setTextColor(Color.parseColor("#4e66f5"));

        selectedButton.setBackgroundResource(R.drawable.blue_button_rounded_edges);
        selectedButton.setTextColor(Color.parseColor("#ffffff"));
    }

    private void setDate() {
        text_date_to_display.setText(editTaskDate.getText().toString().trim());
    }

    @Override
    public void onTaskClick(int position, String id, String check) {
    }

    @Override
    public void feedbackSubmit(String id, String type) {
    }

    @Override
    public void physicalFeedback(String id, String task_type) {
    }

    @Override
    public void onClickEdit(int position, String t_id, String task_id, String date) {
    }

    @Override
    public void onClickApproval(int position, String t_id) {
    }

    private class GetTask_List extends AsyncTask<Void, Void, ArrayList<TaskList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<TaskList> doInBackground(Void... v) {
            final String url = App.IP_ADDRESS + "/index.php/tasksummary/getlisttaskapi";
            AppLog.write("URL:::", url);
            ArrayList<TaskList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getTaskDetailsList((App) getApplication(), settings.getEmployee_ID(), settings.getREGION_ID(), "", "");
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<TaskList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (list != null) {
                t_summary_list.addAll(list);
            }
        }
    }



    private class GetCustomerList extends AsyncTask<Void, Void, ArrayList<CustomerList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<CustomerList> doInBackground(Void... v) {
            ArrayList<CustomerList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getCustomerMasterList((App) getApplication(), settings.getEmployee_ID());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<CustomerList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            Log.e("CustomerList", "" + new Gson().toJson(list));
            customerList.addAll(list);
            mAdapter = new CustomerAdapter(context, R.layout.item_customername, R.id.txt_stations, customerList);
            edit_customer.setThreshold(1);
            edit_customer.showDropDown();
            edit_customer.setAdapter(mAdapter);
        }
    }

    private class CreateTaskSummaryDetails extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Creating Task...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.createTask((App) getApplication(), p[0], p[1], p[2], p[3], p[4], p[5], p[6]);
        }

        @Override
        protected void onPostExecute(SuccessMessage response) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (response != null) {
                if (response.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Task Created..!", Toast.LENGTH_SHORT).show();
                    taskManager.clearTasks();
                    launchClearTop(context, CurrentDayTaskActivity.class);
                } else {
                    Toast.makeText(context, "Failed to create..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Failed to create..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetEmployeeList extends AsyncTask<Void, Void, ArrayList<EmployeeList>> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<EmployeeList> doInBackground(Void... v) {
            ArrayList<EmployeeList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getEmployeeList((App) getApplication(), settings.getEmployee_ID());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<EmployeeList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            employeeList.addAll(list);
            mEmployeeAdapter = new EmployeeNameAdapter(context, R.layout.item_customername, R.id.txt_stations, employeeList);
            edit_assign_to.setThreshold(1);
            edit_assign_to.showDropDown();
            edit_assign_to.setAdapter(mEmployeeAdapter);
        }
    }
}