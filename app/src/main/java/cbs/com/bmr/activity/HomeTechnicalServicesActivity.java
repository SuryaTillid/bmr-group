package cbs.com.bmr.activity;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import cbs.com.bmr.R;
import cbs.com.bmr.adapter.MyPagerAdapter;
import cbs.com.bmr.fragment.HomePhysicalTestFeedback;
import cbs.com.bmr.fragment.HomeTechnicalTestFeedback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HomeTechnicalServicesActivity extends AppCompatActivity implements View.OnClickListener, TabLayout.OnTabSelectedListener {
    private Context mContext;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private MyPagerAdapter mPagerAdapter;
    private List<Fragment> mFragmentList;
    private ImageView mImageBack;
    private String PondId = "", mTaskId = "", FarmerId, CustomerName, GeoJson;
    private TextView toolbar_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_technical_services);
        processBundle();
        initObjects();
        iniCallbacks();
        initTabs();
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            CustomerName = bundle.getString("CustomerName");
            FarmerId = bundle.getString("CustomerId");
            GeoJson = bundle.getString("GeoJson");
        }
    }

    private void iniCallbacks() {
        mImageBack.setOnClickListener(this);
        mTabLayout.addOnTabSelectedListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initObjects() {
        mContext = this;
        mImageBack = findViewById(R.id.img_back);
        mTabLayout = findViewById(R.id.tabs);
        toolbar_title = findViewById(R.id.toolbar_title);
        mViewPager = findViewById(R.id.viewpager);
        mFragmentList = new ArrayList<>();
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFragmentList);
        toolbar_title.setText(CustomerName);
    }

    private void initTabs() {
        mTabLayout.addTab(mTabLayout.newTab().setText("Technical"));
        mFragmentList.add(HomeTechnicalTestFeedback.newInstance(PondId, mTaskId, FarmerId, CustomerName, GeoJson));
        mTabLayout.addTab(mTabLayout.newTab().setText("Feed Quality Test"));
        mFragmentList.add(HomePhysicalTestFeedback.newInstance(PondId, mTaskId, FarmerId, CustomerName, GeoJson));
        mPagerAdapter.notifyDataSetChanged();
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        setTabsFont();
    }

    @Override
    public void onClick(View v) {
        if (v == mImageBack) {
            onBackPressed();
        }
    }

    private void setTabsFont() {
        ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(mContext.getAssets(), "SourceSansPro-Semibold.ttf"), Typeface.BOLD);
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
