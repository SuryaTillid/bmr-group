package cbs.com.bmr.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.TodoListCreateEmpCallback;
import cbs.com.bmr.Listener.TodoListCreatedepartCallback;
import cbs.com.bmr.R;
import cbs.com.bmr.Utilities.Activity;
import cbs.com.bmr.adapter.DepartmentCheckableSpinnerAdapter;
import cbs.com.bmr.adapter.EmployeeCheckableSpinnerAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.model.DepartmentList;
import cbs.com.bmr.model.EmployeeTrackingList;
import cbs.com.bmr.model.MyTodoList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static cbs.com.bmr.Utilities.Utils.getDocumentsDate;

public class CreateTodoListActivity<T> extends AppCompatActivity implements View.OnClickListener, TodoListCreateEmpCallback, TodoListCreatedepartCallback {


    public final List<EmployeeCheckableSpinnerAdapter.SpinnerItem<EmployeeTrackingList>> spinner_items = new ArrayList<>();
    public final List<DepartmentCheckableSpinnerAdapter.SpinnerItem<DepartmentList>> departspinner_items = new ArrayList<>();
    public final Set<EmployeeTrackingList> selected_items = new HashSet<>();
    public final Set<DepartmentList> selected_itemsdepart = new HashSet<>();
    String priorityname = "", AssignedTo = "", AssignedType = "1";
    String duedate;
    private Context mContext;
    private EditText mEditTaskName, mEditDuedate, mEditcomments;
    private ImageView mImageBack, mImageUpload;
    private Spinner mSpinnerPriority, mSpinnerAssignedto;
    private String[] priority = {"Low", "Medium", "High"};
    private Button mBtnSubmit;
    private RadioGroup radioAssignee;
    private ConfigurationSettings configurationSettings;
    private int mYear, mMonth, mDay;
    private MyCustomDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_todo_list);
        initObjects();
        initCallbacks();
        getEployeee((App) getApplication(), configurationSettings.getEmployee_ID());
    }

    Set<String> AssignedList = new HashSet<>();


    private void initObjects() {
        mContext = this;
        configurationSettings = new ConfigurationSettings(mContext);
        radioAssignee = findViewById(R.id.radio_group);
        mSpinnerAssignedto = findViewById(R.id.spinner_assignedto);
        mImageBack = findViewById(R.id.img_back);
        mEditTaskName = findViewById(R.id.edit_task);
        mEditDuedate = findViewById(R.id.edit_duedate);
        mEditcomments = findViewById(R.id.edit_comments);
        mSpinnerPriority = findViewById(R.id.spinner_priority);
        mBtnSubmit = findViewById(R.id.btn_submit);
        ArrayAdapter adapter = new ArrayAdapter<String>(mContext, R.layout.custom_spinner_item, R.id.custom_spinner_item, priority);
        mSpinnerPriority.setAdapter(adapter);
        mSpinnerPriority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mSpinnerPriority.getSelectedItem().toString().equals("Low")) {
                    priorityname = "0";
                } else if (mSpinnerPriority.getSelectedItem().toString().equals("Medium")) {
                    priorityname = "1";
                } else if (mSpinnerPriority.getSelectedItem().toString().equals("High")) {
                    priorityname = "2";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        radioAssignee.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_employee:
                        AssignedType = "1";
                        getEployeee((App) getApplication(), configurationSettings.getEmployee_ID());
                        break;
                    case R.id.radio_department:
                        AssignedType = "2";
                        getDepartment((App) getApplication());
                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == mImageBack) {
            onBackPressed();
        } else if (v == mBtnSubmit) {

            if (Validate(mEditTaskName, mEditDuedate)) {
                processSubmit();
            }

        } else if (v == mEditDuedate) {
            mDatePicker();
        }
    }

    private boolean Validate(EditText taskname, EditText duedate) {

        if (Objects.requireNonNull(taskname.getText()).toString().trim().isEmpty()) {
            taskname.requestFocus();
            taskname.setError("Field cannot be blank");
            return false;
        } else if (Objects.requireNonNull(duedate.getText()).toString().trim().isEmpty()) {
            duedate.requestFocus();
            duedate.setError("Field cannot be blank");
            return false;
        } else if (AssignedTo.isEmpty()) {
            Toast.makeText(mContext, "Please select the Assignee", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void initCallbacks() {
        mBtnSubmit.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
        mEditDuedate.setFocusable(false);
        mEditDuedate.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void processSubmit() {
        String Taskname = mEditTaskName.getText().toString().trim();
        String duedate = mEditDuedate.getText().toString().trim();
        String comments = mEditcomments.getText().toString().trim();
        Log.e("AssignedType", "" + AssignedType);
        Log.e("AssignedTodsd", "" + AssignedTo);
        UpdateList((App) getApplication(), Taskname, "0", priorityname, configurationSettings.getEmployee_ID(),
                    getDocumentsDate(duedate), comments, AssignedTo, AssignedType);

    }

    private void UpdateList(App app, String taskname, String statusname, String priorityname, String employee_id, String duedate,
                            String comments, String assignedTo, String assignedType) {

        dialog = new MyCustomDialog(this, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<MyTodoList> call = apiService.CreateTodoList(taskname, statusname, priorityname, employee_id, duedate, comments, assignedTo, assignedType);
        call.enqueue(new Callback<MyTodoList>() {
            @Override
            public void onResponse(@NonNull Call<MyTodoList> call, @NonNull Response<MyTodoList> response) {
                MyTodoList samplingCycle = response.body();
                if (response.isSuccessful() && samplingCycle != null) {
                    dialog.dismiss();
                    Log.e("dsf", "" + samplingCycle.getSuccess());
                    if (samplingCycle.getSuccess().equals("1")) {
                        Bundle bundle23 = new Bundle();
                        bundle23.putString("priorityname", "");
                        bundle23.putString("statusname", "");
                        bundle23.putString("assintype", "");
                        bundle23.putString("listtype", "1");
                        Activity.launchClearTopWithBundle(mContext, TodoListActivity.class, bundle23);
                    }
                } else {
                    dialog.dismiss();

                }
            }

            @Override
            public void onFailure(@NonNull Call<MyTodoList> call, @NonNull Throwable t) {
                Log.e("dsasd", "" + t.toString());
                dialog.dismiss();
            }
        });
    }

    public void getDepartment(App app) {
        RestApi apiService = app.createRestAdaptor();
        Call<ArrayList<DepartmentList>> call = apiService.getDepartment();
        call.enqueue(new Callback<ArrayList<DepartmentList>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<DepartmentList>> call, @NonNull Response<ArrayList<DepartmentList>> response) {
                ArrayList<DepartmentList> mEmployeeList = response.body();
                Log.e("DepartmentList", "--" + response.toString());
                if (response.isSuccessful() && mEmployeeList != null) {
                    if (mEmployeeList.size() > 0) {
                        for (DepartmentList departmentList : mEmployeeList) {
                            departspinner_items.add(new DepartmentCheckableSpinnerAdapter.SpinnerItem<>
                                    (departmentList, departmentList.getDepartment_name(), departmentList.getId()));

                            DepartmentCheckableSpinnerAdapter adapter = new DepartmentCheckableSpinnerAdapter<>(mContext,
                                    "Select Department", departspinner_items, selected_itemsdepart, CreateTodoListActivity.this);
                            mSpinnerAssignedto.setAdapter(adapter);
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<DepartmentList>> call, @NonNull Throwable t) {
                Log.e("failure", "--" + t.toString());
            }
        });
    }

    private void mDatePicker() {
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if (dayOfMonth > 9 || monthOfYear > 9) {
                            duedate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                            mEditDuedate.setText(duedate);
                        } else {
                            duedate = "0" + dayOfMonth + "-" + "0" + (monthOfYear + 1) + "-" + year;
                            mEditDuedate.setText(duedate);
                        }

                    }
                }, mYear, mMonth, mDay);
        // datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();

    }

    public void getEployeee(App app, String LoginId) {
        RestApi apiService = app.createRestAdaptor();
        Call<ArrayList<EmployeeTrackingList>> call = apiService.getEmployeeList(LoginId);
        call.enqueue(new Callback<ArrayList<EmployeeTrackingList>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<EmployeeTrackingList>> call, @NonNull Response<ArrayList<EmployeeTrackingList>> response) {
                ArrayList<EmployeeTrackingList> mEmployeeList = response.body();
                if (response.isSuccessful() && mEmployeeList != null) {
                    if (mEmployeeList.size() > 0) {

                        for (EmployeeTrackingList employeeTrackingList : mEmployeeList) {
                            spinner_items.add(new EmployeeCheckableSpinnerAdapter.SpinnerItem<>
                                    (employeeTrackingList, employeeTrackingList.getFirstname(),
                                            employeeTrackingList.getId()));
                        }


                        EmployeeCheckableSpinnerAdapter adapter = new EmployeeCheckableSpinnerAdapter<>(mContext,
                                "Select Employee", spinner_items, selected_items, CreateTodoListActivity.this);
                        mSpinnerAssignedto.setAdapter(adapter);


                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<EmployeeTrackingList>> call, @NonNull Throwable t) {
                Log.e("failure", "--" + t.toString());
            }
        });
    }

    @Override
    public <T> void onEmployeeItemClick(Set<T> lists) {
//lists.notify();
        // AssignedTo="";
        AssignedList.add(lists.toString());
        AssignedTo = lists.toString().replaceAll(" ", "");
        Log.e("employee", "--" + AssignedList);


    }

    @Override
    public <T> void onDepartItemClick(Set<T> lists) {

        // AssignedTo="";
        AssignedList.add(lists.toString());
        AssignedTo = lists.toString().replaceAll(" ", "");
        Log.e("deaprtment", "--" + AssignedTo);

    }
}
