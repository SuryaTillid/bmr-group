package cbs.com.bmr.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.model.EmployeeRouteList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EmployeeTrackingMapActivity extends AppCompatActivity implements OnMapReadyCallback {


    String id, date;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private TextView mTextName, mTextdate, mTextBatteryLevel;
    MyCustomDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_tracking_map);
        processBundle();
        initObjects();
        initMap();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    private void initObjects() {
        mTextName = findViewById(R.id.txt_empname);
        mTextdate = findViewById(R.id.txt_datetime);
        mTextBatteryLevel = findViewById(R.id.txt_batterlevel);
    }

    private void processBundle() {
        Intent intent = getIntent();
        id = intent.getStringExtra("EmpId");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        date = dateFormat.format(Calendar.getInstance().getTime());
    }


    private void initMap() {
        if (mMap == null) {
            FragmentManager fm = getSupportFragmentManager();
            mapFragment = ((SupportMapFragment) fm.findFragmentById(R.id.map));
            mapFragment.getMapAsync(EmployeeTrackingMapActivity.this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getEmployeeLocation((App) getApplication(), id, date);
    }


    public void getEmployeeLocation(App app, String empId, String date) {
        dialog = new MyCustomDialog(this, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Log.e("Latfd", "" + empId);

        Call<ArrayList<EmployeeRouteList>> call = apiService.getEmployeeRoute(empId, date);
        call.enqueue(new Callback<ArrayList<EmployeeRouteList>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<EmployeeRouteList>> call, @NonNull Response<ArrayList<EmployeeRouteList>> response) {
                ArrayList<EmployeeRouteList> mEmployeeList = response.body();
                if (response.isSuccessful() && mEmployeeList != null) {

                    if (mEmployeeList.size() > 0) {

                        for (EmployeeRouteList employeeRouteList : mEmployeeList) {


                            String datetime = employeeRouteList.getDatetime();
                            String name = employeeRouteList.getEmp_name();
                            String Battery = employeeRouteList.getBattery_level();

                            if (!employeeRouteList.getGps_coordinates().isEmpty()) {
                                String test = employeeRouteList.getGps_coordinates();
                                test = test.replaceAll("[\\\\[\\\\]\\\":(){}]", "");
                                String[] values = test.split(",");
                                String latitude = values[0].replaceAll("lat", "");
                                String longitude = values[1].replaceAll("lon", "");
                                initgetAddress(latitude, longitude, datetime, name, Battery);
                            }

                            dialog.dismiss();
                        }
                    } else {
                        Toast.makeText(EmployeeTrackingMapActivity.this, "No Location Found", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                        dialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<EmployeeRouteList>> call, @NonNull Throwable t) {
                Log.e("date", "" + t.toString());
                dialog.dismiss();
            }
        });
    }

    private void initgetAddress(String latitude, String longitude, String date, String name, String Battery) {

        mTextName.setText(name);
        mTextdate.setText(date);
        mTextBatteryLevel.setText(String.format("%s%%", Battery));
        String add = "";
        final LatLng myLocation = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        Geocoder geocoder = new Geocoder(EmployeeTrackingMapActivity.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(myLocation.latitude, myLocation.longitude, 1);
            Address obj = addresses.get(0);
            add = obj.getAddressLine(0);

        } catch (IOException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        mMap.moveCamera(CameraUpdateFactory.zoomTo(1));

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            public void onMapLoaded() {
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                        .target(myLocation)
                        .zoom(11)
                        .build()));

            }
        });

        Marker newMarker = mMap.addMarker(new MarkerOptions()
                .position(myLocation)
                .title(add + "\n" + date)
                .snippet(myLocation.toString()));
        newMarker.showInfoWindow();
        mMap.setPadding(0, 0, 0, 0);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
    }

}
