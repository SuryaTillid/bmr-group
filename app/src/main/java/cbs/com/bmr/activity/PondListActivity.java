package cbs.com.bmr.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.FragmentCallback;
import cbs.com.bmr.Listener.Pond_details_click_listener;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.PondNameListAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.CustomerList;
import cbs.com.bmr.model.PondList;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static cbs.com.bmr.Utilities.Activity.launchWithBundle;

/*********************************************************************
 * Created by Barani on 20-08-2019 in TableMateNew
 ***********************************************************************/
public class PondListActivity extends AppCompatActivity implements Pond_details_click_listener, View.OnClickListener, FragmentCallback {

    private PondNameListAdapter adapter;
    private SearchableSpinner edit_customer;
    private LinearLayout l_customer, linear_pond;
    private TextInputLayout t_layout;
    private EditText t_customer;
    private String customerID, customerNAME, TaskId;
    private Context context;
    private ConfigurationSettings settings;
    private RecyclerView rc_pond_list;
    private ArrayList<String> customer_list_spinner = new ArrayList<>();
    private ArrayList<CustomerList> customerList = new ArrayList<>();
    private ArrayList<PondList> pondList = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    private FloatingActionButton fab_pond_create;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pond_list);

        context = this;
        settings = new ConfigurationSettings(context);

        initialize();


        getCustomerValues();


        adapter = new PondNameListAdapter(context, pondList);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        adapter.setPondListListener(this);
        rc_pond_list.setLayoutManager(mLayoutManager);
        rc_pond_list.setAdapter(adapter);

        fab_pond_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putBoolean("IsSampling", true);
                bundle.putString("CustomerName", customerNAME);
                bundle.putString("CustomerId", customerID);

                launchWithBundle(context, PondCreationActivity.class, bundle);
            }
        });

        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void launchFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body1, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    private void getCustomerValues() {
        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null) {
            customerID = bundle.getString("C_ID");
            customerNAME = bundle.getString("C_NAME");
            TaskId = bundle.getString("TaskId");

            Log.e("Camere", customerNAME);
            t_customer.setText(customerNAME);
        }


        new GetPondList().execute(customerID);
    }

    @Override
    public void onResume() {
        super.onResume();
        getCustomerValues();
    }


    private void initialize() {
        mImageBack = findViewById(R.id.img_back);
        edit_customer = findViewById(R.id.edit_customer_name);
        rc_pond_list = findViewById(R.id.rc_pond_list);
        fab_pond_create = findViewById(R.id.fab_pond_create);
        t_layout = findViewById(R.id.t_layout);
        l_customer = findViewById(R.id.l_customer);
        t_customer = findViewById(R.id.t_customer);
        linear_pond = findViewById(R.id.linear_pond);
        //  t_customer.setOnClickListener(this);
    }


    @Override
    public void onPondClick(int position, String id, String cycle_id) {


        Bundle bundle = new Bundle();
        bundle.putString("POND_ID", id);
        bundle.putString("TaskId", TaskId);
        bundle.putString("CYCLE_ID", cycle_id);
        bundle.putString("C_ID", customerID);
        bundle.putString("C_NAME", customerNAME);
        bundle.putString("C_NAME", customerNAME);
        Log.e("customerID", "" + customerID);
        bundle.putString("Noedit", "2");


        launchWithBundle(context, PondDetailedListActivity.class, bundle);


    }

    @Override
    public void onSamplingClick(int position, String id, String cycle_id) {
    }

    @Override
    public void onHistoryClick(int position, String id, String cycle_id) {
    }

    @Override
    public void ondetailsClick(int position, String id, String cycle_id) {
    }

    @Override
    public void onClick(View v) {
        // l_customer.setVisibility(View.VISIBLE);
        //    t_layout.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class GetPondList extends AsyncTask<String, String, ArrayList<PondList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<PondList> doInBackground(String... v) {
            ArrayList<PondList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getPONDName((App) getApplication(), customerID);
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<PondList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != list) {
                pondList.clear();
                for (PondList pond : list) {
                    AppLog.write("Checking_1.", "---------" + new Gson().toJson(pond));
                    pondList.add(pond);
                }
                adapter.MyDataChanged(pondList);
            } else {
                Toast.makeText(context, "No Records Found..!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
