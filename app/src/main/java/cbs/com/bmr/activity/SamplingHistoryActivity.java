package cbs.com.bmr.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.ArrayList;

import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.Sampling_History_Adapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.SampleHarvestHistory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/*********************************************************************
 * Created by Barani on 25-09-2019 in TableMateNew
 ***********************************************************************/
public class SamplingHistoryActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    private RecyclerView rc_history_view;
    private Sampling_History_Adapter adapter;
    private LinearLayoutManager mLayoutManager;
    private String cust_id, cycle_id, TaskId, request, PondId, Customername;
    private ImageView mImageBack;
    private TextView mTextError, mTextCustomerName;
    private ArrayList<SampleHarvestHistory> history_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_history);

        mContext = SamplingHistoryActivity.this;
        getBundleValues();
        initObjects();
        initCallbacks();
        initApiCalls();
    }

    private void initCallbacks() {
        mImageBack.setOnClickListener(this);
    }

    private void getBundleValues() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            cust_id = bundle.getString("customerID");
            cycle_id = bundle.getString("cycleID");
            TaskId = bundle.getString("TaskId");
            request = bundle.getString("Request");
            PondId = bundle.getString("PondId");
            Customername = bundle.getString("CustomerName");
            Log.e("TaskId", "" + TaskId);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initObjects() {
        mImageBack = findViewById(R.id.img_back);
        mTextError = findViewById(R.id.txt_errormessage);
        mTextCustomerName = findViewById(R.id.txt_customer_name);
        rc_history_view = findViewById(R.id.rc_history_list);
        adapter = new Sampling_History_Adapter(mContext, history_list);
        mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rc_history_view.setLayoutManager(mLayoutManager);
        rc_history_view.setAdapter(adapter);
        mTextCustomerName.setText(Customername);
    }

    private void initApiCalls() {
        new CallHarvestHistory().execute();
    }

    @Override
    public void onClick(View v) {

        if (v == mImageBack) {
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class CallHarvestHistory extends AsyncTask<String, String, ArrayList<SampleHarvestHistory>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(mContext, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<SampleHarvestHistory> doInBackground(String... v) {
            RestApiCalls call = new RestApiCalls();

            Log.e("CustId", "" + cust_id);
            Log.e("cycle_id", "" + cycle_id);
            return call.getSamplingHarvestHistory((App) getApplication(), cust_id, cycle_id, PondId, request);
        }

        @Override
        protected void onPostExecute(ArrayList<SampleHarvestHistory> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (list.size() > 0) {
                mTextError.setVisibility(View.GONE);
                history_list.clear();
                for (SampleHarvestHistory pond : list) {
                    AppLog.write("Checking_1.", "---------" + new Gson().toJson(pond));
                    history_list.add(pond);
                }
                adapter.MyDataChanged(history_list);
            } else {
                mTextError.setVisibility(View.VISIBLE);
                rc_history_view.setVisibility(View.INVISIBLE);
            }
        }
    }
}
