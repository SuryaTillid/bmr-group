package cbs.com.bmr.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import cbs.com.bmr.R;
import cbs.com.bmr.Utilities.Activity;

public class TodoListFilterActivity extends AppCompatActivity implements View.OnClickListener {
    public Context mContext;
    String assintype = "", statusname = "", priorityname = "";
    private Spinner mSpinnerCategory, mSpinnerStatus, mSpinnerPriority;

    private Button mBtnFilter;

    private String[] category = {"Select Category", "Employee", "Department"};
    private String[] status = {"Select Status", "Open", "In-Progress", "Completed", "Cancelled"};
    private String[] priority = {"Select Priority", "Low", "Medium", "High"};

    private ImageView mImageback;
    private String listingtype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_list_filter);
        processBundle();
        initObjects();
        initCallbacks();
        initSpinnerview();
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            listingtype = bundle.getString("listtype");
        }
    }

    private void initCallbacks() {
        mBtnFilter.setOnClickListener(this);
        mImageback.setOnClickListener(this);
    }

    private void initObjects() {
        mContext = this;
        mSpinnerCategory = findViewById(R.id.spinner_category);
        mSpinnerStatus = findViewById(R.id.spinner_status);
        mSpinnerPriority = findViewById(R.id.spinner_priority);
        mBtnFilter = findViewById(R.id.submit_filter);
        mImageback = findViewById(R.id.img_back);
    }

    private void initSpinnerview() {

        ArrayAdapter adapter = new ArrayAdapter<String>(mContext, R.layout.custom_spinner_item, R.id.custom_spinner_item, category);
        mSpinnerCategory.setAdapter(adapter);
        ArrayAdapter adapter1 = new ArrayAdapter<String>(mContext, R.layout.custom_spinner_item, R.id.custom_spinner_item, status);
        mSpinnerStatus.setAdapter(adapter1);
        ArrayAdapter adapter2 = new ArrayAdapter<String>(mContext, R.layout.custom_spinner_item, R.id.custom_spinner_item, priority);
        mSpinnerPriority.setAdapter(adapter2);
        mSpinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mSpinnerCategory.getSelectedItem().toString().equals("Employee")) {
                    assintype = "1";
                } else if (mSpinnerCategory.getSelectedItem().toString().equals("Department")) {
                    assintype = "2";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mSpinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mSpinnerStatus.getSelectedItem().toString().equals("Open")) {
                    statusname = "0";
                } else if (mSpinnerStatus.getSelectedItem().toString().equals("In-Progress")) {
                    statusname = "1";
                } else if (mSpinnerStatus.getSelectedItem().toString().equals("Completed")) {
                    statusname = "2";
                } else if (mSpinnerStatus.getSelectedItem().toString().equals("Cancelled")) {
                    statusname = "3";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mSpinnerPriority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mSpinnerPriority.getSelectedItem().toString().equals("Low")) {
                    priorityname = "0";
                } else if (mSpinnerPriority.getSelectedItem().toString().equals("Medium")) {
                    priorityname = "1";
                } else if (mSpinnerPriority.getSelectedItem().toString().equals("High")) {
                    priorityname = "2";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

    }

    @Override
    public void onClick(View v) {

        if (v == mBtnFilter) {
            Bundle bundle = new Bundle();
            bundle.putString("priorityname", priorityname);
            bundle.putString("statusname", statusname);
            bundle.putString("assintype", assintype);
            bundle.putString("listtype", listingtype);
            Activity.launchClearTopWithBundle(mContext, TodoListActivity.class, bundle);
        } else if (v == mImageback) {
            onBackPressed();
        }

    }
}
