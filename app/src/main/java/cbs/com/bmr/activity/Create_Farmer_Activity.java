package cbs.com.bmr.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.DetectConnection;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.StateSpinnerAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.CustomerDetails;
import cbs.com.bmr.model.StateList;
import cbs.com.bmr.model.SuccessMessage;
import cbs.com.bmr.model.ZoneMaster;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/*********************************************************************
 * Created by Barani on 03-10-2019 in TableMateNew
 ***********************************************************************/
public class Create_Farmer_Activity extends AppCompatActivity implements View.OnClickListener {

    public ArrayList<String> StateSet = new ArrayList<>();
    private Button b_own, b_others, b_farmer, b_dealer, b_active, b_in_active, b_customer_create;
    private String type_flag = "Own", category_flag = "2", active_flag = "Active";
    private String fName, lName, primary_address, secondary_address, state, city, contact, comments, zoneID;
    private Context mContext;
    private ArrayList<ZoneMaster> zoneMasterList = new ArrayList<>();
    private ImageView mImageBack;
    private TextView t_reset;
    private String firstName = "", lastName = "", address1 = "", address2 = "", commentts = "";
    private ConfigurationSettings settings;
    String Statename, StateId, CityName;
    ArrayList<StateList> stateListArrayList = new ArrayList<>();
    ArrayList<String> cityArraylist = new ArrayList<>();
    private ArrayList<String> zone_list_spinner = new ArrayList<>();
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    private EditText e_fName, e_lName, e_addr1, e_addr2, e_contact, e_comments;
    private Spinner mSpinnerState, mSpinnerCity, mSpinnerRegion;
    private MyCustomDialog progressDialog;
    private ArrayList<HashMap<String, String>> mStateHashList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_create);
        mContext = this;
        settings = new ConfigurationSettings(mContext);
        getStateList((App) getApplication());

        new GetRegionList().execute();
        initObjects();

        e_contact.requestFocus();

    }

    private void changeColorA(Button selected_button) {
        b_active.setBackgroundResource(R.drawable.grey_hollow_round_button);
        b_in_active.setBackgroundResource(R.drawable.grey_hollow_round_button);
        b_active.setTextColor(Color.parseColor("#4e66f5"));
        b_in_active.setTextColor(Color.parseColor("#4e66f5"));

        selected_button.setBackgroundResource(R.drawable.blue_button_rounded_edges);
        selected_button.setTextColor(Color.parseColor("#ffffff"));
    }

    private void changeColorT(Button selected_button) {
        b_own.setBackgroundResource(R.drawable.grey_hollow_round_button);
        b_others.setBackgroundResource(R.drawable.grey_hollow_round_button);
        b_others.setTextColor(Color.parseColor("#4e66f5"));
        b_own.setTextColor(Color.parseColor("#4e66f5"));

        selected_button.setBackgroundResource(R.drawable.blue_button_rounded_edges);
        selected_button.setTextColor(Color.parseColor("#ffffff"));
    }

    private void changeColor(Button selected_button) {
        b_dealer.setBackgroundResource(R.drawable.grey_hollow_round_button);
        b_farmer.setBackgroundResource(R.drawable.grey_hollow_round_button);
        b_dealer.setTextColor(Color.parseColor("#4e66f5"));
        b_farmer.setTextColor(Color.parseColor("#4e66f5"));

        selected_button.setBackgroundResource(R.drawable.blue_button_rounded_edges);
        selected_button.setTextColor(Color.parseColor("#ffffff"));
    }

    private class CreateCustomer extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog m_dialog;

        @Override
        protected void onPreExecute() {
            m_dialog = new MyCustomDialog(mContext, "Loading...");
            m_dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... strings) {
            RestApiCalls call = new RestApiCalls();
            return call.createCustomerMaster((App) getApplication(), contact, category_flag, fName, lName, primary_address, secondary_address,
                    city, state, zoneID, comments, type_flag, "BMR", active_flag);
        }

        @Override
        protected void onPostExecute(SuccessMessage response) {
            if (null != m_dialog && m_dialog.isShowing())
                m_dialog.dismiss();
            AppLog.write("Response", "--" + new Gson().toJson(response));
            if (response != null) {
                if (!TextUtils.isEmpty(response.getSuccess()))
                    if (response.getSuccess().equalsIgnoreCase("1")) {
                        Toast.makeText(mContext, "Created successfully..!", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    } else if (response.getSuccess().equalsIgnoreCase("0")) {
                        Toast.makeText(mContext, "Farmer  already created..!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, "Failed to create..!", Toast.LENGTH_SHORT).show();
                    }
            } else {
                Toast.makeText(mContext, "Failed to create..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initObjects() {
        mImageBack = findViewById(R.id.img_back);
        e_fName = findViewById(R.id.edit_first_name);
        e_lName = findViewById(R.id.edit_last_name);
        e_addr1 = findViewById(R.id.edit_addr_1);
        e_addr2 = findViewById(R.id.edit_addr_2);
        mSpinnerRegion = findViewById(R.id.spinner_region);
        mSpinnerState = findViewById(R.id.spinner_state);
        mSpinnerCity = findViewById(R.id.spinner_city);
        e_contact = findViewById(R.id.edit_contact);
        e_comments = findViewById(R.id.edit_Comments);
        b_others = findViewById(R.id.btn_others);
        b_own = findViewById(R.id.btn_own);
        b_farmer = findViewById(R.id.btn_farmers);
        b_dealer = findViewById(R.id.btn_dealers);
        b_active = findViewById(R.id.btn_active);
        b_in_active = findViewById(R.id.btn_in_active);
        b_customer_create = findViewById(R.id.btn_create_farmer);
        t_reset = findViewById(R.id.t_reset);
        b_in_active.setOnClickListener(this);
        b_active.setOnClickListener(this);
        e_fName.setOnClickListener(this);
        b_others.setOnClickListener(this);
        b_own.setOnClickListener(this);
        b_farmer.setOnClickListener(this);
        b_dealer.setOnClickListener(this);
        b_customer_create.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
        t_reset.setOnClickListener(this);
        initSpinner();
        //  initFocusChange();
        //  e_contact.requestFocus();
        e_contact.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String mob = e_contact.getText().toString().trim();
                if (!TextUtils.isEmpty(mob)) {
                    if (DetectConnection.isOnline(mContext)) {

                        loadData();

                    } else {
                        Toast.makeText(mContext, "No Internet", Toast.LENGTH_LONG).show();
                    }
                } else {

                }
            }
        });

    }

    private void initFocusChange() {
        e_contact.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                }
            }

        });
    }

    private void loadData() {
        String mobileNumber = e_contact.getText().toString();
        if (!TextUtils.isEmpty(mobileNumber)) {
            String id = settings.getEmployee_ID();
            Log.e("stateListArrayList", "" + id);
            GetContactDetailsByMobile((App) getApplication(), id, mobileNumber);
        }
    }

    private void GetContactDetailsByMobile(App application, String id, String mobileNumber) {
        final MyCustomDialog dialog;
        dialog = new MyCustomDialog(mContext, "Loading...");
        dialog.show();
        RestApi api = application.createRestAdaptor();
        final Call<CustomerDetails> customerDetails = api.getDefaultCustomerDetails(id, mobileNumber);
        customerDetails.enqueue(new Callback<CustomerDetails>() {
            @Override
            public void onResponse(Call<CustomerDetails> call, Response<CustomerDetails> response) {

                CustomerDetails details = response.body();

                if (response.isSuccessful() && details != null) {
                    if (dialog != null) {
                        dialog.dismiss();
                        if (details.getSuccess().equalsIgnoreCase("0")) {

                            displayData(details.getCustomer());
                        } else {
                            resetContent();
                        }
                    }


                    Log.e("stateListArrayList", "" + new Gson().toJson(details.getCustomer()));
                    if (details.getSuccess().equals("0")) {


                    }
                } else {
                    Log.e("stateListArrayList", "customer details null");
                    dialog.dismiss();
                    resetContent();
                }


                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<CustomerDetails> call, Throwable t) {
                dialog.dismiss();
                resetContent();
            }
        });
    }

    private void displayData(CustomerDetails.Customer customer) {

        if (customer != null) {
            Handler handler = new Handler();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (!TextUtils.isEmpty(e_contact.getText().toString().trim())) {
                        // e_contact.requestFocus();
                        e_contact.setError("Number Already Exist");


                    }
                }
            });

            firstName = customer.getFirstName();
            lastName = customer.getLastName();
            address1 = customer.getAddress1();
            address2 = customer.getAdrees2();
            commentts = customer.getComments();
            Statename = customer.getState();


            e_fName.setText(firstName);


            e_lName.setText(lastName);


            e_addr1.setText(address1);


            e_addr2.setText(address2);


            e_comments.setText(commentts);


        } else {
            resetContent();
        }

    }
    private void initSpinner() {
        mSpinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Statename = mStateHashList.get(i).get("Name");
                StateId = mStateHashList.get(i).get("id");
                Log.e("Statd", "" + StateId);
                initCity(StateId);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mSpinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                CityName = mSpinnerCity.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        mSpinnerRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                zoneID = zoneMasterList.get(position).getId();
                String zoneNAME = zoneMasterList.get(position).getZone_name();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private class GetRegionList extends AsyncTask<Void, Void, ArrayList<ZoneMaster>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(mContext, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<ZoneMaster> doInBackground(Void... v) {
            RestApiCalls call = new RestApiCalls();
            return call.getZone_List((App) getApplication());
        }

        @Override
        protected void onPostExecute(ArrayList<ZoneMaster> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            zone_list_spinner.clear();
            for (ZoneMaster z_list : list) {
                zoneMasterList.add(z_list);
                zone_list_spinner.add(z_list.getZone_name());
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mContext, R.layout.custom_spinner_item, R.id.custom_spinner_item, zone_list_spinner);
            mSpinnerRegion.setAdapter(adapter);
        }
    }

    private void initCity(String stateId) {
        cityArraylist.clear();
        cityArraylist.add("Select City");
        for (StateList stateList : stateListArrayList) {
            if (stateList.getId().equals(stateId)) {

                cityArraylist.addAll(stateList.getmCityList());
                ArrayAdapter adapter = new ArrayAdapter<String>(mContext, R.layout.custom_spinner_item, R.id.custom_spinner_item, cityArraylist);
                mSpinnerCity.setAdapter(adapter);
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_active:
                active_flag = "Active";
                changeColorA(b_active);
                break;
            case R.id.btn_in_active:
                active_flag = "Inactive";
                changeColorA(b_in_active);
                break;
            case R.id.btn_own:
                type_flag = "Own";
                changeColorT(b_own);
                break;
            case R.id.btn_others:
                type_flag = "Others";
                changeColorT(b_others);
                break;
            case R.id.btn_farmers:
                category_flag = "2";
                changeColor(b_farmer);
                break;
            case R.id.btn_dealers:
                category_flag = "1";
                changeColor(b_dealer);
                break;
            case R.id.t_reset:
                resetAll();

                break;
            case R.id.btn_create_farmer:
                View selectedView = mSpinnerState.getSelectedView();
                View selectedView1 = mSpinnerCity.getSelectedView();
                if (Statename != null && Statename.equals("Select State")) {
                    TextView selectedTextView = (TextView) selectedView;
                    selectedTextView.setTextColor(Color.RED);
                    selectedTextView.setError("error");
                } else if (CityName != null && CityName.equals("Select City")) {
                    TextView selectedTextView = (TextView) selectedView1;
                    selectedTextView.setTextColor(Color.RED);
                    selectedTextView.setError("error");
                } else {
                    createCustomer();
                }

                break;
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }

    private void resetAll() {
        e_fName.setText("");
        e_lName.setText("");
        e_addr1.setText("");
        e_addr2.setText("");
        e_contact.setText("");
        e_comments.setText("");
        e_contact.requestFocus();
    }

    private void resetContent() {
        e_fName.setText("");
        e_lName.setText("");
        e_addr1.setText("");
        e_addr2.setText("");

        e_comments.setText("");

    }
    private void createCustomer() {
        fName = e_fName.getText().toString().trim();
        lName = e_lName.getText().toString().trim();
        primary_address = e_addr1.getText().toString().trim();
        secondary_address = e_addr2.getText().toString().trim();
        state = Statename;
        city = CityName;
        comments = e_comments.getText().toString().trim();
        contact = e_contact.getText().toString().trim();
        new CreateCustomer().execute();
    }

    private void getStateList(App app) {
        final MyCustomDialog dialog;
        dialog = new MyCustomDialog(mContext, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Observable<Response<ArrayList<StateList>>> testObservable1 = apiService.getStateListRX();
        testObservable1
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ArrayList<StateList>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ArrayList<StateList>> arrayListResponse) {
                        Log.e("stateListArrayList", "" + arrayListResponse.toString());
                        ArrayList<StateList> arrayList = arrayListResponse.body();
                        if (arrayList != null) {
                            if (arrayList.size() > 0) {
                                stateListArrayList = arrayList;

                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                        HashMap<String, String> categorymap1 = new HashMap<>();
                        categorymap1.put("id", "0");
                        categorymap1.put("Name", "Select State");
                        mStateHashList.add(categorymap1);
                        if (stateListArrayList.size() > 0) {
                            for (StateList stateList : stateListArrayList) {
                                HashMap<String, String> categorymap = new HashMap<>();
                                categorymap.put("id", String.valueOf(stateList.getId()));
                                categorymap.put("Name", stateList.getState_name());
                                mStateHashList.add(categorymap);
                            }
                            StateSpinnerAdapter stateSpinnerAdapter = new StateSpinnerAdapter(mContext, mStateHashList);
                            mSpinnerState.setAdapter(stateSpinnerAdapter);
                        }
                    }
                });


    }


}