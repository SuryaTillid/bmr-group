package cbs.com.bmr.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.EmployeeTrackingCallback;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.RegionListAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.model.EmployeeRegionList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EmployeRegionActivity extends AppCompatActivity implements EmployeeTrackingCallback {

    private Context mContext;

    private ArrayList<EmployeeRegionList> mDocumentsList;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ConfigurationSettings configurationSettings;
    private RegionListAdapter mAdapter;
    private TextView mTextName, mTextdate;

    MyCustomDialog dialog;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employe_region);
        initObjects();
        initRecyclerView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initObjects() {

        mContext = this;
        configurationSettings = new ConfigurationSettings(mContext);
        mDocumentsList = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView = findViewById(R.id.recyclerview_DocumentsList);
        mImageBack = findViewById(R.id.img_back);
        mAdapter = new RegionListAdapter(mContext, mDocumentsList, this);
        getEmployeeList((App) getApplication(), configurationSettings.getEmployee_ID());

        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, Home_Page_Activity.class);
        startActivity(i);
    }

    public void getEmployeeList(App app, String loginId) {
        dialog = new MyCustomDialog(mContext, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<ArrayList<EmployeeRegionList>> call = apiService.getRegionList(loginId);
        call.enqueue(new Callback<ArrayList<EmployeeRegionList>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<EmployeeRegionList>> call, @NonNull Response<ArrayList<EmployeeRegionList>> response) {
                ArrayList<EmployeeRegionList> mEmployeeList = response.body();
                Log.e("Region", "--" + response.toString());
                if (response.isSuccessful() && mEmployeeList != null) {
                    dialog.dismiss();
                    if (mEmployeeList.size() > 0) {
                        mDocumentsList.clear();
                        mDocumentsList.addAll(mEmployeeList);
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<EmployeeRegionList>> call, @NonNull Throwable t) {
                Log.e("failure", "--" + t.toString());
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onItemClick(int position) {
        EmployeeRegionList employeeTrackingList = mDocumentsList.get(position);
        Intent intent = new Intent(mContext, EmployeeTrackingActivity.class);
        intent.putExtra("EmpId", employeeTrackingList.getId());
        startActivity(intent);
    }
}