package cbs.com.bmr.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.CustomerNameAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.CustomerList;
import cbs.com.bmr.model.PondDetailedList;
import cbs.com.bmr.model.SuccessMessage;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/*********************************************************************
 * Created by Barani on 21-08-2019 in TableMateNew
 ***********************************************************************/
public class PondUpdateActivity extends AppCompatActivity implements View.OnClickListener {

    private ConfigurationSettings settings;
    private Context context;
    private TextView t_header;
    private EditText e_customer, e_size, e_density, e_comments, e_reference, e_wsa, e_salinity, e_seed_stocking, e_ph, e_pond_name;
    private String stocking_date, recorded_date, emp_id;
    private Button btn_update_pond, btn_active, btn_inactive;
    private String active_status;
    private ArrayList<CustomerList> customerList = new ArrayList<>();
    private ArrayList<String> customer_list_spinner = new ArrayList<>();
    private String customerID, customerNAME;
    private CustomerNameAdapter adapter;
    private String cycle_id, pond_id;
    public Handler handler = new Handler();
    private ImageView mImageBack;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pond_creation);

        context = this;
        settings = new ConfigurationSettings(context);
        emp_id = settings.getEmployee_ID();

        Bundle bundle = getIntent().getExtras();
        cycle_id = bundle.getString("CYCLE_ID");
        pond_id = bundle.getString("POND_ID");
        customerNAME = bundle.getString("CustomerName");

        initialize();

        handler.post(new Runnable() {
            @Override
            public void run() {
                initApiCalls();
            }
        });



        btn_update_pond.setText("UPDATE POND");

        btn_update_pond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                String size = e_size.getText().toString();
                String density = e_density.getText().toString();
                String comments = e_comments.getText().toString();
                String reference = e_reference.getText().toString();
                String wsa = e_wsa.getText().toString();
                String salinity = e_salinity.getText().toString();
                String seed_stocking = e_seed_stocking.getText().toString();
                String ph = e_ph.getText().toString();
                String pond_id = e_pond_name.getText().toString().trim();
                SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                stocking_date = date_format.format(c.getTime());
                recorded_date = date_format.format(c.getTime());
                if (validate()) {
                    new UpdatePOND_Details().execute(comments, size, density, reference, active_status, wsa, salinity, seed_stocking, ph, stocking_date, recorded_date, pond_id);
                }
            }
        });

       /* edit_customer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                customerID = customerList.get(position).getId();
                customerNAME = customerList.get(position).getFirst_name();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    public void onResume() {
        super.onResume();
    }

    private void initApiCalls() {

        new GetPOND_Detailed_List().execute(cycle_id, pond_id);
    }

    private void initialize() {
        // edit_customer = v.findViewById(R.id.edit_customer);
        e_customer = findViewById(R.id.e_customer);
        e_pond_name = findViewById(R.id.edit_pond_name);
        e_comments = findViewById(R.id.edit_comment);
        e_density = findViewById(R.id.edit_density);
        e_ph = findViewById(R.id.edit_ph);
        e_reference = findViewById(R.id.edit_pond_reference);
        e_salinity = findViewById(R.id.edit_salinity);
        e_seed_stocking = findViewById(R.id.edit_seed_stocking);
        e_size = findViewById(R.id.edit_pond_size);
        e_wsa = findViewById(R.id.edit_wsa);
        t_header = findViewById(R.id.toolbar_title);
        btn_update_pond = findViewById(R.id.btn_create_request);
        btn_active = findViewById(R.id.btn_active);
        mImageBack = findViewById(R.id.img_back);
        btn_inactive = findViewById(R.id.btn_in_active);
        btn_inactive.setOnClickListener(this);
        btn_active.setOnClickListener(this);

        t_header.setText("UPDATE POND");
        e_customer.setText(customerNAME);
        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private boolean validate() {
        boolean isValid = true;

        if (e_comments.getText().toString().equals("")) {
            e_comments.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            e_comments.setError(null);
        }

        if (e_pond_name.getText().toString().equals("")) {
            e_pond_name.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            e_pond_name.setError(null);
        }

        if (e_wsa.getText().toString().equals("")) {
            e_wsa.setError("Field cannot be left blank..!");
            isValid = false;
        } else {
            e_wsa.setError(null);
        }

//        if (e_size.getText().toString().equals("")) {
//            e_size.setError("Field cannot be left blank..!");
//            isValid = false;
//        } else {
//            e_size.setError(null);
//        }
//
//        if (e_seed_stocking.getText().toString().equals("")) {
//            e_seed_stocking.setError("Field cannot be left blank..!");
//            isValid = false;
//        } else {
//            e_seed_stocking.setError(null);
//        }
//
//        if (e_salinity.getText().toString().equals("")) {
//            e_salinity.setError("Field cannot be left blank..!");
//            isValid = false;
//        } else {
//            e_salinity.setError(null);
//        }
//
//        if (e_ph.getText().toString().equals("")) {
//            e_ph.setError("Field cannot be left blank..!");
//            isValid = false;
//        } else {
//            e_ph.setError(null);
//        }
//
//        if (e_density.getText().toString().equals("")) {
//            e_density.setError("Field cannot be left blank..!");
//            isValid = false;
//        } else {
//            e_density.setError(null);
//        }
//
//        if (e_reference.getText().toString().equals("")) {
//            e_reference.setError("Field cannot be left blank..!");
//            isValid = false;
//        } else {
//            e_reference.setError(null);
//        }
        return isValid;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_active:
                active_status = "0";
                changeColor(btn_active);
                break;
            case R.id.btn_in_active:
                active_status = "1";
                changeColor(btn_inactive);
                break;
        }
    }

    private void changeColor(Button selected_btn) {
        btn_active.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_inactive.setBackgroundResource(R.drawable.grey_hollow_round_button);
        btn_active.setTextColor(Color.parseColor("#4e66f5"));
        btn_inactive.setTextColor(Color.parseColor("#4e66f5"));
        selected_btn.setBackgroundResource(R.drawable.blue_button_rounded_edges);
        selected_btn.setTextColor(Color.parseColor("#ffffff"));
    }


    private void setPondValues(PondDetailedList p) {
        customerID = p.getFarmer_id();

        Log.e("fdsfsj", "" + p.getFarmer_name());

        e_pond_name.setText(p.getPond_id());
        e_wsa.setText(p.getWSA());
        e_seed_stocking.setText(p.getSeed_stocking());
        e_salinity.setText(p.getSalinity());
        e_ph.setText(p.getPh());
        e_comments.setText(p.getComments());
        e_density.setText(p.getDensity());
        e_size.setText(p.getSize());
        e_reference.setText(p.getPond_refer());
        enableButton_active_status(p.getStatus());
    }

    private void enableButton_active_status(String status) {
        switch (status) {
            case "0":
                active_status = "0";
                changeColor(btn_active);
                break;
            case "1":
                active_status = "1";
                changeColor(btn_inactive);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class UpdatePOND_Details extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Updating Pond Details...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            return call.updatePOND((App) getApplication(), emp_id, customerID, pond_id, cycle_id, s[11],
                    s[0], s[1], s[2], s[3], s[4], s[5], s[6], s[7], s[8], s[9], s[10]);
        }

        @Override
        protected void onPostExecute(SuccessMessage response) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (response != null) {
                if (response.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "POND Details Updated..!", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                } else {
                    Toast.makeText(context, "Failed to update..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Failed to update..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetPOND_Detailed_List extends AsyncTask<String, String, PondDetailedList> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected PondDetailedList doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            PondDetailedList list = call.getPOND_Details((App) getApplication(), s[0], s[1]);
            return list;
        }

        @Override
        protected void onPostExecute(PondDetailedList list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != list) {
                setPondValues(list);
            } else {
                Toast.makeText(context, "No Records Found..!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}