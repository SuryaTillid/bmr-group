package cbs.com.bmr.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import cbs.com.bmr.Listener.EmployeeTrackingCallback;
import cbs.com.bmr.R;


public class EmployeeTrackingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mTextName;
    public ImageView mTextTrack;
    public EmployeeTrackingCallback mCallback;

    public EmployeeTrackingHolder(View itemView, EmployeeTrackingCallback callback) {
        super(itemView);
        mCallback = callback;
        mTextName = itemView.findViewById(R.id.txt_Name);
        mTextTrack = itemView.findViewById(R.id.text_track);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            mCallback.onItemClick(getAdapterPosition());
        }
    }
}
