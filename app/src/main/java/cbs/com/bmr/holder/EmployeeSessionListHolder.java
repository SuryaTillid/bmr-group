package cbs.com.bmr.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import cbs.com.bmr.Listener.EmployeeSessionListCallback;
import cbs.com.bmr.R;


public class EmployeeSessionListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mTextName;
    public ImageView mImageStatus;
    public EmployeeSessionListCallback mCallback;

    public EmployeeSessionListHolder(View itemView, EmployeeSessionListCallback callback) {
        super(itemView);
        mCallback = callback;
        mTextName = itemView.findViewById(R.id.txt_Name);
        mImageStatus = itemView.findViewById(R.id.img_status);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            mCallback.onItemClick(getAdapterPosition());
        }
    }
}
