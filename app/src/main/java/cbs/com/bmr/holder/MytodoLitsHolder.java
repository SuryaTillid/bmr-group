package cbs.com.bmr.holder;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import cbs.com.bmr.Listener.MyTodoListCallback;
import cbs.com.bmr.R;


public class MytodoLitsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mTextTaskName, mTextCreatedDate, mTextStatus, mTextDuedate;
    public View mViewPriority;

    public MyTodoListCallback mCallback;

    public MytodoLitsHolder(View itemView, MyTodoListCallback callback) {
        super(itemView);
        mCallback = callback;
        mTextTaskName = itemView.findViewById(R.id.txt_taskname);
        mTextCreatedDate = itemView.findViewById(R.id.txt_createddate);
        mTextStatus = itemView.findViewById(R.id.txt_status);
        mTextDuedate = itemView.findViewById(R.id.txt_dueddate);
        mViewPriority = itemView.findViewById(R.id.view_status);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            mCallback.onItemClick(getAdapterPosition());
        }
    }
}
