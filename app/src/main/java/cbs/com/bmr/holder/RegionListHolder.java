package cbs.com.bmr.holder;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import cbs.com.bmr.Listener.EmployeeTrackingCallback;
import cbs.com.bmr.R;


public class RegionListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mTextName;

    public EmployeeTrackingCallback mCallback;

    public RegionListHolder(View itemView, EmployeeTrackingCallback callback) {
        super(itemView);
        mCallback = callback;
        mTextName = itemView.findViewById(R.id.txt_Name);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            mCallback.onItemClick(getAdapterPosition());
        }
    }
}
