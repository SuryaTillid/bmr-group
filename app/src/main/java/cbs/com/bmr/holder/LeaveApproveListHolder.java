package cbs.com.bmr.holder;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import cbs.com.bmr.Listener.LeaveApproveListCallback;
import cbs.com.bmr.R;


public class LeaveApproveListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mTextName, mTextFdate, mTextNoofdays, mTextStatus;
    public TextView mTextExpFdate, mTextExpTodate, mTextNoofLeaveAvail, mTextLeaveType, mTextPurpose;

    public LeaveApproveListCallback mCallback;
    public LinearLayout mLayoutExpand, mLayoutApprove;
    public Button mBtnApprove, mBtnCancel;
    public View viewfinalline;
    private int i = 0;

    public LeaveApproveListHolder(View itemView, LeaveApproveListCallback callback) {
        super(itemView);
        mCallback = callback;
        mTextName = itemView.findViewById(R.id.txt_leave_name);
        mTextFdate = itemView.findViewById(R.id.txt_leave_fromdate);
        mTextNoofdays = itemView.findViewById(R.id.txt_leave_days);
        mTextStatus = itemView.findViewById(R.id.txt_leave_status);
        mTextExpFdate = itemView.findViewById(R.id.txt_leaveexp_fromdate);
        mTextExpTodate = itemView.findViewById(R.id.txt_leaveexp_todate);
        mTextNoofLeaveAvail = itemView.findViewById(R.id.txt_leaveexp_leaveAvail);
        mTextLeaveType = itemView.findViewById(R.id.txt_leaveexp_type);
        mTextPurpose = itemView.findViewById(R.id.txt_leaveexp_purpose);
        mBtnApprove = itemView.findViewById(R.id.btn_approve);
        mBtnCancel = itemView.findViewById(R.id.btncancel);
        viewfinalline = itemView.findViewById(R.id.view_finalline);


        mLayoutExpand = itemView.findViewById(R.id.expand_layout);
        mLayoutApprove = itemView.findViewById(R.id.layout_approval);
        itemView.setOnClickListener(this);
        mBtnApprove.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            if (i == 0) {
                mLayoutExpand.setVisibility(View.VISIBLE);
                i++;
            } else {
                mLayoutExpand.setVisibility(View.GONE);
                i--;
            }

        } else if (view == mBtnApprove) {
            mCallback.onItemApproveClick(getAdapterPosition(), 1);
        } else if (view == mBtnCancel) {
            mCallback.onItemApproveClick(getAdapterPosition(), 2);
        }
    }
}
