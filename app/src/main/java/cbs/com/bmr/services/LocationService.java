package cbs.com.bmr.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.UUID;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.DetectConnection;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.Location_Details;
import cbs.com.bmr.model.SuccessMessage;

//import com.google.android.gms.common.GooglePlayServicesUtil;

public class LocationService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {

    private static final String TAG = "LocationService";
    private static final long INTERVAL = 1000 * 60;
    private static final long FASTEST_INTERVAL = 1000 * 120;
    public Handler handler = new Handler();
    double lat;
    double lon;
    private String latitude;
    private String longitude;
    private boolean currentlyProcessingLocation = false;
    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;
    private Context mContext;
    private String lat_lon, time_value;
    private ConfigurationSettings settings;
    String Uniqueid;
    int batLevel;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!currentlyProcessingLocation) {
            currentlyProcessingLocation = true;
            startTracking();
        }
        return START_NOT_STICKY;
    }

    private void startTracking() {
        Log.d(TAG, "startTracking");
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            if (!googleApiClient.isConnected() || !googleApiClient.isConnecting()) {
                googleApiClient.connect();
            }
        } else {
            Log.e(TAG, "unable to connect to google play services.");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        mContext = this;
        settings = new ConfigurationSettings(mContext);
        if (Build.VERSION.SDK_INT >= 26) {
            String CHANNEL_ID = "BMR";

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Channel BMR",
                    NotificationManager.IMPORTANCE_NONE);
            channel.setLightColor(Color.TRANSPARENT);
            channel.setLockscreenVisibility(Notification.VISIBILITY_SECRET);

            notificationManager.createNotificationChannel(channel);


            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("BMR Connect")
                    .setAutoCancel(true)

                    .setPriority(NotificationCompat.PRIORITY_MIN)
//                    .setPriority(NotificationCompat.VISIBILITY_SECRET)
                    .setContentText("Location Service Running").build();
            startForeground(1, notification);
        }

        Uniqueid = UUID.randomUUID().toString();
        super.onCreate();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            lat = location.getLatitude();
            lon = location.getLongitude();
            latitude = Double.toString(lat);
            longitude = Double.toString(lon);
            Log.e("longitude", longitude);
            Log.e("latitude", latitude);
            //  Toast.makeText(getApplicationContext(), "Latitude:" + lat + ", Longitude:" + lon, Toast.LENGTH_SHORT).show();
            lat_lon = new Gson().toJson(getLocationDetails(latitude, longitude));
            time_value = getTime();
            BatteryManager bm = (BatteryManager) getSystemService(BATTERY_SERVICE);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                Log.e("BatteryLevel", "" + batLevel);
            }
            if (DetectConnection.isOnline(mContext)) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        new SendUpdatedLocation().execute(time_value, UUID.randomUUID().toString(), lat_lon, String.valueOf(batLevel));
                    }
                });


            }
        }

    }


    private Location_Details getLocationDetails(String lat, String lon) {
        Location_Details location = new Location_Details();
        location.setLatitude(lat);
        location.setLongitude(lon);
        return location;
    }

    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        if (googleApiClient != null && googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected");

        locationRequest = LocationRequest.create();
        locationRequest.setInterval(INTERVAL); // milliseconds
        locationRequest.setFastestInterval(FASTEST_INTERVAL); // the fastest rate in milliseconds at which your app can handle location updates
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //  locationRequest.setSmallestDisplacement(200);

        LocationServices.FusedLocationApi.requestLocationUpdates(
                googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed");

        stopLocationUpdates();
        stopSelf();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "GoogleApiClient connection has been suspend");
    }

    private String getTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String s = sdf.format(timestamp);
        return s;
    }

    @SuppressLint("StaticFieldLeak")
    private class SendUpdatedLocation extends AsyncTask<String, String, SuccessMessage> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.postEmployeeGPSLog((App) getApplication(), settings.getEmployee_ID(), p[0], p[1], p[2], p[3]);
        }

        @Override
        protected void onPostExecute(SuccessMessage successMessage) {
            super.onPostExecute(successMessage);
            Log.e("Response", "" + new Gson().toJson(successMessage));
        }
    }

}




