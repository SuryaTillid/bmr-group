package cbs.com.bmr.Listener;

/*********************************************************************
 * Created by Barani on 26-03-2019 in TableMateNew
 ***********************************************************************/
public interface TaskDetails_ClickListener {
    void onTaskClick(int position, String id, String check);

    void feedbackSubmit(String id, String task_type);

    void physicalFeedback(String id, String task_type);

    void onClickEdit(int position, String t_id, String task_id, String date);

    void onClickApproval(int position, String t_id);
}
