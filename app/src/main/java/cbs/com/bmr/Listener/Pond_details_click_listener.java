package cbs.com.bmr.Listener;

/*********************************************************************
 * Created by Barani on 20-08-2019 in TableMateNew
 ***********************************************************************/
public interface Pond_details_click_listener {
    void onPondClick(int position, String id, String cycle_id);

    void onSamplingClick(int position, String id, String cycle_id);

    void onHistoryClick(int position, String id, String cycle_id);

    void ondetailsClick(int position, String id, String cycle_id);
}
