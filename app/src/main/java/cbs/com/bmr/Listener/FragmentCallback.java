package cbs.com.bmr.Listener;


import androidx.fragment.app.Fragment;


public interface FragmentCallback {
    void launchFragment(Fragment fragment, boolean addToBackStack);


}
