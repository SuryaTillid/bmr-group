package cbs.com.bmr.Listener;

import java.util.Set;

public interface TodoListCreateEmpCallback {
    <T> void onEmployeeItemClick(Set<T> lists);
}
