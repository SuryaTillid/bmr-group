package cbs.com.bmr.Listener;

import java.util.Set;

public interface TodoListCreatedepartCallback {
    <T> void onDepartItemClick(Set<T> lists);
}
