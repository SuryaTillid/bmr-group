package cbs.com.bmr.Listener;

public interface EmployeeSessionListCallback {
    void onItemClick(int position);
}
