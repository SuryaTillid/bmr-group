package cbs.com.bmr.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.MyTodoListCallback;
import cbs.com.bmr.R;
import cbs.com.bmr.Utilities.Activity;
import cbs.com.bmr.activity.EditMyTodoListActivity;
import cbs.com.bmr.activity.TodoListActivity;
import cbs.com.bmr.activity.TodoListFilterActivity;
import cbs.com.bmr.adapter.MyToDolistAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.model.MyTodoList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cbs.com.bmr.Utilities.Activity.launchWithBundle;

public class MytodolistFragment extends Fragment implements MyTodoListCallback, View.OnClickListener {
    public Context mContext;
    String assintype = "", statusname = "", priorityname = "";
    private View view;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private MyToDolistAdapter mAdapter;
    private ArrayList<MyTodoList> mMyTodoList;
    private MyCustomDialog dialog;
    private ConfigurationSettings configurationSettings;
    private TextView mImageFilter, mImageResetFilter;

    public MytodolistFragment() {
    }

    public static MytodolistFragment newInstance(String priorityname, String statusname, String assintype) {
        Bundle bundle = new Bundle();
        bundle.putString("priorityname", priorityname);
        bundle.putString("statusname", statusname);
        bundle.putString("assintype", assintype);
        MytodolistFragment productFragment = new MytodolistFragment();
        productFragment.setArguments(bundle);
        return productFragment;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_todolist, container, false);

        processBundle();
        initObjects();
        initCallbacks();
        initRecyclerView();
        return view;
    }

    private void processBundle() {
        Bundle bundle = this.getArguments();

        if (bundle != null) {
            if (!bundle.getString("priorityname").isEmpty()) {
                priorityname = bundle.getString("priorityname");
            } else {
                priorityname = "";
            }
            if (!bundle.getString("statusname").isEmpty()) {
                statusname = bundle.getString("statusname");
            } else {
                statusname = "";
            }
            if (!bundle.getString("assintype").isEmpty()) {
                assintype = bundle.getString("assintype");
            }
        }

        Log.e("priorityname", priorityname);
        Log.e("statusname", statusname);
        Log.e("assintype", assintype);

    }

    private void initCallbacks() {
        mImageFilter.setOnClickListener(this);
        mImageResetFilter.setOnClickListener(this);
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }


    private void initObjects() {
        mContext = getActivity();
        configurationSettings = new ConfigurationSettings(mContext);
        mMyTodoList = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(mContext);

        mRecyclerView = view.findViewById(R.id.recyclerview_teamlist);
        mImageFilter = view.findViewById(R.id.img_filter);
        mImageResetFilter = view.findViewById(R.id.img_resetfilter);
        mAdapter = new MyToDolistAdapter(mContext, mMyTodoList, this);
        getEmployeeList((App) getActivity().getApplication(), configurationSettings.getEmployee_ID(), assintype, priorityname, statusname);
    }

    @Override
    public void onItemClick(int position) {
        MyTodoList myTodoList = mMyTodoList.get(position);
        Bundle bundle = new Bundle();
        bundle.putString("TaskName", myTodoList.getTask_name());
        bundle.putString("mId", myTodoList.getId());
        bundle.putString("CreatedBy", myTodoList.getCreated_by());
        bundle.putString("DueDate", myTodoList.getTask_due());
        bundle.putString("priority", myTodoList.getPriority());
        bundle.putString("status", myTodoList.getTask_status());
        launchWithBundle(mContext, EditMyTodoListActivity.class, bundle);


    }

    public void getEmployeeList(App app, String empid, String AssignType, String priority, String status) {
        dialog = new MyCustomDialog(mContext, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<ArrayList<MyTodoList>> call = apiService.getMytodolist(empid, AssignType, priority, status);
        call.enqueue(new Callback<ArrayList<MyTodoList>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<MyTodoList>> call, @NonNull Response<ArrayList<MyTodoList>> response) {
                ArrayList<MyTodoList> mEmployeeList = response.body();

                if (response.isSuccessful() && mEmployeeList != null) {
                    dialog.dismiss();
                    if (mEmployeeList.size() > 0) {
                        mMyTodoList.clear();
                        mMyTodoList.addAll(mEmployeeList);
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<MyTodoList>> call, @NonNull Throwable t) {
                Log.e("failure", "--" + t.toString());
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == mImageFilter) {
            Bundle bundle = new Bundle();
            bundle.putString("listtype", "1");
            launchWithBundle(mContext, TodoListFilterActivity.class, bundle);
        } else if (v == mImageResetFilter) {
            Bundle bundle = new Bundle();
            bundle.putString("priorityname", "");
            bundle.putString("statusname", "");
            bundle.putString("assintype", "0");
            bundle.putString("listtype", "1");
            Activity.launchClearTopWithBundle(mContext, TodoListActivity.class, bundle);
            //getEmployeeList((App) getActivity().getApplication(), configurationSettings.getEmployee_ID(), "0", "", "");

        }
    }
}
