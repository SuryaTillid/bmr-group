package cbs.com.bmr.fragment;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.activity.HomePondListActivity;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.model.Location_Details;
import cbs.com.bmr.model.PhysicalTestFeedbackResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cbs.com.bmr.Utilities.Activity.launchClearTopWithBundle;

public class HomePhysicalTestFeedback extends Fragment implements View.OnClickListener {
    ConfigurationSettings configurationSettings;
    RadioGroup mPowdergroup, mHgGroup, mPrideGroup, mMoldGroup, mFibrousGroup, mStichinhGroup;
    EditText mEditFCR, mEditFeed, mEditComments;
    String powdervalue = "", HGValue = "", PrideValue = "", MoldValue = "", FibrousValue = "", StichingValue = "", date, TaskId;
    String FCRValue = "", Feed = "";
    MyCustomDialog dialog;
    private Context context;
    private Button btnSubmit;
    private String PondId, FarmerID, GeoJson, customerName;

    public static HomePhysicalTestFeedback newInstance(String pondId, String TaskId, String FarmerId, String customerName, String GeoJson) {
        Bundle args = new Bundle();
        args.putString("PondId", pondId);
        args.putString("TaskId", TaskId);
        args.putString("Farmer_id", FarmerId);
        args.putString("CustomerName", customerName);
        args.putString("GeoJson", GeoJson);
        HomePhysicalTestFeedback fragment = new HomePhysicalTestFeedback();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_phyiscal_test, container, false);
        context = getActivity();
        configurationSettings = new ConfigurationSettings(context);

        ProcessBundle();

        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()).getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }

        btnSubmit = view.findViewById(R.id.btnSubmitFeedback);
        mEditComments = view.findViewById(R.id.edit_coments);
        mPowdergroup = view.findViewById(R.id.powder_group);
        mHgGroup = view.findViewById(R.id.HG_group);
        mPrideGroup = view.findViewById(R.id.Pride_group);
        mMoldGroup = view.findViewById(R.id.Molds_group);
        mFibrousGroup = view.findViewById(R.id.Fibrous_group);
        mStichinhGroup = view.findViewById(R.id.Stiching_group);
        mEditFCR = view.findViewById(R.id.edit_fcr);
        mEditFeed = view.findViewById(R.id.edit_feed);
        btnSubmit.setOnClickListener(this);
        initRadioListners();
        return view;
    }

    private void ProcessBundle() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            PondId = bundle.getString("PondId");
            FarmerID = bundle.getString("Farmer_id");
            TaskId = bundle.getString("TaskId");
            customerName = bundle.getString("CustomerName");
            GeoJson = bundle.getString("GeoJson");
        }
    }

    private void initRadioListners() {
        mPowdergroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.powder_yes:
                        powdervalue = "1";
                        break;
                    case R.id.powder_no:
                        powdervalue = "0";
                        break;

                }
            }
        });
        mHgGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.HG_yes:
                        HGValue = "1";
                        break;
                    case R.id.HG_no:
                        HGValue = "0";
                        break;

                }
            }
        });
        mPrideGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.Pride_yes:
                        PrideValue = "1";
                        break;
                    case R.id.Pride_no:
                        PrideValue = "0";
                        break;

                }
            }
        });
        mMoldGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.Molds_yes:
                        MoldValue = "1";
                        break;
                    case R.id.Molds_no:
                        MoldValue = "0";
                        break;

                }
            }
        });
        mFibrousGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.Fibrous_yes:
                        FibrousValue = "1";
                        break;
                    case R.id.Fibrous_no:
                        FibrousValue = "0";
                        break;

                }
            }
        });
        mStichinhGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.Stiching_yes:
                        StichingValue = "1";
                        break;
                    case R.id.Stiching_no:
                        StichingValue = "0";
                        break;

                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private Location_Details getLocationDetails(String lat, String lon) {
        Location_Details location = new Location_Details();
        location.setLatitude(lat);
        location.setLongitude(lon);
        return location;
    }

    @Override
    public void onClick(View v) {
        if (v == btnSubmit) {
            processValidate();
        }
    }

    private void processValidate() {
        FCRValue = mEditFCR.getText().toString().trim();
        Feed = mEditFeed.getText().toString().trim();
        String comments = mEditComments.getText().toString().trim();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        date = dateFormat.format(Calendar.getInstance().getTime());


        getEmployeeListGeoJson((App) getActivity().getApplication(), GeoJson,
                configurationSettings.getEmployee_ID(), powdervalue, HGValue, PrideValue, FCRValue, MoldValue, FibrousValue,
                StichingValue, Feed, PondId, FarmerID, date, comments);


    }

    private boolean validateInput(String powdervalue, String hgValue,
                                  String prideValue, String fcrValue,
                                  String moldValue, String fibrousValue,
                                  String stichingValue, String feed) {
        if (powdervalue.isEmpty() && hgValue.isEmpty() &&
                prideValue.isEmpty() && fcrValue.isEmpty() && moldValue.isEmpty() && fibrousValue.isEmpty() && stichingValue.isEmpty() && feed.isEmpty()) {
            Toast.makeText(context, "Please enter all the details", Toast.LENGTH_SHORT).show();
            return false;
        } else if (powdervalue.isEmpty()) {
            Toast.makeText(context, "Please select Powder HG", Toast.LENGTH_SHORT).show();
            return false;
        } else if (hgValue.isEmpty()) {
            Toast.makeText(context, "Please select HG ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (prideValue.isEmpty()) {
            Toast.makeText(context, "Please select Pride ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (fcrValue.isEmpty()) {
            Toast.makeText(context, "Please enter FCR ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (moldValue.isEmpty()) {
            Toast.makeText(context, "Please Select Mold Infection ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (fibrousValue.isEmpty()) {
            Toast.makeText(context, "Please Select Fibrous ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (stichingValue.isEmpty()) {
            Toast.makeText(context, "Please Select Stiching Problem ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (feed.isEmpty()) {
            Toast.makeText(context, "Please enter feed sinking time ", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    public void getEmployeeListGeoJson(App app, String GeoJson, String employee_id, String powdervalue,
                                       String HGValue, String prideValue, String FCRValue,
                                       String moldValue, String fibrousValue, String stichingValue,
                                       String feed, String PondId, String FarmerID, String date, String comments) {
        dialog = new MyCustomDialog(context, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<PhysicalTestFeedbackResponse> call = apiService.PhysicalTestgeo_json(GeoJson, employee_id, date, powdervalue,
                HGValue, prideValue, FCRValue, moldValue, fibrousValue, stichingValue, feed, FarmerID, PondId, comments);
        call.enqueue(new Callback<PhysicalTestFeedbackResponse>() {
            @Override
            public void onResponse(@NonNull Call<PhysicalTestFeedbackResponse> call, @NonNull Response<PhysicalTestFeedbackResponse> response) {
                PhysicalTestFeedbackResponse physicalTestFeedbackResponse = response.body();
                Log.e("Technicalresponse2pp", "" + response.toString());
                if (response.isSuccessful()) {
                    if (physicalTestFeedbackResponse != null) {
                        if (physicalTestFeedbackResponse.getSuccess().equals("1")) {
                            Toast.makeText(getActivity(), "FeedBack Submitted Successfully", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                            redirect_Homepond_list_page();
                        } else if (physicalTestFeedbackResponse.getSuccess().equals("0")) {
                            Toast.makeText(getActivity(), physicalTestFeedbackResponse.getFeedback_message(), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    }
                } else {

                    redirect_Homepond_list_page();
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<PhysicalTestFeedbackResponse> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.e("failure", "" + t.toString());
            }
        });
    }


    private void redirect_Homepond_list_page() {
        Bundle bundle = new Bundle();
        bundle.putString("GeoJson", GeoJson);
        launchClearTopWithBundle(context, HomePondListActivity.class, bundle);
    }


}