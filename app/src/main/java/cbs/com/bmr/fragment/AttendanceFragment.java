package cbs.com.bmr.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.ExceptionHandler;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.On_BackPressed;
import cbs.com.bmr.R;
import cbs.com.bmr.activity.Home_Page_Activity;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.Location_Details;
import cbs.com.bmr.model.SuccessMessage;

import static cbs.com.bmr.Utilities.Utils.getCheckinDate;

/*********************************************************************
 * Created by Barani on 08-04-2019 in TableMateNew
 *********************************************************************/
public class AttendanceFragment extends Fragment implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, On_BackPressed {

    private static final int REQUEST_ENABLE_LOCATION = 4;
    public Double lLat, lLng;
    LocationManager locationManager;
    Location mLastLocation, location;
    private Context context;
    private Button btn_day_in, btn_day_out;
    private TextView t_check_in_time, t_starting_meter;
    private String emp_id, date, lat_lon = null;
    private LinearLayout layout_check_in;
    private ConfigurationSettings settings;
    private EditText edit_start_read, edit_close_read;
    private String close_meter = "0";
    private String DayIN;
    private String checkIN_Time;
    //value for location details
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private String Cur_latitude, Cur_longitude;
    private ImageView mImageBack;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_attendance, container, false);
        context = getActivity();
        settings = new ConfigurationSettings(context);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(getActivity()));

        initControls(view);
        initAPICalls();

        try {
            if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        emp_id = settings.getEmployee_ID();

        return view;
    }

    private void initAPICalls() {
        new GetDayInStatus().execute();
        initLocationRequest();
        buildGoogleApiClient();
        displayLocationSettingsRequest();
        // getLocation();
    }

    private void displayLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest).setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                if (status.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                    try {
                        status.startResolutionForResult(getActivity(), REQUEST_ENABLE_LOCATION);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @SuppressWarnings("deprecation")
    private void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            requestLocationPermission();
        }
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 4);
    }

    private void initControls(View view) {
        btn_day_in = view.findViewById(R.id.btn_day_in);
        btn_day_out = view.findViewById(R.id.btn_day_out);
        t_check_in_time = view.findViewById(R.id.t_check_in_time);
        layout_check_in = view.findViewById(R.id.layout_check_in_time);
        t_starting_meter = view.findViewById(R.id.t_Starting_meter);
        edit_start_read = view.findViewById(R.id.edit_start_read);
        edit_close_read = view.findViewById(R.id.edit_close_read);
        mImageBack = view.findViewById(R.id.img_back);
        btn_day_in.setOnClickListener(this);
        btn_day_out.setOnClickListener(this);
        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    private void getLocation() {
        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, (android.location.LocationListener) this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private Location_Details getLocationDetails(String lat, String lon) {
        Location_Details location = new Location_Details();
        location.setLatitude(lat);
        location.setLongitude(lon);
        return location;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        lLat = location.getLatitude();
        lLng = location.getLongitude();
        getCurrentLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 4) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentLocation();
                }
            }
        }
    }

    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            Cur_latitude = String.valueOf(latitude);
            Cur_longitude = String.valueOf(longitude);
            lat_lon = new Gson().toJson(getLocationDetails(Cur_latitude, Cur_longitude));
            AppLog.write("LON_LAT------", lat_lon);
        }
    }

    @Override
    public void onClick(View v) {
        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        date = date_format.format(calendar.getTime());
        if (!TextUtils.isEmpty(lat_lon) && !TextUtils.isEmpty(date)) {
            switch (v.getId()) {
                case R.id.btn_day_in:
                    String start_meter = edit_start_read.getText().toString().trim();
                    if (TextUtils.isEmpty(start_meter)) {
                        edit_start_read.requestFocus();
                        edit_start_read.setError("Please enter Meter Reading to Day In...");
                    } else {
                        new Day_In_submit().execute(emp_id, date, lat_lon, start_meter);
                    }

                    break;
                case R.id.btn_day_out:
                    close_meter = edit_close_read.getText().toString().trim();

                    if (TextUtils.isEmpty(close_meter)) {
                        edit_close_read.requestFocus();
                        edit_close_read.setError("Please enter Meter Reading to Day Out...");
                    } else {
                        AppLog.write("CLOSE--", close_meter + "--" + lat_lon);
                        if (check_day_out(date) > 1) {
                            new Day_Out_submit().execute(emp_id, date, lat_lon, close_meter);
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setCancelable(false);
                            builder.setMessage("Are you sure want to checkout?");
                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //if user select "No", just cancel this dialog and continue with app
                                    dialog.cancel();
                                }
                            });
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    new Day_Out_submit().execute(emp_id, date, lat_lon, close_meter);
                                }
                            });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    }




                    break;
            }
        } else {
            Toast.makeText(context, "Please Press again..!", Toast.LENGTH_SHORT).show();
        }
    }

    private int check_day_out(String date) {
        int hours = 0;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        try {
            Date startDate = simpleDateFormat.parse(date);
            Date endDate = simpleDateFormat.parse(date);

            if (!TextUtils.isEmpty(checkIN_Time)) {
                endDate = simpleDateFormat.parse(checkIN_Time);
            }
            long difference = endDate.getTime() - startDate.getTime();
            int days = (int) (difference / (1000 * 60 * 60 * 24));
            hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
            AppLog.write("log_tag", "Hours: " + hours + ", Mins: " + min + " " + days + "-----" + Math.abs(min));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Math.abs(hours);
    }

    @Override
    public boolean onBackPressed() {
        AppLog.write("Worked--", "--");
        callHomeFragment();
        return true;
    }

    private void callHomeFragment() {
        /*CurrentDateTaskFragment fragment = new CurrentDateTaskFragment();
        FragmentTransaction fragmentTransaction = null;
        if (getFragmentManager() != null) {
            fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }*/
        Intent i = new Intent(context, Home_Page_Activity.class);
        startActivity(i);
    }

    private void successCheckIN() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Your Attendance has been logged in successfully, and your current day schedule has been created..!");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callHomeFragment();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private class Day_Out_submit extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            try {
                return call.EmployeeOut_Update_attendance((App) getActivity().getApplication(), p[0], settings.getUPDATE_ID(),
                        p[1], p[2], p[3]);
            } catch (NullPointerException n) {
                n.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("CHECK__OUT", "---" + new Gson().toJson(success));
            if (success != null) {
                if (success.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(getActivity(), "Check-out Successfully..!", Toast.LENGTH_SHORT).show();
                    settings.setIS_CHECKED_IN(false);
                    settings.setCHECKED_OUT_TIME(date);
                    AppLog.write("CHECK__OUT_STATUS", "---" + settings.isCheckIn());
                    btn_day_in.setVisibility(View.VISIBLE);
                    btn_day_out.setVisibility(View.GONE);
                    layout_check_in.setVisibility(View.GONE);
                    edit_close_read.setVisibility(View.GONE);
                    edit_start_read.setVisibility(View.VISIBLE);
                    callHomeFragment();
                } else {
                    Toast.makeText(getActivity(), "Failed to Check-out..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Failed to Check-out..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class Day_In_submit extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.EmployeeIn_Create_attendance((App) getActivity().getApplication(), p[0], p[1], p[2], p[3]);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (success != null) {
                if (success.getSuccess().equalsIgnoreCase("1")) {
                    //Toast.makeText(getActivity(), "Check-In Successfully..!", Toast.LENGTH_SHORT).show();
                    settings.setIS_CHECKED_IN(true);
                    settings.setCHECKED_IN_TIME(date);
                    settings.setUPDATE_ID(success.getUpdate_id());
                    AppLog.write("CHECK_OUT_STATUS", "---1." + settings.isCheckIn());
                    btn_day_out.setVisibility(View.VISIBLE);
                    btn_day_in.setVisibility(View.GONE);
                    layout_check_in.setVisibility(View.VISIBLE);
                    edit_close_read.setVisibility(View.VISIBLE);
                    t_starting_meter.setVisibility(View.VISIBLE);
                    edit_start_read.setVisibility(View.GONE);

                    Log.e("dkfhks", "" + settings.getCHECKED_IN_TIME());
                    t_check_in_time.setText(String.format("CheckIn at :  %s", getCheckinDate(settings.getCHECKED_IN_TIME())));
                    t_starting_meter.setText(String.format("Starting Meter Reading : %s", settings.getSTRATING_TIME()));
                    successCheckIN();
                } else {
                    Toast.makeText(getActivity(), "Failed to Check-In..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Failed to Check-In..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class GetDayInStatus extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            try {
                return call.call_for_getDayIn_status((App) getActivity().getApplication(), settings.getEmployee_ID());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Executed_Day_IN_status", "-" + new Gson().toJson(success));
            if (success != null) {
                checkIN_Time = success.getCheckin_time();
                if (success.getSuccess().equalsIgnoreCase("1")) {
                    DayIN = "1";
                    AppLog.write("Check_In", "----");
                    btn_day_out.setVisibility(View.VISIBLE);
                    btn_day_in.setVisibility(View.GONE);
                    layout_check_in.setVisibility(View.VISIBLE);
                    edit_close_read.setVisibility(View.VISIBLE);
                    edit_start_read.setVisibility(View.GONE);

                    t_starting_meter.setVisibility(View.VISIBLE);
                    t_starting_meter.setText(String.format("Starting Meter Reading : %s", settings.getSTRATING_TIME()));
                    t_check_in_time.setText(String.format("CheckIn at :  %s", getCheckinDate(success.getCheckin_time())));
                } else if (success.getSuccess().equalsIgnoreCase("2")) {
                    DayIN = "2";
                    AppLog.write("Not_Checked_In", "----");
                    btn_day_out.setVisibility(View.GONE);
                    btn_day_in.setVisibility(View.VISIBLE);
                    layout_check_in.setVisibility(View.GONE);
                    edit_close_read.setVisibility(View.GONE);
                    edit_start_read.setVisibility(View.VISIBLE);
                } else if (success.getSuccess().equalsIgnoreCase("0")) {
                    DayIN = "0";
                    AppLog.write("Check_In", "----" + DayIN);
                }
            } else {
                AppLog.write("Day_IN_status", "Not Working");
            }
        }
    }
}