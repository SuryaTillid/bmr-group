package cbs.com.bmr.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.TaskDetails_ClickListener;
import cbs.com.bmr.R;
import cbs.com.bmr.activity.CreateTaskForTodayActivity;
import cbs.com.bmr.activity.GeneralFeedbackActivity;
import cbs.com.bmr.activity.PondListActivity;
import cbs.com.bmr.activity.VisitHarvestingDataListActivity;
import cbs.com.bmr.activity.VisitSamplingDataListActivity;
import cbs.com.bmr.adapter.TaskListAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.Location_Details;
import cbs.com.bmr.model.SuccessMessage;
import cbs.com.bmr.model.TaskList;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static cbs.com.bmr.Utilities.Activity.launch;
import static cbs.com.bmr.Utilities.Activity.launchWithBundle;

/********************************************************************
 * Created by Barani on 27-03-2019 in TableMateNew
 ********************************************************************/
public class CurrentDateTaskFragment extends Fragment implements TaskDetails_ClickListener, View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    LocationManager locationManager;
    private static final int REQUEST_ENABLE_LOCATION = 4;
    public Double lLat, lLng;
    Location mLastLocation, location;
    ConfigurationSettings settings;
    private TaskListAdapter adapter;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView recyclerView_task;
    private ArrayList<TaskList> taskList = new ArrayList<>();
    private Context context;
    private String current_date, approval_status;
    private FloatingActionButton fab_task_create;
    private TextView text_date;
    private String check_in_out, task_id, lat_lon, check_date, s_e_date;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private String Cur_latitude, Cur_longitude;
    private String customer_id, customer_name;
    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_task_for_today, container, false);

        context = getActivity();
        settings = new ConfigurationSettings(context);

        recyclerView_task = view.findViewById(R.id.rc_TaskDetailsList);
        fab_task_create = view.findViewById(R.id.fab_task_create);
        text_date = view.findViewById(R.id.text_date);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        fab_task_create.setOnClickListener(this);

        //get date
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time=>" + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        SimpleDateFormat d_format = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        s_e_date = df.format(c);
        current_date = df.format(c);

        text_date.setText(d_format.format(c));

        adapter = new TaskListAdapter(context, taskList);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        adapter.setTaskListListener(this);
        recyclerView_task.setLayoutManager(mLayoutManager);
        recyclerView_task.setAdapter(adapter);

        //call Tasks List
        new GetTaskList().execute();
        getLocation();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refresh();
                stop();
            }
        });
        return view;
    }

    private void stop() {
        //stop P to R
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private SuccessMessage successMessage;

    @Override
    public void feedbackSubmit(String id, String type) {
        /*Bundle bundle = new Bundle();
        bundle.putString("ID", id);
        bundle.putString("TYPE", type);
        FeedbackFromCustomer fragment = new FeedbackFromCustomer();
        fragment.setArguments(bundle);
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }*/
        call_questionnaire_dialog(id, type);
    }

    private void call_questionnaire_dialog(final String TaskId, final String type) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_questionnarie);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        assert window != null;
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        Button btnFeedback = dialog.findViewById(R.id.btn_feedback);
        Button btnSampling = dialog.findViewById(R.id.btn_sampling);
        Button btnHarvesting = dialog.findViewById(R.id.btn_harvesting);
        Button btnServices = dialog.findViewById(R.id.btn_services);
        ImageView mImageClose = dialog.findViewById(R.id.img_back);

        mImageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        for (TaskList list : taskList) {
            if (list.getId().equalsIgnoreCase(TaskId)) {
                customer_id = list.getCustomer_id();
                customer_name = list.getCustomername();
                AppLog.write("CUST", customer_name);
            }
        }

        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("ID", TaskId);
                bundle.putString("TYPE", type);
                launchWithBundle(context, GeneralFeedbackActivity.class, bundle);
                dialog.dismiss();
            }
        });

        btnSampling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("TaskId", TaskId);
                bundle.putString("C_ID", customer_id);
                bundle.putString("C_NAME", customer_name);
                launchWithBundle(context, VisitSamplingDataListActivity.class, bundle);
                dialog.dismiss();
            }
        });

        btnHarvesting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppLog.write("Clicked_sampling", "----");
                Bundle bundle = new Bundle();
                bundle.putString("TaskId", TaskId);
                bundle.putString("C_ID", customer_id);
                bundle.putString("C_NAME", customer_name);
                launchWithBundle(context, VisitHarvestingDataListActivity.class, bundle);
                dialog.dismiss();
            }
        });

        btnServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("C_ID", customer_id);
                bundle.putString("C_NAME", customer_name);
                bundle.putString("TaskId", TaskId);
                launchWithBundle(context, PondListActivity.class, bundle);
                dialog.dismiss();
            }
        });

        dialog.show();
    }



    @Override
    public void physicalFeedback(String id, String task_type) {
    }



    @Override
    public void onClickApproval(int position, final String t_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Task Approval Status");
        builder.setPositiveButton("Approve", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                approval_status = "1";
                new TaskApprovalCheck().execute(t_id, "1");
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                approval_status = "2";
                new TaskApprovalCheck().execute(t_id, "2");
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onClick(View v) {
        if (v == fab_task_create) {
            launch(context, CreateTaskForTodayActivity.class);
        }
    }

    private void getLocation() {
        initLocationRequest();
        buildGoogleApiClient();
        displayLocationSettingsRequest();
    }

    private void displayLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest).setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(getActivity(), REQUEST_ENABLE_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        });
    }

    @SuppressWarnings("deprecation")
    private void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            requestLocationPermission();
        }
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 4);
    }

    private void initLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    private Location_Details getLocationDetails(String lat, String lon) {
        Location_Details location = new Location_Details();
        location.setLatitude(lat);
        location.setLongitude(lon);
        return location;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        lLat = location.getLatitude();
        lLng = location.getLongitude();
        getCurrentLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 4) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentLocation();
                }
            }
        }
    }

    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            Cur_latitude = String.valueOf(latitude);
            Cur_longitude = String.valueOf(longitude);
            lat_lon = new Gson().toJson(getLocationDetails(Cur_latitude, Cur_longitude));
            AppLog.write("LON_LAT------", lat_lon);
        }
    }


    private void refresh() {
        new GetTaskList().execute();
    }

    private class GetTaskList extends AsyncTask<Void, Void, ArrayList<TaskList>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<TaskList> doInBackground(Void... v) {
            final String url = App.IP_ADDRESS + "/index.php/tasksummary/getlisttaskapi";
            ArrayList<TaskList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getCurrentTaskDetailsList((App) getActivity().getApplication(), settings.getEmployee_ID(), settings.getREGION_ID(), s_e_date, s_e_date);
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<TaskList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();

            ArrayList<TaskList> tList = new ArrayList<>();
            if (null != list) {
                AppLog.write("T_date_list", "" + new Gson().toJson(list));
                for (TaskList task : list) {
                    String task_date = task.getTask_date();
                    try {
                        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault());
                        Date date = format.parse(task_date);
                        DateFormat date_formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

                        task_date = date_formatter.format(date);

                        if (task_date.equalsIgnoreCase(current_date)) {
                            tList.add(task);
                            taskList.add(task);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                }
                adapter.MyDataChanged(tList);
            } else {
                Toast.makeText(context, "No Records Found..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /* private String taskDateFormat(String task_date) {
         SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
         Date myDate = null;
         try {
             myDate = dateFormat.parse(task_date);
         } catch (ParseException e) {
             e.printStackTrace();
         }
         SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
         String finalDate = timeFormat.format(myDate);
         return finalDate;
     }
 */

    @Override
    public void onClickEdit(int position, String t_id, String task_id, String date) {
        Bundle bundle = new Bundle();
        bundle.putString("T_ID", t_id);
        bundle.putString("TASK_ID", task_id);
        bundle.putString("DATE_C", s_e_date);
        bundle.putString("check_date", check_date);
        bundle.putString("lat_lon", lat_lon);
        TaskUpdateFragmentForCurrentDayFragment fragment = new TaskUpdateFragmentForCurrentDayFragment();
        fragment.setArguments(bundle);

        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onTaskClick(int position, String id, String check) {
        AppLog.write("TAG_GEO", lat_lon);
        check_in_out = check;
        task_id = id;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        check_date = date_format.format(calendar.getTime());
        AppLog.write("TAG_GEO", "-" + task_id + "--" + check_in_out + "-" + check_date + "--" + lat_lon);
        if (!TextUtils.isEmpty(lat_lon) && !TextUtils.isEmpty(check_date)) {
            if (check_in_out.equalsIgnoreCase("1")) {
                GetCheck_In_Details((App) getActivity().getApplication(), task_id, check_date, lat_lon);
            } else {
                new GetCheck_Out_Details().execute(task_id, check_date, lat_lon);
            }
        } else {
            Toast.makeText(context, "Location not detected, please try again..!", Toast.LENGTH_SHORT).show();
            getLocation();
        }
    }

    private void GetCheck_In_Details(App app, String td_id, String check_in, String geo_checkin) {
        final MyCustomDialog dialog;
        dialog = new MyCustomDialog(context, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Observable<Response<SuccessMessage>> testObservable1 = apiService.check_in_taskRX(td_id, check_in, geo_checkin, settings.getEmployee_ID());
        testObservable1
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<SuccessMessage>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<SuccessMessage> successMessageResponse) {
                        Log.e("successMessageResponse", "" + successMessageResponse.toString());
                        successMessage = successMessageResponse.body();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                        if (successMessage != null) {
                            if (successMessage.getCheckin_status().equalsIgnoreCase("1")) {
                                Toast.makeText(getActivity(), "Check-in Successfully..!", Toast.LENGTH_SHORT).show();
                                refresh();
                            } else if (successMessage.getCheckin_status().equalsIgnoreCase("2")) {
                                Toast.makeText(getActivity(), "You can't check-in to this task..!", Toast.LENGTH_SHORT).show();
                            } else if (successMessage.getCheckin_status().equalsIgnoreCase("3")) {
                                Toast.makeText(getActivity(), "Required Day-In..!", Toast.LENGTH_SHORT).show();
                            } else if (successMessage.getCheckin_status().equalsIgnoreCase("4")) {
                                Toast.makeText(getActivity(), "Task Not Approved..!", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), "Failed to Check-in..!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Failed to Check-in..!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    private class GetCheck_Out_Details extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.CheckOut_Task_update((App) getActivity().getApplication(), p[0], p[1], p[2], settings.getEmployee_ID());
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("CHECK__OUT", "---" + new Gson().toJson(success));
            if (success != null) {
                if (success.getCheckout_status().equalsIgnoreCase("1")) {
                    Toast.makeText(getActivity(), "Check-out Successfully..!", Toast.LENGTH_SHORT).show();
                    refresh();
                } else {
                    Toast.makeText(getActivity(), "Failed to Check-out..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Failed to Check-out..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class TaskApprovalCheck extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.CheckApprovedStatus((App) getActivity().getApplication(), settings.getEmployee_ID(), p[0], p[1]);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (success != null) {
                if (approval_status.equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Approved", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Cancelled", Toast.LENGTH_SHORT).show();
                }
                refresh();
            }
        }
    }
}