package cbs.com.bmr.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.ControlGenerator;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.On_BackPressed;
import cbs.com.bmr.R;
import cbs.com.bmr.activity.CurrentDayTaskActivity;
import cbs.com.bmr.adapter.FeedbackAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.AppLog;
import cbs.com.bmr.configuration.RestApiCalls;
import cbs.com.bmr.model.EmployeeList;
import cbs.com.bmr.model.FeedbackQuestions;
import cbs.com.bmr.model.Location_Details;
import cbs.com.bmr.model.SuccessMessage;

import static cbs.com.bmr.Utilities.Activity.launchClearTop;

/*********************************************************************
 * Created by Barani on 29-03-2019 in TableMateNew
 ***********************************************************************/
public class SalesFeedbackFromCustomer extends Fragment implements LocationListener, On_BackPressed, View.OnClickListener {
    LocationManager locationManager;
    ConfigurationSettings settings;
    private Context context;
    private FeedbackAdapter adapter;
    private RecyclerView recycleFeedbackList;
    private ArrayList<FeedbackQuestions> feedbackQuestionsList = new ArrayList<>();
    private RecyclerView.LayoutManager mLayoutManager;
    private LinearLayout layout;
    private int counter;
    private ArrayList<View> views;
    private ArrayList<EmployeeList> employeeList = new ArrayList<>();
    private Button btnSubmit;
    private String task_id, task_type, lat_lon, check_date, technical_comments, user_name, email;
    private Dialog t_dialog;


    public SalesFeedbackFromCustomer() {

    }

    public static SalesFeedbackFromCustomer newInstance(String id, String type) {
        Bundle bundle = new Bundle();
        bundle.putString("ID", id);
        bundle.putString("TYPE", type);
        SalesFeedbackFromCustomer productFragment = new SalesFeedbackFromCustomer();
        productFragment.setArguments(bundle);
        return productFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feedbackfromcustomer, container, false);
        context = getActivity();
        processBundle();
        settings = new ConfigurationSettings(context);
        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }

        getLocation();
        getDate();
        layout = view.findViewById(R.id.layout);
        btnSubmit = view.findViewById(R.id.btnSubmitFeedback);
        btnSubmit.setOnClickListener(this);

        AppLog.write("INTENT--", "--" + task_id);
        new GetFeedbackFormQuestionsList().execute();
        new EmployeeDetails().execute();
        return view;
    }

    private void getDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        check_date = date_format.format(calendar.getTime());
    }

    private void processBundle() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            task_id = bundle.getString("ID");
            task_type = bundle.getString("TYPE");
        }
    }

    private void showDialog_getTechnicalComments() {
        t_dialog = new Dialog(context);
        t_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        t_dialog.setContentView(R.layout.activity_comments);

        final EditText ed_tech_comments = t_dialog.findViewById(R.id.edit_tech_comments);
        Button btn_proceed = t_dialog.findViewById(R.id.btn_submit);
        Button btn_skip = t_dialog.findViewById(R.id.btn_skip);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        check_date = date_format.format(calendar.getTime());

        btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation();
                if (validateTechComment(ed_tech_comments, technical_comments)) {
                    if (!TextUtils.isEmpty(lat_lon) && !TextUtils.isEmpty(check_date)) {
                        technical_comments = ed_tech_comments.getText().toString().trim();
                        getAnswers();
                    } else {
                        Toast.makeText(context, "Location not detected..Please try again..!", Toast.LENGTH_SHORT).show();
                        getLocation();
                    }
                }
            }
        });

        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(lat_lon) && !TextUtils.isEmpty(check_date)) {
                    technical_comments = "";
                    getAnswers();
                } else {
                    Toast.makeText(context, "Location not detected..Please try again..!", Toast.LENGTH_SHORT).show();
                    getLocation();
                }
            }
        });
        t_dialog.show();
    }

    private boolean validateTechComment(EditText ed_tech_comments, String technical_comments) {
        boolean isValid = true;
        AppLog.write("LENGTH--", "--" + ed_tech_comments.getText().toString().length());
        if (ed_tech_comments.getText().toString().equals("")) {
            ed_tech_comments.setError("Field cannot be left blank..!");
            isValid = false;
        } else if (ed_tech_comments.getText().toString().length() < 5) {
            ed_tech_comments.setError("Please enter minimum 5 characters..!");
            isValid = false;
        } else {
            ed_tech_comments.setError(null);
        }
        return isValid;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void getLocation() {
        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            String lat = String.valueOf(location.getLatitude());
            String lon = String.valueOf(location.getLongitude());
            if (!TextUtils.isEmpty(lat) && !TextUtils.isEmpty(lon)) {
                lat_lon = new Gson().toJson(getLocationDetails(lat, lon));
                AppLog.write("LAT_LON", lat_lon);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    private Location_Details getLocationDetails(String lat, String lon) {
        Location_Details location = new Location_Details();
        location.setLatitude(lat);
        location.setLongitude(lon);
        return location;
    }

    private void getAnswers() {
        counter = 0;
        for (FeedbackQuestions feedback : feedbackQuestionsList) {
            if (feedback.getQues_type().equalsIgnoreCase("0") || feedback.getQues_type().equalsIgnoreCase("1")) {
                EditText editText = ((EditText) views.get(counter));
                feedback.setAnswer(editText.getText().toString().trim());
            } else if (feedback.getQues_type().equalsIgnoreCase("2") || feedback.getQues_type().equalsIgnoreCase("3")) {
                AppLog.write("Counter", "--" + counter);
                try {
                    RadioGroup rg = ((RadioGroup) views.get(counter));
                    RadioButton rb = rg.findViewById(rg.getCheckedRadioButtonId());
                    String rb_value = rb.getText().toString().trim();
              /*  if (!TextUtils.isEmpty(rb_value)) {
                    feedback.setAnswer(rb_value);
                } else {*/
                    feedback.setAnswer(rb.getText().toString().trim());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //}
            } else if (feedback.getQues_type().equalsIgnoreCase("4")) {
                EditText editText = ((EditText) views.get(counter));
                feedback.setAnswer(editText.getText().toString().trim());
            }
            counter++;
        }
        AddValues_to_feedbackList(feedbackQuestionsList);
        AppLog.write("Results", new Gson().toJson(feedbackQuestionsList));
    }

    private void AddValues_to_feedbackList(ArrayList<FeedbackQuestions> feedbackQuestionsList) {
        String feedback_list = new Gson().toJson(getDetails(feedbackQuestionsList));
        AppLog.write("Feedback_Results", "--" + feedback_list);
        new Feedback_submission().execute(feedback_list);
    }

    private ArrayList<FeedbackQuestions> getDetails(ArrayList<FeedbackQuestions> feedbackQuestionsList) {
        ArrayList<FeedbackQuestions> list = new ArrayList<>();
        try {
            for (FeedbackQuestions t : feedbackQuestionsList) {
                FeedbackQuestions t_list = new FeedbackQuestions();
                t_list.setTd_id(task_id);
                t_list.setQues_name(t.getQues_name());
                t_list.setAnswer(t.getAnswer());
                list.add(t_list);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public boolean onBackPressed() {
        callHomeFragment();
        return true;
    }

    @Override
    public void onClick(View v) {
        getAnswers();
        //showDialog_getTechnicalComments();
    }

    private void callPopBack() {
        launchClearTop(context, CurrentDayTaskActivity.class);
    }

    private void callHomeFragment() {
        launchClearTop(context, CurrentDayTaskActivity.class);
    }

    private class GetFeedbackFormQuestionsList extends AsyncTask<Void, Void, ArrayList<FeedbackQuestions>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<FeedbackQuestions> doInBackground(Void... v) {
            ArrayList<FeedbackQuestions> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getFeedbackQuestionsList((App) getActivity().getApplication(), task_type);
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<FeedbackQuestions> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != list) {
                feedbackQuestionsList = list;
                ControlGenerator generator = new ControlGenerator(getActivity());
                LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                //layout.setBackgroundResource(R.drawable.btn_bg);
                p.topMargin = 16;
                p.leftMargin = 100;
                p.rightMargin = 100;
                counter = 0;
                views = new ArrayList<View>();
                for (FeedbackQuestions feedback : list) {
                    counter++;
                    layout.addView(generator.createTextView(counter + 100, feedback.getQues_name()), p);
                    if (feedback.getQues_type().equalsIgnoreCase("0") || feedback.getQues_type().equalsIgnoreCase("1")) {
                        EditText editText = generator.createEditText(counter, R.drawable.bg_rounded_input_field, feedback.getQues_type());
                        views.add(editText);
                        layout.addView(editText, p);
                    } else if (feedback.getQues_type().equalsIgnoreCase("2")) {
                        String str = feedback.getOption().trim();
                        String[] q_List = str.split(",");
                        RadioGroup rg = generator.createRadioGroup(counter, q_List, RadioGroup.HORIZONTAL);
                        rg.setLayoutParams(p);
                        views.add(rg);
                        layout.addView(rg);
                    } else if (feedback.getQues_type().equalsIgnoreCase("3")) {
                        String str = feedback.getOption().trim();
                        String[] q_List = str.split(",");
                        RadioGroup rg = generator.createRadioGroup(counter, q_List, RadioGroup.VERTICAL);
                        rg.setLayoutParams(p);
                        views.add(rg);
                        layout.addView(rg);
                    } else if (feedback.getQues_type().equalsIgnoreCase("4")) {
                        EditText editText = generator.createDatePicker(counter, R.drawable.bg_rounded_input_field, feedback.getQues_type(), R.drawable.ic_calendar_new);
                        views.add(editText);
                        layout.addView(editText, p);
                    }
                }
            } else {
                AppLog.write("TAG--", "-" + new Gson().toJson(list));
            }
        }
    }

    private class Feedback_submission extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.FeedbackSubmit((App) getActivity().getApplication(), task_id, check_date, lat_lon, p[0], settings.getEmployee_ID(), technical_comments);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Feedback_response---", new Gson().toJson(success));
            if (success != null) {
                if (success.getFeedback_status() == 1) {
                    Toast.makeText(getActivity(), "Feedback Submitted Successfully..!", Toast.LENGTH_SHORT).show();
                    // t_dialog.dismiss();
                    callPopBack();
                } else if (success.getFeedback_status() == 2) {
                    Toast.makeText(getActivity(), "Feedback Already Updated..!", Toast.LENGTH_SHORT).show();
                    //t_dialog.dismiss();
                    callPopBack();
                } else {
                    Toast.makeText(getActivity(), "Failed to submit Feedback..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Failed to submit Feedback..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class SendMail extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.call_for_SendMail((App) getActivity().getApplication(), p[0], p[1], p[2], p[3]);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != success) {
                if (success.getMessage().equalsIgnoreCase("success")) {
                    Toast.makeText(context, "Mail Sent Successfully..!", Toast.LENGTH_SHORT).show();
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    fm.popBackStackImmediate();
                }
            }
        }
    }

    private class EmployeeDetails extends AsyncTask<Void, Void, ArrayList<EmployeeList>> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<EmployeeList> doInBackground(Void... v) {
            ArrayList<EmployeeList> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.getEmployeeList((App) getActivity().getApplication(), settings.getEmployee_ID());
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<EmployeeList> list) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != list) {
                for (EmployeeList e_list : list) {
                    employeeList.add(e_list);
                    if (settings.getEmployee_ID().equalsIgnoreCase(e_list.getId())) {
                        user_name = e_list.getFirstname();
                        email = e_list.getEmail();
                    }
                }
            }
        }
    }
}