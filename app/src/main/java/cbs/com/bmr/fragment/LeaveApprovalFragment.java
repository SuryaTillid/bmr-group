package cbs.com.bmr.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Objects;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.Listener.LeaveApproveListCallback;
import cbs.com.bmr.R;
import cbs.com.bmr.adapter.LeaveAprovalListAdapter;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.model.LeaveApprovalList;
import cbs.com.bmr.model.LeaveApprovalSuccess;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class LeaveApprovalFragment extends Fragment implements LeaveApproveListCallback {
    public Context mContext;
    LeaveApprovalSuccess LeaveApprovalSuccess;
    private View view;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private LeaveAprovalListAdapter mAdapter;
    private ArrayList<LeaveApprovalList> leaveApprovalLists;
    private ArrayList<LeaveApprovalList> leaveApprovalListsnew = new ArrayList<>();
    private ConfigurationSettings configurationSettings;


    public LeaveApprovalFragment() {
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_leave_approval, container, false);
        initObjects();
        initRecyclerView();
        getLeaveApprovalList((App) Objects.requireNonNull(getActivity()).getApplication());
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initObjects() {
        mContext = getActivity();
        configurationSettings = new ConfigurationSettings(mContext);
        leaveApprovalLists = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView = view.findViewById(R.id.recyclerview_leaveApprovelist);
        mAdapter = new LeaveAprovalListAdapter(mContext, leaveApprovalLists, this);
    }

    @Override
    public void onItemApproveClick(int position, int Type) {
        LeaveApprovalList leaveApprovalList = leaveApprovalLists.get(position);

        getApproval((App) getActivity().getApplication(), leaveApprovalList.getEmp_id(), leaveApprovalList.getId(), String.valueOf(Type));
        if (Type == 1) {
            leaveApprovalList.setLeave_status("Approved");
        } else {
            leaveApprovalList.setLeave_status("Cancelled");
        }
        mAdapter.notifyDataSetChanged();

    }

    public void getLeaveApprovalList(App app) {
        final MyCustomDialog dialog;
        dialog = new MyCustomDialog(mContext, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Observable<Response<ArrayList<LeaveApprovalList>>> testObservable1 = apiService.getLeaveApprovalList(configurationSettings.getEmployee_ID());
        testObservable1
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ArrayList<LeaveApprovalList>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ArrayList<LeaveApprovalList>> arrayListResponse) {
                        Log.e("arrayListResponse", "" + arrayListResponse.toString());
                        ArrayList<LeaveApprovalList> arrayList = arrayListResponse.body();
                        if (arrayList != null) {
                            if (arrayList.size() > 0) {
                                leaveApprovalListsnew = arrayList;
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                        leaveApprovalLists.clear();
                        leaveApprovalLists.addAll(leaveApprovalListsnew);
                        mAdapter.notifyDataSetChanged();

                    }
                });
    }

    public void getApproval(App app, String Empid, String Leaveid, String status) {
        final MyCustomDialog dialog;
        dialog = new MyCustomDialog(mContext, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Observable<Response<LeaveApprovalSuccess>> testObservable1 = apiService.createLeaveApproval(Empid, Leaveid, status);
        testObservable1
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<LeaveApprovalSuccess>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }
                    @Override
                    public void onNext(Response<LeaveApprovalSuccess> arrayListResponse) {
                        Log.e("arrayListResponse", "" + arrayListResponse.toString());
                        LeaveApprovalSuccess = arrayListResponse.body();

                    }
                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                        if (LeaveApprovalSuccess != null && LeaveApprovalSuccess.getSuccess().equals("1")) {
                            Log.e("leavestatus", LeaveApprovalSuccess.getLeave_status());
                            if (LeaveApprovalSuccess.getLeave_status().equals("1")) {
                                    Toast.makeText(mContext, "Approved Successfully", Toast.LENGTH_SHORT).show();
                                } else {

                                    Toast.makeText(mContext, "Cancelled Successfully", Toast.LENGTH_SHORT).show();
                                }
                                mAdapter.notifyDataSetChanged();


                        }
                    }
                });
    }


}
