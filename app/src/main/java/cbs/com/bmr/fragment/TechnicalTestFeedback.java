package cbs.com.bmr.fragment;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import java.util.Objects;

import cbs.com.bmr.Helper.ConfigurationSettings;
import cbs.com.bmr.Helper.MyCustomDialog;
import cbs.com.bmr.R;
import cbs.com.bmr.activity.HomePondListActivity;
import cbs.com.bmr.activity.PondListActivity;
import cbs.com.bmr.configuration.App;
import cbs.com.bmr.configuration.RestApi;
import cbs.com.bmr.model.PhysicalTestFeedbackResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cbs.com.bmr.Utilities.Activity.launchClearTopWithBundle;

public class TechnicalTestFeedback extends Fragment implements View.OnClickListener {
    private ConfigurationSettings settings;
    private RadioGroup mSedimentationgroup, mPretreatmentGroup, mFencingGroup, mFermentationGroup, mProboticroup, mChlorinationGroup;
    private EditText mEditWatersources, mEditPumping, mEditTreatment, mEditFeedOty, mEditComments;
    private String Sedimentationvalue = "", PretreatmentValue = "", FencingValue = "", FermantationValue = "", ProboticValue = "", ChlorinationValue = "", date, TaskId;
    private MyCustomDialog dialog;
    private Context context;
    private Button btnSubmit;
    private String PondId, FarmerID, lat_lon, customerName, GeoJson;

    public static TechnicalTestFeedback newInstance(String PonId, String TaskId, String FarmerId, String customerName, String GeoJson) {

        Bundle args = new Bundle();
        args.putString("PondId", PonId);
        args.putString("TaskId", TaskId);
        args.putString("Farmer_id", FarmerId);
        args.putString("CustomerName", customerName);
        args.putString("GeoJson", GeoJson);

        TechnicalTestFeedback fragment = new TechnicalTestFeedback();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_technical_test, container, false);
        context = getActivity();
        settings = new ConfigurationSettings(context);

        ProcessBundle();

        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }

        btnSubmit = view.findViewById(R.id.btnSubmitFeedback);
        mEditComments = view.findViewById(R.id.edit_coments);
        mSedimentationgroup = view.findViewById(R.id.sedimentation_group);
        mPretreatmentGroup = view.findViewById(R.id.Pretreatment_group);
        mFencingGroup = view.findViewById(R.id.Fencing_group);
        mFermentationGroup = view.findViewById(R.id.Fermentation_group);
        mProboticroup = view.findViewById(R.id.Probiotic_group);
        mChlorinationGroup = view.findViewById(R.id.Chlorination_group);
        mEditWatersources = view.findViewById(R.id.edit_water_sources);
        mEditPumping = view.findViewById(R.id.edit_pumping);
        mEditTreatment = view.findViewById(R.id.edit_treatment);
        mEditFeedOty = view.findViewById(R.id.edit_check_tray_feed);
        btnSubmit.setOnClickListener(this);
        initRadioListners();
        return view;
    }

    private void ProcessBundle() {

        Bundle bundle = getArguments();
        if (bundle != null) {
            PondId = bundle.getString("PondId");
            FarmerID = bundle.getString("Farmer_id");
            TaskId = bundle.getString("TaskId");
            customerName = bundle.getString("CustomerName");
            GeoJson = bundle.getString("GeoJson");
        }
    }

    private void initRadioListners() {
        mSedimentationgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.sedimentation_yes:
                        Sedimentationvalue = "1";
                        break;
                    case R.id.sedimentation_no:
                        Sedimentationvalue = "0";
                        break;

                }
            }
        });
        mPretreatmentGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.Pretreatment_yes:
                        PretreatmentValue = "1";
                        break;
                    case R.id.Pretreatment_no:
                        PretreatmentValue = "0";
                        break;

                }
            }
        });
        mFencingGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.Fencing_yes:
                        FencingValue = "1";
                        break;
                    case R.id.Fencing_no:
                        FencingValue = "0";
                        break;

                }
            }
        });
        mFermentationGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.Fermentation_yes:
                        FermantationValue = "1";
                        break;
                    case R.id.Fermentation_no:
                        FermantationValue = "0";
                        break;

                }
            }
        });
        mChlorinationGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.Chlorination_yes:
                        ChlorinationValue = "1";
                        break;
                    case R.id.Chlorination_no:
                        ChlorinationValue = "0";
                        break;

                }
            }
        });

        mProboticroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.Probiotic_yes:
                        ProboticValue = "1";
                        break;
                    case R.id.Probiotic_no:
                        ProboticValue = "0";
                        break;
                }
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        if (v == btnSubmit) {
            processValidate();
        }
    }

    private void processValidate() {
        String WaterSources = mEditWatersources.getText().toString().trim();
        String Pumping = mEditPumping.getText().toString().trim();
        String treatement = mEditTreatment.getText().toString().trim();
        String FeedOty = mEditFeedOty.getText().toString().trim();
        String comments = mEditComments.getText().toString().trim();


        if (!TaskId.isEmpty()) {
            getTechnicalTest((App) Objects.requireNonNull(getActivity()).getApplication(), TaskId, settings.getEmployee_ID(), WaterSources, Pumping, treatement, FeedOty,
                    Sedimentationvalue, PretreatmentValue, FencingValue, FermantationValue, ProboticValue, ChlorinationValue, PondId, FarmerID, comments);
        } else {
            getTechnicalTestGeoJson((App) Objects.requireNonNull(getActivity()).getApplication(), GeoJson, settings.getEmployee_ID(), WaterSources, Pumping, treatement, FeedOty,
                    Sedimentationvalue, PretreatmentValue, FencingValue, FermantationValue, ProboticValue, ChlorinationValue, PondId, FarmerID, comments);
        }


    }

    private boolean validateInput(String waterSources, String pumping, String treatement, String FeedOty, String Sedimentationvalue, String PretreatmentValue,
                                  String FencingValue, String FermantationValue,
                                  String ProboticValue, String ChlorinationValue) {
        if (waterSources.isEmpty() && pumping.isEmpty() &&
                treatement.isEmpty() && FeedOty.isEmpty() && Sedimentationvalue.isEmpty() && PretreatmentValue.isEmpty()
                && FencingValue.isEmpty() && FermantationValue.isEmpty() && ProboticValue.isEmpty() && ChlorinationValue.isEmpty()) {
            Toast.makeText(context, "Please enter all the details", Toast.LENGTH_SHORT).show();
            return false;
        } else if (waterSources.isEmpty()) {
            Toast.makeText(context, "Please select Water Sources", Toast.LENGTH_SHORT).show();
            return false;
        } else if (pumping.isEmpty()) {
            Toast.makeText(context, "Please select Pumping", Toast.LENGTH_SHORT).show();
            return false;
        } else if (treatement.isEmpty()) {
            Toast.makeText(context, "Please select Treatment", Toast.LENGTH_SHORT).show();
            return false;
        } else if (FeedOty.isEmpty()) {
            Toast.makeText(context, "Please enter  Check Tray Feed Qty", Toast.LENGTH_SHORT).show();
            return false;
        } else if (Sedimentationvalue.isEmpty()) {
            Toast.makeText(context, "Please Select Sedimentation Tank", Toast.LENGTH_SHORT).show();
            return false;
        } else if (PretreatmentValue.isEmpty()) {
            Toast.makeText(context, "Please Select Pre-Treatment Available", Toast.LENGTH_SHORT).show();
            return false;
        } else if (FencingValue.isEmpty()) {
            Toast.makeText(context, "Please Select Fencing and Disinfectants", Toast.LENGTH_SHORT).show();
            return false;
        } else if (FermantationValue.isEmpty()) {
            Toast.makeText(context, "Please Select Applied any Fermentation", Toast.LENGTH_SHORT).show();
            return false;
        } else if (ProboticValue.isEmpty()) {
            Toast.makeText(context, "Please Select Probiotic Supplementation", Toast.LENGTH_SHORT).show();
            return false;
        } else if (ChlorinationValue.isEmpty()) {
            Toast.makeText(context, "Please enter Aerators Chlorination after Crap", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void getTechnicalTest(App app, String task_id, String employee_id, String WaterSources,
                                 String Pumping, String treatement, String FeedOty,
                                 String Sedimentationvalue, String PretreatmentValue, String FencingValue,
                                 String FermantationValue, String ProboticValue, String ChlorinationValue, String PondId, String FarmerID, String Comments) {
        dialog = new MyCustomDialog(context, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<PhysicalTestFeedbackResponse> call = apiService.TechnicalTest(task_id, employee_id, date,
                FarmerID, PondId, WaterSources, Pumping, treatement, FeedOty, Sedimentationvalue, PretreatmentValue,
                FencingValue, FermantationValue, ProboticValue, ChlorinationValue, Comments);
        call.enqueue(new Callback<PhysicalTestFeedbackResponse>() {
            @Override
            public void onResponse(@NonNull Call<PhysicalTestFeedbackResponse> call, @NonNull Response<PhysicalTestFeedbackResponse> response) {
                PhysicalTestFeedbackResponse physicalTestFeedbackResponse = response.body();

                Log.e("Technicalresponse", "" + response.toString());
                if (response.isSuccessful()) {

                    if (physicalTestFeedbackResponse != null) {
                        if (physicalTestFeedbackResponse.getSuccess().equals("1")) {
                            Toast.makeText(getActivity(), "FeedBack Submitted Successfully", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                            redirect_pond_list();
                        } else if (physicalTestFeedbackResponse.getSuccess().equals("0")) {
                            Toast.makeText(getActivity(), physicalTestFeedbackResponse.getFeedback_message(), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    }
                } else {

                    redirect_pond_list();
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<PhysicalTestFeedbackResponse> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.e("failure", "" + t.toString());
            }
        });
    }

    public void getTechnicalTestGeoJson(App app, String GeoJson, String employee_id, String WaterSources,
                                        String Pumping, String treatement, String FeedOty,
                                        String Sedimentationvalue, String PretreatmentValue, String FencingValue,
                                        String FermantationValue, String ProboticValue, String ChlorinationValue, String PondId, String FarmerID, String Comments) {
        dialog = new MyCustomDialog(context, "Loading...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<PhysicalTestFeedbackResponse> call = apiService.TechnicalTestGeoJson(GeoJson, employee_id, date,
                FarmerID, PondId, WaterSources, Pumping, treatement, FeedOty, Sedimentationvalue, PretreatmentValue,
                FencingValue, FermantationValue, ProboticValue, ChlorinationValue, Comments);
        call.enqueue(new Callback<PhysicalTestFeedbackResponse>() {
            @Override
            public void onResponse(@NonNull Call<PhysicalTestFeedbackResponse> call, @NonNull Response<PhysicalTestFeedbackResponse> response) {
                PhysicalTestFeedbackResponse physicalTestFeedbackResponse = response.body();

                Log.e("Technicalresponse2", "" + response.toString());
                if (response.isSuccessful()) {

                    if (physicalTestFeedbackResponse != null) {
                        if (physicalTestFeedbackResponse.getSuccess().equals("1")) {
                            Toast.makeText(getActivity(), "FeedBack Submitted Successfully", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                            redirect_Homepond_list_page();
                        } else if (physicalTestFeedbackResponse.getSuccess().equals("0")) {
                            Toast.makeText(getActivity(), physicalTestFeedbackResponse.getFeedback_message(), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    }
                } else {

                    redirect_Homepond_list_page();
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<PhysicalTestFeedbackResponse> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.e("failure", "" + t.toString());
            }
        });
    }

    private void redirect_Homepond_list_page() {
        Bundle bundle = new Bundle();
        bundle.putString("GeoJson", GeoJson);
        launchClearTopWithBundle(context, HomePondListActivity.class, bundle);
    }
    private void redirect_pond_list() {
        Bundle bundle = new Bundle();
        bundle.putString("TaskId", TaskId);
        bundle.putString("C_ID", FarmerID);
        bundle.putString("C_NAME", customerName);

        launchClearTopWithBundle(context, PondListActivity.class, bundle);
    }

    private void callPopBack() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.popBackStackImmediate();
    }
}