package cbs.com.bmr.Utilities;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.net.NetworkInterface;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class Utils {
    public static String getCurrentDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",
                Locale.getDefault());
        return dateFormat.format(Calendar.getInstance().getTime());
    }

    public static String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy",
                Locale.getDefault());
        return dateFormat.format(Calendar.getInstance().getTime());
    }

    public static String getCurrentDateNew() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy",
                Locale.getDefault());
        return dateFormat.format(Calendar.getInstance().getTime());
    }

    public static String getNextDay() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy",
                Locale.getDefault());
        return dateFormat.format(Calendar.getInstance().getTimeInMillis() + 1 * 24 * 60 * 60 * 1000);
    }

    public static String getNextWeek() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy",
                Locale.getDefault());


        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int weekday = calendar.get(Calendar.DAY_OF_WEEK);
        int days = Calendar.MONDAY - weekday;
        if (days < 0) {
            days += 7;
        }
        calendar.add(Calendar.DAY_OF_YEAR, days);
        return dateFormat.format(calendar.getTime());
    }

    public static String getCurrentDateTime1() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS",
                Locale.getDefault());
        return dateFormat.format(Calendar.getInstance().getTime());
    }


    public static String getRelativeTime(String createdOn) {
        if (createdOn != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",
                    Locale.getDefault());
            try {
                Date date = dateFormat.parse(createdOn);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy",
                        Locale.getDefault());
                return simpleDateFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return createdOn;
    }

    public static String getSamplindate(String createdOn) {
        if (createdOn != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",
                    Locale.getDefault());
            try {
                Date date = dateFormat.parse(createdOn);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy",
                        Locale.getDefault());
                return simpleDateFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return createdOn;
    }

    public static String getPreviousSamplindate(String createdOn) {
        if (createdOn != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",
                    Locale.getDefault());
            try {
                Date date = dateFormat.parse(createdOn);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy",
                        Locale.getDefault());
                return simpleDateFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return createdOn;
    }
    public static String getUpdateTaskDate(String createdOn) {
        if (createdOn != null) {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm",
                    Locale.getDefault());
            try {
                Date date = dateFormat.parse(createdOn);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd yyyy",
                        Locale.getDefault());
                return simpleDateFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return createdOn;
    }
    public static String getDocumentsDate(String createdOn) {
        if (createdOn != null) {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            try {
                Date date = dateFormat.parse(createdOn);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());

                return simpleDateFormat.format(date);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return createdOn;
    }

    public static String getDate(String createdOn) {
        if (createdOn != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS",
                    Locale.getDefault());
            try {
                Date date = dateFormat.parse(createdOn);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm",
                        Locale.getDefault());

                return simpleDateFormat.format(date);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return createdOn;
    }

    public static String getCheckinDate(String createdOn) {
        if (createdOn != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                    Locale.getDefault());
            try {
                Date date = dateFormat.parse(createdOn);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a",
                        Locale.getDefault());

                return simpleDateFormat.format(date);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return createdOn;
    }

    public static String getMacAddress() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }
                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:", b));
                }
                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "02:00:00:00:00:00";
    }


    public static String getTime(String createdOn) {
        if (createdOn != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS",
                    Locale.getDefault());
            try {
                Date date = dateFormat.parse(createdOn);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a",
                        Locale.getDefault());
                return simpleDateFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return createdOn;
    }

    public static void extractSpinnerPositiondSet(Spinner spinnerWidget, String valueToFind) {
        if (!TextUtils.isEmpty(valueToFind)) {
            if (spinnerWidget != null && spinnerWidget.getAdapter() != null) {
                int position = ((ArrayAdapter<String>) spinnerWidget.getAdapter()).getPosition(valueToFind);
                setSpinnerPosition(spinnerWidget, position);
                Log.e("djfk", "" + spinnerWidget.getAdapter());
                Log.e("djfk", "" + spinnerWidget.getAdapter());
            }
        }
    }


    public static void extractSpinnerPositionAndSet(View view, int spinnerId, String valueToFind) {
        Spinner spinnerWidget = view.findViewById(spinnerId);
        if (!TextUtils.isEmpty(valueToFind)) {
            extractSpinnerPositiondSet(spinnerWidget, valueToFind);
        }
    }

    private static void setSpinnerPosition(Spinner spinner, int position) {
        if (spinner != null && position > -1) {
            spinner.setSelection(position);
        }
    }
}
