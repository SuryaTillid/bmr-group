package cbs.com.bmr.model;

/*********************************************************************
 * Created by Barani on 20-08-2019 in TableMateNew
 ***********************************************************************/
public class PondDetailedList {
    private String id;
    private String pond_id;
    private String pond_refer;
    private String comments;
    private String size;
    private String density;
    private String cycle_id;
    private String wsa;
    private String seed_stocking;
    private String salinity;
    private String ph;
    private String stocking_date;
    private String recorded_date;
    private String emp_id;
    private String farmer_id;
    private String cycle_status;
    private String status;
    private String last_cycle_id;
    private String farmer_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPond_id() {
        return pond_id;
    }

    public void setPond_id(String pond_id) {
        this.pond_id = pond_id;
    }

    public String getPond_refer() {
        return pond_refer;
    }

    public void setPond_refer(String pond_refer) {
        this.pond_refer = pond_refer;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDensity() {
        return density;
    }

    public void setDensity(String density) {
        this.density = density;
    }

    public String getCycle_id() {
        return cycle_id;
    }

    public void setCycle_id(String cycle_id) {
        this.cycle_id = cycle_id;
    }

    public String getWSA() {
        return wsa;
    }

    public void setWSA(String WSA) {
        this.wsa = WSA;
    }

    public String getSeed_stocking() {
        return seed_stocking;
    }

    public void setSeed_stocking(String seed_stocking) {
        this.seed_stocking = seed_stocking;
    }

    public String getSalinity() {
        return salinity;
    }

    public void setSalinity(String salinity) {
        this.salinity = salinity;
    }

    public String getPh() {
        return ph;
    }

    public void setPh(String ph) {
        this.ph = ph;
    }

    public String getStocking_date() {
        return stocking_date;
    }

    public void setStocking_date(String stocking_date) {
        this.stocking_date = stocking_date;
    }

    public String getRecorded_date() {
        return recorded_date;
    }

    public void setRecorded_date(String recorded_date) {
        this.recorded_date = recorded_date;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getFarmer_id() {
        return farmer_id;
    }

    public void setFarmer_id(String farmer_id) {
        this.farmer_id = farmer_id;
    }

    public String getCycle_status() {
        return cycle_status;
    }

    public void setCycle_status(String cycle_status) {
        this.cycle_status = cycle_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLast_cycle_id() {
        return last_cycle_id;
    }

    public void setLast_cycle_id(String last_cycle_id) {
        this.last_cycle_id = last_cycle_id;
    }

    public String getFarmer_name() {
        return farmer_name;
    }

    public void setFarmer_name(String farmer_name) {
        this.farmer_name = farmer_name;
    }
}
