package cbs.com.bmr.model;

import com.google.gson.annotations.SerializedName;

public class SamplingCycle {


    @SerializedName("cycle_id")
    private String cycle_id;
    @SerializedName("culture_seed_date")
    private String culture_seed_date;
    @SerializedName("doc")
    private String doc;
    @SerializedName("stocking_date")
    private String stocking_date;
    @SerializedName("seed_stocking")
    private String seed_stocking;
    @SerializedName("success")
    private String success;
    @SerializedName("previous_sampling")
    private String previous_sampling;
    @SerializedName("message")
    private String message;
    public String getCycle_id() {
        return cycle_id;
    }

    public void setCycle_id(String cycle_id) {
        this.cycle_id = cycle_id;
    }

    public String getCulture_seed_date() {
        return culture_seed_date;
    }

    public void setCulture_seed_date(String culture_seed_date) {
        this.culture_seed_date = culture_seed_date;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public String getStocking_date() {
        return stocking_date;
    }

    public void setStocking_date(String stocking_date) {
        this.stocking_date = stocking_date;
    }

    public String getSeed_stocking() {
        return seed_stocking;
    }

    public void setSeed_stocking(String seed_stocking) {
        this.seed_stocking = seed_stocking;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPrevious_sampling() {
        return previous_sampling;
    }

    public void setPrevious_sampling(String previous_sampling) {
        this.previous_sampling = previous_sampling;
    }
}
