package cbs.com.bmr.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/*********************************************************************
 * Created by Barani on 21-03-2019 in TableMateNew
 ***********************************************************************/
public class LeaveApprovalList implements Serializable {


    @SerializedName("id")
    private String id;
    @SerializedName("emp_id")
    private String emp_id;
    @SerializedName("created_emp_name")
    private String created_emp_name;
    @SerializedName("approved_emp_name")
    private String approved_emp_name;


    @SerializedName("purpose")
    private String purpose;

    @SerializedName("leave_days")
    private String leave_days;
    @SerializedName("from_date")
    private String from_date;
    @SerializedName("to_date")
    private String to_date;
    @SerializedName("created_date")
    private String created_date;
    @SerializedName("leavetype")
    private String leavetype;
    @SerializedName("leave_status")
    private String leave_status;
    @SerializedName("casual_leave")
    private String casual_leave;
    @SerializedName("sick_leave")
    private String sick_leave;
    @SerializedName("privilege_leave")
    private String privilege_leave;
    @SerializedName("success")
    private String success;


    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getCasual_leave() {
        return casual_leave;
    }

    public void setCasual_leave(String casual_leave) {
        this.casual_leave = casual_leave;
    }

    public String getSick_leave() {
        return sick_leave;
    }

    public void setSick_leave(String sick_leave) {
        this.sick_leave = sick_leave;
    }

    public String getPrivilege_leave() {
        return privilege_leave;
    }

    public void setPrivilege_leave(String privilege_leave) {
        this.privilege_leave = privilege_leave;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getCreated_emp_name() {
        return created_emp_name;
    }

    public void setCreated_emp_name(String created_emp_name) {
        this.created_emp_name = created_emp_name;
    }

    public String getApproved_emp_name() {
        return approved_emp_name;
    }

    public void setApproved_emp_name(String approved_emp_name) {
        this.approved_emp_name = approved_emp_name;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getLeave_days() {
        return leave_days;
    }

    public void setLeave_days(String leave_days) {
        this.leave_days = leave_days;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getLeavetype() {
        return leavetype;
    }

    public void setLeavetype(String leavetype) {
        this.leavetype = leavetype;
    }

    public String getLeave_status() {
        return leave_status;
    }

    public void setLeave_status(String leave_status) {
        this.leave_status = leave_status;
    }
}
