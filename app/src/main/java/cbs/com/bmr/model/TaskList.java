package cbs.com.bmr.model;

/*********************************************************************
 * Created by Barani on 21-03-2019 in TableMateNew
 ***********************************************************************/
public class TaskList {

    private String id;
    private String td_id;
    private String time;
    private String geo_json;
    private String ts_id;
    private String task_date;
    private String task_type;
    private String customer_id;
    private String description;
    private String comments;
    private String approved_status;
    private String approved_date;
    private String approved_by;
    private String checked_in;
    private String checked_out;
    private String geo_in;
    private String geo_out;
    private String status;
    private String last_updated;
    private String farm_flag;
    private String location;
    private String firstname;
    private String emp_id;
    private String fb_geo_json;
    private String fb_submit_time;
    private String image;
    private String transport;
    private String emp_name;
    private String created_by_emp;
    private String customername;
    private String approved_emp_name;
    private String created_emp_name;
    private String region_id;


    public String getCreated_emp_name() {
        return created_emp_name;
    }

    public void setCreated_emp_name(String created_emp_name) {
        this.created_emp_name = created_emp_name;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public String getCreated_by_emp() {
        return created_by_emp;
    }

    public void setCreated_by_emp(String created_by_emp) {
        this.created_by_emp = created_by_emp;
    }

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getApproved_emp_name() {
        return approved_emp_name;
    }

    public void setApproved_emp_name(String approved_emp_name) {
        this.approved_emp_name = approved_emp_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTs_id() {
        return ts_id;
    }

    public void setTs_id(String ts_id) {
        this.ts_id = ts_id;
    }

    public String getTask_date() {
        return task_date;
    }

    public void setTask_date(String task_date) {
        this.task_date = task_date;
    }

    public String getTask_type() {
        return task_type;
    }

    public void setTask_type(String task_type) {
        this.task_type = task_type;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getApproved_status() {
        return approved_status;
    }

    public void setApproved_status(String approved_status) {
        this.approved_status = approved_status;
    }

    public String getApproved_date() {
        return approved_date;
    }

    public void setApproved_date(String approved_date) {
        this.approved_date = approved_date;
    }

    public String getApproved_by() {
        return approved_by;
    }

    public void setApproved_by(String approved_by) {
        this.approved_by = approved_by;
    }

    public String getChecked_in() {
        return checked_in;
    }

    public void setChecked_in(String checked_in) {
        this.checked_in = checked_in;
    }

    public String getChecked_out() {
        return checked_out;
    }

    public void setChecked_out(String checked_out) {
        this.checked_out = checked_out;
    }

    public String getGeo_in() {
        return geo_in;
    }

    public void setGeo_in(String geo_in) {
        this.geo_in = geo_in;
    }

    public String getGeo_out() {
        return geo_out;
    }

    public void setGeo_out(String geo_out) {
        this.geo_out = geo_out;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(String last_updated) {
        this.last_updated = last_updated;
    }

    public String getFarm_flag() {
        return farm_flag;
    }

    public void setFarm_flag(String farm_flag) {
        this.farm_flag = farm_flag;
    }

    public String getFirst_name() {
        return firstname;
    }

    public void setFirst_name(String first_name) {
        this.firstname = first_name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTd_id() {
        return td_id;
    }

    public void setTd_id(String td_id) {
        this.td_id = td_id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getGeo_json() {
        return geo_json;
    }

    public void setGeo_json(String geo_json) {
        this.geo_json = geo_json;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getFb_geo_json() {
        return fb_geo_json;
    }

    public void setFb_geo_json(String fb_geo_json) {
        this.fb_geo_json = fb_geo_json;
    }

    public String getFb_submit_time() {
        return fb_submit_time;
    }

    public void setFb_submit_time(String fb_submit_time) {
        this.fb_submit_time = fb_submit_time;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }
}
