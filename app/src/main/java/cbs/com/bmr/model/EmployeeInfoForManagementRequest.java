package cbs.com.bmr.model;

/*********************************************************************
 * Created by Barani on 10-05-2019 in TableMateNew
 ***********************************************************************/
public class EmployeeInfoForManagementRequest {
    private String employee_name;
    private String id;
    private String emp_dep;
    private String department_name;
    private String department_description;

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmp_dep() {
        return emp_dep;
    }

    public void setEmp_dep(String emp_dep) {
        this.emp_dep = emp_dep;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }
}
