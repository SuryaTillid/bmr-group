package cbs.com.bmr.model;

/*********************************************************************
 * Created by Barani on 19-08-2019 in TableMateNew
 ***********************************************************************/
public class PondList {
    private String id;
    private String pond_id;
    private String pond_refer;
    private String comments;
    private String size;
    private String density;
    private String cycle_id;
    private String farmer_name;
    private String farmer_id;
    private String stocking_date;
    private String cycle_status;
    private String status;
    private String wsa;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPond_id() {
        return pond_id;
    }

    public void setPond_id(String pond_id) {
        this.pond_id = pond_id;
    }

    public String getPond_refer() {
        return pond_refer;
    }

    public void setPond_refer(String pond_refer) {
        this.pond_refer = pond_refer;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDensity() {
        return density;
    }

    public void setDensity(String density) {
        this.density = density;
    }

    public String getCycle_id() {
        return cycle_id;
    }

    public void setCycle_id(String cycle_id) {
        this.cycle_id = cycle_id;
    }

    public String getFarmer_name() {
        return farmer_name;
    }

    public void setFarmer_name(String farmer_name) {
        this.farmer_name = farmer_name;
    }

    public String getFarmer_id() {
        return farmer_id;
    }

    public void setFarmer_id(String farmer_id) {
        this.farmer_id = farmer_id;
    }

    public String getStocking_date() {
        return stocking_date;
    }

    public void setStocking_date(String stocking_date) {
        this.stocking_date = stocking_date;
    }

    public String getCycle_status() {
        return cycle_status;
    }

    public void setCycle_status(String cycle_status) {
        this.cycle_status = cycle_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWsa() {
        return wsa;
    }

    public void setWsa(String wsa) {
        this.wsa = wsa;
    }
}