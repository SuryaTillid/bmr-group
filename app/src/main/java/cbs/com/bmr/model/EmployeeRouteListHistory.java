package cbs.com.bmr.model;

import com.google.gson.annotations.SerializedName;

public class EmployeeRouteListHistory {
    @SerializedName("log_time")
    private String loginTime;
    @SerializedName("gps_coordinates")
    private String gps;
    @SerializedName("battery_level")
    private String battery;

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public String getGps() {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }
    /* @SerializedName("checkins")
    private ArrayList<CheckinList> checkins = new ArrayList<>();
    @SerializedName("destination")
    private Destination destination;
    @SerializedName("origin")
    private OriginS origin;


    public ArrayList<CheckinList> getCheckins() {
        return checkins;
    }

    public void setCheckins(ArrayList<CheckinList> checkins) {
        this.checkins = checkins;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public OriginS getOrigin() {
        return origin;
    }

    public void setOrigin(OriginS origin) {
        this.origin = origin;
    }

    public class OriginS {
        @SerializedName("task_detail_id")
        private String task_detail_id;
        @SerializedName("checkin_time")
        private String checkin_time;
        @SerializedName("checkout_time")
        private String checkout_time;
        @SerializedName("ts_task_date")
        private String ts_task_date;
        @SerializedName("cust_name")
        private String cust_name;
        @SerializedName("emp_name")
        private String emp_name;
        @SerializedName("geo_in")
        private String geo_in;
        @SerializedName("emp_id")
        private String emp_id;

        public String getTask_detail_id() {
            return task_detail_id;
        }

        public void setTask_detail_id(String task_detail_id) {
            this.task_detail_id = task_detail_id;
        }

        public String getCheckin_time() {
            return checkin_time;
        }

        public void setCheckin_time(String checkin_time) {
            this.checkin_time = checkin_time;
        }

        public String getCheckout_time() {
            return checkout_time;
        }

        public void setCheckout_time(String checkout_time) {
            this.checkout_time = checkout_time;
        }

        public String getTs_task_date() {
            return ts_task_date;
        }

        public void setTs_task_date(String ts_task_date) {
            this.ts_task_date = ts_task_date;
        }

        public String getCust_name() {
            return cust_name;
        }

        public void setCust_name(String cust_name) {
            this.cust_name = cust_name;
        }

        public String getEmp_name() {
            return emp_name;
        }

        public void setEmp_name(String emp_name) {
            this.emp_name = emp_name;
        }

        public String getGeo_in() {
            return geo_in;
        }

        public void setGeo_in(String geo_in) {
            this.geo_in = geo_in;
        }

        public String getEmp_id() {
            return emp_id;
        }

        public void setEmp_id(String emp_id) {
            this.emp_id = emp_id;
        }
    }

    public class Destination {
        @SerializedName("emp_id")
        private String emp_id;
        @SerializedName("geo_in")
        private String geo_in;
        @SerializedName("check_in")
        private String check_in;
        @SerializedName("check_out")
        private String check_out;
        @SerializedName("checkin_time")
        private String checkin_time;
        @SerializedName("checkout_time")
        private String checkout_time;
        @SerializedName("emp_name")
        private String emp_name;
        public String getEmp_id() {
            return emp_id;
        }

        public void setEmp_id(String emp_id) {
            this.emp_id = emp_id;
        }

        public String getGeo_in() {
            return geo_in;
        }

        public void setGeo_in(String geo_in) {
            this.geo_in = geo_in;
        }

        public String getCheck_in() {
            return check_in;
        }

        public void setCheck_in(String check_in) {
            this.check_in = check_in;
        }

        public String getCheck_out() {
            return check_out;
        }

        public void setCheck_out(String check_out) {
            this.check_out = check_out;
        }

        public String getCheckin_time() {
            return checkin_time;
        }

        public void setCheckin_time(String checkin_time) {
            this.checkin_time = checkin_time;
        }

        public String getCheckout_time() {
            return checkout_time;
        }

        public void setCheckout_time(String checkout_time) {
            this.checkout_time = checkout_time;
        }

        public String getEmp_name() {
            return emp_name;
        }

        public void setEmp_name(String emp_name) {
            this.emp_name = emp_name;
        }
    }

    public class CheckinList {
        @SerializedName("emp_id")
        private String emp_id;
        @SerializedName("geo_in")
        private String geo_in;
        @SerializedName("check_in")
        private String check_in;
        @SerializedName("check_out")
        private String check_out;
        @SerializedName("checkin_time")
        private String checkin_time;
        @SerializedName("checkout_time")
        private String checkout_time;

        public String getEmp_id() {
            return emp_id;
        }

        public void setEmp_id(String emp_id) {
            this.emp_id = emp_id;
        }

        public String getGeo_in() {
            return geo_in;
        }

        public void setGeo_in(String geo_in) {
            this.geo_in = geo_in;
        }

        public String getCheck_in() {
            return check_in;
        }

        public void setCheck_in(String check_in) {
            this.check_in = check_in;
        }

        public String getCheck_out() {
            return check_out;
        }

        public void setCheck_out(String check_out) {
            this.check_out = check_out;
        }

        public String getCheckin_time() {
            return checkin_time;
        }

        public void setCheckin_time(String checkin_time) {
            this.checkin_time = checkin_time;
        }

        public String getCheckout_time() {
            return checkout_time;
        }

        public void setCheckout_time(String checkout_time) {
            this.checkout_time = checkout_time;
        }
    }*/
}
