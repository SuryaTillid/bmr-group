package cbs.com.bmr.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 21-03-2019 in TableMateNew
 ***********************************************************************/
public class StateList {

    @SerializedName("id")
    private String id;
    @SerializedName("state_name")
    private String state_name;
    @SerializedName("city")
    private ArrayList<String> mCityList = new ArrayList<>();


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public ArrayList<String> getmCityList() {
        return mCityList;
    }

    public void setmCityList(ArrayList<String> mCityList) {
        this.mCityList = mCityList;
    }
}
