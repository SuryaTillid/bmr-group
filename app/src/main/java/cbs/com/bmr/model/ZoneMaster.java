package cbs.com.bmr.model;

/*********************************************************************
 * Created by Barani on 07-06-2019 in TableMateNew
 ***********************************************************************/
public class ZoneMaster {
    private String id;
    private String zone_name;
    private String state;
    private String district;
    private String geo_jason;
    private String zone_area;
    private String incharge_level_one;
    private String incharge_level_two;
    private String incharge_level_three;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getZone_name() {
        return zone_name;
    }

    public void setZone_name(String zone_name) {
        this.zone_name = zone_name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getGeo_jason() {
        return geo_jason;
    }

    public void setGeo_jason(String geo_jason) {
        this.geo_jason = geo_jason;
    }

    public String getZone_area() {
        return zone_area;
    }

    public void setZone_area(String zone_area) {
        this.zone_area = zone_area;
    }

    public String getIncharge_level_one() {
        return incharge_level_one;
    }

    public void setIncharge_level_one(String incharge_level_one) {
        this.incharge_level_one = incharge_level_one;
    }

    public String getIncharge_level_two() {
        return incharge_level_two;
    }

    public void setIncharge_level_two(String incharge_level_two) {
        this.incharge_level_two = incharge_level_two;
    }

    public String getIncharge_level_three() {
        return incharge_level_three;
    }

    public void setIncharge_level_three(String incharge_level_three) {
        this.incharge_level_three = incharge_level_three;
    }
}
