package cbs.com.bmr.model;

/*********************************************************************
 * Created by Barani on 22-05-2019 in TableMateNew
 ***********************************************************************/
public class StateSpinner {
    private String text_value;
    private boolean selected;

    public String getText_value() {
        return text_value;
    }

    public void setText_value(String text_value) {
        this.text_value = text_value;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
