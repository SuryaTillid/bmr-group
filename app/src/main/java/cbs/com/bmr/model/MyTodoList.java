package cbs.com.bmr.model;

import com.google.gson.annotations.SerializedName;

/*********************************************************************
 * Created by Barani on 02-05-2019 in TableMateNew
 ***********************************************************************/
public class MyTodoList {

    @SerializedName("id")
    private String id;
    @SerializedName("task_name")
    private String task_name;
    @SerializedName("notes")
    private String notes;
    @SerializedName("created_by")
    private String created_by;
    @SerializedName("created_on")
    private String created_on;
    @SerializedName("assigned_to")
    private String assigned_to;
    @SerializedName("task_due")
    private String task_due;
    @SerializedName("attached_file")
    private String attached_file;
    @SerializedName("task_status")
    private String task_status;
    @SerializedName("assignee_type")
    private String assignee_type;
    @SerializedName("priority")
    private String priority;
    @SerializedName("updated_by")
    private String updated_by;
    @SerializedName("emp_name")
    private String emp_name;
    @SerializedName("formate_task_date")
    private String formate_task_date;
    @SerializedName("formate_created_on")
    private String formate_created_on;

    @SerializedName("success")
    private String success;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTask_name() {
        return task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getAssigned_to() {
        return assigned_to;
    }

    public void setAssigned_to(String assigned_to) {
        this.assigned_to = assigned_to;
    }

    public String getTask_due() {
        return task_due;
    }

    public void setTask_due(String task_due) {
        this.task_due = task_due;
    }

    public String getAttached_file() {
        return attached_file;
    }

    public void setAttached_file(String attached_file) {
        this.attached_file = attached_file;
    }

    public String getTask_status() {
        return task_status;
    }

    public void setTask_status(String task_status) {
        this.task_status = task_status;
    }

    public String getAssignee_type() {
        return assignee_type;
    }

    public void setAssignee_type(String assignee_type) {
        this.assignee_type = assignee_type;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public String getFormate_task_date() {
        return formate_task_date;
    }

    public void setFormate_task_date(String formate_task_date) {
        this.formate_task_date = formate_task_date;
    }

    public String getFormate_created_on() {
        return formate_created_on;
    }

    public void setFormate_created_on(String formate_created_on) {
        this.formate_created_on = formate_created_on;
    }


    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
