package cbs.com.bmr.model;

/*********************************************************************
 * Created by Barani on 02-05-2019 in TableMateNew
 ***********************************************************************/
public class TODO_Taskslist {
    private String task_name;
    private String notes;
    private String task_due;
    private String id;
    private String created_by;
    private String created_on;
    private String assigned_to;
    private String attached_file;
    private String task_status;

    public String getTask_name() {
        return task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getTask_due() {
        return task_due;
    }

    public void setTask_due(String task_due) {
        this.task_due = task_due;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getAssigned_to() {
        return assigned_to;
    }

    public void setAssigned_to(String assigned_to) {
        this.assigned_to = assigned_to;
    }

    public String getAttached_file() {
        return attached_file;
    }

    public void setAttached_file(String attached_file) {
        this.attached_file = attached_file;
    }

    public String getTask_status() {
        return task_status;
    }

    public void setTask_status(String task_status) {
        this.task_status = task_status;
    }
}
