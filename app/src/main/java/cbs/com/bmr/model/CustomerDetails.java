package cbs.com.bmr.model;

import com.google.gson.annotations.SerializedName;

public class CustomerDetails {
    @SerializedName("success")
    private String success;
    @SerializedName("success_message")
    private String success_message;
    @SerializedName("customer")
    private Customer customer;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getSuccess_message() {
        return success_message;
    }

    public void setSuccess_message(String success_message) {
        this.success_message = success_message;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public class Customer {
        @SerializedName("id")
        private String id = "";
        @SerializedName("first_name")
        private String firstName = "";
        @SerializedName("last_name")
        private String lastName = "";
        @SerializedName("address1")
        private String address1 = "";
        @SerializedName("address2")
        private String adrees2 = "";
        @SerializedName("city_village")
        private String cityVillage = "";
        @SerializedName("state")
        private String state = "";
        @SerializedName("comments")
        private String comments = "";

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getAdrees2() {
            return adrees2;
        }

        public void setAdrees2(String adrees2) {
            this.adrees2 = adrees2;
        }

        public String getCityVillage() {
            return cityVillage;
        }

        public void setCityVillage(String cityVillage) {
            this.cityVillage = cityVillage;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }


}
