package cbs.com.bmr.model;

/*********************************************************************
 * Created by Barani on 02-04-2019 in TableMateNew
 ***********************************************************************/
public class Location_Details {
    private String lat;
    private String lon;

    public String getLatitude() {
        return lat;
    }

    public void setLatitude(String lati) {
        this.lat = lati;
    }

    public String getLongitude() {
        return lon;
    }

    public void setLongitude(String lon) {
        this.lon = lon;
    }
}
