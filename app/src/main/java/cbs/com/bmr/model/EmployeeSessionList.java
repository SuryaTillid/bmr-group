package cbs.com.bmr.model;

import com.google.gson.annotations.SerializedName;

/*********************************************************************
 * Created by Barani on 04-04-2019 in TableMateNew
 ***********************************************************************/
public class EmployeeSessionList {
    @SerializedName("emp_id")
    private String emp_id;
    @SerializedName("status")
    private String status;
    @SerializedName("emp_name")
    private String emp_name;
    @SerializedName("username")
    private String username;

    @SerializedName("success")
    private String success;

    @SerializedName("val")
    private String val;

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
