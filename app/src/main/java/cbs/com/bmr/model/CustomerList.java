package cbs.com.bmr.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/*********************************************************************
 * Created by Barani on 21-03-2019 in TableMateNew
 ***********************************************************************/
public class CustomerList implements Serializable {

    @SerializedName("id")
    private String id;
    @SerializedName("cat_id")
    private String cat_id;
    @SerializedName("first_name")
    private String first_name;
    @SerializedName("last_name")
    private String last_name;
    @SerializedName("address1")
    private String address1;
    @SerializedName("address2")
    private String address2;
    @SerializedName("city_village")
    private String city_village;
    @SerializedName("state")
    private String state;
    @SerializedName("zone_id")

    private String zone_id;
    @SerializedName("comments")

    private String comments;
    @SerializedName("customer_type")

    private String customer_type;
    @SerializedName("own_other")

    private String own_other;
    @SerializedName("customer_of")

    private String customer_of;
    @SerializedName("active_status")

    private String active_status;
    @SerializedName("primary_geo_json")

    private String primary_geo_json;
    @SerializedName("farm_area")

    private String farm_area;
    @SerializedName("cat_name")

    private String cat_name;


    public String getPrimary_geo_json() {
        return primary_geo_json;
    }

    public void setPrimary_geo_json(String primary_geo_json) {
        this.primary_geo_json = primary_geo_json;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity_village() {
        return city_village;
    }

    public void setCity_village(String city_village) {
        this.city_village = city_village;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZone_id() {
        return zone_id;
    }

    public void setZone_id(String zone_id) {
        this.zone_id = zone_id;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCustomer_type() {
        return customer_type;
    }

    public void setCustomer_type(String customer_type) {
        this.customer_type = customer_type;
    }

    public String getOwn_other() {
        return own_other;
    }

    public void setOwn_other(String own_other) {
        this.own_other = own_other;
    }

    public String getCustomer_of() {
        return customer_of;
    }

    public void setCustomer_of(String customer_of) {
        this.customer_of = customer_of;
    }

    public String getActive_status() {
        return active_status;
    }

    public void setActive_status(String active_status) {
        this.active_status = active_status;
    }



    public String getFarm_area() {
        return farm_area;
    }

    public void setFarm_area(String farm_area) {
        this.farm_area = farm_area;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }


}
