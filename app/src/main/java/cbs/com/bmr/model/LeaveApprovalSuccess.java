package cbs.com.bmr.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/*********************************************************************
 * Created by Barani on 21-03-2019 in TableMateNew
 ***********************************************************************/
public class LeaveApprovalSuccess implements Serializable {


    @SerializedName("leave_status")
    private String leave_status;
    @SerializedName("success")
    private String success;


    public String getLeave_status() {
        return leave_status;
    }

    public void setLeave_status(String leave_status) {
        this.leave_status = leave_status;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
