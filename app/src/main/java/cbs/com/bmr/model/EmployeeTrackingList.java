package cbs.com.bmr.model;

import com.google.gson.annotations.SerializedName;

public class EmployeeTrackingList {
    @SerializedName("id")
    private String id;
    @SerializedName("firstname")
    private String firstname;
    @SerializedName("lastname")
    private String lastname;
    @SerializedName("emp_name")
    private String emp_name;
    @SerializedName("category_id")
    private String category_id;
    @SerializedName("contactno")
    private String contactno;
    @SerializedName("email")
    private String email;
    @SerializedName("employee_no")
    private String employee_no;

    @SerializedName("address1")
    private String address1;
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;
    @SerializedName("region_id")
    private String region_id;
    @SerializedName("emp_dep")
    private String emp_dep;

    @SerializedName("reporting_to")
    private String reporting_to;
    @SerializedName("department_name")
    private String department_name;
    @SerializedName("category_name")
    private String category_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getContactno() {
        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmployee_no() {
        return employee_no;
    }

    public void setEmployee_no(String employee_no) {
        this.employee_no = employee_no;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }

    public String getEmp_dep() {
        return emp_dep;
    }

    public void setEmp_dep(String emp_dep) {
        this.emp_dep = emp_dep;
    }

    public String getReporting_to() {
        return reporting_to;
    }

    public void setReporting_to(String reporting_to) {
        this.reporting_to = reporting_to;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }
}
