package cbs.com.bmr.model;

/*********************************************************************
 * Created by Barani on 24-09-2019 in TableMateNew
 ***********************************************************************/
public class SampleHarvestHistory {
    private String id;
    private String abw;
    private String daily_feed;
    private String recorded_date;
    private String emp_id;
    private String sample_harvest_flag;
    private String cycle_id;
    private String sampling_date;
    private String ph;
    private String salinity;
    private String survival;
    private String e_biomass;
    private String a_biomass;
    private String density;
    private String adg;
    private String fcr;
    private String productivity;
    private String harvest_quantity;
    private String task_id;
    private String pond_id;
    private String seed_source;
    private String acres;
    private String doc;
    private String total_feed;
    private String sampling_file;
    private String seed_stocking;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAbw() {
        return abw;
    }

    public void setAbw(String abw) {
        this.abw = abw;
    }

    public String getDaily_feed() {
        return daily_feed;
    }

    public void setDaily_feed(String daily_feed) {
        this.daily_feed = daily_feed;
    }

    public String getRecorded_date() {
        return recorded_date;
    }

    public void setRecorded_date(String recorded_date) {
        this.recorded_date = recorded_date;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getSample_harvest_flag() {
        return sample_harvest_flag;
    }

    public void setSample_harvest_flag(String sample_harvest_flag) {
        this.sample_harvest_flag = sample_harvest_flag;
    }

    public String getCycle_id() {
        return cycle_id;
    }

    public void setCycle_id(String cycle_id) {
        this.cycle_id = cycle_id;
    }

    public String getSampling_date() {
        return sampling_date;
    }

    public void setSampling_date(String sampling_date) {
        this.sampling_date = sampling_date;
    }

    public String getPh() {
        return ph;
    }

    public void setPh(String ph) {
        this.ph = ph;
    }

    public String getSalinity() {
        return salinity;
    }

    public void setSalinity(String salinity) {
        this.salinity = salinity;
    }

    public String getSurvival() {
        return survival;
    }

    public void setSurvival(String survival) {
        this.survival = survival;
    }

    public String getE_biomass() {
        return e_biomass;
    }

    public void setE_biomass(String e_biomass) {
        this.e_biomass = e_biomass;
    }

    public String getA_biomass() {
        return a_biomass;
    }

    public void setA_biomass(String a_biomass) {
        this.a_biomass = a_biomass;
    }

    public String getDensity() {
        return density;
    }

    public void setDensity(String density) {
        this.density = density;
    }

    public String getAdg() {
        return adg;
    }

    public void setAdg(String adg) {
        this.adg = adg;
    }

    public String getFcr() {
        return fcr;
    }

    public void setFcr(String fcr) {
        this.fcr = fcr;
    }

    public String getProductivity() {
        return productivity;
    }

    public void setProductivity(String productivity) {
        this.productivity = productivity;
    }

    public String getHarvest_quantity() {
        return harvest_quantity;
    }

    public void setHarvest_quantity(String harvest_quantity) {
        this.harvest_quantity = harvest_quantity;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getPond_id() {
        return pond_id;
    }

    public void setPond_id(String pond_id) {
        this.pond_id = pond_id;
    }

    public String getSeed_source() {
        return seed_source;
    }

    public void setSeed_source(String seed_source) {
        this.seed_source = seed_source;
    }

    public String getAcres() {
        return acres;
    }

    public void setAcres(String acres) {
        this.acres = acres;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public String getTotal_feed() {
        return total_feed;
    }

    public void setTotal_feed(String total_feed) {
        this.total_feed = total_feed;
    }

    public String getSampling_file() {
        return sampling_file;
    }

    public void setSampling_file(String sampling_file) {
        this.sampling_file = sampling_file;
    }

    public String getSeed_stocking() {
        return seed_stocking;
    }

    public void setSeed_stocking(String seed_stocking) {
        this.seed_stocking = seed_stocking;
    }
}
