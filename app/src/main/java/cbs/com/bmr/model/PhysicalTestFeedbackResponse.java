package cbs.com.bmr.model;

import com.google.gson.annotations.SerializedName;

/*********************************************************************
 * Created by Barani on 21-03-2019 in TableMateNew
 ***********************************************************************/
public class PhysicalTestFeedbackResponse {
    @SerializedName("success")
    private String success;
    @SerializedName("message")
    private String feedback_message;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getFeedback_message() {
        return feedback_message;
    }

    public void setFeedback_message(String feedback_message) {
        this.feedback_message = feedback_message;
    }
}
