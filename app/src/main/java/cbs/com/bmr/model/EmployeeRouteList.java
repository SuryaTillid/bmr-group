package cbs.com.bmr.model;

import com.google.gson.annotations.SerializedName;

public class EmployeeRouteList {
    @SerializedName("id")
    private String id;
    @SerializedName("timestamp")
    private String timestamp;
    @SerializedName("emp_id")
    private String emp_id;
    @SerializedName("category_id")
    private String category_id;
    @SerializedName("gps_coordinates")
    private String gps_coordinates;
    @SerializedName("emp_name")
    private String emp_name;
    @SerializedName("datetime")
    private String datetime;
    @SerializedName("battery_level")
    private String battery_level;

    @SerializedName("success")
    private String success;


    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getBattery_level() {
        return battery_level;
    }

    public void setBattery_level(String battery_level) {
        this.battery_level = battery_level;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getGps_coordinates() {
        return gps_coordinates;
    }

    public void setGps_coordinates(String gps_coordinates) {
        this.gps_coordinates = gps_coordinates;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
