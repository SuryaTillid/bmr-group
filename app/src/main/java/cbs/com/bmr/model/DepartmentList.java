package cbs.com.bmr.model;

import com.google.gson.annotations.SerializedName;

public class DepartmentList {
    @SerializedName("id")
    private String id;
    @SerializedName("department_name")
    private String department_name;
    @SerializedName("department_description")
    private String department_description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    public String getDepartment_description() {
        return department_description;
    }

    public void setDepartment_description(String department_description) {
        this.department_description = department_description;
    }
}
