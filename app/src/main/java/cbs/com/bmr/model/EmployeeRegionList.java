package cbs.com.bmr.model;

import com.google.gson.annotations.SerializedName;

public class EmployeeRegionList {
    @SerializedName("id")
    private String id;
    @SerializedName("zone_name")
    private String zone_name;
    @SerializedName("state")
    private String state;
    @SerializedName("district")
    private String district;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getZone_name() {
        return zone_name;
    }

    public void setZone_name(String zone_name) {
        this.zone_name = zone_name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
