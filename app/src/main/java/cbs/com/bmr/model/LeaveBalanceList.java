package cbs.com.bmr.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/*********************************************************************
 * Created by Barani on 21-03-2019 in TableMateNew
 ***********************************************************************/
public class LeaveBalanceList implements Serializable {

    @SerializedName("casual_leave")
    private String casual_balance;
    @SerializedName("sick_leave")
    private String sick_balance;
    @SerializedName("privilege_leave")
    private String privilege_blance;

    @SerializedName("success")
    private String success;

    @SerializedName("id")
    private String id;


    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCasual_balance() {
        return casual_balance;
    }

    public void setCasual_balance(String casual_balance) {
        this.casual_balance = casual_balance;
    }

    public String getSick_balance() {
        return sick_balance;
    }

    public void setSick_balance(String sick_balance) {
        this.sick_balance = sick_balance;
    }

    public String getPrivilege_blance() {
        return privilege_blance;
    }

    public void setPrivilege_blance(String privilege_blance) {
        this.privilege_blance = privilege_blance;
    }
}
